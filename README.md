app.tronis.lv
================================================================================


# Docker

## Setup local environment

1. Install Docker from https://docs.docker.com/install/
2. If you are no linux/windows (not required for Mac):
    ```sh
    sudo usermod -aG docker $USER # https://sudhakaryblog.wordpress.com/2020/08/01/how-to-fix-docker-permission-denied-error-on-ubuntu/
    newgrp docker # so that you don't have to restart workstation.
    cat /etc/group | grep docker # confirm you have the group. If confused, re-login.
    ll /var/run/docker.sock # confirm it's owned by root:docker
    pip3 install docker-compose
    ```

3. Add to `/etc/hosts` file from terminal or `sudo nano /etc/hosts` if on MAC
    ```
    127.0.0.1       lapi.tronis.lv
    127.0.0.1       lpma.tronis.lv
    127.0.0.1       lapp.tronis.lv
    127.0.0.1       lcron.tronis.lv
    ```

## Clean start in local env

1. run `yarn docker` and leave the terminal open

2. Initialize database
   1. open in browser: http://lpma.tronis.lv
   2. login: `root` password: `qwerty`
   3. create new db `tronis` new from left menu
   4. import `/packages/db/migrations/*.sql` in ascending order as numbered. Import **one** test state of your choice


## Reset local env

1. close the terminal with docker
2. redo "clean start in local env"

Some useful commands
```sh
docker-compose -f ./docker-compose-dev.yaml stop t-nginx t-db-mysql t-admin-mysql t-api t-memcached
docker-compose -f ./docker-compose-dev.yaml rm t-nginx t-db-mysql t-admin-mysql t-api t-memcached
sudo rm -rf packages/db/tmp/data
docker volume prune
docker network prune
```

## Full cycle testing


### Start main game
1. Pull git
2. Restart docker
3. Make db
4. Import SQLs
5. (conditional) Disable setting "is-feast-night" to test main game.
6. Change "start-time" attribute in settings table to a bit behind now (in UTC timezone, e.g. "2021-08-11 18:00:00")
7. MANUAL TESTING EXCEPTION:
   1. (optional) in browser open "http://lcron.tronis.lv/seasonChange.php?run&log=all".
   2. (optional) confirm that in table "settings" value "Game has started" has changed to 1.
   3. launch in bash `packages/run/files/run-manually.sh` and leave it open. (in production will be done inside server itself.)
   4. FYI: setting "start-time" doesn't matter anymore. It's only for triggering the start of game.
   5. FYI: in table "periods" can change value "started" and it will be taken into account. (TEST: change time to 2h ago and observe season change).
8. In terminal  go to `packages/app` and execute `yarn start`
9. Using Firefox or Chromium open https://lapp.tronis.lv:8080
10. Give necessary permissions to browser (repeats every day)
11. Open phpMyAdmin in browser -> devices table from tronis DB -> change team_id and is_enabled attributes to not NULL
12. Refresh tronis app


## Testing notes

principles
- clean-state (setup is just another test)
- deterministic everything
- extract biz logic to be reused between app and tests (?)

1. reset env
2. turn on BE
3. run tests
4. log stuff

### Test harness (dev makes it), a.k.a. Throne App SDK
- Env object
- bunch of helpers
- etc.

### Tests themselves (human-readable)
- call helpers
- tests themselves are reusable and composable (declare deps and whatnot)

### Deployment on prod

Prepare AWS instance with more than default 8 GB disk space, open HTTP/HTTPS ports to it and point Route 53 to it's elastic IP, then ssh into:

```sh
sudo reboot
git clone https://gitlab.com/akuukis/throne-app.git
sudo apt-get remove docker docker-engine docker.io containerd runc
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io -y
sudo usermod -aG docker $USER
sudo newgrp docker
# CTLR+D
cat /etc/group | grep docker
# relogin
docker run hello-world
sudo apt install unzip
curl -fsSL https://fnm.vercel.app/install | bash
source /home/ubuntu/.bashrc
fnm install 14
npm install -g yarn
cd throne-app/
yarn
sudo apt install python3-pip
pip3 install docker-compose
nano ~/.bashrc # add a line "export PATH=/home/ubuntu/.local/bin:$PATH"
source ~/.bashrc
yarn prod
docker-compose up -d
```


Below likely outdated info:

1. open SSH session to
   - host: api.tronis.lv
   - port: 22
   - username: root
   - password: qazASD13%z
2. run
```sh
cd ~/tronis.lv/throne-app/
git pull
cd ~/tronis.lv/throne-app/packages/db/
bash migrations.sh throne-app_frontend 'asdPO79M))124(oaNo' state-ThroneMini2-init
cd ~/tronis.lv/throne-app/packages/webserver/
yarn build
cd ~/tronis.lv/throne-app/
docker-compose restart t-api
```
3. set start time field to necessary UTC time when game should start and save

