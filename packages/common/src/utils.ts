import { isApolloError, ApolloError } from '@apollo/client/errors'


type KeyValue<T extends Record<string, unknown>, K extends keyof T = keyof T> = [K, T[K]]
export const recordEntries = <T extends Record<string, unknown>>(o: T) => Object.entries(o) as KeyValue<T>[]
export const recordKeys = <T extends Record<string, unknown>>(o: T) => Object.keys(o) as (keyof T)[]
export const recordValues = <T extends Record<string, unknown>>(o: T) => Object.values(o) as T[keyof T][]

export const sleep = async (seconds: number): Promise<void> => new Promise<void>((resolve) => setTimeout(() => resolve(), seconds * 1000))
export const timebomb = async (seconds: number): Promise<never> => new Promise<never>((_, reject) => setTimeout(() => reject(new Error('Timeouted.')), seconds * 1000))
export const timeout = async <TRes>(seconds: number, promise: Promise<TRes>): Promise<TRes> => Promise.race([promise, timebomb(seconds)])

/**
 * Asserts a real Error. For {@link ApolloError} use {@link assertApolloError} instead, it's not instance of Error.
 */
export function assertError(maybeError: unknown, notify?: (error: Error) => void): asserts maybeError is Error {
    if (maybeError instanceof Error) return

    const realError = new Error(`Non-Error was thrown: ${maybeError}`)
    notify?.(realError)
    throw realError
}

/**
 * Asserts a {@link ApolloError}. Don't use {@link assertError} also because it's not instance of Error.
 */
export function assertApolloError(maybeError: unknown, notify?: (error: Error) => void): asserts maybeError is ApolloError {
    if (isApolloError(maybeError as Error)) return // It's not instanceof Error tho, so can't `assertError` it.

    const realError = new Error(`Non-ApolloError was thrown: ${maybeError}`)
    notify?.(realError)
    throw realError
}
