import { crc16 } from 'crc'


const BASE = 56
const ALPHABET = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz'


const decode = (text: string, length = text.length): number => {
    return Array.from(text)
        .map((char) => ALPHABET.indexOf(char))
        .reduce((sum, prefix, i) => sum + prefix * Math.pow(BASE, length - (i+1)), 0)
}

const encode = (value: number, length = 1): string => {
    return new Array(length).fill(null)
        .map((_, i) => ALPHABET[Math.floor(value/Math.pow(BASE, length-i-1)) % BASE])
        .join('')
}

export const uidToQR = (uid: number, prefixLength = 1) => {
    const uidEncoded = encode(uid, 4)
    const uidEncodedDashed = `${uidEncoded.slice(0, prefixLength)}-${uidEncoded.slice(prefixLength)}`

    const buf = Buffer.from(BigInt(uid).toString(16).padStart(6, '0').slice(0, 6), 'hex')
    const crc = Number(crc16(buf))
    const crcEncoded = encode(crc, 2)

    return `${uidEncodedDashed}-${crcEncoded}`
}

export const qrToUid = (codeEncodedDashed: string, offset = 0) => {
    const codeEncoded = codeEncodedDashed.replace(/-/g, '')
    if (codeEncoded.length !== 6) throw new Error(`Code must be 6 characters long (excluding dashes): "${codeEncoded}"`)

    const uidEncoded = codeEncoded.slice(0, 4)
    const uid = decode(uidEncoded) + offset

    const crcEncoded = codeEncoded.slice(4, 6)
    const uidBuf = Buffer.from(BigInt(uid).toString(16).padStart(6, '0').slice(0, 6), 'hex')
    const crc = Number(crc16(uidBuf))
    const crcReconstructed = encode(crc, 2)
    if (crcEncoded !== crcReconstructed) throw new Error('Checksum does not match.')

    return uid
}
