/* eslint-disable @typescript-eslint/require-await */
/* eslint-disable unused-imports/no-unused-vars-ts */
/* eslint-disable import/no-unused-modules */
/* eslint-disable @typescript-eslint/no-var-requires */
/**
 * Example:
 *   ts-node-script gen-qr-codes.ts TE 112
 *   ts-node-script gen-qr-codes.ts Fa 336
 *   ts-node-script gen-qr-codes.ts W 1008
 */


const { exec } = require('child_process')
const { readFileSync, writeFileSync, mkdirSync } = require('fs')

const { crc16 } = require('crc')
const { join } = require('path')
const QRCode = require('qrcode')


const execAsync = async (cmd: string): Promise<string> =>
    new Promise<string>((res, rej) => exec(cmd, (err: string, str: string) => err ? rej(err) : res(str)))


type KeyValue<T extends Record<string, unknown>, K extends keyof T = keyof T> = [K, T[K]]
const recordEntries = <T extends Record<string, unknown>>(o: T) => Object.entries(o) as KeyValue<T>[]


const BASE = 56
const ALPHABET = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz'


const decode = (text: string, length = text.length): number => {
    return Array.from(text)
        .map((char) => ALPHABET.indexOf(char))
        .reduce((sum, prefix, i) => sum + prefix * Math.pow(BASE, length - (i+1)), 0)
}

const encode = (value: number, length = 1): string => {
    return new Array(length).fill(null)
        .map((_, i) => ALPHABET[Math.floor(value/Math.pow(BASE, length-i-1)) % BASE])
        .join('')
}

function uidToQR (uid: number, prefixLength = 1) {
    const uidEncoded = encode(uid, 4)
    const uidEncodedDashed = `${uidEncoded.slice(0, prefixLength)}-${uidEncoded.slice(prefixLength)}`

    // TODO: optimize Buffer into Int8Array (https://github.com/alexgorbatchev/crc/releases/tag/v4.0.0)
    const buf = Buffer.from(BigInt(uid).toString(16).padStart(6, '0').slice(0, 6), 'hex')
    const crc = Number(crc16(buf))
    const crcEncoded = encode(crc, 2)

    return `${uidEncodedDashed}-${crcEncoded}`
}

function qrToUid (codeEncodedDashed: string, offset = 0) {
    const codeEncoded = codeEncodedDashed.replace(/-/g, '')
    if (codeEncoded.length !== 6) throw new Error(`Code must be 6 characters long (excluding dashes): "${codeEncoded}"`)

    const uidEncoded = codeEncoded.slice(0, 4)
    const uid = decode(uidEncoded) + offset

    const crcEncoded = codeEncoded.slice(4, 6)
    const uidBuf = Buffer.from(BigInt(uid).toString(16).padStart(6, '0').slice(0, 6), 'hex')
    const crc = Number(crc16(uidBuf))
    const crcReconstructed = encode(crc, 2)
    if (crcEncoded !== crcReconstructed) throw new Error('Checksum does not match.')

    return uid
}

/**
 * Example:
 *   ts-node-script gen-qr-codes.ts TE 112
 *   ts-node-script gen-qr-codes.ts Fa 336
 *   ts-node-script gen-qr-codes.ts W 1008
 */
// try {
//     const DIGITS = 4
//     const type = process.argv[2] as string
//     const max = Number(process.argv[3]) as number
//     if (typeof max !== 'number') throw new Error('max have to a number.')

//     const prefixValue = decode(type, 4)
//     if (max > Math.pow(BASE, DIGITS - type.length)) throw new Error('max is too big.')

//     for (let i = 0; i < max; i++) {
//         const uid = prefixValue + i
//         const code = uidToQR(uid, type.length)
//         const uid2 = qrToUid(code)
//         console.log(uid, code, uid2)
//     }
// } catch (err) {
//     console.error(err)
// }


const todo: Record<string, {start: string, amount: number}> = {
    WOOD: { start: 'WD', amount: 1008 },
    IRON: { start: 'RN', amount: 1008 },
    HIDES: { start: 'HD', amount: 1008 },
    'FOOD: HONEY': { start: 'Fh', amount: 336 },
    'FOOD: TOMATOES': { start: 'Ft', amount: 224 },
    'FOOD: CUCUMBERS': { start: 'Fc', amount: 224 },
    'FOOD: RASPBERRIES': { start: 'Fr', amount: 112 },
    'FOOD: MUSHROOMS': { start: 'Fm', amount: 112 },
    'FOOD: LAVENDER': { start: 'Fv', amount: 336 },
    PLAYER: { start: 'PL', amount: 56*3 + 420-336 },
    FLAG: { start: 'FL', amount: 56+28 },
}

;(async () => {
    const svgTemplate = readFileSync(join(__dirname, 'template.svg')).toString()
    mkdirSync(join('dist', 'svg'), { recursive: true })
    mkdirSync(join('dist', 'pdf'), { recursive: true })
    let id = 1
    for (const [title, { start, amount }] of recordEntries(todo)) {
        console.log(`-- === ${title} ===`)
        const prefixValue = decode(start, 4)

        for (let i = 0; i < amount; i++) {
            const uid = prefixValue + i
            const code = uidToQR(uid, start.length)
            // const qr = (await QRCode.toString(code, {
            //     errorCorrectionLevel: 'Q',
            //     version: 1,
            //     mode: 'alphanumeric',
            //     width: 232,
            //     type: 'svg',
            //     scale: 10,
            // }) as string).slice(
            //     '<svg xmlns="http://www.w3.org/2000/svg" width="232" height="232" viewBox="0 0 29 29" shape-rendering="crispEdges">'.length,
            //     -('</svg>'.length+1),
            // )
            // const svg = svgTemplate
            //     .replace('$QR', String(qr))
            //     .replace('$CODE', code).replace('$TYPE', title)
            // const filename = `${title.replace(' ', '-')}-${i.toString().padStart(4, '0')}-${code}`
            // const filepathSvg = join('dist', 'svg', `${filename}.svg`)
            // const filepathPdf = join('dist', 'pdf', `${filename}.pdf`)
            // writeFileSync(filepathSvg, svg)
            // await execAsync(`cairosvg -d 254 -o ${filepathPdf} ${filepathSvg}`)

            // process.stdout.write('.')
            const category = code[0] === 'P' ? 3
                : code[0] === 'F' ? 2
                    : 1
            console.log(`(${id}, '${code}', 0, ${category}, NULL, NULL, ${uid}, NULL),`)
            id++

            // const uid2 = qrToUid(code)
            // console.log(uid, code)
            // if (Math.random() * 10 < 2) process.exit()
        }
        console.log('')
    }
})().catch((err) => {
    console.error(err)
    process.exit(1)
})

