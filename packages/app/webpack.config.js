/* eslint-env node */
/* eslint-disable import/no-unused-modules */
/* eslint-disable @typescript-eslint/no-var-requires */
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const dotenv = require('dotenv')
const path = require('path')
const webpack = require('webpack')
dotenv.config()


const ROOT_DIR = path.resolve(__dirname)
const SRC_DIR = path.join(ROOT_DIR, './src')
const STATIC_DIR = path.join(ROOT_DIR, './static')
const IS_PRODUCTION = process.argv.some((arg) => arg === 'production' || arg === '--production')

const ENV_PATH = path.resolve(__dirname, '.env')
const defineDynamicEnvVariable = (name, cb = (value) => value) => ({
  [`process.env.${name}`]: webpack.DefinePlugin.runtimeValue(
    () => {
      const newestEnv = dotenv.config({path: path.resolve(__dirname, '.env')}).parsed
      const value = newestEnv ? newestEnv[name] : process.env[name]
      return JSON.stringify(cb(value))
    },
    [ENV_PATH]
  ),
})


module.exports = {
    mode: IS_PRODUCTION ? 'production' : 'development',
    entry: {
        main: [
            './src/index',
        ],
    },
    output: {
        filename: '[name].[contenthash].bundle.js',
        publicPath: '/',
    },
    devtool: IS_PRODUCTION ? false : 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            'assets': path.join(ROOT_DIR, 'assets'),
            'src/api': path.join(SRC_DIR, 'api'),
            'src/common': path.join(SRC_DIR, 'common'),
            'src/components': path.join(SRC_DIR, 'components'),
            'src/constants': path.join(SRC_DIR, 'constants'),
            'src/layouts': path.join(SRC_DIR, 'layouts'),
            'src/stores': path.join(SRC_DIR, 'stores'),
        },
        fallback: { 'path': require.resolve('path-browserify') },
    },
    // node: {
    //     __filename: true,  // To simplify makeStyles naming.
    // },
    module: {
        rules: [
            { test: /\.css$/, use: [ { loader: 'style-loader' }, { loader: 'css-loader' } ] },
            { test: /\.html$/, use: { loader: 'html-loader' } },
            { test: /\.svg$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.jpg$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.png$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.ttf$/, use: { loader: 'file-loader', options: { name: 'fonts/[name].[ext]?[contenthash]' } } },
            { test: /\.mp3$/, use: { loader: 'file-loader', options: { name: 'sounds/[name].[ext]?[contenthash]' } } },

            {
                test: /\.(j|t)sx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                        babelrc: false,
                        presets: [
                            [
                                '@babel/preset-env',
                                { targets: { browsers: '> 1%' } }, // or whatever your project requires
                            ],
                            ['@babel/preset-typescript', { allowNamespaces: true }],
                            ['@babel/preset-react', { loose: true }],
                        ],
                        plugins: [
                            // plugin-proposal-decorators is only needed if you're using experimental decorators in TypeScript
                            ['@babel/plugin-proposal-decorators', { legacy: true }],
                            ['@babel/plugin-proposal-class-properties'],
                            ['@babel/plugin-proposal-optional-chaining'],
                            ['@babel/plugin-proposal-nullish-coalescing-operator'],
                            !IS_PRODUCTION && require.resolve('react-refresh/babel'),
                        ].filter(Boolean),
                    },
                },
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(IS_PRODUCTION ? 'production' : 'development'),
            ...defineDynamicEnvVariable('API_URL'),
        }),
        new ForkTsCheckerWebpackPlugin({
            logger: {
                issues: process.argv.includes('--json') ? 'silent' : 'console',
            },
            typescript: { configFile: 'tsconfig.json' },
        }),
        new HtmlWebpackPlugin({
            title: 'LARP Throne App',
            template: path.join(STATIC_DIR, 'index.ejs'),
        }),
        new FaviconsWebpackPlugin(path.join(STATIC_DIR, 'favicon.svg')),
        new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
        }),
        !IS_PRODUCTION && new ReactRefreshWebpackPlugin(),
    ].filter(Boolean),

    devServer: {
        https: true,
        historyApiFallback: true,  // for SPA to fallback to index.
        allowedHosts: ['lapp.tronis.lv'],
        sockPort: 8080,
        sockHost: 'lapp.tronis.lv',
        host: '0.0.0.0',
    },

}
