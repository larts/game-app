import React from 'react'

import { Divider, Grid, Paper } from '@material-ui/core'
import { Typography } from '@material-ui/core'

import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        minWidth: 320,
        backgroundColor: theme.palette.primary.background,
        position: 'relative',
    },
    main: {
        flexGrow: 1,
        background: theme.palette.background.default,
        padding: theme.spacing(1, 2),
        transition: theme.transitions.create('padding'),
    },
    banner: {
        display: 'block',
        objectFit: 'cover',
        width: '100%',
        height: 420,
    },
})


interface IProps {
    title?: React.ReactNode
    info?: React.ReactNode
    footer?: JSX.Element
}


export default createFC(import.meta.url, styles)<IProps>(function _({ children, classes, theme, ...props }) {
    const { title, footer } = props

    return (
        <Grid
            container
            className={classes.root}
            direction="row"
            wrap='nowrap'
            justifyContent="center"
            justifyItems="stretch"
        >
            <Divider flexItem />
            <Grid item xs container direction='column' wrap='nowrap'>
                <Paper component='main' className={classes.main}>
                    {title && <Typography variant='h1' color='textSecondary'>{title}</Typography>}
                    {children}
                </Paper>
                {footer}
            </Grid>
        </Grid>
    )
}) /* ============================================================================================================== */
