import { QuerySeason } from '@larts/api'

import useTickGame from './useTickGame'

import { useQuery } from '.'


export default () => {
    const tickGame = useTickGame()
    const querySeason = useQuery(QuerySeason, { fetchPolicy: 'cache-and-network' })

    return tickGame - (querySeason.data?.season?.started ?? 0)
}
