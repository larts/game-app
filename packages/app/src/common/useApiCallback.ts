import { useCallback, useContext } from 'react'

import { useApolloClient } from '@apollo/client'
import { QuerySession, QueryTeamBase } from '@larts/api'
import { assertApolloError, assertError } from '@larts/common'

import { NotifyContext } from 'src/constants/context'


/**
 * Automatically catches and notifies of the error.
 */
export default <T extends unknown[] = unknown[]>(callback: (...args: T) => Promise<unknown>, deps: React.DependencyList) => {
    const notify = useContext(NotifyContext)
    const { cache } = useApolloClient()

    return useCallback(async (...args: T): Promise<undefined | Error> => {
        try {
            await callback(...args)
            return undefined
        } catch (error) {
            try {
                assertApolloError(error, notify)
                if (error.graphQLErrors.some((gqlError) => gqlError.extensions?.['code'] === 'AUTH_CHECK_NO_DEVICE')) {
                    notify('💥 Device not approved.')
                    cache.writeQuery({
                        ...QueryTeamBase,
                        data: {
                            team: null,
                        },
                    })
                } else if (error.graphQLErrors.some((gqlError) => gqlError.extensions?.['code'] === 'AUTH_CHECK_NO_USER')) {
                    notify('💥 User session expired.')
                    cache.writeQuery({
                        ...QuerySession,
                        data: {
                            user: null,
                            baseData: {
                                userSessionEnd: null,
                                isMarketDevice: false,
                            },
                        },
                    })
                } else {
                    notify(`💥 ${error.name}: ${error.message}`)
                }
            } catch (err) {
                assertError(error, notify)
                notify(`💥 ${error.name}: ${error.message}`)
            }
            return error
        }
    }, deps)
}
