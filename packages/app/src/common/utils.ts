export const sleep = async (seconds: number): Promise<void> => new Promise<void>((resolve) => setTimeout(() => resolve(), seconds * 1000))
export const timebomb = async (seconds: number): Promise<never> => new Promise<never>((_, reject) => setTimeout(() => reject(new Error('Timeouted.')), seconds * 1000))
export const timeout = async <TRes>(seconds: number, promise: Promise<TRes>): Promise<TRes> => Promise.race([promise, timebomb(seconds)])

type KeyValue<T extends Record<string, unknown>, K extends keyof T = keyof T> = [K, T[K]]
export const recordEntries = <T extends Record<string, unknown>>(o: T) => Object.entries(o) as KeyValue<T>[]
export const recordKeys = <T extends Record<string, unknown>>(o: T) => Object.keys(o) as (keyof T)[]
export const recordValues = <T extends Record<string, unknown>>(o: T) => Object.values(o) as T[keyof T][]

export const formatTimeRelative = (elapsed?: number) => {
    if (elapsed === undefined) return '..'

    const date = new Date(elapsed * 1000)

    return [
        date.getUTCHours() > 0 ? `${date?.getUTCHours()} h` : '',
        date.getUTCMinutes() > 0 ? `${date?.getUTCMinutes()} min` : '',
        date.getUTCHours() === 0 && date?.getUTCSeconds() > 0 ? `${date?.getUTCSeconds()} sec` : '',
    ].join(' ')
}

export const formatTimeAbsolute = (elapsed?: number | Date) => {
    if (elapsed === undefined) return '..'

    const date = elapsed instanceof Date ? elapsed : new Date(elapsed * 1000 + new Date().getTimezoneOffset() * 60 * 1000)

    return date.toTimeString().slice(0, 8)
}
