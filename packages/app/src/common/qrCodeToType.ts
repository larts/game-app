import { RESOURCE_TYPE } from '@larts/api'


const LOWERCASE_PREFIXES = {
    wd: RESOURCE_TYPE.Wood,
    rn: RESOURCE_TYPE.Iron,
    hd: RESOURCE_TYPE.Hides,
    fh: RESOURCE_TYPE.Food_Honey,
    ft: RESOURCE_TYPE.Food_Tomatoes,
    fc: RESOURCE_TYPE.Food_Cucumbers,
    fr: RESOURCE_TYPE.Food_Raspberries,
    fm: RESOURCE_TYPE.Food_Mushrooms,
    fv: RESOURCE_TYPE.Food_Lavender,
}


/**
 * Lower-case first.
 */
export const codeToType = (code: string): RESOURCE_TYPE => LOWERCASE_PREFIXES[code.slice(0, 2).toLocaleLowerCase()] ?? null

