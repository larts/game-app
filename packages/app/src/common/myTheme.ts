/* eslint-disable import/no-relative-parent-imports */
import { TEAM_COLOR } from '@larts/api'
import { Theme } from '@material-ui/core'
import { PaletteOptions, SimplePaletteColorOptions, alpha, createTheme, lighten } from '@material-ui/core/styles'
import { BaseCSSProperties } from '@material-ui/styles'

import OpenSansBold from 'assets/fonts/OpenSans-Bold.ttf'
import OpenSansBoldItalic from 'assets/fonts/OpenSans-BoldItalic.ttf'
import OpenSansExtraBold from 'assets/fonts/OpenSans-ExtraBold.ttf'
import OpenSansExtraBoldItalic from 'assets/fonts/OpenSans-ExtraBoldItalic.ttf'
import OpenSansItalic from 'assets/fonts/OpenSans-Italic.ttf'
import OpenSansLight from 'assets/fonts/OpenSans-Light.ttf'
import OpenSansLightItalic from 'assets/fonts/OpenSans-LightItalic.ttf'
import OpenSansRegular from 'assets/fonts/OpenSans-Regular.ttf'
import OpenSansSemiBold from 'assets/fonts/OpenSans-SemiBold.ttf'
import OpenSansSemiBoldItalic from 'assets/fonts/OpenSans-SemiBoldItalic.ttf'


enum FONT_WEIGHT {
    LIGHT = 300,
    REGULAR = 400,
    // MEDIUM = 500,  // OpenSans doesn't have it.
    SEMIBOLD = 600,
    BOLD = 700,
    // EXTRABOLD = 800,  // ChakraPetch doesn't have it.
}

const OpenSansFontUrls = {
    'Bold': OpenSansBold,
    'BoldItalic': OpenSansBoldItalic,
    'ExtraBold': OpenSansExtraBold,
    'ExtraBoldItalic': OpenSansExtraBoldItalic,
    'Italic': OpenSansItalic,
    'Light': OpenSansLight,
    'LightItalic': OpenSansLightItalic,
    'Regular': OpenSansRegular,
    'SemiBold': OpenSansSemiBold,
    'SemiBoldItalic': OpenSansSemiBoldItalic,
}

const getOpenSansSrc = (variant: keyof typeof OpenSansFontUrls) => {
    return `url('${OpenSansFontUrls[variant]}')`
}

export const OpenSansFontFaces: BaseCSSProperties['@font-face'] = [
    { fontFamily: 'Open Sans', fontWeight: 300, fontStyle: 'italic', src: getOpenSansSrc('LightItalic') },
    { fontFamily: 'Open Sans', fontWeight: 300, fontStyle: 'normal', src: getOpenSansSrc('Light') },
    { fontFamily: 'Open Sans', fontWeight: 400, fontStyle: 'italic', src: getOpenSansSrc('Italic') },
    { fontFamily: 'Open Sans', fontWeight: 400, fontStyle: 'normal', src: getOpenSansSrc('Regular') },
    { fontFamily: 'Open Sans', fontWeight: 600, fontStyle: 'italic', src: getOpenSansSrc('SemiBoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 600, fontStyle: 'normal', src: getOpenSansSrc('SemiBold') },
    { fontFamily: 'Open Sans', fontWeight: 700, fontStyle: 'italic', src: getOpenSansSrc('BoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 700, fontStyle: 'normal', src: getOpenSansSrc('Bold') },
    { fontFamily: 'Open Sans', fontWeight: 800, fontStyle: 'italic', src: getOpenSansSrc('ExtraBoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 800, fontStyle: 'normal', src: getOpenSansSrc('ExtraBold') },
]

const cssifyFontFace = (fontFace: BaseCSSProperties['@font-face']): string => {
    if (!fontFace) return ''
    if (Array.isArray(fontFace)) return fontFace.map(cssifyFontFace).join('\n')

    return `
        @font-face {
            font-family: ${fontFace.fontFamily};
            font-style: ${fontFace.fontStyle};
            font-display: ${fontFace.fontDisplay ?? 'swap'};
            font-weight: ${fontFace.fontWeight};
            src: ${fontFace.src};
        }`
}

declare module '@material-ui/core/styles/createPalette' {
    interface SimplePaletteColorOptions {
        border?: string,
        background?: string,
    }
    interface PaletteColor {
        border: string,
        background: string,
    }
}

export type IMyTheme = Theme


const SPACING = 8  // Default is 8.
const { spacing, breakpoints, typography: typographyRaw } = createTheme({
    spacing: SPACING,
    typography: {
    // htmlFontSize: 16,  // 16 is default.
    },
})

// www.color-hex.com/color
export const makeMyTheme = (paletteOptions: PaletteOptions & {primary: SimplePaletteColorOptions}) => {
    const { palette: paletteRaw } = createTheme({ palette: {
        divider: '#EEEEEE',
        ...paletteOptions,
        primary: {
            border: lighten(paletteOptions.primary.main, 0.5),
            background: alpha(paletteOptions.primary.main, 0.1),
            ...paletteOptions.primary,
        },
        info: {
            border: lighten(paletteOptions.primary.main, 0.5),
            background: alpha(paletteOptions.primary.main, 0.1),
            ...paletteOptions.primary,
        },
        error: {
            border: lighten(paletteOptions.primary.main, 0.5),
            background: alpha(paletteOptions.primary.main, 0.1),
            ...paletteOptions.primary,
        },
        warning: {
            border: lighten(paletteOptions.primary.main, 0.5),
            background: alpha(paletteOptions.primary.main, 0.1),
            ...paletteOptions.primary,
        },
        success: {
            border: lighten(paletteOptions.primary.main, 0.5),
            background: alpha(paletteOptions.primary.main, 0.1),
            ...paletteOptions.primary,
        },
        secondary: {
            dark: '#6D1B7B',
            main: '#9C27B0',
            light: '#AF52BF',
            contrastText: '#FFF',
            border: '#CD93D7',  // 50% figma-ligther than main.
            background: '#FBF6FC',
            ...paletteOptions.secondary,
        },
        background: {
            default: '#FFF',
            paper: '#F5F5F5',
            ...paletteOptions.background,
        },
        text: {
            secondary: alpha('#000', 0.6),
            ...paletteOptions.text,
        },
    },
    })

    return createTheme({
        spacing: SPACING,
        palette: {
            ...paletteRaw,
        },

        typography: {
            fontFamily: '"Open Sans", "Roboto", sans-serif',
            fontWeightLight: FONT_WEIGHT.LIGHT,
            fontWeightRegular: FONT_WEIGHT.REGULAR,
            fontWeightMedium: FONT_WEIGHT.SEMIBOLD,
            fontWeightBold: FONT_WEIGHT.BOLD,

            h1: {
                fontSize: typographyRaw.pxToRem(31.25),
                fontWeight: FONT_WEIGHT.SEMIBOLD,
                lineHeight: 1.25,
                letterSpacing: '0em',
            },
            h2: {
                fontSize: typographyRaw.pxToRem(25),
                fontWeight: FONT_WEIGHT.SEMIBOLD,
                lineHeight: 1.25,
                letterSpacing: '0em',
            },
            h3: {
                fontSize: typographyRaw.pxToRem(20),
                fontWeight: FONT_WEIGHT.SEMIBOLD,
                lineHeight: 1.25,
                letterSpacing: '0em',
            },
            h4: {
                color: 'red',
            },
            h5: {
                color: 'red',
            },
            h6: {
                color: 'red',
            },

            subtitle1: {
                fontSize: typographyRaw.pxToRem(16),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.50,
                letterSpacing: '0.03em',
            },
            subtitle2: {
                color: 'red',
            },

            body1: {  // Also a label.
                fontSize: typographyRaw.pxToRem(16),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.25,
                letterSpacing: '0em',
            },
            body2: {
                fontSize: typographyRaw.pxToRem(14),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.25,
                letterSpacing: '-0.02em',  // Default 0.01071em
            },

            button: {
                fontSize: typographyRaw.pxToRem(18),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.25,
                letterSpacing: '0em',
                textTransform: 'uppercase',
            },

            caption: {
                fontSize: typographyRaw.pxToRem(12),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.25,
                letterSpacing: '0em',
            },

            overline: {
                fontSize: typographyRaw.pxToRem(12),
                fontWeight: FONT_WEIGHT.REGULAR,
                lineHeight: 1.50,
                letterSpacing: '0.15em',
                textTransform: 'uppercase',
            },

            // // Custom style for monospace, e.g. raw filters textarea.
            // mono: {
            //     fontFamily: '"Roboto Mono", monospace',
            //     fontWeight: FONT_WEIGHT.REGULAR,
            //     fontSize: '0.75rem',  // 11px
            //     lineHeight: 1.2,
            //     letterSpacing: 0,
            // },
        },

        components: {
            MuiCssBaseline: {
                styleOverrides: 'html, body, #root { height: 100%; }\n' + cssifyFontFace([...OpenSansFontFaces]),
            },
            MuiAppBar: {
                defaultProps: {
                    elevation: 0,
                },
                styleOverrides: {
                    root: {
                        borderColor: paletteRaw.primary.border,
                        borderStyle: 'solid',
                        borderWidth: 0,
                        '&:first-of-type': {
                            borderBottomWidth: 1,
                        },
                        '&:last-of-type': {
                            borderTopWidth: 1,
                        },
                        flexDirection: 'row',  // why this is even 'column' by default??
                    },
                },
            },
            MuiBottomNavigation: {
                styleOverrides: {
                    root: {
                        backgroundColor: 'transparent',
                    },
                },
            },
            MuiButton: {
                defaultProps: {
                    disableElevation: true,
                    color: 'primary',
                },
                styleOverrides: {
                    root: {
                        borderRadius: 12,
                    },
                    outlined: {
                        borderWidth: 2,
                        '&:hover': {
                            borderWidth: 2,
                        },
                    },
                },
            },
            MuiIconButton: {
                defaultProps: {
                    size: 'small',
                },
            },
            MuiDivider: {
                styleOverrides: {
                    root: {
                        borderColor: paletteRaw.primary.border,
                    },
                },
            },
            MuiPaper: {
                defaultProps: {
                    elevation: 0,
                    square: true,
                },
            },
            MuiSvgIcon: {
                styleOverrides: {
                    fontSizeLarge: {
                        fontSize: typographyRaw.pxToRem(36),  // Why default is 35?
                    },
                },
            },
            MuiToolbar: {
                defaultProps: {
                    disableGutters: true,
                },
                styleOverrides: {
                    root: {
                        minHeight: 52,
                        width: '100%',
                        [breakpoints.up('sm')]: {
                            minHeight: 52,
                        },
                    },
                },
            },
            MuiTableCell: {
                styleOverrides: {
                    root: {
                        borderColor: paletteRaw.divider,
                        borderStyle: 'solid',
                        borderWidth: 1,
                        padding: spacing(1, 1),
                        [breakpoints.up('sm')]: {
                            padding: spacing(1, 2),
                        },
                    },
                },
            },
            MuiTableHead: {
                styleOverrides: {
                    root: {
                        backgroundColor: paletteRaw.primary.background,
                    },
                },
            },
            MuiTypography: {
                styleOverrides: {
                    paragraph: {
                        '&:last-child': {
                            marginBottom: 0,
                        },
                    },
                },
            },
            MuiListSubheader: {
                styleOverrides: {
                    root: {
                        ...typographyRaw.overline,
                        backgroundColor: 'unset',
                        color: paletteRaw.primary.contrastText,
                        lineHeight: 1,
                        textAlign: 'right',
                        margin: spacing(1, 0, -2.5),
                        position: 'static',
                    },
                },
            },
            MuiCard: {
                styleOverrides: {
                    root: {
                        padding: 0,
                        borderRadius: 16,
                        borderWidth: 1,
                        margin: spacing(2, 1),
                    },
                },
            },
            MuiCardHeader: {
                defaultProps: {
                    titleTypographyProps: {
                        component: 'h3' as any,
                        variant: 'h3',
                    },
                },
                styleOverrides: {
                    root: {
                        width: '100%',
                        paddingBottom: 'unset',
                        '&:last-child': {
                            paddingBottom: spacing(2),
                        },
                    },
                },
            },
            MuiCardContent: {
                styleOverrides: {
                    root: {
                        padding: spacing(1, 2),
                        '&:last-child': {
                            paddingBottom: spacing(2),
                        },
                    },
                },
            },
            MuiCardActions: {
                styleOverrides: {
                    root: {
                        padding: spacing(1, 2),
                    },
                },
            },
            MuiSnackbar: {
                defaultProps: {
                    anchorOrigin: { vertical: 'bottom', horizontal: 'center' },
                    autoHideDuration: 5000,
                },
            },
            MuiChip: {
                defaultProps: {
                    size: 'small',
                },
            },
            MuiDialog: {
                styleOverrides: {
                    paper: {
                        backgroundColor: paletteRaw.background.default,
                    },
                },
            },
            MuiDialogContent: {
                styleOverrides: {
                    root: {
                        padding: 0,
                    },
                },
            },
            MuiDialogTitle: {
                styleOverrides: {
                    root: {
                        // Copied h2
                        fontSize: typographyRaw.pxToRem(25),
                        fontWeight: FONT_WEIGHT.SEMIBOLD,
                        lineHeight: 1.25,
                        letterSpacing: '0em',
                        color: paletteRaw.text.primary,
                    },
                },
            },
            MuiLink: {
                styleOverrides: {
                    root: {
                        color: 'inherit',
                        textDecoration: 'underline',
                        '&:hover': {
                            color: 'inherit',
                        },
                    },
                },
            },
            MuiAlert: {
                styleOverrides: {
                    outlined: {
                        backgroundColor: paletteRaw.background.default,
                    },
                },
            },
        },
    })
}


const goldTheme    = makeMyTheme({ primary: { main: '#B88938', dark: '#816027', light: '#DCC49C' } })
const redTheme     = makeMyTheme({ primary: { main: '#EB1E28', dark: '#A5151C', light: '#F58F94' } })
const greenTheme   = makeMyTheme({ primary: { main: '#4EA653', dark: '#37743A', light: '#A7D3A9' } })
const purpleTheme  = makeMyTheme({ primary: { main: '#8551B8', dark: '#5D3981', light: '#C2A8DC' } })
const blueTheme    = makeMyTheme({ primary: { main: '#1A81EF', dark: '#125AA7', light: '#8DC0F7' } })
const orangeTheme  = makeMyTheme({ primary: { main: '#F26418', dark: '#A94611', light: '#F9B28C' } })
const pinkTheme    = makeMyTheme({ primary: { main: '#D83171', dark: '#97224F', light: '#EC98B8' } })

const DEFAULT_THEME = makeMyTheme({ primary: { main: '#666666' } })

const TEAM_THEMES: Record<TEAM_COLOR, Theme> = {
    [TEAM_COLOR.Gold]: goldTheme,
    [TEAM_COLOR.Red]: redTheme,
    [TEAM_COLOR.Green]: greenTheme,
    [TEAM_COLOR.Purple]: purpleTheme,
    [TEAM_COLOR.Blue]: blueTheme,
    [TEAM_COLOR.Orange]: orangeTheme,
    [TEAM_COLOR.Pink]: pinkTheme,
}

export const getTheme = (color?: TEAM_COLOR) => {
    return color ? TEAM_THEMES[color] : DEFAULT_THEME
}
