import { useContext } from 'react'

import { NotifyContext } from 'src/constants/context'


export default () => {
    return useContext(NotifyContext)
}
