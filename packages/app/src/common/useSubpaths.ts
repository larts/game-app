import { useLocation, useResolvedPath } from 'react-router'


export default () => {
    const location = useLocation()
    const resolvedPath = useResolvedPath('')
    const subpaths = location.pathname.replace(resolvedPath.pathname, '').split('/').filter(Boolean)

    return subpaths
}
