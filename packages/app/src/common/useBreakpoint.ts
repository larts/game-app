import { Breakpoint, useMediaQuery, useTheme } from '@material-ui/core'


export function useBreakpoint(breakpoint: Breakpoint | number): boolean {
    const theme = useTheme()
    return useMediaQuery(theme.breakpoints.up(breakpoint), { noSsr: true })
}
