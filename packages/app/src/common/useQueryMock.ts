import { useMemo } from 'react'

import { useAsync } from 'react-use'

import { sleep } from './utils'


export const useQueryMock = <TData = any>(queryMockData: TData, deps: any[] = []) => {
    const mockResponse = useMemo(() => queryMockData, deps)
    return useAsync(async (): Promise<TData> => {
        await sleep(Math.random())
        return Promise.resolve(queryMockData)
    }, [mockResponse])
}
