import * as React from 'react'

import { SvgIcon, SvgIconProps } from '@material-ui/core'


// TODO: make icons colorable.


/**
 * Use this helper to create SVG icons for ref to be properly forwarded.
 */
export const createSvgIconFC = (svg: React.ReactNode, config?: SvgIconProps) => React.forwardRef<SVGSVGElement, SvgIconProps>(({ ...otherProps }, ref): JSX.Element => (
    <SvgIcon {...config} {...otherProps} ref={ref}>
        {svg}
    </SvgIcon>
))
