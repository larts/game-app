import { useEffect, useState } from 'react'

import { CONFIG, QueryTime } from '@larts/api'

import useConfig from './useConfig'

import { useQuery } from '.'


export default () => {
    const gameHasStarted = useConfig(CONFIG.SwitchGameStarted, Number)  // Note: Boolean thinks that `!'0' -> true` :(
    const emergencyPause = useConfig(CONFIG.EmergTickerStop, Number)  // Note: Boolean thinks that `!'0' -> true` :(
    const queryTime = useQuery(QueryTime, { fetchPolicy: 'cache-and-network' })
    const tickGame = queryTime.data?.baseData.currentTime ?? 0

    const [tickGameOptimistic, setSimulatedTicks] = useState(0)

    useEffect(() => {
        setSimulatedTicks(0)

        if (!gameHasStarted) return
        if (emergencyPause) return

        const interval = setInterval(() => setSimulatedTicks((prev) => prev + 1), 1000)
        return () => clearInterval(interval)
    }, [tickGame])

    return tickGame + tickGameOptimistic
}
