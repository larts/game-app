import { MouseEvent, useCallback } from 'react'

import { useNavigate } from 'react-router'


export const useNavigateToHref = (callback?: (e: MouseEvent<HTMLAnchorElement>, pathname: string) => void) => {
    const navigate = useNavigate()

    return useCallback((e: MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault()
        const pathname = (e.target as HTMLAnchorElement).pathname
        navigate(pathname)
        callback?.(e, pathname)
    }, [callback])
}
