import { OperationVariables, QueryHookOptions, useQuery as useQueryBase } from '@apollo/client'
import { ApiBaseQueryOptions } from '@larts/api/src/queries/common'


export const useQuery = <TData = any, TVariables extends OperationVariables = OperationVariables>(apiQuery: ApiBaseQueryOptions<TData, TVariables>, options?: QueryHookOptions<TData, TVariables>) => {
    const { query, ...baseOptions } = apiQuery
    return useQueryBase<TData, TVariables>(query, { ...baseOptions, ...options })
}
