/** Global definitions for developement */


// for style loader
// eslint-disable-next-line import/no-unused-modules
declare module '*.jpg' {
    const content: string
    export default content
}
declare module '*.png' {
    const content: string
    export default content
}
declare module '*.svg' {
    const content: string
    export default content
}

declare module '*.ttf' {
    const content: string
    export default content
}
declare module '*.mp3' {
    const content: string
    export default content
}
