import React, { useCallback, useContext, useRef } from 'react'
import { useState } from 'react'

import { AppBar, BottomNavigation, BottomNavigationAction, Toolbar } from '@material-ui/core'

import useMutationNightScore from 'src/api/useMutationNightScore'
import { createFC } from 'src/common'
import { codeToType } from 'src/common/qrCodeToType'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import IconQR from 'src/components/icons/IconQR'
import Resource from 'src/components/Resource'
import { NotifyContext } from 'src/constants/context'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const [open, setOpen] = useState(false)
    const handleOpenCoins = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])

    const notify = useContext(NotifyContext)
    const recentScans = useRef(new Set<string>())

    const [nightScore] = useMutationNightScore()

    const handleScan = useCallback(async (code: string) => {
        if (recentScans.current.has(code)) return

        notify(<><Resource type={codeToType(code)} /> - {code} scanned..</>)
        recentScans.current.add(code)
        await nightScore(code)
    }, [notify])

    return (
        <div>
            <Toolbar />
            <AppBar position='fixed' sx={{ top: 'unset', bottom: 0 }}>
                <BottomNavigation
                    sx={{ width: '100%' }}
                    showLabels
                >
                    <BottomNavigationAction sx={{ color: theme.palette.primary.contrastText }} onClick={handleOpenCoins} icon={<IconQR />} label='Scan Favors' />
                </BottomNavigation>
            </AppBar>
            <DialogScanQrCode open={open} onClose={handleClose} onScan={handleScan} />
        </div>
    )
}) /* =============================================================================================================== */
