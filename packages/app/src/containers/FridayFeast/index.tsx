import React from 'react'

import { PILLAR } from '@larts/api'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'

import PillarPointsSummaryChart from '../Leaderboard/PillarPointsSummaryChart'
import VictoryPointsSummaryChart from '../Leaderboard/VictoryPointsSummaryChart'

import BottomFeastActions from './BottomFeastActions'


export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Royal Feast'>
                During Royal feast, earn favors by participating in competitions and other activities.
                Enter then using the button below.
            </PageHeader>
            <PageSection color='paper' title='Victory'>
                <PageSectionPart>
                    <VictoryPointsSummaryChart />
                </PageSectionPart>
            </PageSection>
            <PageSection color='paper' title='Royal Feast Points'>
                <PageSectionPart>
                    <PillarPointsSummaryChart seasonId={0} pillarId={PILLAR.FridayFeastSpecial} />
                </PageSectionPart>
            </PageSection>
            <BottomFeastActions />
        </div>
    )
}) /* =============================================================================================================== */
