import React, { useMemo } from 'react'

import { CONFIG, FOOD_SUBTYPE, QueryRecipes, QueryTeamResources, RESOURCE_TYPE } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import useConfig from 'src/common/useConfig'
import useTickGame from 'src/common/useTickGame'
import NoItemsHere from 'src/components/NoItemsHere'
import PageSection from 'src/components/PageSection'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { SUBROUTE_RECIPES } from 'src/constants'

import AvailableResources from '../../components/AvailableResources'

import RecipeCardAvailable from './RecipeCardAvailable'
import RecipeCardCooking from './RecipeCardCooking'


interface Props {
    filter: SUBROUTE_RECIPES.Invent | SUBROUTE_RECIPES.Copy
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, filter }) {
    const tickGame = useTickGame()
    const queryTeamResources = useQuery(QueryTeamResources)
    const myResourcesEnough = queryTeamResources.data?.team?.resources
        .filter((resource) => resource.type.id in FOOD_SUBTYPE && resource.available >= 1)

    const cookingTime = useConfig(CONFIG.RecipeTime, Number) ?? 0

    const queryRecipes = useQuery(QueryRecipes)

    const cooking = (queryRecipes.data?.recipes ?? [])
        .map((recipe) => {
            const cookLogEntry = recipe.cookLog.find((cookLog) => cookLog.team.id === queryTeamResources.data?.team?.id)
            if (!cookLogEntry) return null
            if (cookLogEntry.starts + cookingTime < tickGame) return null

            return {
                recipe,
                cookLogEntry,
            }
        })
        .sort((a, b) => (a?.cookLogEntry.starts ?? 0) - (b?.cookLogEntry.starts ?? 0))

    const available = queryRecipes.data?.recipes
        .filter((recipe) => (recipe.firstCookedBy === null) === (filter === SUBROUTE_RECIPES.Invent))
        .filter((recipe) => recipe.cookLog.every((logEntry) => logEntry.team.id !== queryTeamResources.data?.team?.id))
        .sort((a, b) => b.resources.length - a.resources.length)

    const groups = useMemo(() => {
        const groups = {
            available: [] as QueryRecipes.Response['recipes'][0][],
            missing1: [] as QueryRecipes.Response['recipes'][0][],
            missing2: [] as QueryRecipes.Response['recipes'][0][],
            other: [] as QueryRecipes.Response['recipes'][0][],
        }
        available?.forEach((recipe) => {
            const missing = recipe.resources.filter((resType) => !myResourcesEnough?.some((myRes) => myRes.type.id === resType.id)).length
            switch (missing) {
                case (0): {
                    groups.available.push(recipe)
                    break
                }
                case (1): {
                    groups.missing1.push(recipe)
                    break
                }
                case (2): {
                    groups.missing2.push(recipe)
                    break
                }
                default: {
                    groups.other.push(recipe)
                    break
                }
            }
        })
        return groups
    }, [available, myResourcesEnough])

    if (queryTeamResources.loading) return <ProgressFullscreen />
    if (queryRecipes.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection>
                <AvailableResources
                    threshold={{
                        [RESOURCE_TYPE.Coins]: 1,
                        [FOOD_SUBTYPE.Cucumbers]: 1,
                        [FOOD_SUBTYPE.Honey]: 1,
                        [FOOD_SUBTYPE.Lavender]: 1,
                        [FOOD_SUBTYPE.Mushrooms]: 1,
                        [FOOD_SUBTYPE.Raspberries]: 1,
                        [FOOD_SUBTYPE.Tomatoes]: 1,
                    }}
                />
            </PageSection>
            <PageSection title='Cooking'>
                {cooking.map((cookingEntry) => !cookingEntry ? null : (
                    <RecipeCardCooking recipe={cookingEntry.recipe} starts={cookingEntry.cookLogEntry.starts}  />
                ))}
                {!cooking && <NoItemsHere />}
            </PageSection>
            <PageSection title={`Recipes available (${groups.available.length})`}>
                {groups.available.length === 0 ? <NoItemsHere /> : groups.available.map((recipe) => <RecipeCardAvailable key={recipe.id} recipe={recipe} />)}
            </PageSection>
            <PageSection title={`One ingredient missing (${groups.missing1.length})`}>
                {groups.missing1.length  === 0 ? <NoItemsHere /> : groups.missing1.map((recipe) => <RecipeCardAvailable key={recipe.id} recipe={recipe} />)}
            </PageSection>
            <PageSection title={`Two ingredients missing (${groups.missing2.length})`}>
                {groups.missing2.length  === 0 ? <NoItemsHere /> : groups.missing2.map((recipe) => <RecipeCardAvailable key={recipe.id} recipe={recipe} />)}
            </PageSection>
            <PageSection title={`3+ ingredients missing (${groups.other.length})`}>
                {groups.other.length     === 0 ? <NoItemsHere /> : groups.other.map((recipe) => <RecipeCardAvailable key={recipe.id} recipe={recipe} />)}
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
