import React, { ChangeEvent, useCallback, useState } from 'react'

import { QueryRecipes, QueryTeamResources, QueryUserName, RESOURCE_TYPE } from '@larts/api'
import { Button, Chip, Dialog, Grid, IconButton, TextField, Toolbar, Typography } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'

import useMutationRecipeScore from 'src/api/useMutationRecipeScore'
import { IMyTheme, createFC, createStyles, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'

import IconFeast from '../../components/icons/IconFeast'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
    feast: {
        verticalAlign: 'text-bottom',
    },
})

interface Props {
    recipe: QueryRecipes.Response['recipes'][0]
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, recipe }) {
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryUser = useQuery(QueryUserName)

    const requirements = [{ id: RESOURCE_TYPE.Coins }, ...recipe.resources].map((resourceType) => [
        resourceType,
        (queryTeamResources.data?.team?.resources.find((res) => res.type?.id === resourceType.id)?.available ?? 0) >= 1,
    ] as const)


    const [open, setOpen] = useState(false)
    const [newTitle, setNewTitle] = useState('')
    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => {setOpen(false); setNewTitle('')}, [setOpen, setNewTitle])
    const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value
        setNewTitle(value)
    }, [setNewTitle])

    const [recipeScore] = useMutationRecipeScore()
    const handleCopy = useCallback(() => recipeScore(recipe.id), [recipe])
    const handleInvent = useCallback(async () => {
        await recipeScore(recipe.id, newTitle)
        handleClose()
    }, [recipe, handleClose, newTitle])

    return (
        <PageCard
            classes={{
                root: classes.root,
            }}
            action={(
                <Button
                    variant='contained'
                    onClick={recipe.firstCookedBy ? handleCopy : handleOpen}
                    disabled={!queryUser.data?.user || requirements.some(([, isOk]) => !isOk)}
                >
                    Cook
                </Button>
            )}
            title={recipe.title ?? <Typography variant='h3' sx={{ color: theme.palette.text.disabled, fontStyle: 'italic' }}>not invented yet</Typography>}
        >
            <Typography paragraph>
                get {recipe.resources.length} AP
                {!recipe.firstCookedBy && (
                    <>
                        {' '}
                        & Feast token {new Array(recipe.resources.length - 2).fill(null).map((_, i) => (<IconFeast key={i} className={classes.feast} />))}
                    </>
                )}
            </Typography>
            <Grid container spacing={1} sx={{ marginBottom: 1 }}>
                {requirements.map(([resourceType, isOk]) => (
                    <Grid key={resourceType.id} item>
                        <Chip
                            variant={isOk ? 'filled' : 'outlined'}
                            label={<Resource amount={resourceType.id === RESOURCE_TYPE.Coins ? 2 : 1} type={resourceType.id} />}
                        />
                    </Grid>
                ))}
            </Grid>
            <Dialog fullScreen open={open} onClose={handleClose}>
                <Toolbar>
                    <Grid item xs />
                    <IconButton size='medium' onClick={handleClose}>
                        <IconClose />
                    </IconButton>
                </Toolbar>
                <PageSection title='Invent a new recipe'>
                    <PageSectionPart>
                        <Typography paragraph>
                            get {recipe.resources.length} AP
                            {!recipe.firstCookedBy && (
                                <>
                                    {' '}
                                    & Feast token {new Array(recipe.resources.length - 2).fill(null).map((_, i) => (<IconFeast key={i} className={classes.feast} />))}
                                </>
                            )}
                        </Typography>
                        <Grid container spacing={1} sx={{ marginBottom: 1 }}>
                            {requirements.map(([resourceType, isOk]) => (
                                <Grid key={resourceType.id} item>
                                    <Chip
                                        variant={isOk ? 'filled' : 'outlined'}
                                        label={<Resource amount={resourceType.id === RESOURCE_TYPE.Coins ? 2 : 1} type={resourceType.id} />}
                                    />
                                </Grid>
                            ))}
                        </Grid>
                        <Typography paragraph>
                            This is an invention - you are the first one cooking it!
                        </Typography>
                        <Typography paragraph>
                            As the chef, please name your invention:
                        </Typography>
                        <TextField
                            autoComplete='off'
                            id='tributes'
                            margin='dense'
                            value={newTitle}
                            onChange={handleChange}
                            placeholder='enter creative recipe name'
                            error={!!newTitle && (newTitle.length < 6 || newTitle.length > 33) }
                            helperText={`Title is between 6 and 33 characters long (${newTitle.length})`}
                            fullWidth
                        />
                    </PageSectionPart>
                    <PageSectionPart>
                        <Button
                            fullWidth
                            variant='contained'
                            onClick={handleInvent}
                            disabled={!newTitle || newTitle.length < 6 || newTitle.length > 33}
                        >
                            Invent recipe
                        </Button>
                    </PageSectionPart>
                </PageSection>

            </Dialog>
        </PageCard>
    )
}) /* =============================================================================================================== */
