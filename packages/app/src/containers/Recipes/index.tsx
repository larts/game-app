import React from 'react'

import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'
import { Link } from 'react-router-dom'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { ROUTE, SUBROUTE_RECIPES, makeRoute } from 'src/constants'

import RecipesLogs from './RecipesLogs'
import RecipesMain from './RecipesMain'


const TABS = {
    [SUBROUTE_RECIPES.Invent]: 'Invent',
    [SUBROUTE_RECIPES.Copy]: 'Copy',
    [SUBROUTE_RECIPES.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Recipes'>
                <Typography paragraph>
                    Make recipes to generate Agriculture points (AP) - 1 AP per unique ingredient in recipe.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Combine unique food variants
                </Typography>
                <Typography paragraph>
                    There are 6 variants of Food. A unique combination of 4-6 of food variants (ingredients) is called a recipe. There are 22 recipes and making one requires 1 of each ingredient and 2 coins.
                </Typography>
                <Typography paragraph>
                    <strong>Your Faction can make a recipe only once </strong> either by inventing it, or by copying recipes that other Factions have invented.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Invent dishes to Feast
                </Typography>
                <Typography paragraph>
                    When you invent a dish, it gives you a Grand Dish token 🍱 that enables a Faction to host a <Link to={makeRoute(ROUTE.Feasts)}>Feast</Link> at their yard. Inventor also names the recipe.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Get Lavander from Merchant or in the market
                </Typography>
                <Typography paragraph>
                    The resource camps produce any of the first 5 food variants, but Lavander. Lavander can only be bought with coins at Merchant.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_RECIPES.Invent} element={<RecipesMain filter={SUBROUTE_RECIPES.Invent} />} />
                <Route path={SUBROUTE_RECIPES.Copy} element={<RecipesMain filter={SUBROUTE_RECIPES.Copy} />} />
                <Route path={SUBROUTE_RECIPES.Logs} element={<RecipesLogs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_RECIPES.Invent} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
