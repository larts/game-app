import React, { useEffect, useState } from 'react'

import { QueryRecipes, QueryTeamBase, RESOURCE_TYPE } from '@larts/api'
import { Chip, Grid, Typography } from '@material-ui/core'

import { IMyTheme, createFC, createStyles, formatTimeAbsolute, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import Resource from 'src/components/Resource'

import IconFeast from '../../components/icons/IconFeast'
import PageCardProgress from '../../components/PageCardProgress'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
    feast: {
        verticalAlign: 'text-bottom',
    },
})

interface Props {
    recipe: QueryRecipes.Response['recipes'][0]
    starts: number
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, recipe, starts }) {
    const queryTeamBase = useQuery(QueryTeamBase)
    const eta = starts + TASK_LENGTH

    const requirements = [{ id: RESOURCE_TYPE.Coins }, ...recipe.resources]

    const [progress, setProgress] = useState(0)

    useEffect(() => {
        if (!recipe) return

        const interval = setInterval(() => {
            const elapsed = Date.now() / 1000 - starts
            const newProgress = elapsed / TASK_LENGTH
            setProgress(newProgress)
        }, 1000)
        return () => clearInterval(interval)
    }, [starts, setProgress])

    return (
        <PageCard className={classes.root} title={recipe.title}>
            <Typography gutterBottom>
                get {recipe.resources.length} AP
                {recipe?.firstCookedBy?.id === queryTeamBase.data?.team?.id && (
                    <>
                        {' '}
                        & Feast token {new Array(recipe.resources.length - 2).fill(null).map((_, i) => (<IconFeast key={i} className={classes.feast} />))}
                    </>
                )}
            </Typography>
            <Typography gutterBottom>
                ETA: {formatTimeAbsolute(eta)}
            </Typography>
            <Grid container spacing={1} sx={{ marginBottom: 1 }}>
                {requirements.map((resourceType) => (
                    <Grid item key={resourceType.id}>
                        <Chip
                            color='primary'
                            variant="filled"
                            label={<Resource amount={resourceType.id === RESOURCE_TYPE.Coins ? 2 : 1} type={resourceType.id} />}
                        />
                    </Grid>
                ))}
            </Grid>
            <PageCardProgress amount={recipe.resources.length} startTime={starts} taskLength={TASK_LENGTH} />
        </PageCard>
    )
}) /* =============================================================================================================== */

const TASK_LENGTH = 12*60  // TODO: unhardcode
