import React from 'react'
import { useEffect } from 'react'

import { QueryTeamNextRespawn } from '@larts/api'
import { Button, Typography } from '@material-ui/core'
import { Box } from '@material-ui/system'
import { useAsyncFn } from 'react-use'
import { Pie, PieChart } from 'recharts'

import useMutationCharacterRespawn from 'src/api/useMutationCharacterRespawn'
import { createFC, formatTimeAbsolute, useQuery } from 'src/common'
import useTickGame from 'src/common/useTickGame'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'


interface IProps {
    newWristband: number
    onContinue: () => void
    reuse: boolean
}

const MAX_WAIT = 30  // todo: from config

const pieProps = {
    data: [{ value: 1 }],
    dataKey: 'value',
    animationDuration: 1000,
    animationEasing: 'linear' as const,
    animationBegin: 0,
    startAngle: 90,
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, reuse, newWristband, onContinue }) {
    const tickGame = useTickGame()
    const queryNextRespawn = useQuery(QueryTeamNextRespawn, { fetchPolicy: 'network-only' })

    const [characterRespawn] = useMutationCharacterRespawn()
    const respawnAfterTick = queryNextRespawn.data?.team?.nextRespawnTime ?? 0

    const [respawningRequest, handleRespawn] = useAsyncFn(async () => {
        const error = await characterRespawn(reuse ? undefined : newWristband)
        if (!error) onContinue()
    }, [])

    const endAngle = Math.max(0, (tickGame + MAX_WAIT - respawnAfterTick)/MAX_WAIT * 360) ?? 0

    // auto-respawn if can respawn instantly.
    useEffect(() => {
        if (respawnAfterTick < tickGame) handleRespawn()
    }, [respawnAfterTick])

    if (queryNextRespawn.loading) return <ProgressFullscreen />
    if (respawningRequest.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection title='Please wait'>
                <PageSectionPart>
                    <Typography paragraph>Someone else from your team respawned recently.</Typography>
                    <Typography paragraph>Wait to respawn.</Typography>
                </PageSectionPart>
            </PageSection>
            <PageSection>
                <Box position='relative'>
                    {/* eslint-disable-next-line react/forbid-component-props */}
                    <PieChart width={256} height={256} style={{ margin: 'auto' }}>
                        <Pie {...pieProps} endAngle={450}           outerRadius={90 }                  fill={theme.palette.primary.background} />
                        <Pie {...pieProps} endAngle={450}           outerRadius={110} innerRadius={98} fill={theme.palette.action.disabledBackground} />
                        <Pie {...pieProps} endAngle={90 - endAngle} outerRadius={110} innerRadius={98} fill={theme.palette.primary.main} />
                    </PieChart>
                    <Typography variant='h1' sx={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)' }}>
                        {formatTimeAbsolute(Math.max(0, respawnAfterTick - tickGame))}
                    </Typography>
                </Box>
            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Button
                        variant='contained'
                        fullWidth
                        disabled={respawnAfterTick >= tickGame}
                        onClick={handleRespawn}
                    >
                        Respawn now
                    </Button>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */

const timeDiff = (from: number, to: number) => {
    const diff = to - from
    if (diff < 0) return 'OK'

    const min = String(Math.floor(diff / 1000/60 % 60)).padStart(2, '0')
    const sec = String(Math.floor(diff / 1000    % 60)).padStart(2, '0')

    return `${min}:${sec}`
}
