import React, { useCallback } from 'react'

import { Grid, Paper, Typography } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { Check as IconCheck } from '@material-ui/icons'

import { createFC } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import { TextFieldWhite } from 'src/components/TextFieldWhite'


interface IProps {
    newWristband: number
    setNewWristband: (code: number) => void
    onContinue: () => void
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, newWristband, setNewWristband, onContinue }) {
    const handleChange = useCallback((event) => {
        const number_regex = /^\d+$/
        if (number_regex.test(event.target.value) || event.target.value === '') {
            setNewWristband(Number(event.target.value))
        }
    }, [setNewWristband])
    const handleSubmit = useCallback((event) => {
        event.preventDefault()

        onContinue()
    }, [])


    return (
        <form onSubmit={handleSubmit}>
            <PageSection>
                <PageSectionPart>
                    <Typography paragraph>
                        Sorry to hear your character is dead... Better luck next time!
                    </Typography>
                </PageSectionPart>
            </PageSection>
            <PageSection>
                <PageCard variant='standard' color='neutral' title="To respawn:">
                    <ol>
                        <Typography component='li'>Take a new wristband from your faction castle inventory box.</Typography>
                        <Typography component='li'>Put the wristband on your wrist.</Typography>
                        <Typography component='li'>Enter the new character's wristband number:</Typography>
                    </ol>
                </PageCard>
            </PageSection>
            <PageSection>
                <PageCard variant='filled' title="New character">
                    <Grid container spacing={2} alignItems='center' sx={{ width: '100%' }}>
                        <Grid item xs>
                            <TextFieldWhite
                                id='newUserId'
                                type='string'
                                placeholder='enter wristband nr. here'
                                value={newWristband || ''}
                                onChange={handleChange}
                                fullWidth
                            />
                        </Grid>
                        <Grid item>
                            <Paper elevation={9} sx={{ bgcolor: theme.palette.primary.main, borderRadius: '50%', color: 'inherit' }}>
                                <IconButton color='inherit' type='submit' size='medium'>
                                    <IconCheck />
                                </IconButton>
                            </Paper>
                        </Grid>
                    </Grid>
                </PageCard>
            </PageSection>
        </form>
    )
}) /* =============================================================================================================== */
