import React, { MouseEvent, useCallback, useEffect, useState } from 'react'

import { QueryUserCharacter, QueryUserName } from '@larts/api'
import { Button, Typography } from '@material-ui/core'
import { Check as IconCheck } from '@material-ui/icons'
import { Box } from '@material-ui/system'
import { Pie, PieChart } from 'recharts'

import useMutationUserLogout from 'src/api/useMutationUserLogout'
import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { ROUTE, makeRoute } from 'src/constants'


interface IProps {
}

const LOGOUT_WAIT = '15'

const pieProps = {
    data: [{ value: 1 }],
    dataKey: 'value',
    animationDuration: 0,
    startAngle: 90,
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()
    const [timer, setTimer] = useState<string>(LOGOUT_WAIT)
    const [userLogout] = useMutationUserLogout()
    const handleLogout = useCallback(async (event: MouseEvent<HTMLAnchorElement>) => {
        await userLogout()
        navigateToHref(event)
    }, [userLogout, navigateToHref])

    const queryUserName = useQuery(QueryUserName)
    const queryUserCharacter = useQuery(QueryUserCharacter)


    // todo: use tickGame
    useEffect(() => {
        const state_timer = Number(timer)
        if (state_timer === 0) {
            setTimer('...')
            userLogout()
            return undefined
        } else if (state_timer > 0) {
            const timeout = setTimeout(() => setTimer(String(state_timer - 1)), 1000)
            return () => clearTimeout(timeout)
        } else {
            return undefined
        }
    }, [timer, userLogout])

    if (queryUserName.loading || queryUserCharacter.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection title='Respawn complete!'>
                <PageSectionPart>
                    <Typography paragraph>You just started life nr. {queryUserCharacter.data?.user?.respawnCount}</Typography>
                    <Typography paragraph>Current life wristband No: {queryUserCharacter.data?.user?.character?.charcode}</Typography>
                </PageSectionPart>
            </PageSection>
            <PageSection>
                <Box position='relative'>
                    {/* eslint-disable-next-line react/forbid-component-props */}
                    <PieChart width={256} height={256} style={{ margin: 'auto' }}>
                        <Pie {...pieProps} endAngle={450} outerRadius={90} fill={theme.palette.primary.background} />
                        <Pie {...pieProps} endAngle={450} outerRadius={110} innerRadius={98} fill={theme.palette.primary.main} />
                    </PieChart>
                    <IconCheck color='primary' sx={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)', fontSize: 60 }} />
                </Box>
            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Box display="flex" justifyContent='space-between'>
                        <Button href={makeRoute(ROUTE.Home)} onClick={navigateToHref}>
                            Home
                        </Button>
                        <Button variant='contained' href={makeRoute(ROUTE.Respawn)} onClick={handleLogout}>
                            Logging out in {timer}
                        </Button>
                    </Box>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
