import React, { useCallback, useState } from 'react'

import { QueryUserCharacter } from '@larts/api'
import { Typography } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import RespawnStep1 from './RespawnStep1'
import RespawnStep2 from './RespawnStep2'
import RespawnStep3 from './RespawnStep3'
import RespawnStep4 from './RespawnStep4'
import RespawnStep5 from './RespawnStep5'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const [step, setStep] = useState(1)
    const [newWristband, setNewWristband] = useState<number>()

    const queryUserCharacter = useQuery(QueryUserCharacter, { fetchPolicy: 'network-only' })

    const setStep2 = useCallback(() => setStep(2), [setStep])
    const setStep3 = useCallback(() => setStep(3), [setStep])
    const setStep4 = useCallback(() => setStep(4), [setStep])
    const setStep5 = useCallback(() => setStep(5), [setStep])

    const handleReuseWristband = useCallback((code: number) => {
        setNewWristband(code)
        setStep4()
    }, [setNewWristband, setStep4])

    if (queryUserCharacter.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageHeader title='Respawn'>
                <Typography gutterBottom variant='h3'>
                    Dying
                </Typography>
                <Typography paragraph>
                    Each Player has 2-6 HP. If Player HP reaches 0, they are severely wounded until healed, executed or bled out. A dead Player gives up loot and Life wristband (tribute) to the killer and becomes a Ghost that can respawn for free at his Castle.
                </Typography>
                <Typography paragraph>
                    Player character can die by:
                </Typography>
                <ul>
                    <Typography component='li'>
                        Being executed: the opponent stabs the victim and says “I execute you”.
                    </Typography>
                    <Typography component='li'>
                        Being severely wounded for 5 minutes without healing.
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Ghost rules
                </Typography>
                <ul>
                    <Typography component='li'>
                        A Ghost cannot talk or interact with any other Player or game objects;
                    </Typography>
                    <Typography component='li'>
                        Ghost can move only empty boxes and their own equipment;
                    </Typography>
                    <Typography component='li'>
                        A Player is free to run/walk directly to their Castle to respawn;
                    </Typography>
                    <Typography component='li'>
                        Ghosts can respawn at their Castle immediately with full HP using the Throne Game App. There’s a 30 second cooldown until the next Ghost will be able to respawn.
                    </Typography>
                    <Typography component='li'>
                        If a Ghost comes back to the Castle with any lootable items (Coins, Resource tokens and Life wristband), all must be trashed.
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Tribute token
                </Typography>
                <Typography paragraph>
                    When killed, the Player's Life Wristband becomes a lootable Tribute token. When a Player is killed in combat, <strong>if asked they must give the looter all lootable items</strong>: their tribute token, any carried tribute tokens and coins, resources and any boxes.
                </Typography>
            </PageHeader>
            {step === 1 && <RespawnStep1 onContinue={setStep3} setWristband={handleReuseWristband} currentWristband={queryUserCharacter.data?.user?.character?.charcode} />}
            {step === 2 && <RespawnStep2 onContinue={setStep3} />}
            {step === 3 && <RespawnStep3 onContinue={setStep4} newWristband={newWristband ?? 0} setNewWristband={setNewWristband} />}
            {step === 4 && <RespawnStep4 onContinue={setStep5} newWristband={newWristband ?? 0} reuse={newWristband === queryUserCharacter.data?.user?.character?.charcode} />}
            {step === 5 && <RespawnStep5 />}
        </div>
    )
}) /* =============================================================================================================== */
