import React from 'react'

import { QuerySession, QueryUserEquipment, RESOURCE_TYPE } from '@larts/api'
import { Button, ButtonGroup, Typography } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'


interface IProps {
    onContinue: () => void
    setWristband: (code: number) => void
    currentWristband?: number
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, onContinue, setWristband, currentWristband }) {
    const querySession = useQuery(QuerySession)
    const queryMyEP = useQuery(QueryUserEquipment)
    const isMerchant = querySession.data?.baseData.isMarketDevice
    const isPhoenix = queryMyEP?.data?.user?.equipmentHasPhoenix

    return (
        <div>
            <PageSection>
                <PageSectionPart>
                    <PageCard variant='standard' color='neutral'>
                        <Typography paragraph>
                            <strong>
                                🚨 Put all tokens and coins <Resource type={RESOURCE_TYPE.Coins} /> in used token bag.
                            </strong>
                        </Typography>
                        <Typography paragraph>
                            When you have done it, come back and continue respawn process.
                        </Typography>
                    </PageCard>
                </PageSectionPart>
                {isMerchant && (
                    <PageSectionPart>
                        <PageCard variant='standard' color='neutral'>
                            You <strong>do {!isPhoenix ? 'not ' : '' }</strong>have Amulet of Phoenix required to respawn at Merchant.
                        </PageCard>
                    </PageSectionPart>
                )}
                {(!isMerchant || isPhoenix) && (
                    <PageCard title='Do you still have your Life wristband on?'>
                        <Typography paragraph>
                            It’s ok to continue using the same wristband.
                        </Typography>
                        <ButtonGroup fullWidth>
                            <Button variant='contained' disabled={!currentWristband} onClick={() => setWristband(currentWristband!)}>
                                Yes
                            </Button>
                            <Button variant='contained' onClick={onContinue}>
                                No
                            </Button>
                        </ButtonGroup>
                    </PageCard>
                )}
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
