import React from 'react'

import { Grid } from '@material-ui/core'


import { createFC } from 'src/common'
import IconMale from 'src/components/icons/IconMale'
import PageCard from 'src/components/PageCard'
import PageSectionPart from 'src/components/PageSectionPart'


interface Props {
}

export default createFC(import.meta.url)<Props>(function _({ children, theme }) {
    return (
        <Grid item xs={6}>
            <PageCard color='neutral'>
                <PageSectionPart>
                    <Grid container justifyContent='center'>
                        <IconMale fontSize='large' color='disabled' />
                    </Grid>
                </PageSectionPart>
            </PageCard>
        </Grid>
    )
}) /* =============================================================================================================== */
