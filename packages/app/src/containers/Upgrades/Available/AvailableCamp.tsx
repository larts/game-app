import React from 'react'

import { QuerySeason, QueryTeamUpgrades } from '@larts/api'
import { Chip, Typography } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import Resource from 'src/components/Resource'


interface Props {
    upgrade: NonNullable<QueryTeamUpgrades.Response['team']>['expansions'][0]
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, upgrade }) {
    const season1 = useQuery(QuerySeason, { variables: { season: 1 } })
    const season2 = useQuery(QuerySeason, { variables: { season: 2 } })
    const season3 = useQuery(QuerySeason, { variables: { season: 3 } })
    const season4 = useQuery(QuerySeason, { variables: { season: 4 } })

    const seasons = new Map<number, number>([
        [season1.data?.season?.started ?? 0, 1],
        [season2.data?.season?.started ?? 0, 2],
        [season3.data?.season?.started ?? 0, 3],
        [season4.data?.season?.started ?? 0, 4],
    ])

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const thisSeason = seasons.get(upgrade.showedAt)!
    const nextSeason = thisSeason + 1

    return (
        <PageCard title={`Camp  #${upgrade.camp?.displayTitle}`}>
            <Typography paragraph variant='body2' color='textSecondary'>
                {/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */}
                <Resource type={upgrade.camp!.resourceType.id} /> {upgrade.camp!.displayTitle}
            </Typography>
            <Typography paragraph>
                Upgrade code: <Chip label={upgrade.code} />
            </Typography>
            <Typography paragraph color='textSecondary'>
                Submit for reward during season <strong>{thisSeason} & {nextSeason}</strong>
            </Typography>
        </PageCard>
    )
}) /* =============================================================================================================== */
