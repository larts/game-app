import React from 'react'

import { createFC } from 'src/common'

import Available from './Available'
import Redeem from './Redeem'
import Submitted from './Submitted'


export default createFC(import.meta.url)(function _({ children, theme }) {


    return (
        <div>
            <Redeem />
            <Available />
            <Submitted />
        </div>
    )
}) /* =============================================================================================================== */
