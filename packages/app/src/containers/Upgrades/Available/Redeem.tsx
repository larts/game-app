import React, { ChangeEvent, useCallback, useState } from 'react'

import { QueryUserName } from '@larts/api'
import { Grid, Paper } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { Check as IconCheck } from '@material-ui/icons'

import useMutationUpgradeReward from 'src/api/useMutationUpgradeReward'
import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import { TextFieldWhite } from 'src/components/TextFieldWhite'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const [upgradesReward] = useMutationUpgradeReward()
    const queryUser = useQuery(QueryUserName)

    const [input, setInput] = useState('')
    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setInput(event.target.value.toUpperCase())
    }, [setInput])
    const handleSubmit = useCallback((event) => {
        event.preventDefault()

        upgradesReward(input)
    }, [input])


    return (
        <PageCard variant='filled' title="Redeem reward">
            <Grid component='form' onSubmit={handleSubmit} container spacing={2} alignItems='center' sx={{ width: '100%' }}>
                <Grid item xs>
                    <TextFieldWhite
                        id='reward'
                        type='string'
                        placeholder='reward code'
                        value={input || ''}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid>
                <Grid item>
                    <Paper elevation={9} sx={{ bgcolor: theme.palette.primary.main, borderRadius: '50%', color: 'inherit' }}>
                        <IconButton color='inherit' type='submit' size='medium' disabled={!queryUser.data?.user}>
                            <IconCheck />
                        </IconButton>
                    </Paper>
                </Grid>
            </Grid>
        </PageCard>
    )
}) /* =============================================================================================================== */
