import React from 'react'

import { QueryTeamBase, QueryTeamUpgrades, TEAM_COLOR } from '@larts/api'
import { Chip, ThemeProvider, Typography } from '@material-ui/core'

import { createFC, formatTimeAbsolute, getTheme, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import Resource from 'src/components/Resource'


interface Props {
    upgrade: NonNullable<QueryTeamUpgrades.Response['team']>['expansions'][0]
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, upgrade }) {
    const queryTeam = useQuery(QueryTeamBase)
    const teamId = queryTeam.data?.team?.id

    return (
        <PageCard title={`Camp #${upgrade.camp?.displayTitle}`}>
            <Typography paragraph variant='body2' color='textSecondary'>
                {/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */}
                <Resource type={upgrade.camp!.resourceType.id} /> {upgrade.camp!.displayTitle}
            </Typography>
            <ThemeProvider theme={getTheme(upgrade.owners.color as TEAM_COLOR)}>
                <Typography paragraph>
                    Upgrade code: <Chip color='primary' label={upgrade.code} />
                </Typography>
            </ThemeProvider>
            <Typography paragraph color='textSecondary'>
                Redeemed at {formatTimeAbsolute(upgrade.rewardedAt)}
                {upgrade.rewarded?.id !== teamId && (
                    <>
                        {' '}
                        by <Chip color='primary' label={upgrade.rewarded?.title} />
                    </>
                )}
            </Typography>
        </PageCard>
    )
}) /* =============================================================================================================== */
