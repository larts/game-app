import React from 'react'

import { QueryTeamUpgrades } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import AvailableCamp from './AvailableCamp'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryTeamUpgrades = useQuery(QueryTeamUpgrades)

    if (queryTeamUpgrades.loading) return <ProgressFullscreen />

    const availableUpgrades = queryTeamUpgrades.data?.team?.expansions?.filter((upgrade) => !upgrade.rewarded) ?? []

    return (
        <PageSection title={`Available camp upgrades (${availableUpgrades.length})`}>
            {availableUpgrades.map((upgrade) => (
                <AvailableCamp key={upgrade.code} upgrade={upgrade} />
            ))}
        </PageSection>
    )
}) /* =============================================================================================================== */
