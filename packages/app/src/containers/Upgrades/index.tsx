import React from 'react'

import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { SUBROUTE_FEASTS } from 'src/constants'

import Available from './Available'
import Logs from './Logs'


const TABS = {
    [SUBROUTE_FEASTS.Available]: 'Available',
    [SUBROUTE_FEASTS.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Camp upgrades'>
                <Typography gutterBottom variant='h3'>
                    Camp upgrade increases resource camp productivity. Redeem reward code to gain industry points.
                </Typography>
                <Typography paragraph>
                    Enter corresponding camp upgrade code to a resource camp to increase that camps productivity. First upgrade increases production from 12 resources per hour to 14; second upgrade - from 14 resources to 16 per hour. Next upgrades don't increase productivity any further.
                </Typography>
                <Typography paragraph>
                    When you enter the upgrade code in its corresponding resource camp, it will show you a reward code that you can redeem here in the Throne App to gain industry points.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Each camp upgrade responds to exactly one specific camp
                </Typography>
                <Typography paragraph>
                    At the start of each season every Faction gains 2 camp upgrade codes for specific camp that are showed in this section. Those camps can be anywhere on the map.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Submit reward codes for Industry points (IP)
                </Typography>
                <Typography paragraph>
                    When the reward code is submitted in the Throne Game App, rewards are distributed:
                </Typography>
                <ul>
                    <Typography component='li'>
                        2 IP to the Faction that gained camp the upgrade code
                    </Typography>
                    <Typography component='li'>
                        2 IP to the Faction that submitted the reward code
                    </Typography>
                </ul>
                <Typography paragraph>
                    Both rewards for an upgrade can be recieved by the same faction or two different factions.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    The last upgrades will not increase camp productivity (but you'll still recieve reward)
                </Typography>
                <Typography paragraph>
                    During the game, there are 3-4 upgrade codes available for each resource camp. All available upgrade codes can be entered in the camp, <strong>but only the first 2 entered upgrade codes will improve the production</strong>.
                </Typography>
                <Typography paragraph>
                    Any further upgrades will not improve the production, although camp will still show the reward code - so the reward can be claimed as normal.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Share upgrade codes with others
                </Typography>
                <Typography paragraph>
                    A camp upgrade code can be safely shared - whenever someone on the other edge of the map submits it, your Faction will get 2 IP even if you never went that far yourself.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Be quick with upgrades
                </Typography>
                <Typography paragraph>
                    The value of a camp upgrade code reduces after 2 seasons. If camp upgrade code is entered late, the camp will still increase productivity. The team submitting the code will get only 1 IP.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_FEASTS.Available} element={<Available />} />
                <Route path={SUBROUTE_FEASTS.Logs} element={<Logs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_FEASTS.Available} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
