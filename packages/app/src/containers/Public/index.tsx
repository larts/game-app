import React from 'react'

import { createFC } from 'src/common'
import SectionTime from 'src/components/SectionTime'

import Leaderboard from '../Leaderboard'

import TodoTournamentPublic from './TodoTournamentPublic'


export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <SectionTime>
                <TodoTournamentPublic />
            </SectionTime>
            <Leaderboard />
        </div>
    )
}) /* =============================================================================================================== */
