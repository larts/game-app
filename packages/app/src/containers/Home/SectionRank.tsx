import React, { useMemo } from 'react'

import { PILLAR, QueryScoreVictorySummary, QueryTeamBase, QueryTeamResources, QueryTeamsScoreTotals } from '@larts/api'
import { Button, CircularProgress, Grid, Typography } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryTeamScoreTotals = useQuery(QueryTeamsScoreTotals)
    const queryTeam = useQuery(QueryTeamBase)

    const scoreTotalsSorted = useMemo(() => {
        return queryTeamScoreTotals.data?.team?.scoreTotals
            .filter((score) => score.pillar.id !== PILLAR.Victory)
            .slice()
            .sort((a, b) => a.pillar.id - b.pillar.id)
            ?? []
    }, [queryTeamScoreTotals.data?.team?.scoreTotals])

    const queryVictoryPoints = useQuery(QueryScoreVictorySummary)

    const teamId = queryTeam.data?.team?.id
    const pillar = queryVictoryPoints.data?.victoryPoints

    const dataSorted = pillar?.totals?.slice().sort((a, b) => b.points - a.points) ?? []
    const myScore = dataSorted.find((score) => score.team.id === teamId)?.points
    const myPlace = dataSorted.findIndex((score) => score.team.id === teamId) + 1


    if (queryTeamResources.loading || queryTeamScoreTotals.loading) return (
        <PageSection color='paper'>
            <PageSectionPart>
                <Grid container justifyContent='center'>
                    <CircularProgress variant='indeterminate' />
                </Grid>
            </PageSectionPart>
        </PageSection>
    )

    return (
        <PageSection color='paper'>
            <PageSectionPart title='Your Rank' action={<Button href={makeRoute(ROUTE.Leaderboard)} onClick={navigateToHref}>to Stats</Button>}>
                <Grid container spacing={0}>
                    <Grid item xs={3}>
                        <PageCard variant='standardOutlined' sx={{ '&> div': { padding: theme.spacing(1, 1, 2) } }}>
                            <Typography component='div' variant='overline' align='center'>{myScore} VP</Typography>
                            <Typography variant='subtitle1' align='center'>
                                <strong>{myPlace}{myPlace === 1 ? 'st' : myPlace === 2 ? 'nd' : myPlace === 3 ? 'rd' : 'th'}</strong>
                            </Typography>
                        </PageCard>
                    </Grid>
                    {scoreTotalsSorted.map((score) => (
                        <Grid key={score.id} item xs={3}>
                            <PageCard sx={{ '&> div': { padding: theme.spacing(1, 1, 2) } }}>
                                <Typography component='div' variant='overline' align='center'>{score.score} {PILLAR_ID_TO_SHORT[score.pillar.id]}</Typography>
                                <Typography variant='subtitle1' align='center'>
                                    <strong>{score.rank}{score.rank === 1 ? 'st' : score.rank === 2 ? 'nd' : score.rank === 3 ? 'rd' : 'th'}</strong>
                                </Typography>
                            </PageCard>
                        </Grid>
                    ))}
                </Grid>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */

const PILLAR_ID_TO_SHORT = {
    [PILLAR.Victory]: 'VP',
    [PILLAR.Agriculture]: 'AP',
    [PILLAR.Industry]: 'IP',
    [PILLAR.Military]: 'MP',
    [PILLAR.FridayFeastSpecial]: 'RP',
}
