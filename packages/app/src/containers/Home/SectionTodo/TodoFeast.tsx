import React, { useEffect } from 'react'

import { QueryTeamFeasts } from '@larts/api'
import { Alert, AlertTitle, Button } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const navigateToHref = useNavigateToHref()
    const queryFeasts = useQuery(QueryTeamFeasts)

    const available = queryFeasts.data?.team?.feastTokens.filter((feast) => !feast.usedAt).length ?? 0

    const visible = available > 0

    useEffect(() => {
        onTodo('feast', visible)
    }, [visible, onTodo])


    if (!visible) return null

    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined' action={<Button href={makeRoute(ROUTE.Feasts)} onClick={navigateToHref}>Feast</Button>}>
                <AlertTitle>You can make {available} feasts</AlertTitle>
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
