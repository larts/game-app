import React, { useEffect } from 'react'

import { QueryTeamEquipment } from '@larts/api'
import { Alert, AlertTitle, Button } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const navigateToHref = useNavigateToHref()
    const queryTeamEP = useQuery(QueryTeamEquipment)

    const visible = !!queryTeamEP.data?.team?.availableEq
    useEffect(() => {
        onTodo('equipment', visible)
    }, [visible, onTodo])


    if (!visible) return null

    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined' action={<Button href={makeRoute(ROUTE.EquipmentPoints)} onClick={navigateToHref}>Equipment</Button>}>
                <AlertTitle>{queryTeamEP.data?.team?.availableEq} unused EP</AlertTitle>
                give out equipment to faction members
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
