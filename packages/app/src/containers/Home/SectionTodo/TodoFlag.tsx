import React, { useEffect } from 'react'

import { QueryTeamFlag } from '@larts/api'
import { Alert, AlertTitle, Button } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const navigateToHref = useNavigateToHref()
    const queryTeamFlag = useQuery(QueryTeamFlag)

    const visible = !queryTeamFlag.data?.team?.flagRaisedTime
    useEffect(() => {
        onTodo('flag', visible)
    }, [visible, onTodo])


    if (!visible) return null

    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined' action={<Button href={makeRoute(ROUTE.Flag)} onClick={navigateToHref}>Flags</Button>}>
                <AlertTitle>Flag to be raised</AlertTitle>
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
