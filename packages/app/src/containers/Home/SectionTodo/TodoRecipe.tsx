import React, { useEffect } from 'react'

import { FOOD_SUBTYPE, QueryRecipes, QueryTeamResources } from '@larts/api'
import { Alert, AlertTitle, Button } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, makeRoute } from 'src/constants'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const navigateToHref = useNavigateToHref()
    const queryRecipes = useQuery(QueryRecipes)

    const queryTeamResources = useQuery(QueryTeamResources)
    const myResourcesEnough = queryTeamResources.data?.team?.resources
        .filter((resource) => resource.type.id in FOOD_SUBTYPE && resource.available >= 2)

    const available = queryRecipes.data?.recipes
        .filter((recipe) => !recipe.firstCookedBy)
        .filter((recipe) => recipe.cookLog.every((logEntry) => logEntry.team.id !== queryTeamResources.data?.team?.id))
        .filter((recipe) => recipe.resources.every((resource) => myResourcesEnough?.some((myRes) => myRes.type.id === resource.id)))
        .length ?? 0

    const visible = available > 0

    useEffect(() => {
        onTodo('recipe', visible)
    }, [visible, onTodo])


    if (!visible) return null

    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined' action={<Button href={makeRoute(ROUTE.Recipes)} onClick={navigateToHref}>Recipes</Button>}>
                <AlertTitle>You can invent {available} recipe(s)</AlertTitle>
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
