import React, { useEffect } from 'react'

import { CONFIG } from '@larts/api'
import { Alert, AlertTitle } from '@material-ui/core'

import { createFC, formatTimeRelative } from 'src/common'
import useConfig from 'src/common/useConfig'
import useTickSeason from 'src/common/useTickSeason'
import PageSectionPart from 'src/components/PageSectionPart'


interface Props {
    onTodo: (name: string, value: boolean) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, onTodo }) {
    const tickSeason = useTickSeason()
    const seasonAvg = useConfig(CONFIG.SeasonAvg, Number) ?? 0

    const remaining = (seasonAvg * 60 ?? 0) / 2 - (tickSeason ?? 0)
    const visible = remaining > 0


    useEffect(() => {
        onTodo('tournament', visible)
    }, [visible, onTodo])

    if (!visible) return null


    return (
        <PageSectionPart>
            <Alert severity='info' variant='outlined'>
                <AlertTitle>Tournament coming up</AlertTitle>
                in {formatTimeRelative(remaining)} at Merchant
            </Alert>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
