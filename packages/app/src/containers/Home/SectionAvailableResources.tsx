import React from 'react'

import { FOOD_SUBTYPE, Resource as GqlResource, QueryTeamResources, RESOURCE_TYPE } from '@larts/api'
import { Button, Chip, CircularProgress, Grid } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'
import { ROUTE, makeRoute } from 'src/constants'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()
    const queryTeamResources = useQuery(QueryTeamResources)

    if (queryTeamResources.loading) return (
        <PageSection color='paper'>
            <PageSectionPart>
                <Grid container justifyContent='center'>
                    <CircularProgress variant='indeterminate' />
                </Grid>
            </PageSectionPart>
        </PageSection>
    )

    const resources = [
        ...(queryTeamResources.data?.team?.resources ?? []).filter((res) => !(res.type.id in FOOD_SUBTYPE)),
        {
            type: { id: RESOURCE_TYPE.Food },
            available: queryTeamResources.data?.team?.resources?.filter((res) => res.type.id in FOOD_SUBTYPE).reduce((sum, { available }) => sum + available, 0),
        } as GqlResource,
    ]

    return (
        <PageSection color='paper'>
            <PageSectionPart title='Available' action={<Button href={makeRoute(ROUTE.Resources)} onClick={navigateToHref}>to Resources</Button>}>
                <Grid container spacing={1}>
                    {resources
                        .sort((a, b) => b.available - a.available)
                        .map((res) => (
                            <Grid key={res.type.id} item>
                                <Chip
                                    color={res.available >= 2 ? 'primary' : 'default'}
                                    variant={res.available >= 2 ? 'filled' : 'outlined'}
                                    label={<Resource amount={res.available} type={res.type.id} />}
                                />
                            </Grid>
                        ))
                    }
                </Grid>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */
