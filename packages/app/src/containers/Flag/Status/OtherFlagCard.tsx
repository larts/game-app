import React, { useCallback, useState } from 'react'

import { CONFIG, FLAG_STATUS, QuerySession, TEAM_COLOR, TEAM_ID } from '@larts/api'
import { Team } from '@larts/api'
import { Button, Chip, Grid, ThemeProvider, Typography } from '@material-ui/core'

import useMutationFlagScore from 'src/api/useMutationFlagScore'
import { createFC, formatTimeRelative, getTheme, useQuery } from 'src/common'
import useConfig from 'src/common/useConfig'
import useTickGame from 'src/common/useTickGame'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import Flag from 'src/components/Flag'
import PageCard from 'src/components/PageCard'


interface Props {
    flagTeam: Team
    userTeamId: TEAM_ID
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, flagTeam, userTeamId }) {
    const querySession = useQuery(QuerySession)
    const [open, setOpen] = useState(false)
    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])
    const flagLength = useConfig(CONFIG.FlagHomeScoringAt, Number) ?? 0
    const tickGame = useTickGame()

    const [flagScore] = useMutationFlagScore()
    const handleScore = useCallback(async (qrCode: string) => {
        await flagScore(qrCode)
        handleClose()
    }, [flagScore, handleClose])

    const status = !flagTeam.flagRaisedTime ? 'lowered'
        : !flagTeam.flagScoredTime ? 'raised'
            : flagTeam.flagStatusId === FLAG_STATUS.Stolen ? 'captured'
                : 'defended'

    const action = ({
        lowered: <Chip variant='filled' size='small' label='lowered' />,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        raised: ((flagTeam.id !== userTeamId) && querySession.data?.baseData.isMarketDevice)
            ? <Button variant='contained' onClick={handleOpen}>Steal flag</Button>
            : <Chip color='primary' label={`Raised: ${Math.floor((tickGame - flagTeam.flagRaisedTime!) / 60)}min`} />,
        captured: <Chip color='primary' label="captured" />,
        defended: <Chip color='primary' label="defended" />,
    })[status]

    const raisedRemaining = tickGame - flagTeam.flagRaisedTime! < flagLength*60 ? (flagLength - Math.floor((tickGame - flagTeam.flagRaisedTime!) / 60))+' min' : 'the last moment'

    const text = ({
        lowered: `Wait when raised and capture in ${formatTimeRelative(flagLength * 60)}`,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        raised: `You have ${raisedRemaining} to steal the flag, and then score at merchant for ${flagTeam.flagStatusId === FLAG_STATUS.Shamed ? 2 : 6} MP`,
        captured: 'Can\'t score this season anymore.',
        defended: 'Can\'t score this season anymore.',
    })[status]

    return (
        <ThemeProvider theme={getTheme(flagTeam.color as TEAM_COLOR)}>
            <PageCard
                title={<Flag color={flagTeam.color as TEAM_COLOR} title={flagTeam.title} />}
                action={action}
                variant='standard'
            >
                <Grid container alignItems='flex-end'>
                    <Grid item xs>
                        <Typography paragraph variant='body2'>
                            {text}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Chip variant='outlined' size='small' label={FLAG_STATUS_TITLE[flagTeam.flagStatusId]} />
                    </Grid>
                </Grid>
            </PageCard>
            <DialogScanQrCode open={open} onClose={handleClose} onScan={handleScore} />
        </ThemeProvider>
    )
}) /* =============================================================================================================== */

const FLAG_STATUS_TITLE = {
    [FLAG_STATUS.Stolen]: 'shamed',
    [FLAG_STATUS.Shamed]: 'shamed',
    [FLAG_STATUS.Glory]: 'glory',
}
