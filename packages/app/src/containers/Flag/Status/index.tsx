import React from 'react'

import { QueryTeamFlag, QueryUserName, TEAM_ID, Team } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import OtherFlagCard from './OtherFlagCard'
import OwnedFlagCard from './OwnedFlagCard'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryUser = useQuery(QueryUserName)

    const queryTeams = [
        useQuery(QueryTeamFlag, { variables: { id: TEAM_ID.Lynx } }),
        useQuery(QueryTeamFlag, { variables: { id: TEAM_ID.Bears } }),
        useQuery(QueryTeamFlag, { variables: { id: TEAM_ID.Snakes } }),
    ]

    const teamsSorted = queryTeams
        .map((queryTeam) => queryTeam.data?.team)
        .filter((team): team is Team => !!team)
        .sort((a, b) => (b.flagStatusId ?? 0) - (a.flagStatusId ?? 0))
        .sort((a, b) => queryUser.data?.user?.team.id === a.id ? -1 : queryUser.data?.user?.team.id === b.id ? 1 : 0)

    if (queryUser.loading) return <ProgressFullscreen />

    return (
        <div>
            {teamsSorted.map((team) => {
                const Component = queryUser.data?.user?.team.id === team.id ? OwnedFlagCard : OtherFlagCard
                return (
                    <PageSection key={team.id}>
                        <Component flagTeam={team} userTeamId={queryUser.data?.user?.team.id as TEAM_ID} />
                    </PageSection>
                )
            })}
        </div>
    )
}) /* =============================================================================================================== */
