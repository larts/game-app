import React from 'react'

import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { SUBROUTE_FLAG } from 'src/constants'

import Logs from './Logs'
import Status from './Status'


const TABS = {
    [SUBROUTE_FLAG.Status]: 'Current Status',
    [SUBROUTE_FLAG.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Flag'>
                <Typography paragraph>
                    Each Faction has a Flag in the Faction's Yard. A <strong>raised</strong> Flag is lootable during a siege. A Flag can be scored only once per season. There are 2 ways a Faction can score a Flag:
                </Typography>
                <ul>
                    <Typography component='li'>
                        <strong>Defend</strong> your own Faction's flag (8 Military points (MP))
                    </Typography>
                    <Typography component='li'>
                        <strong>Capture</strong> other Faction's flag by bringing it to Merchant (2 or 6 MP, depending on flag's level)
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>Defend</Typography>
                <Typography paragraph>
                    To defend your Flag, have it <strong>raised in the Castle until mid-season (40th minute)</strong>. It will be raised automatically. If you have successfully defended the Flag, enter it's QR code again to score it for 8 MP. After you have defended the Flag, no other faction can gain points from this Flag this Season.
                </Typography>
                <Typography gutterBottom variant='h3'>Capture</Typography>
                <Typography paragraph>
                    To capture another Faction's Flag, <strong>bring a raised Flag to the Merchant</strong> until the minimum length of season (60th minute). That usually implies sieging the Castle but not necessary. The Flag carrier has diplomatic immunity against everyone but Flag's owners.
                </Typography>
                <Typography gutterBottom variant='h3'>Captured flag reward at Merchant by it's level</Typography>
                <Typography paragraph>
                    Shamed = 2 MP; Glory = 6 MP
                </Typography>
                <Typography paragraph>
                    At the start of the game, the Flag's level is "Glory". When a Flag is stolen, it's level resets to "Shamed". When a flag is defended, it’s level stays or becomes "Glory" again.
                </Typography>
                <Typography gutterBottom variant='h3'>Bring Flag home</Typography>
                <Typography paragraph>
                    The owners of the Flag are responsible for bringing its Flag back from Merchant to Castle until the start of next Season. If the Flag is lost or broken, it's possible to negotiate with GMs to make a makeshift flag.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_FLAG.Status} element={<Status />} />
                <Route path={SUBROUTE_FLAG.Logs} element={<Logs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_FLAG.Status} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
