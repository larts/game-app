import React from 'react'
import { useCallback } from 'react'

import { QueryTeamBase, QueryTeams, TEAM_COLOR } from '@larts/api'
import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'

import useMutationTeamLogin from 'src/api/useMutationTeamLogin'
import useMutationTeamLogout from 'src/api/useMutationTeamLogout'
import useMutationUserLogout from 'src/api/useMutationUserLogout'
import { createFC, getTheme, useQuery } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


const BorderlessRow = createFC('BorderlessRow')(function ({ children }) {
    return (
        // TODO: add row-level borders.
        <TableRow sx={{
            'td, th': { border: 0 },
        }}
        >{children}</TableRow>
    )
})


const contacts = [
    { role: 'Main Organizer (Game)', name: 'Aksels', tel: '+371 26471183' },
    { role: 'Merchant and Weapons', name: 'Elons', tel: '+371 27175063' },
    { role: 'Resource Camps', name: 'Andrievs', tel: '+371 24602727' },
    { role: 'Game App', name: 'Kalvis', tel: '+371 22118827' },
    { role: 'Registration and Game App', name: 'Rota', tel: '+371 22036914' },
]

export default createFC(import.meta.url)(function _({ children, theme }) {
    const [,, token] = useMutationTeamLogin()
    const queryTeam = useQuery(QueryTeamBase)
    const queryTeams = useQuery(QueryTeams)
    const [removeDevice] = useMutationTeamLogout()
    const [removeUser] = useMutationUserLogout()

    const removeDeviceAndUser = useCallback(async () => {
        await removeUser()
        await removeDevice()
    }, [])

    const teamId = queryTeam.data?.team?.id

    return (
        <div>
            <PageHeader title='Contacts' />
            <PageSection title='Game Masters'>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <BorderlessRow>
                                <TableCell>Role</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Tel.Nr.</TableCell>
                            </BorderlessRow>
                        </TableHead>
                        <TableBody>
                            {contacts.map((contact) => (
                                <BorderlessRow key={contact.name}>
                                    <TableCell>{contact.role}</TableCell>
                                    <TableCell>{contact.name}</TableCell>
                                    <TableCell><a href={`tel:${contact.tel}`}>{contact.tel}</a></TableCell>
                                </BorderlessRow>
                            ))}
                        </TableBody>
                    </Table>
                </PageSectionPart>
            </PageSection>
            <PageSection title='Factions'>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Team</TableCell>
                                <TableCell>Color</TableCell>
                                <TableCell>Castle</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {queryTeams.data?.teams.slice().sort((a, b) => b.title < a.title ? -1 : 1).map((team, i) => {
                                const sx = {
                                    backgroundColor: getTheme(team.color as TEAM_COLOR).palette.primary.light,
                                    fontWeight: (teamId && team.id === teamId) ? 'bold' : undefined,
                                }
                                return (
                                    <TableRow key={team.id}>
                                        <TableCell sx={sx}>{team.title}</TableCell>
                                        <TableCell sx={sx}>{team.color}</TableCell>
                                        <TableCell sx={sx}>{team.castle?.title}</TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </PageSectionPart>

            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Typography>
                        Your setup code: <strong>{token?.slice(0, 4) ?? '-'}</strong>
                        {' '}
                        {token && (
                            <>
                                (<button onClick={removeDeviceAndUser}>remove device</button>)
                            </>
                        )}
                    </Typography>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
