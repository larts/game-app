import React, { ChangeEvent, useCallback, useMemo, useState } from 'react'

import { QueryTeamResources, QueryUserName, RESOURCE_TYPE } from '@larts/api'
import { Button, Grid, InputAdornment, MenuItem, TextField, Typography } from '@material-ui/core'

import useMutationResourcesSell from 'src/api/useMutationResourcesSell'
import { createFC, useQuery } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import Resource from 'src/components/Resource'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryUser = useQuery(QueryUserName)

    const options = useMemo(() => {
        return queryTeamResources.data?.team?.resources
            .filter((res) => res.type.id !== RESOURCE_TYPE.Coins)
            .sort((a, b) => b.available - a.available)
            .map((res) => (
                <MenuItem key={res.type.id} value={res.type.id} disabled={!res.available}>
                    <Resource type={res.type.id} title sx={{ marginRight: theme.spacing(0.5) }} />({res.available})
                </MenuItem>
            ))
    }, [queryTeamResources])

    const [resourceId, setResourceId] = useState<RESOURCE_TYPE | null>(null)
    const handleResourceChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setResourceId(Number(event.target.value) as RESOURCE_TYPE), [setResourceId])

    const [amount, setAmount] = useState<null | number>(2)
    const handleAmountChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setAmount(event.target.value === '' ? null : Math.floor(Math.max(1, Number(event.target.value)))), [setAmount])

    const available = queryTeamResources.data?.team?.resources.find((res) => res.type.id === resourceId)?.available ?? 0
    const value = Math.floor((amount ?? 0) * (resourceId === RESOURCE_TYPE.Hides ? 4/2 : 3/2))

    const [resourceSell] = useMutationResourcesSell()
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const handleSell = useApiCallback(() => resourceSell(resourceId!, amount ?? 0), [resourceSell, resourceId, amount, value])

    if (queryTeamResources.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection>
                <PageCard variant='standard' color='neutral' title='Sell resource for coins:'>
                    <Typography component='ul'>
                        <li>2 Hides can be sold for 4 coins</li>
                        <li>Any other 2 resources can be sold for 3 coin</li>
                    </Typography>
                </PageCard>
            </PageSection>
            <PageSection title='Sell:'>
                <PageSectionPart>
                    <Grid container spacing={1}>
                        <Grid item xs={8}>
                            <TextField
                                size='small'
                                color='primary'
                                margin='dense'
                                label='Resource'
                                variant='outlined'
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={resourceId ?? ''}
                                onChange={handleResourceChange}
                                select
                                SelectProps={{
                                    renderValue: (_selected) => {
                                        const selected = _selected as RESOURCE_TYPE | undefined
                                        if (!selected) return <em>Select resource..</em>
                                        return <Resource type={selected} title />
                                    },
                                }}
                            >
                                {options}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                value={amount ?? ''}
                                onChange={handleAmountChange}
                                type='number'
                                size='small'
                                color='primary'
                                margin='dense'
                                label='Amount'
                                variant='outlined'
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                helperText={available ? `${available} available` : ' '}
                                error={!amount || amount % 2 === 1}
                                inputProps={{
                                    ...{ min: 2, step: 2 },
                                }}
                            />
                        </Grid>
                    </Grid>
                </PageSectionPart>
            </PageSection>
            <PageSection title='Receive:'>
                <PageSectionPart>
                    <TextField
                        value={value}
                        onChange={handleAmountChange}
                        size='small'
                        color='primary'
                        margin='dense'
                        label='Amount'
                        variant='outlined'
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            readOnly: true,
                            startAdornment: <InputAdornment position='start'><Resource type={RESOURCE_TYPE.Coins} /></InputAdornment>,
                        }}
                        helperText={`${queryTeamResources.data?.team?.resources.find((res) => res.type.id === RESOURCE_TYPE.Coins)?.available ?? 0} available`}
                    />
                </PageSectionPart>
                <PageSection>
                    <PageSectionPart>
                        <Button
                            variant='contained'
                            fullWidth
                            disabled={!queryUser.data?.user || resourceId === null || !amount || amount % 2 === 1}
                            onClick={handleSell}
                        >
                            Sell
                        </Button>
                    </PageSectionPart>
                </PageSection>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
