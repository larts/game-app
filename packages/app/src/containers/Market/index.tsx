import React from 'react'

import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { SUBROUTE_MARKET } from 'src/constants'

import UnderConstruction from '../UnderConstruction'

import Trade from './Trade'


const TABS = {
    [SUBROUTE_MARKET.Trade]: 'Local Trade',
    [SUBROUTE_MARKET.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Market'>
                <Typography gutterBottom variant='h3'>
                    Local Market
                </Typography>
                <Typography paragraph>
                    The Local market does the same job as Merchant only from the safety of your castle walls and <strong>twice as expensive.</strong>
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Merchant
                </Typography>
                <Typography  paragraph>
                    At approximately the center of the game territory there is a Merchant where you can trade with better prices in 2 ways:
                </Typography>
                <Typography paragraph>
                    <strong>Regional trade:</strong> Players can swap with Merchant any resource at 1:1 ratio with any other resource that's in Merchant's local stock. Merchant starting local stock is around 50 honey.
                </Typography>
                <Typography paragraph>
                    <strong>Foreign trade:</strong> Merchant also can trade infinite amount of resources from his far-away trade routes:
                    <ul>
                        <Typography component='li'>
                            Players can sell at Merchant any resource for 3 coins, or Hides for 4 coins.
                        </Typography>
                        <Typography component='li'>
                            Players can buy at Merchant any resource for 5 coins. All Food bought is Honey.
                        </Typography>
                    </ul>
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_MARKET.Trade} element={<Trade />} />
                <Route path={SUBROUTE_MARKET.Logs} element={<UnderConstruction />} />
                <Route path="*" element={<Navigate to={SUBROUTE_MARKET.Trade} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
