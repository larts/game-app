import React, { ReactNode, useCallback, useEffect } from 'react'

import { QueryTeamBase, TEAM_COLOR } from '@larts/api'
import { Button, Snackbar, ThemeProvider, Toolbar } from '@material-ui/core'
import { useLocation, useNavigate, useOutlet } from 'react-router-dom'

import { createFC, getTheme, useQuery } from 'src/common'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { ROUTE, makeRoute } from 'src/constants'
import { NotifyContext } from 'src/constants/context'

import AnnouncementBanner from './AnnouncementBanner'
import AssertNoEmergency from './AssertNoEmergency'
import ExtendSessionBanner from './ExtendSessionBanner'
import Header from './Header'

// eslint-disable-next-line import/no-unassigned-import
import './index.css'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const outlet = useOutlet()
    const navigate = useNavigate()
    const [snackbar, setSnackbar] = React.useState<ReactNode | null>(null)
    const handleCloseSnackbar = useCallback((event, reason?: string) => (reason !== 'clickaway') && setSnackbar(null), [setSnackbar])

    const queryTeamBase = useQuery(QueryTeamBase)

    // Reset scroll on top to navigation.
    const { pathname } = useLocation()
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [pathname])

    // Redirect 404s.
    useEffect(() => {
        if (!outlet) navigate(makeRoute(ROUTE.Home))
    }, [outlet])

    if (queryTeamBase.loading) return <ProgressFullscreen />

    return (
        <ThemeProvider theme={getTheme(queryTeamBase.data?.team?.color as TEAM_COLOR | undefined)}>
            <NotifyContext.Provider value={setSnackbar}>
                <AssertNoEmergency>
                    <Header />
                    <Toolbar />
                    <AnnouncementBanner />
                    <ExtendSessionBanner />
                    {outlet}
                </AssertNoEmergency>
            </NotifyContext.Provider>
            <Snackbar
                open={!!snackbar}
                onClose={handleCloseSnackbar}
                message={snackbar}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                sx={{ marginTop: theme.spacing(6) }}
                action={<Button onClick={handleCloseSnackbar}>Close</Button>}
            />
        </ThemeProvider>
    )
}) /* =============================================================================================================== */
