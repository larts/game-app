import React from 'react'

import { Button } from '@material-ui/core'

import { createFC, useNavigateToHref } from 'src/common'
import IconCastle from 'src/components/icons/IconCastle'
import { ROUTE, makeRoute } from 'src/constants'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()

    return (
        <Button
            href={makeRoute(ROUTE.Setup)}
            onClick={navigateToHref}
            variant='outlined'
            color='inherit'
            startIcon={<IconCastle />}
        >
            Setup
        </Button>
    )
}) /* =============================================================================================================== */
