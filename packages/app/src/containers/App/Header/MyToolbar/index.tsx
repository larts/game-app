import React from 'react'

import { QuerySession } from '@larts/api'
import { Toolbar } from '@material-ui/core'
import { Box } from '@material-ui/system'

import { createFC, useQuery } from 'src/common'

import ButtonLogin from './ButtonLogin'
import ButtonPlayer from './ButtonPlayer'
import ButtonSetup from './ButtonSetup'
import ButtonTeam from './ButtonTeam'


interface IProps {
    teamName?: string
    username?: string | null
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, teamName, username }) {
    const querySession = useQuery(QuerySession)

    return (
        <Toolbar
            sx={{ width: '100%', maxWidth: theme.breakpoints.values.sm }}
            disableGutters
        >
            <Box sx={{ flexGrow: 1, maxWidth: theme.spacing(1) }} />
            {teamName && <ButtonTeam teamName={teamName} />}
            {!teamName && <ButtonSetup />}
            <Box sx={{ flexGrow: 1 }} />
            {username && <ButtonPlayer username={username} />}
            {!username && (teamName || querySession.data?.baseData.isMarketDevice) && <ButtonLogin />}
            <Box sx={{ flexGrow: 1, maxWidth: theme.spacing(1) }} />
        </Toolbar>
    )
}) /* =============================================================================================================== */
