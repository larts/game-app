import React from 'react'

import { Button } from '@material-ui/core'

import { createFC, useNavigateToHref } from 'src/common'
import IconCastle from 'src/components/icons/IconCastle'
import { ROUTE, makeRoute } from 'src/constants'


interface IProps {
    teamName: string
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, teamName }) {
    const navigateToHref = useNavigateToHref()

    return (
        <Button
            href={makeRoute(ROUTE.Home)}
            onClick={navigateToHref}
            variant='contained'
            startIcon={<IconCastle />}
        >
            {teamName}
        </Button>
    )
}) /* =============================================================================================================== */
