import React, { MouseEvent, useCallback, useState } from 'react'

import { Button } from '@material-ui/core'

import useMutationUserLogin from 'src/api/useMutationUserLogin'
import { createFC, useNavigateToHref } from 'src/common'
import useNotify from 'src/common/useNotify'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import IconPlayer from 'src/components/icons/IconPlayer'
import { ROUTE, makeRoute } from 'src/constants'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const notify = useNotify()
    const [open, setOpen] = useState(false)
    const navigateToHref = useNavigateToHref()
    const closeScan = useCallback(() => setOpen(false), [setOpen])
    const openScan = useCallback((e: MouseEvent<HTMLAnchorElement>) => {
        navigateToHref(e)
        setOpen(true)
    }, [setOpen])

    const [loginUser] = useMutationUserLogin()
    const handleLoginUser = useCallback(async (qrCode: string) => {
        notify(`${qrCode} scanned..`)
        await loginUser(qrCode)
        sessionStorage.setItem('current-user', qrCode)
        setOpen(false)
    }, [loginUser])

    return (
        <>
            <Button
                href={makeRoute(ROUTE.Home)}
                onClick={openScan}
                variant='outlined'
                color='inherit'
                startIcon={<IconPlayer />}
            >
                Login
            </Button>

            <DialogScanQrCode open={open} onClose={closeScan} onScan={handleLoginUser} />
        </>
    )
}) /* =============================================================================================================== */
