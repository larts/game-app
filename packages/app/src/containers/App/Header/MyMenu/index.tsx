import React, { useCallback, useState } from 'react'

import { QuerySession, QueryTeamBase, QueryUserName } from '@larts/api'
import { AppBar, BottomNavigationActionProps, Divider, IconButton, List, ListSubheader, SwipeableDrawer, Toolbar } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'
import IconMenu from '@material-ui/icons/Menu'

import useMutationUserLogout from 'src/api/useMutationUserLogout'
import { IMyTheme, createFC, createStyles, useQuery } from 'src/common'
import { ROUTE, makeRoute } from 'src/constants'

import MyMenuListItem from './MyMenuListItem'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        '&.MuiIconButton-root': {
            color: theme.palette.primary.contrastText,
            width: 52,
        },
    },
    drawerPaper: {
        '&.MuiPaper-root': {
            backgroundColor: theme.palette.primary.dark,
            height: '100%',
            overflowX: 'hidden',
        },
    },
})


interface IProps extends BottomNavigationActionProps {
}


export default createFC(import.meta.url, styles)<IProps>(function _({ children, theme, classes, ...props }) {
    const queryUserName = useQuery(QueryUserName)
    const queryTeamBase = useQuery(QueryTeamBase)
    const querySession = useQuery(QuerySession)

    const [open, setOpen] = useState(false)

    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])

    const [userLogout] = useMutationUserLogout()
    const handleLogout = useCallback(async () => {
        await userLogout()
        handleClose()
    }, [userLogout, handleClose])

    return (
        <>

            <IconButton
                onClick={handleOpen}
                className={classes.root}
                size="small"
            >
                <IconMenu color='inherit' />
            </IconButton>
            <SwipeableDrawer
                open={open}
                onClose={handleClose}
                onOpen={() => null}
                classes={{ paper: classes.drawerPaper }}
            >
                <AppBar
                    position='static'
                    sx={{
                        backgroundColor: 'primary.dark',
                        boxSizing: 'content-box',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        height: 52,
                    }}
                >
                    <Toolbar>
                        <IconButton
                            onClick={handleClose}
                            size='medium'
                            sx={{ color: 'primary.contrastText' }}
                        >
                            <IconClose />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <List sx={{ color: 'primary.contrastText', minWidth: 300 }}>
                    {queryTeamBase.data?.team && (
                        <>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Home)} title='Home' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Resources)} title='Resources' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Leaderboard)} title='Leaderboard' />

                            <Divider />
                            <ListSubheader>Military</ListSubheader>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Flag)} title='Flag' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.EquipmentPoints)} title='Equipment Points' />

                            <Divider />
                            <ListSubheader>Industry</ListSubheader>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Workshops)} title='Workshops' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Upgrades)} title='Camp upgrades' />

                            <Divider />
                            <ListSubheader>Agriculture</ListSubheader>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Recipes)} title='Recipes' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Feasts)} title='Feast' />

                            <Divider />
                            <ListSubheader>External</ListSubheader>
                            {/* <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Diplomacy)} title='Diplomacy' /> */}
                            {/* <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Mercenaries)} title='Mercenaries' /> */}
                            {/* <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Events)} title='Events' /> */}
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Market)} title='Market' />
                            <Divider />
                        </>
                    )}
                    {(querySession.data?.baseData.isMarketDevice) && (
                        <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Flag)} title='Flag' />
                    )}
                    {queryUserName.data?.user && (
                        <>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Profile)} title='Profile' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Respawn)} title='Respawn' />
                            <MyMenuListItem onClick={handleLogout} href={makeRoute(ROUTE.Home)} title='Log out' />
                            <Divider />
                        </>
                    )}
                    {!queryTeamBase.data?.team && (
                        <>
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Public)} title='Public' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Recipes)} title='Recipes' />
                            <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Setup)} title='Setup' />
                        </>
                    )}
                    <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Contacts)} title='Contacts' />
                    {/* <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Logs)} title='News (logs)' /> */}
                    {/* <MyMenuListItem onClick={handleClose} href={makeRoute(ROUTE.Respawn)} title='FAQ' /> */}
                </List>
            </SwipeableDrawer>
        </>
    )
}) /* =============================================================================================================== */
