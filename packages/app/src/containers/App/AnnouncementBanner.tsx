import React from 'react'

import { CONFIG } from '@larts/api'
import { Toolbar, Typography } from '@material-ui/core'
import IconInfo from '@material-ui/icons/Info'

import { createFC } from 'src/common'
import useConfig from 'src/common/useConfig'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const announcement = useConfig(CONFIG.Announcement)

    if (!announcement || announcement === '') return null

    return (
        <Toolbar disableGutters={false} sx={{ backgroundColor: theme.palette.primary.dark, color: theme.palette.primary.contrastText }}>
            <IconInfo sx={{ marginRight: '.5em' }} />
            <Typography variant='caption'>
                {announcement}
            </Typography>
        </Toolbar>
    )

}) /* =============================================================================================================== */
