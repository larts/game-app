import React, { MouseEvent, useCallback, useEffect, useState } from 'react'

import { QuerySession } from '@larts/api'
import { Button, Grid, Toolbar, Typography } from '@material-ui/core'
import IconWarning from '@material-ui/icons/Warning'

import useMutationUserLogin from 'src/api/useMutationUserLogin'
import useMutationUserLogout from 'src/api/useMutationUserLogout'
import { createFC, useQuery } from 'src/common'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const querySession = useQuery(QuerySession)
    const session = querySession.data?.baseData.userSessionEnd
    const userId = querySession.data?.user?.id

    const [remaining, setRemaining] = useState<number | null>(null)

    const [loginUser] = useMutationUserLogin()
    const handleLoginUser = useCallback(async (qrCode: string) => {
        await loginUser(qrCode)
        sessionStorage.setItem('current-user', qrCode)
        setRemaining(null)
    }, [loginUser])
    const handleExtend = useCallback((e: MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
        e.preventDefault()
        const lastQrCode = sessionStorage.getItem('current-user')
        if (lastQrCode) handleLoginUser(lastQrCode)
    }, [handleLoginUser])

    const [logoutUser] = useMutationUserLogout()
    useEffect(() => {
        if (!userId || !session) return

        const interval = setInterval(() => {
            const remainingSecondsNew = session - Date.now() / 1000 - 3

            if (remainingSecondsNew < 0) {
                setRemaining(null)
                logoutUser()
            } else if (remainingSecondsNew < 30) {
                setRemaining(Math.ceil(remainingSecondsNew))
            } else {
                setRemaining(null)
            }
        }, 1000)
        return () => clearInterval(interval)
    }, [session, userId, setRemaining, logoutUser])

    // auto-login on dev
    // if (process.env.NODE_ENV === 'development') {
    //     useAsync(async () => {
    //         if (!!querySession.loading || !!userId) return
    //         await sleep(3)
    //         await handleLoginUser('PL-25-3K')
    //     }, [querySession.loading, userId, handleLoginUser])
    // }

    if (remaining === null) return null

    return (
        <Toolbar disableGutters={false} sx={{ backgroundColor: theme.palette.primary.dark, color: theme.palette.primary.contrastText }}>
            <IconWarning sx={{ marginRight: '.5em' }} />
            <Typography variant='caption'>
                Session will expire in {remaining} seconds.
            </Typography>
            <Grid item xs />
            <Button size='small' variant='text' onClick={handleExtend} sx={{ color: theme.palette.primary.contrastText }}>Extend</Button>
        </Toolbar>
    )

}) /* =============================================================================================================== */
