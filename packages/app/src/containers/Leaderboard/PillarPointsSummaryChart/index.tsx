import React from 'react'

import { PILLAR, QueryScorePillarSummary, QueryTeamBase } from '@larts/api'
import { CircularProgress, Typography } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'

import HorizontalBarChart from './HorizontalBarChart'


interface IProps {
    pillarId: PILLAR
    seasonId: number
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, seasonId, pillarId }) {
    const queryTeam = useQuery(QueryTeamBase)
    const queryPillar = useQuery(QueryScorePillarSummary, { variables: { pillarId } })

    const teamId = queryTeam.data?.team?.id
    const pillarOfSeason = queryPillar.data?.pillar?.allSeasons?.find(({ season }) => season.id === seasonId)

    const dataSorted = pillarOfSeason?.totalScores?.slice().sort((a, b) => a.rank - b.rank) ?? []
    const myScore = dataSorted.find((score) => score.team.id === teamId)

    if (!pillarOfSeason) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />

    return (
        <PageSectionPart>
            {myScore && (
                <Typography gutterBottom>
                    Your place: {myScore.rank} ({myScore?.score} points)
                </Typography>
            )}
            <Typography gutterBottom>
                Pillar is {Math.round((pillarOfSeason?.currentTotal ?? 0) / (pillarOfSeason?.target ?? 1) * 100)}% full ({pillarOfSeason?.currentTotal}/{pillarOfSeason?.target} points)
            </Typography>
            <HorizontalBarChart
                dataSorted={dataSorted}
                teamId={teamId}
                target={pillarOfSeason.target}
            />
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
