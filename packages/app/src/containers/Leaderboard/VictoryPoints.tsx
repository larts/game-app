import React, { useMemo } from 'react'

import { QueryScoreVictorySummary, TEAM_ID } from '@larts/api'
import { recordValues } from '@larts/common'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'

import MultiLineChart from './MultiLineChart'
import VictoryPointsSummaryChart from './VictoryPointsSummaryChart'
import VictoryPointsSummaryTable from './VictoryPointsSummaryTable'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const vpSummary = useQuery(QueryScoreVictorySummary)

    const tabularData = useMemo(() => {
        if (vpSummary.loading) return []
        const seasons = {} as Record<number, {elapsed: number} & Record<TEAM_ID, number>>
        for (const datum of (vpSummary.data?.victoryPoints?.points ?? [])) {
            const id = datum.season.id
            if (!seasons[id]) seasons[id] = { elapsed: id } as unknown as {elapsed: number} & Record<TEAM_ID, number>
            seasons[id][datum.team.id] = (seasons[id][datum.team.id] ?? 0) + datum.reason.amount
        }
        return recordValues(seasons)
    }, [vpSummary.data])

    return (
        <div>
            <PageSection color='paper' title='At this moment'>
                <VictoryPointsSummaryChart />
                <VictoryPointsSummaryTable />
            </PageSection>
            <PageSection color='paper' title='During the game'>
                <MultiLineChart
                    historyData={tabularData}
                    ticks={[1, 2, 3, 4]}
                />
                <PageSectionPart>
                </PageSectionPart>
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
