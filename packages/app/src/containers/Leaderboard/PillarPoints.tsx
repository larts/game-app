import React, { ChangeEvent, useState } from 'react'
import { useCallback } from 'react'
import { useEffect } from 'react'

import { PILLAR, PILLAR_REAL, QuerySeason } from '@larts/api'
import { MenuItem, TextField } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'

import PageSectionPart from '../../components/PageSectionPart'

import PillarPointsCumulativeTimeseries from './PillarPointsCumulativeTimeseries'
import PillarPointsSummaryChart from './PillarPointsSummaryChart'
import PillarPointsSummaryTable from './PillarPointsSummaryTable'
import PillarPointsTeamTimeseries from './PillarPointsTeamTimeseries'


interface IProps {
    id: PILLAR_REAL
}

export default createFC(import.meta.url)<IProps>(function _({ children, theme, id }) {
    const querySeason = useQuery(QuerySeason, { fetchPolicy: 'cache-and-network' })

    const [seasonId, setSeasonId] = useState(1)
    const handleChange = useCallback((event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setSeasonId(Number(event.target.value)), [setSeasonId])

    useEffect(() => {
        if (!querySeason.data?.season?.id) return

        setSeasonId(querySeason.data?.season?.id)
    }, [querySeason.data?.season?.id])

    if (!querySeason.data?.season?.id) return null

    return (
        <div>
            <PageSection>
                <PageSectionPart>
                    <TextField select value={String(seasonId)} onChange={handleChange}>
                        {/* TODO: only UP TO current season dynamically */}
                        {[1, 2, 3, 4, 5, 6].map((seasonId) => (
                            <MenuItem key={seasonId} value={seasonId}>Season {seasonId}</MenuItem>
                        ))}
                    </TextField>
                </PageSectionPart>
            </PageSection>
            <PageSection color='paper' title='At this moment'>
                <PillarPointsSummaryChart seasonId={seasonId} pillarId={id as any as PILLAR} />
                <PillarPointsSummaryTable seasonId={seasonId} pillarId={id as any as PILLAR} />
            </PageSection>
            <PageSection color='paper' title="Faction Points this season">
                <PillarPointsTeamTimeseries seasonId={seasonId} pillarId={id} />
            </PageSection>
            <PageSection color='paper' title="All Points this season">
                <PillarPointsCumulativeTimeseries seasonId={seasonId} pillarId={id} />
            </PageSection>
        </div>
    )
}) /* =============================================================================================================== */
