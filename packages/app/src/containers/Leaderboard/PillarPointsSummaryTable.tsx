import React from 'react'

import { PILLAR, QueryScorePillarSummary, QueryTeamBase, QueryTeams, TEAM_COLOR } from '@larts/api'
import { CircularProgress, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'

import { createFC, getTheme, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'


interface IProps {
    pillarId: PILLAR
    seasonId: number
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, seasonId, pillarId }) {
    const queryTeam = useQuery(QueryTeamBase)
    const queryTeams = useQuery(QueryTeams)
    const queryPillar = useQuery(QueryScorePillarSummary, { variables: { pillarId } })
    const pillarOfSeason = queryPillar.data?.pillar?.allSeasons?.find(({ season }) => season.id === seasonId)

    if (queryTeam.loading || queryTeams.loading || queryPillar.loading) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />
    if (!pillarOfSeason) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />

    return (
        <PageSectionPart>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Place</TableCell>
                        <TableCell>Points</TableCell>
                        <TableCell>Team</TableCell>
                        <TableCell>Color</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {pillarOfSeason.totalScores.slice().sort((a, b) => a.rank - b.rank).map(({ rank, score, team }) => (
                        <TableRow key={team.id}>
                            <TableCell sx={{ fontWeight: team.id === queryTeam.data?.team?.id ? 'bold' : undefined }}>{rank}</TableCell>
                            <TableCell sx={{ fontWeight: team.id === queryTeam.data?.team?.id ? 'bold' : undefined }}>{score}</TableCell>
                            <TableCell sx={{ fontWeight: team.id === queryTeam.data?.team?.id ? 'bold' : undefined }}>{team.title}</TableCell>
                            <TableCell sx={{ backgroundColor: getTheme(team.color as TEAM_COLOR).palette.primary.main, color: theme.palette.primary.contrastText }}></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
