import React from 'react'

import { PILLAR, PILLAR_REAL, QueryScorePillarsBase } from '@larts/api'
import { Button, Typography } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { SUBROUTE_LEADERBOARD } from 'src/constants'

import PillarPointsSummaryChart from './PillarPointsSummaryChart'
import VictoryPointsSummaryChart from './VictoryPointsSummaryChart'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const navigateToHref = useNavigateToHref()
    const queryPillars = useQuery(QueryScorePillarsBase)

    if (queryPillars.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageSection>
                <PageSectionPart>
                    <Typography color='textSecondary'>
                        Stats about points in pillars this season and victory points overall
                    </Typography>
                </PageSectionPart>
            </PageSection>
            <PageSection
                color='paper'
                title='Victory'
                action={<Button href={SUBROUTE_LEADERBOARD.Victory} onClick={navigateToHref}>Details</Button>}
            >
                <PageSectionPart>
                    <VictoryPointsSummaryChart
                        subroute={SUBROUTE_LEADERBOARD.Victory}
                    />
                </PageSectionPart>
            </PageSection>
            {queryPillars.data?.pillars.filter(({ id }) => id in PILLAR_REAL).map((pillar) => (
                <PageSection
                    key={pillar.id}
                    color='paper'
                    title={pillar.title}
                    action={<Button href={PILLAR_ID_TO_SUBROUTE_LEADERBOARD[pillar.id]} onClick={navigateToHref}>Details</Button>}
                >
                    <PageSectionPart>
                        <PillarPointsSummaryChart seasonId={0} pillarId={pillar.id as any as PILLAR} />
                    </PageSectionPart>
                </PageSection>
            ))}
        </div>
    )
}) /* =============================================================================================================== */

const PILLAR_ID_TO_SUBROUTE_LEADERBOARD = {
    [PILLAR_REAL.Military]: SUBROUTE_LEADERBOARD.Military,
    [PILLAR_REAL.Agriculture]: SUBROUTE_LEADERBOARD.Agriculture,
    [PILLAR_REAL.Industry]: SUBROUTE_LEADERBOARD.Industry,
}
