import React from 'react'

import { QueryScoreVictorySummary, QueryTeamBase, TEAM_COLOR } from '@larts/api'
import { CircularProgress, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'

import { createFC, getTheme, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const queryTeam = useQuery(QueryTeamBase)
    const queryVictoryPoints = useQuery(QueryScoreVictorySummary)

    const teamId = queryTeam.data?.team?.id
    const pillar = queryVictoryPoints.data?.victoryPoints

    const dataSorted = pillar?.totals?.slice().sort((a, b) => b.points - a.points) ?? []
    const myScore = dataSorted.find((score) => score.team.id === teamId)

    if (!pillar) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />

    return (
        <PageSectionPart>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Place</TableCell>
                        <TableCell>Points</TableCell>
                        <TableCell>Team</TableCell>
                        <TableCell>Color</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {dataSorted.slice().sort((a, b) => b.points - a.points).map(({ points, team }, i) => (
                        <TableRow key={team.id}>
                            <TableCell sx={{ fontWeight: (teamId && team.id === teamId) ? 'bold' : undefined }}>{i + 1}</TableCell>
                            <TableCell sx={{ fontWeight: (teamId && team.id === teamId) ? 'bold' : undefined }}>{points}</TableCell>
                            <TableCell sx={{ fontWeight: (teamId && team.id === teamId) ? 'bold' : undefined }}>{team.title}</TableCell>
                            <TableCell sx={{ backgroundColor: getTheme(team.color as TEAM_COLOR).palette.primary.main, color: theme.palette.primary.contrastText }}></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
