import React from 'react'

import { QueryScoreVictorySummary, QueryTeamBase } from '@larts/api'
import { Button, CardActions, CircularProgress, Typography } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'
import { ROUTE, SUBROUTE_LEADERBOARD, makeRoute } from 'src/constants'

import VerticalBarChart from './VerticalBarChart'


interface IProps {
    subroute?: SUBROUTE_LEADERBOARD.Victory
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, subroute }) {
    const navigateToHref = useNavigateToHref()
    const queryTeam = useQuery(QueryTeamBase)
    const queryVictoryPoints = useQuery(QueryScoreVictorySummary)

    const teamId = queryTeam.data?.team?.id
    const pillar = queryVictoryPoints.data?.victoryPoints

    const dataSorted = pillar?.totals?.slice().sort((a, b) => b.points - a.points) ?? []
    const myScore = dataSorted.find((score) => score.team.id === teamId)

    if (!pillar) return <CircularProgress sx={{ display: 'block', margin: 'auto' }} />

    return (
        <PageSectionPart>
            {myScore && (
                <Typography gutterBottom>
                    Until this moment you have gained {myScore.points} victory points in this game
                </Typography>
            )}
            {myScore && (
                <Typography gutterBottom>
                    Your place: {dataSorted.findIndex(({ team: { id } }) => id === teamId) + 1}
                </Typography>
            )}
            <VerticalBarChart
                dataSorted={dataSorted}
                teamId={teamId}
                target={0}
            />
            {subroute && (
                <CardActions sx={{ justifyContent: 'flex-end' }}>
                    <Button href={makeRoute(ROUTE.Leaderboard, subroute)} onClick={navigateToHref}>
                        Historical Victory Data →
                    </Button>
                </CardActions>
            )}
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
