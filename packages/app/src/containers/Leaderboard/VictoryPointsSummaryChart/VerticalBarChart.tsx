import React from 'react'

import { QueryScoreVictorySummary, TEAM_COLOR, TEAM_ID } from '@larts/api'
import { Bar, BarChart, Cell, ResponsiveContainer, XAxis, YAxis } from 'recharts'

import { createFC, getTheme } from 'src/common'


interface IProps {
    dataSorted: NonNullable<QueryScoreVictorySummary.Response['victoryPoints']>['totals']
    teamId?: TEAM_ID
    target: number
}


const height = 100

export default createFC(import.meta.url)<IProps>(function _({ children, theme, dataSorted, teamId, target }) {
    const { fontWeight: _, ...body2exceptFontWeight } = theme.typography.body2

    return (
        <ResponsiveContainer width='100%' height={height}>
            <BarChart
                data={dataSorted}
                margin={{ bottom: 0, left: 0, right: 0, top: 24 }}
                barSize={height}
            >
                <YAxis hide type="number" domain={[0, target]} />
                <XAxis
                    type="category"
                    dataKey="id"
                    interval={0}
                    minTickGap={0}
                    tick={{
                        ...theme.typography.caption,
                    } as any}
                />
                <Bar
                    dataKey="points"
                    label={{
                        // valueAccessor: (props: typeof dataMap) => props[id],
                        // fontWeight: 'bold',
                        position: 'top',
                        fill: theme.palette.text.primary,
                        ...body2exceptFontWeight,
                    } as any}
                >
                    {dataSorted.map(({ team }) => (
                        <Cell
                            key={team.id}
                            fill={getTheme(team.color as TEAM_COLOR).palette.primary[(teamId && team.id === teamId) ? 'main' : 'light']}
                            fontWeight={(teamId && team.id === teamId) ? 'bold' : undefined}
                        />
                    ))}
                </Bar>
            </BarChart>
        </ResponsiveContainer>
    )
}) /* =============================================================================================================== */
