import React from 'react'

import { PILLAR_REAL } from '@larts/api'
import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'
import { Link } from 'react-router-dom'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { ROUTE, SUBROUTE_LEADERBOARD, makeRoute } from 'src/constants'

import PillarPoints from './PillarPoints'
import Summary from './Summary'
import VictoryPoints from './VictoryPoints'


const TABS = {
    [SUBROUTE_LEADERBOARD.Summary]: 'Summary',
    [SUBROUTE_LEADERBOARD.Victory]: 'Victory',
    [SUBROUTE_LEADERBOARD.Industry]: 'Industry',
    [SUBROUTE_LEADERBOARD.Agriculture]: 'Agriculture',
    [SUBROUTE_LEADERBOARD.Military]: 'Military',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Leaderboard'>
                <Typography gutterBottom variant='h3'>
                    Get the most VP to win
                </Typography>
                <Typography paragraph>
                    <strong>The Faction with the most Victory Points (VP) wins the game</strong>. Every season collect points in each of three pillars - Military, Industry, Agriculture. Get as high a score as possible and fight for objectives (not kills) in the final battle.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    How to get pillar points
                </Typography>
                <Typography paragraph>
                    <strong>Military points</strong> (MP) are gained by:
                </Typography>
                <ul>
                    <Typography component='li'>
                        killing enemies <Link to={makeRoute(ROUTE.Resources)}>Tributes</Link>
                    </Typography>
                    <Typography component='li'>
                        defending your or capturing other’s flag. Every season collect points in each of three pillars - Military, Industry, Agriculture. Get as high a score as possible and fight for objectives (not kills) in the final battle.
                    </Typography>
                </ul>
                <Typography paragraph>
                    <strong>Industry points</strong> (IP) are gained by:
                </Typography>
                <ul>
                    <Typography component='li'>
                        using <Link to={makeRoute(ROUTE.Workshops)}>Workshops</Link>
                    </Typography>
                    <Typography component='li'>
                        <Link to={makeRoute(ROUTE.Upgrades)}>Upgrading</Link> resource camps
                    </Typography>
                </ul>
                <Typography paragraph>
                    <strong>Agriculture points</strong> (AP) are gained by:
                </Typography>
                <ul>
                    <Typography component='li'>
                        making <Link to={makeRoute(ROUTE.Recipes)}>Recipes</Link>
                    </Typography>
                    <Typography component='li'>
                        by hosting and being a guest in <Link to={makeRoute(ROUTE.Feasts)}>Feasts</Link>
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Season ends when 2/3 pillars are full
                </Typography>
                <Typography paragraph>
                    Season ends when 2 out of 3 pillars have reached their target points. Points in each pillar are summed together from all Factions.
                </Typography>
                <Typography paragraph>
                    There are constraints: min time in a Season is 60 min, max time - 100 min.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    VP are calculated at the end of Season
                </Typography>
                <Typography paragraph>
                    Victory points are calculated only when the Season has finished. So, even if 1 pillar is full, you can still change your place in it.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Place in pillar determines VP
                </Typography>
                <Typography paragraph>
                    The higher place in the pillar, the more Victory Points a Faction will get from the pillar. If points in pillar are equal, the first who got them is in higher place.
                </Typography>
                <Typography paragraph>
                    Within each Pillar, the VP are awarded as:
                </Typography>
                <ul>
                    <Typography component='li'>
                        10 VP to the 1st place
                    </Typography>
                    <Typography component='li'>
                        9 VP to the 2nd place
                    </Typography>
                    <Typography component='li'>
                        ...
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Difference in points is carried over
                </Typography>
                <Typography paragraph>
                    After the Season has ended, all pillar points are reset. The difference in points between your and and the team below you is brought over to the next Season.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Aim for 6+ points in each pillar
                </Typography>
                <Typography paragraph>
                    If at the end of a Season you have less than 6 points in a pillar, you will have 1 VP less than normally awarded to your place in pillar.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_LEADERBOARD.Summary} element={<Summary />} />
                <Route path={SUBROUTE_LEADERBOARD.Victory} element={<VictoryPoints />} />
                <Route path={SUBROUTE_LEADERBOARD.Industry} element={<PillarPoints id={PILLAR_REAL.Industry} />} />
                <Route path={SUBROUTE_LEADERBOARD.Agriculture} element={<PillarPoints id={PILLAR_REAL.Agriculture} />} />
                <Route path={SUBROUTE_LEADERBOARD.Military} element={<PillarPoints id={PILLAR_REAL.Military} />} />
                <Route path="*" element={<Navigate to={SUBROUTE_LEADERBOARD.Summary} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
