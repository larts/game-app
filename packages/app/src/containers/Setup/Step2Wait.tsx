import React, { useContext } from 'react'

import { Button, Grid, Typography } from '@material-ui/core'


import useMutationTeamLogout from 'src/api/useMutationTeamLogout'
import { createFC } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import { NotifyContext } from 'src/constants/context'


interface IProps {
    token: string
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, token }) {
    const notify = useContext(NotifyContext)
    const [removeDevice] = useMutationTeamLogout()

    const handleRemove = useApiCallback(async () => {
        await removeDevice()
        notify('🎉 Device access request cancelled.')
    }, [removeDevice, notify])

    return (
        <>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Typography paragraph>
                            To activate Throne Game App on this device, call Game Masters and repeat the setup code:
                        </Typography>
                        <Typography align='center' fontSize='5em'>
                            {token.slice(0, 4)}
                        </Typography>
                    </PageSectionPart>
                </PageSection>
            </Grid>
            <Grid item flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Button variant='contained' fullWidth href='tel:+37129650836'>
                            Call GM Aksels
                        </Button>
                    </PageSectionPart>
                    <PageSectionPart>
                        <Button fullWidth onClick={handleRemove}>
                            Re-request access
                        </Button>
                    </PageSectionPart>
                </PageSection>
            </Grid>
        </>
    )
}) /* =============================================================================================================== */
