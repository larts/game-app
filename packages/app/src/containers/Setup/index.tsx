import React, { useEffect } from 'react'

import { QueryTeamBase } from '@larts/api'
import { Alert, AlertTitle, Grid } from '@material-ui/core'

import logo from 'assets/logo.svg'

import useMutationTeamLogin from 'src/api/useMutationTeamLogin'
import { createFC, useQuery } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import Step1Request from './Step1Request'
import Step2Wait from './Step2Wait'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme }) {
    const [registerDevice,, token] = useMutationTeamLogin()

    const queryTeamBase = useQuery(QueryTeamBase)

    useEffect(() => {
        if (queryTeamBase.loading) return

        if (queryTeamBase.data?.team) {
            queryTeamBase.stopPolling()
        } else {
            queryTeamBase.startPolling(5000)
        }
    }, [queryTeamBase.loading, queryTeamBase.data])


    return (

        <Grid
            container
            sx={{ height: 'calc(100% - 52px)', backgroundImage: `url(${logo})`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat', backgroundPosition: 'center' }}
            direction='column'
            justifyContent='space-between'
            wrap='nowrap'
        >
            <Grid item flexBasis={0}>
                <PageHeader title='Setup Device' />
            </Grid>
            {queryTeamBase.loading && <ProgressFullscreen />}
            {!queryTeamBase.loading && !queryTeamBase.data?.team && token && <Step2Wait token={token} />}
            {!queryTeamBase.loading && !queryTeamBase.data?.team && !token && <Step1Request register={registerDevice} />}
            {(queryTeamBase.error) && (
                <Grid item flexBasis={0}>
                    <PageSection>
                        <PageSectionPart>
                            <Alert severity='error'>
                                <AlertTitle>{queryTeamBase.error?.name}</AlertTitle>
                                {queryTeamBase.error?.message}
                            </Alert>
                        </PageSectionPart>
                    </PageSection>
                </Grid>
            )}

        </Grid>
    )

}) /* =============================================================================================================== */
