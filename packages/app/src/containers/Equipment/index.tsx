import React, { useCallback } from 'react'
import { useMemo } from 'react'

import { QuerySeason, QuerySettings, QueryTeamEquipment, QueryTeamResources, QueryUserEquipment, RESOURCE_TYPE, equipmentPrice } from '@larts/api'
import { Button, Chip, Grid, Link, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'

import useMutationEquipmentBuy from 'src/api/useMutationEquipmentBuy'
import { createFC, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageHeader from 'src/components/PageHeader'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import Resource from 'src/components/Resource'

import AvailableResources from '../../components/AvailableResources'

import { pricePhoenix } from './common'
import EpPricelist from './EpPricelist'
import PlayerEp from './PlayerEp'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const [dialog, setDialog] = React.useState(false)
    const querySeason = useQuery(QuerySeason, { fetchPolicy: 'cache-and-network' })

    const querySettings = useQuery(QuerySettings)
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryTeamEP = useQuery(QueryTeamEquipment)
    const queryMyEP = useQuery(QueryUserEquipment)

    const prices = querySeason.data?.season?.id ? equipmentPrice(querySeason.data?.season?.id) : undefined

    const [handleBuy] = useMutationEquipmentBuy()

    const handleOpenDialog = useCallback((e: React.MouseEvent<HTMLAnchorElement>) => {e.preventDefault(); setDialog(true)}, [setDialog])
    const handleCloseDialog = useCallback(() => setDialog(false), [setDialog])

    const playersSorted = useMemo(() =>
        queryTeamEP.data?.team?.players?.slice()
            .sort((a, b) => (a.name || '') > (b.name || '') ? 1 : -1)
            .sort((a, b) => Number(b.equipmentHasPhoenix) - Number(a.equipmentHasPhoenix))
            .sort((a, b) => b.equipmentPoints - a.equipmentPoints)
    , [queryTeamEP.data?.team?.players])

    if (querySettings.loading) return <ProgressFullscreen />
    if (queryTeamResources.loading) return <ProgressFullscreen />
    if (queryTeamEP.loading) return <ProgressFullscreen />
    if (queryMyEP.loading) return <ProgressFullscreen />

    return (
        <div>
            <PageHeader title='Equipment Points'>
                <Typography gutterBottom variant='h3'>
                    Carry basic equipment for free
                </Typography>
                <Typography paragraph>
                    Every Player is allowed to carry one short (Arming) sword or a dagger without any costs. To equip additional weapons, wear armor or use some of the larger weapons, Equipment Points (EP) are needed.
                </Typography>
                <Typography paragraph>
                    The price of EPs (Iron) becomes cheaper each Season.
                </Typography>
                <Typography paragraph>
                    <strong>If a Player dies during the game, their EPs stay.</strong>
                </Typography>
                <Typography paragraph>
                    <Link onClick={handleOpenDialog}>what equipment can I afford to wear?</Link>
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Use only equipment you have selected from the stash
                </Typography>
                <Typography paragraph>
                    A Player is always bound to the items that they have selected from the stash - if they have a dagger or arrows, using one as a ranged attack does not allow the Player to pick up another from the weapon stash or another Player. Instead, they must reclaim the used item. Faction mates can help in reclaiming these items, but not use them themselves. A Player can reclaim their personal items while dead. It is allowed to suicide during a Siege to reclaim your arrows or daggers.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Swap equipment in your Castle
                </Typography>
                <Typography paragraph>
                    While in the Castle, a Player can change what Equipment they are carrying, as long as their EP allows them to wield the chosen Equipment. To change Equipment, use the weapon stash or swap items with Faction mates. It is possible to do this also as a Defender while the Castle is under attack.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Equipment is not lootable
                </Typography>
                <Typography paragraph>
                    Equipment is not lootable and it is not allowed to touch or take other Players' equipment even if you kill them. Each Player is responsible for the weapons and items they are carrying - they must make sure to not lose their weapon, armor or arrows. In the case of being killed, the Player takes all of their equipment with them back to the Castle.
                </Typography>
                <Typography gutterBottom variant='h3'>
                    Take found equipment to GM
                </Typography>
                <Typography paragraph>
                    If any equipment items are found unattended during the game, please take them to GM to be reclaimed by the Player who lost them later on.
                </Typography>
            </PageHeader>
            <PlayerEp />
            <PageSection title='Faction Equipment'>
                <AvailableResources
                    threshold={{
                        [RESOURCE_TYPE.Coins]: 0,
                        [RESOURCE_TYPE.Iron]: 0,
                    }}
                />
                <PageCard
                    title={`Unused EP: ${queryTeamEP.data?.team?.availableEq}`}
                    action={<Button variant='contained' onClick={handleBuy} disabled={!queryMyEP?.data?.user}>Buy EP</Button>}
                >
                    <Typography paragraph>
                        Total Faction EP: {(queryTeamEP.data?.team?.availableEq ?? 0) + (queryTeamEP.data?.team?.players?.reduce((sum, { equipmentPoints }) => sum + equipmentPoints, 0) ?? 0)}
                    </Typography>
                    <Grid container spacing={1}>
                        {prices?.map((res) => (
                            <Grid item key={res.type}>
                                <Chip label={<Resource type={res.type} amount={res.amount} />} />
                            </Grid>
                        ))}
                    </Grid>
                </PageCard>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Player</TableCell>
                                <TableCell>Equipped EP</TableCell>
                                <TableCell>Amulet of Phoenix</TableCell>
                                <TableCell>"Physical" EP</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {playersSorted?.map(({ name, equipmentPoints, equipmentHasPhoenix }) => (
                                <TableRow key={name}>
                                    <TableCell>{name}</TableCell>
                                    <TableCell>{equipmentPoints || '-'}</TableCell>
                                    <TableCell>{equipmentHasPhoenix ? 'Yes' : '-'}</TableCell>
                                    <TableCell>{equipmentPoints - (equipmentHasPhoenix ? pricePhoenix(equipmentPoints) : 0) || '-'}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </PageSectionPart>
            </PageSection>
            <EpPricelist open={dialog} ep={queryMyEP?.data?.user?.equipmentPoints ?? 0} onClose={handleCloseDialog} />
        </div>
    )
}) /* =============================================================================================================== */

