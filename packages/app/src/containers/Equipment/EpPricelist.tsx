import React from 'react'

import { Dialog, DialogContent, Grid, IconButton, Link, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'
import { DialogTitle } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'

import { createFC } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


interface Props {
    open: boolean
    ep: number
    onClose: () => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, open, ep, onClose }) {
    return (
        <Dialog open={open} fullScreen onClose={onClose}>
            <Grid container justifyContent='space-between' alignItems='flex-start'>
                <DialogTitle>What can I afford?</DialogTitle>
                <IconButton onClick={onClose}><IconClose /></IconButton>
            </Grid>
            <DialogContent>
                <PageSection>
                    <PageCard variant='standard' color='neutral' title={`Your EP: ${ep}`} />
                </PageSection>
                <PageSection>
                    <PageSectionPart>
                        <Typography paragraph>
                            The amount of your EP translates to the cost of equipment you can take with you.
                            Check out the table below:
                        </Typography>
                    </PageSectionPart>
                    <PageSectionPart>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>EP Cost</TableCell>
                                    <TableCell>Equipment Category</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow><TableCell>0 (free)</TableCell><TableCell>Arming Sword*</TableCell></TableRow>
                                <TableRow><TableCell>0 (free)</TableCell><TableCell>First dagger</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Dagger ({'<'}30cm)</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Bastard Sword*</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Two-handed Sword*</TableCell></TableRow>
                                <TableRow><TableCell>2</TableCell><TableCell>Bow / Arbalet AND 3 arrows/bots</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Buckler ({'<'}35cm diameter)</TableCell></TableRow>
                                <TableRow><TableCell>2</TableCell><TableCell>Shield ({'<'}85cm diameter, or 70x90 square/triangle)</TableCell></TableRow>
                            </TableBody>
                        </Table>
                    </PageSectionPart>
                    <PageSectionPart>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>EP Cost</TableCell>
                                    <TableCell>Special Modifiers</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow><TableCell>1</TableCell><TableCell>Learn "dual wield"**</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>1st Armor (gives +1 HP)</TableCell></TableRow>
                                <TableRow><TableCell>2</TableCell><TableCell>2nd Armor (gives +1 HP)</TableCell></TableRow>
                                <TableRow><TableCell>3</TableCell><TableCell>3rd Armor (gives +1 HP)</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Extra 3 arrows/bolts</TableCell></TableRow>
                                <TableRow><TableCell>3</TableCell><TableCell>Battering ram (see section Siege)</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Pouch - haul 1 resource in the pouch without occupying a hand</TableCell></TableRow>
                                <TableRow><TableCell>1</TableCell><TableCell>Phoenix amulet (if 1-4 EP total) - can respawn at merchant</TableCell></TableRow>
                                <TableRow><TableCell>2</TableCell><TableCell>Phoenix amulet (if 5+ EP total) - can respawn at merchant</TableCell></TableRow>
                            </TableBody>
                        </Table>
                    </PageSectionPart>
                    <PageSectionPart>
                        <Typography paragraph>
                            * See <Link href='tronis.lv/equipment'>Throne website</Link> for a more detailed explanation between sword types.
                        </Typography>
                        <Typography paragraph>
                            ** dual-wield is required both for fighting with weapons in each hand, as well as holding a weapon in one hand and throwing a dagger with the other hand.
                            Without dual-wield, Player must sheathe or drop the main weapon before taking out a dagger and throwing it.
                        </Typography>
                    </PageSectionPart>
                </PageSection>
            </DialogContent>
        </Dialog>
    )
}) /* =============================================================================================================== */
