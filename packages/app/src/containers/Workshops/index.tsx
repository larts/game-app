import React from 'react'

import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { SUBROUTE_WORKSHOPS } from 'src/constants'

import PageSectionPart from '../../components/PageSectionPart'

import WorkshopsLogs from './WorkshopsLogs'
import WorkshopsTasks from './WorkshopsTasks'


const TABS = {
    [SUBROUTE_WORKSHOPS.Use]: 'Tasks',
    [SUBROUTE_WORKSHOPS.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Workshop'>
                <Typography gutterBottom variant='h3'>
                    Use Workshop to produce Industry points (IP).
                </Typography>
                <Typography paragraph>
                    Each faction has a Workshop and it produces Industry Points (IPs) via queued Tasks, one by one.
                </Typography>
                <Typography paragraph>
                    All tasks assigned to Workshops require wood, coins and one kind of recources and produce IP over time. Any amount can be substituted with Wood.
                </Typography>
                <Typography paragraph>
                    Each <strong>task takes 25 min to finish.</strong>
                </Typography>
                <Typography paragraph>
                    Tasks produce IP as per table below.
                </Typography>
                <PageSectionPart>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Type</TableCell>
                                <TableCell>Produced IP</TableCell>
                                <TableCell>Required Wood</TableCell>
                                <TableCell>Required Coins</TableCell>
                                <TableCell>Required resource and/or Wood</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow><TableCell>Tiny</TableCell><TableCell>4</TableCell><TableCell>1</TableCell><TableCell>1</TableCell><TableCell>1</TableCell></TableRow>
                            <TableRow><TableCell>Small</TableCell><TableCell>5</TableCell><TableCell>1</TableCell><TableCell>2</TableCell><TableCell>3</TableCell></TableRow>
                            <TableRow><TableCell>Medium</TableCell><TableCell>6</TableCell><TableCell>1</TableCell><TableCell>3</TableCell><TableCell>7</TableCell></TableRow>
                            <TableRow><TableCell>Large</TableCell><TableCell>7</TableCell><TableCell>1</TableCell><TableCell>4</TableCell><TableCell>13</TableCell></TableRow>
                            <TableRow><TableCell>Excessive</TableCell><TableCell>8</TableCell><TableCell>1</TableCell><TableCell>5</TableCell><TableCell>21</TableCell></TableRow>
                        </TableBody>
                    </Table>
                </PageSectionPart>
                <Typography gutterBottom variant='h3'>
                    Queue tasks for future
                </Typography>
                <Typography paragraph>
                    To automatize producing, workshop tasks can be queued into the future. When a task is queued, required  <strong>resources are locked</strong> in and cannot be used elsewhere. You can always remove a task from queue if it has not started yet.
                </Typography>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_WORKSHOPS.Use} element={<WorkshopsTasks />} />
                <Route path={SUBROUTE_WORKSHOPS.Logs} element={<WorkshopsLogs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_WORKSHOPS.Use} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
