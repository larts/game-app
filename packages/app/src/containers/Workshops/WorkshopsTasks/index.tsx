import React from 'react'

import { QueryTeamWorkshop } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import ProgressFullscreen from 'src/components/ProgressFullscreen'

import WorkshopQueueWizard from './WorkshopQueueWizard'
import WorkshopSection from './WorkshopSection'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryTeamWorkshops = useQuery(QueryTeamWorkshop, { pollInterval: 10000 })

    if (queryTeamWorkshops.loading) return <ProgressFullscreen />

    return (
        <div>
            <WorkshopSection
                key={queryTeamWorkshops.data?.team?.id}
                workshopActivations={queryTeamWorkshops.data?.team?.workshopActivations}
            />
            <WorkshopQueueWizard />
        </div>
    )
}) /* =============================================================================================================== */
