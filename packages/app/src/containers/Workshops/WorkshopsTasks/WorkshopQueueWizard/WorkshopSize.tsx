import React from 'react'

import { FOOD_SUBTYPE, QueryWorkshops, RESOURCE_TYPE } from '@larts/api'
import { recordEntries } from '@larts/common'
import { Chip } from '@material-ui/core'

import { IMyTheme, createFC, createStyles } from 'src/common'
import Resource from 'src/components/Resource'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    workshop: QueryWorkshops.Response['workshops'][0]
    wood?: number
    resource?: RESOURCE_TYPE | null
    foodVariants?: Partial<Record<FOOD_SUBTYPE, number>>
    color?: 'primary' | 'default'
}

export default createFC(import.meta.url, styles)<Props>(function _({
    children,
    theme,
    classes,
    workshop,
    wood = workshop.wood,
    resource,
    foodVariants = {},
}) {
    const foodVariantsArray = recordEntries(foodVariants)
    const requiredResources = workshop.resourceCost + workshop.wood - wood - (resource === RESOURCE_TYPE.Food ? foodVariantsArray.reduce((sum, [_, v]) => sum + (v ?? 0), 0) : 0)
    return (
        <>
            {workshop.ip}
            &nbsp;IP:&nbsp;
            <Chip
                color="default"
                variant="filled"
                label={<Resource amount={workshop.coin} type={RESOURCE_TYPE.Coins} />}
            />
            &nbsp;
            <Chip
                color="default"
                variant="filled"
                label={<Resource amount={wood} type={RESOURCE_TYPE.Wood} />}
            />
            &nbsp;
            {resource === RESOURCE_TYPE.Food && foodVariantsArray.map(([variant, amount]) => (
                <>
                    <Chip
                        color="default"
                        variant="filled"
                        label={<Resource amount={amount} type={variant as unknown as RESOURCE_TYPE} />}
                    />
                    &nbsp;
                </>
            ))}
            {!!requiredResources && (
                <>
                    <Chip
                        color="default"
                        variant={resource === RESOURCE_TYPE.Food ? 'outlined' : 'filled'}
                        label={(
                            <>
                                {requiredResources}
                        &nbsp;
                                {resource ? <Resource type={resource as unknown as RESOURCE_TYPE} /> : 'of one resource'}
                            </>
                        )}
                    />
                    &nbsp;
                </>
            )}
            25&nbsp;min
        </>
    )
}) /* =============================================================================================================== */
