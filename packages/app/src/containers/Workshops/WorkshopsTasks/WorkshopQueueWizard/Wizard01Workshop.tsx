import React, { useCallback } from 'react'

import { QueryWorkshops } from '@larts/api'
import { Button, FormControlLabel, Radio, RadioGroup, Stack } from '@material-ui/core'

import { IMyTheme, createFC, createStyles, useQuery } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'

import WorkshopSize from './WorkshopSize'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    onClose: () => void
    setStep: (step: 0|1|2) => void
    selectedWorkshop: QueryWorkshops.Response['workshops'][0] | null
    setSelectWorkshop: (workshop: QueryWorkshops.Response['workshops'][0]) => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, onClose, setStep, selectedWorkshop, setSelectWorkshop }) {
    const queryWorkshops = useQuery(QueryWorkshops)

    const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectWorkshop(queryWorkshops.data!.workshops.find((w) => w.id === Number((event.target as HTMLInputElement).value))!)
    }, [queryWorkshops, setSelectWorkshop])

    const handleBack= useCallback(() => onClose(), [onClose])
    const handleNext= useCallback(() => setStep(1), [setStep])

    if (queryWorkshops.loading) return null

    return (
        <PageSection>
            <PageSectionPart title='Pick a task size'>
                <RadioGroup
                    value={selectedWorkshop?.id ?? null}
                    onChange={handleChange}
                >
                    {queryWorkshops.data?.workshops.map((w) => (
                        <FormControlLabel
                            sx={{ margin: theme.spacing(0.5,0) }}
                            key={w.id}
                            value={w.id}
                            control={<Radio />}
                            label={<WorkshopSize workshop={w} />}
                        />
                    ))}
                </RadioGroup>
            </PageSectionPart>
            <PageSectionPart>
                <Stack direction="row" justifyContent='end'>
                    <Button onClick={handleBack}>Close</Button>
                    <Button onClick={handleNext} variant='contained' disabled={!selectedWorkshop}>Next</Button>
                </Stack>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */
