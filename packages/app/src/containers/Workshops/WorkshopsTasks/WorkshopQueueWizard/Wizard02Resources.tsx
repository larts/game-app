import React, { useCallback } from 'react'

import { FOOD_SUBTYPE, QueryTeamResources, QueryWorkshops, RESOURCE_TYPE } from '@larts/api'
import { recordValues } from '@larts/common'
import { Alert, AlertTitle, Button, Chip, FormControlLabel, Grid, Radio, RadioGroup, Slider, Stack, Typography } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'

import { IMyTheme, createFC, createStyles, recordEntries, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'
import Resource from 'src/components/Resource'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    setStep: (step: 0|1|2) => void
    selectedWorkshop: QueryWorkshops.Response['workshops'][0]
    selectedResources: RESOURCE_TYPE | null
    setSelectResources: (selectedResources: RESOURCE_TYPE | null) => void
    selectedWood: number
    setSelectWood: (selectedWood: number) => void
    selectedFoodVariants: Partial<Record<FOOD_SUBTYPE, number>>
    setSelectFoodVariants: (selectedFoodVariants: Partial<Record<FOOD_SUBTYPE, number>>) => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, setStep, selectedWorkshop, selectedResources, setSelectResources, selectedWood, setSelectWood, selectedFoodVariants, setSelectFoodVariants }) {
    const queryTeamResources = useQuery(QueryTeamResources)
    const queryWorkshops = useQuery(QueryWorkshops)

    const handleChangeResources = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectResources(Number((event.target as HTMLInputElement).value) as RESOURCE_TYPE)
    }, [setSelectResources])
    const handleChangeWood = useCallback((event: unknown, value: number | number[]) => {
        setSelectWood(value as number)
    }, [setSelectWood])

    const handleBack= useCallback(() => setStep(0), [setStep])
    const handleNext= useCallback(() => setStep(2), [setStep])

    const handleSelect = useCallback((event: React.MouseEvent<HTMLElement>) => {
        const foodSubtype = Number(event.currentTarget.dataset.subtype) as FOOD_SUBTYPE
        setSelectFoodVariants({
            ...selectedFoodVariants,
            [foodSubtype]: (selectedFoodVariants[foodSubtype] ?? 0) + 1,
        })
    }, [selectedFoodVariants, setSelectFoodVariants])

    const handleDeselect = useCallback((event: React.MouseEvent<HTMLElement>) => {
        const foodSubtype = Number(event.currentTarget.dataset.subtype) as FOOD_SUBTYPE
        const { [foodSubtype]: deselected, ...others } = selectedFoodVariants
        if (deselected && deselected > 1) {
            setSelectFoodVariants({
                ...others,
                [foodSubtype]: deselected - 1,
            })
        } else {
            setSelectFoodVariants(others)
        }
    }, [selectedFoodVariants, setSelectFoodVariants])

    if (queryWorkshops.loading) return null

    const availableWood = queryTeamResources.data?.team?.resources.find((res) => res.type.id === RESOURCE_TYPE.Wood)?.available ?? 0
    const availableRes = queryTeamResources.data?.team?.resources
        .filter((res) => selectedResources !== RESOURCE_TYPE.Food ? res.type.id === selectedResources : res.type.id in FOOD_SUBTYPE)
        .reduce((sum, res) => sum + res.available, 0) ?? 0
    const max = Math.min(availableWood, selectedWorkshop.resourceCost - 1 + selectedWorkshop.wood)
    const min = Math.max(selectedWorkshop.wood, selectedWorkshop.resourceCost + selectedWorkshop.wood - availableRes)
    const hideSlider = !selectedResources || min >= max

    const requiredRes = selectedWorkshop.resourceCost + 1 - selectedWood
    const selectedFood = recordValues(selectedFoodVariants).reduce((sum, v) => (sum ?? 0) + (v ?? 0), 0) ?? 0
    const moreFood = requiredRes - selectedFood
    const noteFood = moreFood > 0 ? ` (need ${moreFood} more)`
        : moreFood < 0 ? ` (need ${-moreFood} less)`
            : ''

    const disabled = !selectedResources || min > max || (selectedResources === RESOURCE_TYPE.Food && moreFood !== 0)

    return (
        <PageSection>
            <PageSectionPart title='Pick workshop resource type'>
                <RadioGroup
                    value={selectedResources}
                    onChange={handleChangeResources}
                >
                    {[RESOURCE_TYPE.Hides, RESOURCE_TYPE.Iron, RESOURCE_TYPE.Food].map((resId) => (
                        <FormControlLabel
                            key={resId}
                            value={resId}
                            sx={{ margin: theme.spacing(0.5, 0) }}
                            control={<Radio />}
                            label={(
                                <Resource type={resId} title variant='colored' />
                            )}
                        />
                    ))}
                </RadioGroup>
            </PageSectionPart>
            {!hideSlider && (
                <PageSectionPart title='Substitude with wood'>
                    <Stack direction="row" justifyContent='end' alignItems='center'>
                        <Slider
                            sx={{ margin: theme.spacing(0, 3, 4, 3), width: `calc(100% - ${theme.spacing(6)})` }}
                            min={min}
                            max={max}
                            marks={new Array(max - min + 1).fill(null).map((_, i) => ({ value: min + i, label: `${min + i}` }))}
                            value={selectedWood}
                            onChange={handleChangeWood}
                        />
                        <Typography>Wood</Typography>
                    </Stack>
                </PageSectionPart>
            )}
            {selectedResources === RESOURCE_TYPE.Food && (
                <>
                    <PageSectionPart title={<>Choose <Resource type={RESOURCE_TYPE.Food} amount={requiredRes} /> food tokens from available {availableRes}:</>}>
                        <Grid container spacing={0}>
                            {queryTeamResources.data?.team?.resources.filter((res) => res.type.id in FOOD_SUBTYPE).map((res) => (
                                <Grid key={res.type.id} item>
                                    <PageCard color={res.available === 0 ? 'neutral' : undefined}>
                                        <Typography align='center' gutterBottom>
                                            <Resource type={res.type.id} amount={res.available} />
                                        </Typography>
                                        <Grid container justifyContent='center'>
                                            <Button
                                                {...{ 'data-subtype': res.type.id }}
                                                onClick={handleSelect}
                                                disabled={res.available <= (selectedFoodVariants[res.id] ?? 0)}
                                            >
                                                Add
                                            </Button>
                                        </Grid>
                                    </PageCard>
                                </Grid>
                            ))}
                        </Grid>
                    </PageSectionPart>
                    <PageSectionPart title={`Selected Food${noteFood}`}>
                        <Grid container spacing={1}>
                            {recordEntries(selectedFoodVariants).map(([type, amount]) => (
                                <Grid key={type} item>
                                    <Chip
                                        size='medium'
                                        color='primary'
                                        onDelete={handleDeselect}
                                        deleteIcon={<IconClose {...{ 'data-subtype': type }} />}
                                        label={<Resource type={type as unknown as RESOURCE_TYPE} amount={amount} />}
                                        onClick={handleDeselect}
                                    />
                                </Grid>
                            ))}
                        </Grid>
                    </PageSectionPart>
                    <PageSectionPart>
                        {selectedFood > requiredRes && (
                            <Alert severity='error'>
                                <AlertTitle>Too many tokens</AlertTitle>
                                Unselect some tokens to use workshop.
                            </Alert>
                        )}
                    </PageSectionPart>
                </>
            )}
            <PageSectionPart>
                <Stack direction="row" justifyContent='end'>
                    <Button onClick={handleBack}>Back</Button>
                    <Button onClick={handleNext} variant='contained' disabled={disabled}>Next</Button>
                </Stack>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */
