import React, { useCallback, useState } from 'react'


import { Dialog, DialogContent, DialogTitle, Fab, Grid, IconButton, Toolbar, Typography } from '@material-ui/core'
import IconAdd from '@material-ui/icons/Add'
import IconClose from '@material-ui/icons/Close'

import { IMyTheme, createFC, createStyles } from 'src/common'

import Wizard from './Wizard'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
    fab: {
        margin: theme.spacing(2),
        right: 0,
        bottom: 0,
        position: 'fixed',
    },
})

export default createFC(import.meta.url, styles)(function _({ children, theme, classes }) {
    const [open, setOpen] = useState(false)
    const handleClose = useCallback(() => setOpen(false), [setOpen])
    const handleOpen = useCallback(() => setOpen(true), [setOpen])


    return (
        <>
            <Fab onClick={handleOpen} color='primary' className={classes.fab} variant='extended'><IconAdd />Queue task</Fab>
            <Dialog fullScreen open={open} onClose={handleClose}>
                <DialogTitle>
                    <Toolbar>
                        <Typography variant='h2'>Queue Workshop Task</Typography>
                        <Grid item xs />
                        <IconButton size='medium' onClick={handleClose}>
                            <IconClose />
                        </IconButton>
                    </Toolbar>
                </DialogTitle>
                <DialogContent>
                    <Wizard onClose={handleClose} />
                </DialogContent>
            </Dialog>
        </>
    )
}) /* =============================================================================================================== */
