import React, { useCallback } from 'react'

import { Button, Stack } from '@material-ui/core'

import { IMyTheme, createFC, createStyles } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    setStep: (step: 0|1|2) => void
    queueTask: () => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, setStep, queueTask }) {
    const handleBack = useCallback(() => setStep(1), [setStep])
    const handleNext = useCallback(() => queueTask(), [queueTask])

    return (
        <PageSection>
            <PageSectionPart>
                <Stack direction="row" justifyContent='end'>
                    <Button onClick={handleBack}>Back</Button>
                    <Button onClick={handleNext} variant='contained'>Queue Task</Button>
                </Stack>
            </PageSectionPart>
        </PageSection>
    )
}) /* =============================================================================================================== */
