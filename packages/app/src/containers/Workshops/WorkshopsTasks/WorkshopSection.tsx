import React from 'react'
import { useCallback } from 'react'

import { QueryTeamWorkshop } from '@larts/api'

import useMutationTaskCancel from 'src/api/useMutationTaskCancel'
import { IMyTheme, createFC, createStyles } from 'src/common'
import useTickGame from 'src/common/useTickGame'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


import WorkshopTaskCard from './WorkshopTaskCard'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    workshopActivations?: NonNullable<QueryTeamWorkshop.Response['team']>['workshopActivations']
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, workshopActivations }) {
    const tickGame = useTickGame()

    const [taskCancel] = useMutationTaskCancel()
    const handleCancel = useCallback(() => taskCancel(), [])

    const notDoneActivations = workshopActivations?.filter((task) => task.finishTime > tickGame && !task.cancelled) ?? []

    return (
        <PageSection className={classes.root}>
            <PageSectionPart
                title='Assigned tasks'
            >
            </PageSectionPart>
            {notDoneActivations.map((task, i, array) => (
                <WorkshopTaskCard
                    key={task.id}
                    task={task}
                    inProgress={task.startTime < tickGame}
                    onCancel={i === array.length - 1 ? handleCancel : undefined}
                />
            ))}
        </PageSection>
    )
}) /* =============================================================================================================== */
