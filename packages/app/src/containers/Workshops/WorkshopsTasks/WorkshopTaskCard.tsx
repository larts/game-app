import React from 'react'

import { QueryTeamWorkshop, QueryUserName, RESOURCE_TYPE } from '@larts/api'
import { Chip, Divider, Grid, IconButton, Typography } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'

import { IMyTheme, createFC, createStyles, formatTimeAbsolute, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import PageCardProgress from 'src/components/PageCardProgress'
import Resource from 'src/components/Resource'


const styles = (theme: IMyTheme) => createStyles({
    root: {
    },
})

interface Props {
    task: NonNullable<QueryTeamWorkshop.Response['team']>['workshopActivations'][0]
    onCancel?: () => void
    inProgress: boolean
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, task, inProgress, onCancel }) {
    const queryUser = useQuery(QueryUserName)
    const usedWood = task.priceDetails.reduce((sum, detail) => sum + (detail.type === RESOURCE_TYPE.Wood ? detail.amount : 0), 0)

    return (
        <PageCard className={classes.root} sx={{ borderBottomLeftRadius: inProgress ? '4px' : undefined, borderBottomRightRadius: inProgress ? '4px' : undefined  }}>
            <Grid container spacing={1} sx={{ marginBottom: 1 }} alignItems='flex-start'>
                <Grid item>
                    <Typography>{inProgress ? 'active' : 'queued'}</Typography>
                </Grid>
                <Grid item>
                    <Divider orientation='vertical' sx={{ height: 20 }} />
                </Grid>
                <Grid item>
                    <Typography>{task.size.score.title.split(' ').slice(1,2)} {RESOURCE_TYPE[task.variant]} Task</Typography>
                </Grid>
                <Grid item>
                    <Divider orientation='vertical' sx={{ height: 20 }} />
                </Grid>
                <Grid item>
                    <Typography>{task.size.ip} IP</Typography>
                </Grid>
                <Grid item>
                    <Divider orientation='vertical' sx={{ height: 20 }} />
                </Grid>
                <Grid item>
                    <Typography>ETA {formatTimeAbsolute(task.finishTime)}</Typography>
                </Grid>
                <Grid item xs />
                {onCancel && !inProgress && (
                    <Grid item>
                        <IconButton onClick={onCancel} edge='end' disabled={!queryUser.data?.user}><IconClose /></IconButton>
                    </Grid>
                )}
            </Grid>
            <Grid container spacing={1} sx={{ marginBottom: 1 }}>
                <Grid item>
                    <Chip color='primary' label={<Resource amount={usedWood} type={RESOURCE_TYPE.Wood} />} />
                </Grid>
                <Grid item>
                    <Chip color='primary' label={<Resource amount={task.size.wood + task.size.resourceCost - usedWood} type={task.variant} />} />
                </Grid>
                <Grid item>
                    <Chip color='primary' label={<Resource amount={task.size.coin} type={RESOURCE_TYPE.Coins} />} />
                </Grid>
            </Grid>
            <PageCardProgress amount={task.size.ip} startTime={task.startTime} taskLength={30*60} />   {/* TODO: unhardcode */}
        </PageCard>
    )
}) /* =============================================================================================================== */
