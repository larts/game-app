import React from 'react'

import { Typography } from '@material-ui/core'
import { Navigate, Route, Routes } from 'react-router'

import { createFC } from 'src/common'
import PageHeader from 'src/components/PageHeader'
import PageTabs from 'src/components/PageTabs'
import { SUBROUTE_FEASTS } from 'src/constants'

import Available from './Available'
import Logs from './Logs'


const TABS = {
    [SUBROUTE_FEASTS.Available]: 'Available',
    [SUBROUTE_FEASTS.Logs]: 'Logs',
}

export default createFC(import.meta.url)(function _({ children, theme }) {
    return (
        <div>
            <PageHeader title='Feasts'>
                <Typography paragraph>
                    Feast is ~5 min long group activity with group dances (danči) <strong>between at least two different Factions</strong>, happening at one’s Yard. It generates Agriculture points (AP).
                </Typography>
                <Typography gutterBottom variant='h3'>
                    A feast requires
                </Typography>
                <ul>
                    <Typography component='li'>
                        couple(s) of players that are of opposite sex and from different Factions
                    </Typography>
                    <Typography component='li'>
                        a feast token
                    </Typography>
                </ul>
                <Typography gutterBottom variant='h3'>
                    Guests get more AP than hosts
                </Typography>
                <Typography paragraph>
                    In feast, Agriculture Points (AP) are split:
                </Typography>
                <ul>
                    <Typography component='li'>
                        1 AP to host faction per couple
                    </Typography>
                    <Typography component='li'>
                        2 AP to guest faction per couple
                    </Typography>
                </ul>
            </PageHeader>
            <PageTabs tabs={TABS} />
            <Routes>
                <Route path={SUBROUTE_FEASTS.Available} element={<Available />} />
                <Route path={SUBROUTE_FEASTS.Logs} element={<Logs />} />
                <Route path="*" element={<Navigate to={SUBROUTE_FEASTS.Available} replace />} />
            </Routes>
        </div>
    )
}) /* =============================================================================================================== */
