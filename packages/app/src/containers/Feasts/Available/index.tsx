import React from 'react'

import { QueryTeamFeasts } from '@larts/api'

import { createFC, useQuery } from 'src/common'
import FeastCard from 'src/components/FeastCard'
import NoItemsHere from 'src/components/NoItemsHere'
import PageSection from 'src/components/PageSection'
import ProgressFullscreen from 'src/components/ProgressFullscreen'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryFeasts = useQuery(QueryTeamFeasts)

    if (queryFeasts.loading) return <ProgressFullscreen />

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const available = queryFeasts.data!.team!.feastTokens.filter((feast) => !feast.usedAt)

    return (
        <PageSection title={`Available Feast tokens ${available.length}`}>
            {available.map((feast) => (
                <FeastCard key={feast.id} feast={feast} />
            ))}
            {available.length === 0 && <NoItemsHere />}
        </PageSection>
    )
}) /* =============================================================================================================== */
