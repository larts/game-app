import { MutationTeamLogout } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [teamLogout, result] = useMutation(MutationTeamLogout)

    return [
        useApiCallback(async () => {
            await teamLogout()
            localStorage.removeItem('device-token')  // See also `useMutationTeamLogin`.
            notify('🎉 Team logged out')
        }, [teamLogout, notify]),
        result,
    ] as const
}
