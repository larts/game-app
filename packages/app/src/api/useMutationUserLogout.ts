import { MutationUserLogout } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [userLogout, result] = useMutation(MutationUserLogout)

    return [
        useApiCallback(async () => {
            await userLogout()
            notify('🎉 User logged out')
        }, [userLogout, notify]),
        result,
    ] as const
}
