import React from 'react'

import { MutationResourcesAdd } from '@larts/api'

import { codeToType, useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'
import Resource from 'src/components/Resource'


export default () => {
    const notify = useNotify()
    const [resourcesAdd, mutation] = useMutation(MutationResourcesAdd)

    return [
        useApiCallback(async (qrCode: string) => {
            await resourcesAdd({ variables: { qrCode } })
            notify(<>🎉 <Resource type={codeToType(qrCode)} amount={1} /> deposited.</>)
        }, [resourcesAdd, notify]),
        mutation,
    ] as const
}
