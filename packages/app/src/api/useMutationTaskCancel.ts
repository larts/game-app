import { MutationTaskCancel } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [taskCancel, mutation] = useMutation(MutationTaskCancel)

    return [
        useApiCallback(async () => {
            await taskCancel()
            notify('🎉 task cancel!')
        }, [taskCancel, notify]),
        mutation,
    ] as const
}

