import { MutationEquipmentRemove } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [equipmentRemove, mutation] = useMutation(MutationEquipmentRemove)

    return [
        useApiCallback(async () => {
            await equipmentRemove()
            notify('🎉 EP removed')
        }, [equipmentRemove, notify]),
        mutation,
    ] as const
}
