import { MutationEquipmentEquip } from '@larts/api/src/mutations'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [equipmentEquip, mutation] = useMutation(MutationEquipmentEquip)

    return [
        useApiCallback(async () => {
            await equipmentEquip()
            notify('🎉 Amulet of Phoenix equipped!')
        }, [equipmentEquip, notify]),
        mutation,
    ] as const
}
