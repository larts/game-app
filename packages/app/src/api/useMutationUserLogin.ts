import { MutationUserLogin } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [userLogin, result] = useMutation(MutationUserLogin)

    return [
        useApiCallback(async (qrCode: string) => {
            if (!qrCode) return

            await userLogin({ variables: { qrCode } })
            notify('🎉 User logged in')
        }, [userLogin, notify]),
        result,
    ] as const
}
