import { MutationTaskQueue, RESOURCE_TYPE, WorkshopActivateFoodInput } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [queueTaskMutation, mutation] = useMutation(MutationTaskQueue)

    return [
        useApiCallback(async (sizeId: number, variant: RESOURCE_TYPE, wood: number, food?: WorkshopActivateFoodInput[]) => {
            await queueTaskMutation({ variables: { input: { sizeId, variant, wood, food } } })
            notify('🎉 task queued!')
        }, [queueTaskMutation, notify]),
        mutation,
    ] as const
}

