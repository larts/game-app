import { useEffect, useState } from 'react'

import { MutationTeamLogin, TEAM_ID } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [token, setToken] = useState(localStorage.getItem('device-token'))
    const [teamLogin, result] = useMutation(MutationTeamLogin)

    // Lazy solution for detecting logout. See also `useMutationTeamLogout`.
    useEffect(() => {
        const interval = setInterval(() => {
            setToken(localStorage.getItem('device-token'))
        }, 1000)
        return () => clearInterval(interval)
    })

    return [
        useApiCallback(async (team: TEAM_ID | null, name: string) => {
            const result = await teamLogin({ variables: { team, name } })
            if (result.data) {
                localStorage.setItem('device-token', result.data.auth.deviceRegister)
                setToken(result.data.auth.deviceRegister)
                notify('🎉 Access Requested')
            } else {
                localStorage.removeItem('device-token')
                notify(`Error: ${result.errors}`)
            }
        }, [teamLogin, notify]),
        result,
        token,
    ] as const
}
