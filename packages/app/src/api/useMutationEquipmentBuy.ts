import { MutationEquipmentBuy } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [equipmentBuy, mutation] = useMutation(MutationEquipmentBuy)

    return [
        useApiCallback(async () => {
            await equipmentBuy()
            notify('🎉 EP bought')
        }, [equipmentBuy, notify]),
        mutation,
    ] as const
}
