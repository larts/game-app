import { MutationFlagScore } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [flagScore, mutation] = useMutation(MutationFlagScore)

    return [
        useApiCallback(async (qrCode: string) => {
            await flagScore({ variables: { qrCode } })
            notify('🎉 Flag scored for MP!')
        }, [flagScore, notify]),
        mutation,
    ] as const
}
