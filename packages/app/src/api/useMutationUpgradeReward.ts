import { MutationUpgradeReward } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [upgradeReward, mutation] = useMutation(MutationUpgradeReward)

    return [
        useApiCallback(async (code: string) => {
            await upgradeReward({ variables: { code } })
            notify('🎉 Upgrade rewarded for 2 IP!')
        }, [upgradeReward, notify]),
        mutation,
    ] as const
}
