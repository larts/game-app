import { MutationFeastScore } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'


export default () => {
    const notify = useNotify()
    const [feastScore, mutation] = useMutation(MutationFeastScore)

    return [
        useApiCallback(async (id: number, qrList: string[]) => {
            await feastScore({ variables: { id, qrList } })
            notify('🎉 Feast started!')
        }, [feastScore, notify]),
        mutation,
    ] as const
}
