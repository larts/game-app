import React from 'react'

import { MutationResourcesAddCoins, RESOURCE_TYPE } from '@larts/api'

import { useMutation } from 'src/common'
import useApiCallback from 'src/common/useApiCallback'
import useNotify from 'src/common/useNotify'
import Resource from 'src/components/Resource'


export default () => {
    const notify = useNotify()
    const [addCoins, mutation] = useMutation(MutationResourcesAddCoins)

    return [
        useApiCallback(async (amount: number) => {
            await addCoins({ variables: { amount } })
            notify(<>🎉 <Resource type={RESOURCE_TYPE.Coins} amount={amount} /> deposited to Throne App</>)
        }, [addCoins, notify]),
        mutation,
    ] as const
}
