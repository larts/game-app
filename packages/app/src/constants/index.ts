import { PILLAR } from '@larts/api'
import { join } from 'path-browserify'


export enum ROUTE {
    Home = 'home',
    Logs = 'logs',
    Resources = 'resources',
    Profile = 'profile',
    Respawn = 'respawn',
    Leaderboard = 'leaderboard',

    Workshops = 'workshops',
    Upgrades = 'upgrades',

    Diplomacy = 'diplomacy',
    Mercenaries = 'mercenaries',
    Events = 'events',

    Recipes = 'recipes',
    Feasts = 'feasts',
    Feast = 'feast',
    Market = 'market',

    Flag = 'flag',
    EquipmentPoints = 'equipment',

    Public = 'public',
    Setup = 'setup',

    Contacts = 'contacts',
    FridayFeast = 'friday',
}

export enum SUBROUTE_RESOURCES {
    Input = 'input',
    List = 'list',
    Logs = 'logs',
}

export enum SUBROUTE_PROFILE {
    Profile = 'summary',
    Logs = 'logs',
}

export enum SUBROUTE_WORKSHOPS {
    Use = 'use',
    Build = 'build',
    Logs = 'logs',
}

export enum SUBROUTE_RECIPES {
    Invent = 'invent',
    Copy = 'copy',
    Logs = 'logs',
}

export enum SUBROUTE_FLAG {
    Status = 'status',
    Logs = 'logs',
}

export enum SUBROUTE_MARKET {
    Trade = 'trade',
    Logs = 'logs',
}

export enum SUBROUTE_FEASTS {
    Available = 'available',
    Logs = 'logs',
}

export enum SUBROUTE_UPGRADES {
    Available = 'available',
    Logs = 'logs',
}

export enum SUBROUTE_LEADERBOARD {
    Summary = 'summary',
    Victory = 'victory',
    Industry = 'industry',
    Agriculture = 'agriculture',
    Military = 'military',
}

type SUBROUTES = (SUBROUTE_LEADERBOARD | PILLAR) | SUBROUTE_RESOURCES | SUBROUTE_WORKSHOPS | SUBROUTE_PROFILE | SUBROUTE_FLAG | SUBROUTE_RECIPES | SUBROUTE_MARKET | SUBROUTE_FEASTS

export function makeRoute(a: ROUTE): string
export function makeRoute(a: ROUTE.Resources, b?: SUBROUTE_RESOURCES): string
export function makeRoute(a: ROUTE.Profile, b?: SUBROUTE_PROFILE): string
export function makeRoute(a: ROUTE.Workshops, b?: SUBROUTE_WORKSHOPS): string
export function makeRoute(a: ROUTE.Flag, b?: SUBROUTE_FLAG): string
export function makeRoute(a: ROUTE.Recipes, b?: SUBROUTE_RECIPES): string
export function makeRoute(a: ROUTE.Market, b?: SUBROUTE_MARKET): string
export function makeRoute(a: ROUTE.Feasts, b?: SUBROUTE_FEASTS): string
export function makeRoute(a: ROUTE.Feast, b: number): string
export function makeRoute(a: ROUTE.Leaderboard, b?: SUBROUTE_LEADERBOARD): string
export function makeRoute(a: ROUTE, b?: SUBROUTES): string {
    if (b) {
        return join('/', a, String(b))
    } else {
        return join('/', a)
    }
}
