import React from 'react'

import { Box } from '@material-ui/system'

import { IMyTheme, createFC, createStyles } from 'src/common'
import useTickGame from 'src/common/useTickGame'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        width: `calc(100% + ${theme.spacing(4)})`,
        height: theme.spacing(1.5),
        position: 'relative',
        margin: theme.spacing(0, -2, -2),
        borderTop: `solid 1px ${theme.palette.primary.light}`,
    },
    filler: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        backgroundColor: theme.palette.primary.light,
    },
    dot: {
        position: 'absolute',
        width: '4px',
        top: 0,
        bottom: 0,
    },
})

interface Props {
    amount: number
    startTime: number
    taskLength: number
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, amount, startTime, taskLength }) {
    const tickGame = useTickGame()

    const progress = (tickGame - startTime) / taskLength

    if (!progress || progress < 0) return null

    return (
        <Box className={classes.root}>
            <Box className={classes.filler} sx={{ right: `${(1 - progress) * 100}%` }} />
            {new Array(amount).fill(null).map((_, i) => {
                const percent = (i + 1) * (1 / amount)
                const right = `${(1 - percent) * 100}%`  // TODO: exact timestamp.
                // const color = task.scorePoints[i] ? theme.palette.primary.contrastText : theme.palette.action.disabled  // exact
                const backgroundColor = percent < progress ? theme.palette.primary.main : theme.palette.text.disabled
                return (
                    <Box key={i} className={classes.dot} sx={{ right, backgroundColor }} />
                )
            })}
        </Box>
    )
}) /* =============================================================================================================== */
