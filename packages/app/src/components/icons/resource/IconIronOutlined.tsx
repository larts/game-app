import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M7 12H17L15 5H9L7 12ZM20 13L22 20L12 20L14 13H20ZM10 13L12 20L2 20L4 13H10ZM10 6.5H14L15 10.5H9L10 6.5ZM14 18.5L15 14.5H19L20 18.5H14ZM9 14.5L10 18.5H4L5 14.5H9Z" fill="currentColor" />
</g>)
