import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path d="M11.0057 19.25L2.9943 19.25L4.56573 13.75H9.43427L11.0057 19.25Z" fill="#FFB800" stroke="black" strokeWidth="1.5" />
    <path d="M21.0057 19.25H12.9943L14.5657 13.75H19.4343L21.0057 19.25Z" fill="#FFB800" stroke="black" strokeWidth="1.5" />
    <path d="M16.0057 11.25H7.9943L9.56573 5.75H14.4343L16.0057 11.25Z" fill="#FFB800" stroke="black" strokeWidth="1.5" />
</g>)
