import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path d="M9 15H15.1818L16.5124 20.1229C16.7592 21.0729 16.0421 22 15.0606 22H8.9886C7.99203 22 7.27253 21.0461 7.54631 20.0879L9 15Z" fill="currentColor" />
    <path d="M21.9935 14H2.00586C2.00586 14 2.99951 -1.00002 13.4995 2.49999C23.9995 6 21.9935 14 21.9935 14Z" fill="currentColor" />
</g>)
