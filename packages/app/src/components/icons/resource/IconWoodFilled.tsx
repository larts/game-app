import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M7.02651 4H17.0265V11.9427L18.7 10.0189L20.9981 11.9473L17.0265 16.513V20H7.02651V8.84331L4 6.38904L5.28558 4.85695L7.02651 6.26872V4Z" fill="currentColor" />
</g>)
