import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path d="M12 20L2 20L4 13H10L12 20Z" fill="currentColor" />
    <path d="M22 20H12L14 13H20L22 20Z" fill="currentColor" />
    <path d="M17 12H7L9 5H15L17 12Z" fill="currentColor" />
</g>)
