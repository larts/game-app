import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M6 6.99574C6 6.99574 7.77136 3.27633 12.0031 3.00008C20.9948 2.41311 23.3985 13.8423 13.9991 16.9848L14.004 22.9783H10.0022V16.9824L10 14.4876C10.7464 14.7744 11.1795 14.8986 12.0031 14.9838C19.9933 15.2249 19.9812 3.76365 12.0031 3.999C8.14678 4.11276 6.5 7.49519 6.5 7.49519L6 6.99574ZM12 15.9859H11V21.9794H12V15.9859Z" fill="currentColor" />
    <line x1="20.0026" y1="6.4859" x2="22.9445" y2="6.76662" stroke="currentColor" />
    <line x1="18.3024" y1="1.14295" x2="18.0217" y2="4.08484" stroke="currentColor" />
    <line x1="19.3808" y1="5.11446" x2="21.6596" y2="3.23273" stroke="currentColor" />
</g>)
