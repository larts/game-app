import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M9 8.00002V22H12V8.00001H15V6.00001L17 8.00001L20 8.00002V2.00001H17L15 4.00001V2.00001H5.99991C4.44928 1.99332 2.00654 4.40094 2 6.00001H5.99991V8.00001L9 8.00002ZM19 3.00002H18V7.00002H19V3.00002Z" fill="black" />
    <path d="M18 3.00002H19V7.00002H18V3.00002Z" fill="#FFB800" />
    <line x1="21.2514" y1="6.56779" x2="22.9333" y2="7.54602" stroke="#FFB800" />
    <line x1="22.8389" y1="2.42174" x2="21.2686" y2="3.42174" stroke="#FFB800" />
    <line x1="21" y1="5" x2="24" y2="5" stroke="#FFB800" />
</g>)
