import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path fillRule="evenodd" clipRule="evenodd" d="M6 6.99549C6 6.99549 7.77136 3.27608 12.0031 2.99984C20.9948 2.41287 23.3985 13.842 13.9991 16.9846L14.004 22.978H10.0022L10 14.4873C10.7464 14.7741 11.1795 14.8983 12.0031 14.9836C19.9933 15.2247 19.9812 3.7634 12.0031 3.99876C8.14678 4.11252 6.5 7.49495 6.5 7.49495L6 6.99549ZM12 15.9857H11V21.9791H12V15.9857Z" fill="black" />
    <path d="M11 15.9857H12V21.9791H11V15.9857Z" fill="#FFB800" />
    <line x1="20.0026" y1="6.4859" x2="22.9445" y2="6.76662" stroke="#FFB800" />
    <line x1="18.3024" y1="1.14295" x2="18.0217" y2="4.08484" stroke="#FFB800" />
    <line x1="19.3808" y1="5.11446" x2="21.6596" y2="3.23273" stroke="#FFB800" />
</g>)
