import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<g>
    <path d="M13 5L12 4V15H13V5ZM12 22H9V21H10V17H6V15H9V5L12 2L15 5V15H18V17H14V21H15V22H12Z" fill="currentColor" />
    <line x1="17.3536" y1="1.35355" x2="15.2322" y2="3.47487" stroke="currentColor" />
    <line x1="16" y1="5.5" x2="19" y2="5.5" stroke="currentColor" />
    <line y1="-0.5" x2="3" y2="-0.5" transform="matrix(0.707107 0.707107 0.707107 -0.707107 7 1)" stroke="currentColor" />
    <line x1="8" y1="5.5" x2="5" y2="5.5" stroke="currentColor" />
</g>)
