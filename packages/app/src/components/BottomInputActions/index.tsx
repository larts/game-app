import React, { useCallback } from 'react'
import { useState } from 'react'

import { QueryUserName, RESOURCE_TYPE } from '@larts/api'
import { AppBar, BottomNavigation, BottomNavigationAction, Toolbar } from '@material-ui/core'

import { createFC, useQuery } from 'src/common'
import IconDead from 'src/components/icons/IconDead'
import IconQR from 'src/components/icons/IconQR'
import Resource from 'src/components/Resource'

import DialogDepositCoins from './DialogDepositCoins'
import DialogDepositResources from './DialogDepositResources'
import DialogDepositTributes from './DialogDepositTributes'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const queryUser = useQuery(QueryUserName)

    const [open, setOpen] = useState<'coins'|'resources'|'tributes'|null>(null)
    const handleOpenCoins = useCallback(() => setOpen('coins'), [setOpen])
    const handleOpenResources = useCallback(() => setOpen('resources'), [setOpen])
    const handleOpenTributes = useCallback(() => setOpen('tributes'), [setOpen])
    const handleClose = useCallback(() => setOpen(null), [setOpen])

    if (!queryUser.data?.user) return null

    return (
        <div>
            <Toolbar />
            <AppBar position='fixed' sx={{ top: 'unset', bottom: 0 }}>
                <BottomNavigation
                    sx={{ width: '100%' }}
                    showLabels
                >
                    <BottomNavigationAction sx={{ color: theme.palette.primary.contrastText }} onClick={handleOpenCoins} icon={<Resource type={RESOURCE_TYPE.Coins} />} label='Swap coins' />
                    <BottomNavigationAction sx={{ color: theme.palette.primary.contrastText }} onClick={handleOpenResources} icon={<IconQR />} label='Scan resource' />
                    <BottomNavigationAction sx={{ color: theme.palette.primary.contrastText }} onClick={handleOpenTributes} icon={<IconDead />} label='Enter tribute' />
                </BottomNavigation>
            </AppBar>
            <DialogDepositCoins open={open === 'coins'} onClose={handleClose} />
            <DialogDepositResources open={open === 'resources'} onClose={handleClose} />
            <DialogDepositTributes open={open === 'tributes'} onClose={handleClose} />
        </div>
    )
}) /* =============================================================================================================== */
