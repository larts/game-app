import React, { ChangeEvent, FormEvent } from 'react'
import { useState } from 'react'
import { useCallback } from 'react'

import { Button, Dialog, Grid, IconButton, TextField, Toolbar, Typography } from '@material-ui/core'
import IconCheck from '@material-ui/icons/Check'
import IconClose from '@material-ui/icons/Close'

import imageTributes from 'assets/scan/tributes.png'

import useMutationCharacterTribute from 'src/api/useMutationCharacterTribute'
import { createFC } from 'src/common'
import PageSection from 'src/components/PageSection'
import PageSectionPart from 'src/components/PageSectionPart'


interface Props {
    open: boolean
    onClose: () => void
}


export default createFC(import.meta.url)<Props>(function _({ children, theme, open, onClose }) {
    const [input, setInput] = useState('')
    const [errorMsg, setError] = useState<null | string>(null)

    const handleChange = useCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        const value = e.currentTarget.value
        setInput(value)
        if (!value) {
            setError(null)
        } else if (!/^\d+$/.test(value)) {
            setError('Digits only.')
        } else if (value.length !== 5) {
            setError('Exactly five digits.')
        } else {
            setError(null)
        }
    }, [setInput, setError])

    const [scoreTribute] = useMutationCharacterTribute()

    const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const error = await scoreTribute(Number(input))
        if (error) setError(error.message)
        setInput('')
    }, [setInput, input])

    return (
        <Dialog fullScreen open={open} onClose={onClose}>
            <Toolbar>
                <Grid item xs />
                <IconButton size='medium' onClick={onClose}>
                    <IconClose />
                </IconButton>
            </Toolbar>
            <Grid item xs />
            <img src={imageTributes} />
            <Grid item xs />
            <PageSection title='Enter enemy Tribute'>
                <PageSectionPart>
                    <Typography paragraph>
                        the number on the wristband for 1 Military Point
                    </Typography>
                </PageSectionPart>
            </PageSection>
            <PageSection>
                <PageSectionPart>
                    <Grid
                        component='form'
                        onSubmit={handleSubmit}
                        container
                        alignItems='flex-start'
                        justifyContent='space-between'
                        wrap='nowrap'
                        spacing={2}
                    >
                        <Grid item xs>
                            <TextField
                                autoComplete='off'
                                id='tributes'
                                value={input}
                                onChange={handleChange}
                                placeholder='enter wristband number'
                                error={!!errorMsg}
                                helperText={errorMsg ?? ' '}
                                fullWidth
                            />
                        </Grid>
                        <Grid item>
                            <IconButton type='submit' color='inherit' size='medium' sx={{ backgroundColor: theme.palette.primary.main }} disabled={!!errorMsg}>
                                <IconCheck color='inherit' />
                            </IconButton>
                        </Grid>
                    </Grid>
                </PageSectionPart>
            </PageSection>
            <Grid item xs={12} flexBasis={0}>
                <PageSection>
                    <PageSectionPart>
                        <Button onClick={onClose}>
                            Back
                        </Button>
                    </PageSectionPart>
                </PageSection>
            </Grid>
        </Dialog>
    )
}) /* =============================================================================================================== */
