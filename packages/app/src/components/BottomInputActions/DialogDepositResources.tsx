import React, { useCallback, useContext, useRef } from 'react'

import useMutationResourcesAdd from 'src/api/useMutationResourcesAdd'
import { createFC } from 'src/common'
import { codeToType } from 'src/common/qrCodeToType'
import DialogScanQrCode from 'src/components/DialogScanQrCode'
import PageSection from 'src/components/PageSection'
import Resource from 'src/components/Resource'
import { NotifyContext } from 'src/constants/context'


interface Props {
    open: boolean
    onClose: () => void
}

export default createFC(import.meta.url)<Props>(function _({ open, onClose }) {
    const notify = useContext(NotifyContext)
    const recentScans = useRef(new Set<string>())

    const [resourceAdd] = useMutationResourcesAdd()

    const handleScan = useCallback(async (code: string) => {
        if (recentScans.current.has(code)) return

        notify(<><Resource type={codeToType(code)} /> - {code} scanned..</>)
        recentScans.current.add(code)
        await resourceAdd(code)
    }, [notify])

    return (
        <PageSection>
            <DialogScanQrCode open={open} onClose={onClose} onScan={handleScan} />
        </PageSection>
    )
}) /* =============================================================================================================== */
