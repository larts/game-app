import React from 'react'

import { QueryTeamFeasts, QueryUserName } from '@larts/api'
import { Button, Divider, Grid, Typography } from '@material-ui/core'

import { createFC, useNavigateToHref, useQuery } from 'src/common'
import PageCard from 'src/components/PageCard'
import ProgressFullscreen from 'src/components/ProgressFullscreen'
import { ROUTE, makeRoute } from 'src/constants'


interface Props {
    feast: NonNullable<QueryTeamFeasts.Response['team']>['feastTokens'][0]
    action?: boolean
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, feast, action = true }) {
    const navigateToHref = useNavigateToHref()
    const queryFeasts = useQuery(QueryTeamFeasts)
    const queryUser = useQuery(QueryUserName)

    if (queryFeasts.loading) return <ProgressFullscreen />

    return (
        <PageCard
            title={feast.recipe?.title ?? 'cienasts no orgiem'}
            action={action ? (
                <Button
                    variant='contained'
                    href={makeRoute(ROUTE.Feast, feast.id)}
                    onClick={navigateToHref}
                    disabled={!queryUser.data?.user}
                >
                    Select
                </Button>
            ) : (
                undefined
            )}
        >
            <Grid container spacing={1} sx={{ marginBottom: 1 }} alignItems='flex-start'>
                <Grid item>
                    <Typography>1-3 couples</Typography>
                </Grid>
                <Grid item>
                    <Divider orientation='vertical' sx={{ height: 20 }} />
                </Grid>
                <Grid item>
                    <Typography>Hosts: 1-3 IP</Typography>
                </Grid>
                <Grid item>
                    <Divider orientation='vertical' sx={{ height: 20 }} />
                </Grid>
                <Grid item>
                    <Typography>Guests: 2-6 IP</Typography>
                </Grid>
            </Grid>
            <Typography color='textSecondary'>
                Use during season {feast.season.id} & {feast.season.id + 1} {/* TODO */}
            </Typography>
        </PageCard>
    )
}) /* =============================================================================================================== */
