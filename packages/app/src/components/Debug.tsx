import 'react'

import { createFC } from 'src/common'


interface IProps {
    log?: string
}


export default createFC(import.meta.url)<IProps>(function _({ children, theme, ...props }) {
    console.error(new Error(props.log ?? 'debug component'))
    return null
}) /* ============================================================================================================== */
