import React, { useCallback, useState } from 'react'
import { ReactNode } from 'react'

import { Button, Collapse, Divider, Grid, Paper, Typography } from '@material-ui/core'
import { Description, DescriptionOutlined } from '@material-ui/icons'

import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        width: '100%',
        padding: theme.spacing(1, 1, 0, 1),
        marginBottom: theme.spacing(2),
    },
    details: {
        paddingTop: theme.spacing(2),
    },
    detailsPaper: {
        padding: theme.spacing(2),
        borderRadius: theme.spacing(2),
    },
})

interface Props {
    title: ReactNode
}


export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, title }) {
    const [open, setOpen] = useState(false)

    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])

    return (
        <div className={classes.root}>

            <Grid width='100%' pb={1} container justifyContent='space-between' alignContent='center'>
                <Typography variant='h1' color='textSecondary'>
                    {title}
                </Typography>
                {children && <Button onClick={handleClose} sx={{ display: open ? undefined : 'none' }} startIcon={<Description />}>Rules</Button>}
                {children && <Button onClick={handleOpen} sx={{ display: open ? 'none' : undefined }} startIcon={<DescriptionOutlined />}>Rules</Button>}

            </Grid>
            <Divider  />
            {children && (
                <Collapse classes={{ wrapperInner: classes.details }} in={open}>
                    <Paper className={classes.detailsPaper}>
                        {children}
                    </Paper>
                </Collapse>
            )}
        </div>
    )
}) /* =============================================================================================================== */
