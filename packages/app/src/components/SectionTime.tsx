import React from 'react'

import { CONFIG, QuerySeason } from '@larts/api'
import { CircularProgress, Grid, Typography } from '@material-ui/core'

import { createFC, formatTimeRelative, useQuery } from 'src/common'
import useConfig from 'src/common/useConfig'
import useTickGame from 'src/common/useTickGame'
import useTickSeason from 'src/common/useTickSeason'

import PageSection from './PageSection'
import PageSectionPart from './PageSectionPart'


export default createFC(import.meta.url)(function _({ children, theme }) {
    const querySeason = useQuery(QuerySeason, { fetchPolicy: 'cache-and-network' })
    const tickGame = useTickGame()
    const tickSeason = useTickSeason()

    const gameHasStarted = useConfig(CONFIG.SwitchGameStarted, Number) || false
    const gameStartsAtRaw = useConfig(CONFIG.StartTime)
    const gameStartsAt = gameStartsAtRaw ? new Date(gameStartsAtRaw) : undefined
    const now = new Date()

    if (!gameStartsAt) return (
        <PageSection color='paper'>
            <PageSectionPart>
                <Grid container justifyContent='center'>
                    <CircularProgress variant='indeterminate' />
                </Grid>
            </PageSectionPart>
        </PageSection>
    )

    return (
        <PageSection color='paper'>
            {!gameHasStarted && now < gameStartsAt && (
                <PageSectionPart title={`Time until start of game: ${formatTimeRelative((gameStartsAt.getTime() - now.getTime()) / 1000)}`}>
                    <Typography gutterBottom>Games starts at <em>{gameStartsAt.toString().split(' GMT')[0]}</em></Typography>
                </PageSectionPart>
            )}
            {gameHasStarted && (
                <PageSectionPart title={`${formatTimeRelative(tickSeason)} into Season ${querySeason.data?.season?.id ?? '..'}`}>
                    <Typography gutterBottom>Time passed since start of game: {formatTimeRelative(tickGame)}</Typography>
                </PageSectionPart>
            )}
            {!gameHasStarted && now > gameStartsAt && (
                <PageSectionPart title="Games Over">
                    <Typography gutterBottom>Please go to the Last Battle.</Typography>
                </PageSectionPart>
            )}
            {children}
        </PageSection>
    )
}) /* =============================================================================================================== */
