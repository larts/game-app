import React, { ReactNode } from 'react'

import { Grid, Typography } from '@material-ui/core'
import { SxProps } from '@material-ui/system'
import { Box } from '@material-ui/system'
import classNames from 'classnames'

import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        margin: theme.spacing(2, 0),
        position: 'relative',
    },
    paper: {
        padding: theme.spacing(2, 0),
        backgroundColor: theme.palette.background.paper,
    },
    firstLine: {
        padding: theme.spacing(0, 1),
    },
    overline: {
        margin: theme.spacing(0, 1),
        color: theme.palette.text.secondary,
    },
    title: ({ color = 'default' }: Props) => ({
        color: color === 'default' ? theme.palette.text.secondary : theme.palette.text.primary,
    }),
})

interface Props {
    overline?: ReactNode
    title?: ReactNode
    action?: ReactNode
    color?: 'default' | 'paper'
    sx?: SxProps
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, sx, overline, title, color = 'default', action }) {

    return (
        <Box
            component='section'
            sx={sx}
            className={classNames(classes.root, {
                [classes.paper]: color === 'paper',
            })}
        >
            {(overline || title || action) && (
                <Grid className={classes.firstLine} container wrap='nowrap' justifyContent='space-between'>
                    {(overline || title) && (
                        <Grid item>
                            {overline && (
                                <Typography className={classes.overline} gutterBottom variant='overline'>
                                    {overline}
                                </Typography>
                            )}
                            {title && (
                                <Typography className={classes.title} gutterBottom variant='h2'>
                                    {title}
                                </Typography>
                            )}
                        </Grid>
                    )}
                    {action}
                </Grid>
            )}
            {children}
        </Box>
    )
}) /* =============================================================================================================== */
