import React from 'react'
import { useCallback } from 'react'

import { recordKeys } from '@larts/common'
import { Tab, Tabs } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { IMyTheme, createFC, createStyles, recordEntries } from 'src/common'
import useSubpaths from 'src/common/useSubpaths'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        marginBottom: theme.spacing(2),
    },
})

interface Props {
    tabs: Record<string, string>
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, tabs }) {
    const subpaths = useSubpaths()
    const navigate = useNavigate()

    const handleChange = useCallback((e, tab: string) => navigate(tab === '/' ? '.' : tab), [navigate])

    return (
        <Tabs className={classes.root} value={subpaths[0] ?? recordKeys(tabs)[0]} onChange={handleChange} variant='scrollable' scrollButtons='auto'>
            {recordEntries(tabs).map(([path, label]) => (
                <Tab key={path} sx={{ flexGrow: 1 }} value={path} label={label} />
            ))}
        </Tabs>
    )
}) /* =============================================================================================================== */
