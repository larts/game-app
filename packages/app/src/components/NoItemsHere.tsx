import React from 'react'

import { Typography } from '@material-ui/core'

import { createFC } from 'src/common'
import PageSectionPart from 'src/components/PageSectionPart'


interface Props {
    text?: string
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, text = 'No items here' }) {

    return (
        <PageSectionPart>
            <Typography color='textSecondary'>
                {text}
            </Typography>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
