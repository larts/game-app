import React, { ChangeEvent, FormEvent } from 'react'
import { useState } from 'react'
import { useCallback } from 'react'

import { assertError } from '@larts/common'
import { Grid, IconButton, TextField } from '@material-ui/core'
import IconCheck from '@material-ui/icons/Check'

import { createFC } from 'src/common'

import PageSectionPart from '../PageSectionPart'


interface Props {
    handleScan: (code: string | null) => void
}

export default createFC(import.meta.url)<Props>(function _({ children, theme, handleScan }) {
    const [input, setInput] = useState('')
    const [error, setError] = useState<null | Error>(null)

    const handleChange = useCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        const raw = e.currentTarget.value.replace(/[^A-z0-9]/g, '')
        const isLastDash = e.currentTarget.value[e.currentTarget.value.length - 1] === '-'
        const value = raw.substr(0,2) + (raw.length >= 3 || (raw.length >= 2 && isLastDash) ? '-' : '') + raw.substr(2,2) + (raw.length >= 5 || (raw.length >= 4 && isLastDash) ? '-' : '') + raw.substr(4,2)
        setInput(value)
        setError(null)
    }, [setInput, setError])
    const handleSubmit = useCallback((e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        try {
            handleScan(input)
            setInput(input.slice(0, 3))
        } catch (error) {
            assertError(error, setError)
            setError(error)
        }
    }, [handleScan, setInput, setError, input])

    return (
        <PageSectionPart>
            <Grid
                component='form'
                onSubmit={handleSubmit}
                container
                alignItems='flex-start'
                justifyContent='space-between'
                wrap='nowrap'
                spacing={2}
            >
                <Grid item xs>
                    <TextField
                        autoComplete='off'
                        id='qr'
                        value={input}
                        onChange={handleChange}
                        placeholder='Aa-Bc-Cc'
                        error={!!error}
                        helperText={error ? error.message : 'Dashes will be added automatically.'}
                        fullWidth
                    />
                </Grid>
                <Grid item>
                    <IconButton type='submit' color='inherit' size='medium' sx={{ backgroundColor: theme.palette.primary.main }}>
                        <IconCheck color='inherit' />
                    </IconButton>
                </Grid>
            </Grid>
        </PageSectionPart>
    )
}) /* =============================================================================================================== */
