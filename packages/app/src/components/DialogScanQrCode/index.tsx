import React, { ChangeEvent } from 'react'
import { useState } from 'react'
import { useCallback } from 'react'

import { Button, Dialog, Grid, IconButton, MenuItem, TextField } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'
import QrReader from 'react-qr-scanner-throne-fork'
import { useAsync } from 'react-use'

import audioFile from 'assets/sounds/qr-scan.mp3'

import { IMyTheme, createFC, createStyles } from 'src/common'

import PageSection from '../PageSection'
import PageSectionPart from '../PageSectionPart'

import QrCodeField from './QrCodeField'


const Alert = new Audio(audioFile)

const styles = (theme: IMyTheme) => createStyles({
    root: {
        '& > video': {
            width: '100%',
        },
    },
})

interface Props {
    open: boolean
    onClose: () => void
    onScan: (code: string) => void
}

export default createFC(import.meta.url, styles)<Props>(function _({ children, theme, classes, open, onClose, onScan }) {
    const [devices, setDevices] = useState<MediaDeviceInfo[]>([])
    const [selectedDeviceId, setSelectedDeviceId] = useState<string | undefined>()

    useAsync(async () => {
        if (!open) return  // Dont ask permission before Dialog is open.

        await navigator.mediaDevices.getUserMedia({ video: true })
        const devices = (await navigator.mediaDevices.enumerateDevices()).filter((datum) => datum.kind==='videoinput')
        setDevices(devices)
    }, [open, setDevices])

    const handleScan = useCallback((qrCode: string | null) => {
        if (!qrCode) return

        Alert.play()
        onScan(qrCode)
    }, [onScan])

    const handleCamera = useCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setSelectedDeviceId(e.target.value), [setSelectedDeviceId])

    const deviceId = selectedDeviceId ?? devices[devices.length - 1]?.deviceId ?? ''

    return (
        <Dialog fullScreen open={open} onClose={onClose}>
            <Grid container sx={{ height: '100%' }} direction='column' wrap='nowrap'>
                <Grid item xs={12} flexBasis={0}>
                    <PageSection sx={{ margin: theme.spacing(1, 0) }}>
                        <PageSectionPart>
                            <Grid container spacing={1} wrap='nowrap'>
                                <Grid item xs>
                                    <TextField
                                        id='camera'
                                        label='Camera'
                                        margin='dense'
                                        select
                                        value={deviceId}
                                        onChange={handleCamera}
                                        fullWidth
                                    >
                                        {devices.map((device, i) => (
                                            <MenuItem key={device.deviceId} value={device.deviceId}>{device.label || `Camera No.${i+1}`}</MenuItem>
                                        ))}
                                    </TextField>
                                </Grid>
                                <Grid item>
                                    <IconButton size='medium' onClick={onClose}>
                                        <IconClose />
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </PageSectionPart>
                    </PageSection>
                </Grid>
                <Grid item xs={12} flexGrow={1} container alignItems='center' overflow='hidden'>
                    {deviceId && (
                        <QrReader
                            key={deviceId}  // Force remount, and so to re-chooseDeviceId.
                            className={classes.root}
                            delay={500}
                            // eslint-disable-next-line @typescript-eslint/no-explicit-any
                            chooseDeviceId={() => deviceId as any}  // TODO: fix typings to return deviceId.
                            // onLoad={() => console.log('loaded')}
                            onError={(data) => {console.info('ErrorResult:', data)}}
                            onScan={handleScan}
                        />
                    )}
                </Grid>
                <Grid item xs={12} flexBasis={0}>
                    <PageSection>
                        <QrCodeField handleScan={handleScan} />
                        <PageSectionPart>
                            <Button onClick={onClose}>
                                Back
                            </Button>
                        </PageSectionPart>
                    </PageSection>
                </Grid>
            </Grid>
        </Dialog>
    )
}) /* =============================================================================================================== */
