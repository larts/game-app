import { TextField, Theme } from '@material-ui/core'
import { withStyles } from '@material-ui/styles'


export const TextFieldWhite = withStyles((theme: Theme) => ({
    root: {
        'input': {

            color: theme.palette.primary.contrastText,
        },
        '& label.Mui-focused': {
            color: theme.palette.primary.contrastText,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            color: theme.palette.primary.contrastText,
            '& fieldset': {
                borderColor: theme.palette.primary.contrastText,
            },
            '&:hover fieldset': {
                borderColor: theme.palette.primary.contrastText,
            },
            '&.Mui-focused fieldset': {
                borderColor: theme.palette.primary.contrastText,
            },
        },
    },
}))(TextField)
