Run migrations in the following order:
------

1. `0-limited-db-user.sql` (! Run this only on first time. Not affected by DB drops!!!)
1. `1-tables.sql`
1. `2-foreign-keys.sql`
1. `3a-constants.sql`
1. `3b-constants-qr-codes.sql`
1. `4-updates.sql`

then run any other migrations that contain date in their name in order from oldest to newest

Data owners:
------

- `0-limited-db-user.sql` - Aivis
- `1-tables.sql` dara Aivis
- `2-foreign-keys.sql` dara Aivis
- `3a-constants.sql` dara Kalvis + Rota
- `3b-constants-qr-codes.sql` dara Kalvis
- `4-updates.sql` dara Kalvis + Rota

test-state-1.sql dara Rota

...


to drop database for a fresh start in php admin:
------
1. cilck on tronis
2. select SQL
3. enter `drop DATABASE tronis;`  and go
4. create New named "tronis"
5. import files from /db/migrations in the correct order

to start the app:
-----
run `yarn start` from folder `packages/app`
open `https://app.tronis.lv:8080/dashboard`

to remove auto login:
-----
`src/containers/App/AssertPlayerLogin/index`
comment snippet: Auto-login on dev
