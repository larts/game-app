SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

INSERT INTO `periods` (`id`, `title`, `started`, `ended`, `next_periods_id`) VALUES
    (1, '1. Season', NULL, NULL, 2),
    (2, '2. Season', NULL, NULL, 3),
    (3, '3. Season', NULL, NULL, 4),
    (4, '4. Season', NULL, NULL, NULL),
    (99, '0. Season', NULL, NULL, NULL);

-- First season score target values
INSERT INTO `score_type2period` (`id`, `score_type_id`, `period_id`, `currentTotal`, `target`, `overflow`, `event`) VALUES
    (NULL, '2', '1', '0', '60', '0', '0'),
    (NULL, '3', '1', '0', '50', '0', '0'),
    (NULL, '4', '1', '0', '70', '0', '0');


-- 6 Iron, 6 Wood, 7 Food (1 Honey, 2 Tomatoes, 2 Cucumbers, 1 Raspberry, 1 Mushroom)
INSERT INTO `camps` (`id`, `latitude`, `longitude`, `title`, `display_id`, `resource_id`) VALUES
    -- food
    ( 1, NULL, NULL, 'Rough Berry'        ,  1, 10),
    ( 4, NULL, NULL, 'Savory Cherries'    ,  4, 7),
    ( 7, NULL, NULL, 'Squishy Red Umami'  ,  7, 7),
    (10, NULL, NULL, 'Yeasty Fungi'       , 10, 9),
    (13, NULL, NULL, 'The Green Water'    , 13, 8),
    (16, NULL, NULL, 'Baby Pickles'       , 16, 8),
    (19, NULL, NULL, 'Hexagon Candy'      , 19, 5),
    -- iron
    ( 2, NULL, NULL, 'Adamantium Well'    ,  2, 3),
    ( 5, NULL, NULL, 'Lost Ingot'         ,  5, 3),
    ( 8, NULL, NULL, 'Steal the Steel'    ,  8, 3),
    (11, NULL, NULL, 'Iron Will'          , 11, 3),
    (14, NULL, NULL, 'Groundbreaking'     , 14, 3),
    (17, NULL, NULL, 'Steel of a Legend'  , 17, 3),
    -- wood
    ( 3, NULL, NULL, 'Celluloose'         ,  3, 1),
    ( 6, NULL, NULL, 'Bamboozal'          ,  6, 1),
    ( 9, NULL, NULL, 'Birch Song'         ,  9, 1),
    (12, NULL, NULL, 'Lumber Jacket'      , 24, 1),
    (15, NULL, NULL, 'Maple Stirrup'      , 15, 1),
    (18, NULL, NULL, 'Oakloan'            , 18, 1);

INSERT INTO `expansions` (`id`, `show_code`, `reward_code`, `owner_team_id`, `rewarded_team_id`, `showed`, `rewarded`, `camp_id`, `assignment_period_id`) VALUES
    ( 1, 'MASHES'   ,'PERCENT', NULL, NULL, NULL, NULL, '1', NULL),
    ( 2, 'TESTATE'  ,'FASHION', NULL, NULL, NULL, NULL, '1', NULL),
    ( 3, 'HOTSHOT'  ,'THEATER', NULL, NULL, NULL, NULL, '1', NULL),
    ( 5, 'HOOHAS'   ,'DILEMMA', NULL, NULL, NULL, NULL, '2', NULL),
    ( 6, 'OSTEOMATA','CONTROL', NULL, NULL, NULL, NULL, '2', NULL),
    ( 7, 'STEMMATA' ,'EYEBROW', NULL, NULL, NULL, NULL, '2', NULL),
    ( 9, 'STEMMA'   ,'SURFACE', NULL, NULL, NULL, NULL, '3', NULL),
    (10, 'SMOOTHE'  ,'TRAGEDY', NULL, NULL, NULL, NULL, '3', NULL),
    (11, 'HEATHS'   ,'ADVISER', NULL, NULL, NULL, NULL, '3', NULL),
    (13, 'TATTOO'   ,'REVENGE', NULL, NULL, NULL, NULL, '4', NULL),
    (14, 'MESHES'   ,'COLLEGE', NULL, NULL, NULL, NULL, '4', NULL),
    (15, 'HEMOSTATS','PUDDING', NULL, NULL, NULL, NULL, '4', NULL),
    (17, 'TESTATES' ,'SUPPORT', NULL, NULL, NULL, NULL, '5', NULL),
    (18, 'TOMATOE'  ,'CERTAIN', NULL, NULL, NULL, NULL, '5', NULL),
    (19, 'SHAMES'   ,'SECTION', NULL, NULL, NULL, NULL, '5', NULL),
    (21, 'HOTSEAT'  ,'GLACIER', NULL, NULL, NULL, NULL, '6', NULL),
    (22, 'HOTSHOE'  ,'RELEASE', NULL, NULL, NULL, NULL, '6', NULL),
    (23, 'SHEATH'   ,'FEATHER', NULL, NULL, NULL, NULL, '6', NULL),
    (25, 'ASTHMA'   ,'WRESTLE', NULL, NULL, NULL, NULL, '7', NULL),
    (26, 'ESTATES'  ,'HOLIDAY', NULL, NULL, NULL, NULL, '7', NULL),
    (27, 'ETHOSES'  ,'SCRATCH', NULL, NULL, NULL, NULL, '7', NULL),
    (29, 'MOTTES'   ,'DESPAIR', NULL, NULL, NULL, NULL, '8', NULL),
    (30, 'HAMATES'  ,'COMPACT', NULL, NULL, NULL, NULL, '8', NULL),
    (31, 'SOOTHES'  ,'SPEAKER', NULL, NULL, NULL, NULL, '8', NULL),
    (33, 'MAESTOSO' ,'DICTATE', NULL, NULL, NULL, NULL, '9', NULL),
    (34, 'STEATOMAS','DIAMOND', NULL, NULL, NULL, NULL, '9', NULL),
    (35, 'SMOOTH'   ,'VARIANT', NULL, NULL, NULL, NULL, '9', NULL),
    (36, 'MOTMOTS'  ,'SOCIETY', NULL, NULL, NULL, NULL, '10', NULL),
    (37, 'STOMATA'  ,'EXPLOIT', NULL, NULL, NULL, NULL, '10', NULL),
    (38, 'HOMMOS'   ,'SPEAKER', NULL, NULL, NULL, NULL, '10', NULL),
    (40, 'OSMOSE'   ,'MORNING', NULL, NULL, NULL, NULL, '11', NULL),
    (41, 'MESOSOME' ,'DEFICIT', NULL, NULL, NULL, NULL, '11', NULL),
    (42, 'TSAMMAS'  ,'MONARCH', NULL, NULL, NULL, NULL, '11', NULL),
    (44, 'SEAMATE'  ,'MINIMUM', NULL, NULL, NULL, NULL, '12', NULL),
    (45, 'HEMOSTAT' ,'ENHANCE', NULL, NULL, NULL, NULL, '12', NULL),
    (46, 'TOMATOES' ,'TRAINER', NULL, NULL, NULL, NULL, '12', NULL),
    (48, 'TATTOOS'  ,'PERSIST', NULL, NULL, NULL, NULL, '13', NULL),
    (49, 'TEASETS'  ,'OPINION', NULL, NULL, NULL, NULL, '13', NULL),
    (50, 'EMOTES'   ,'REALITY', NULL, NULL, NULL, NULL, '13', NULL),
    (52, 'SMOOSH'   ,'BARGAIN', NULL, NULL, NULL, NULL, '14', NULL),
    (53, 'HOMEOSES' ,'WARRANT', NULL, NULL, NULL, NULL, '14', NULL),
    (54, 'HOTSHOTS' ,'INCLUDE', NULL, NULL, NULL, NULL, '14', NULL),
    (56, 'TOESHOE'  ,'HABITAT', NULL, NULL, NULL, NULL, '15', NULL),
    (57, 'HEMATOMA' ,'SOLDIER', NULL, NULL, NULL, NULL, '15', NULL),
    (58, 'SMOOTHEST','SWEATER', NULL, NULL, NULL, NULL, '15', NULL),
    (60, 'SHEATHES' ,'LECTURE', NULL, NULL, NULL, NULL, '16', NULL),
    (61, 'MOTTOES'  ,'STADIUM', NULL, NULL, NULL, NULL, '16', NULL),
    (62, 'TEAMMATE' ,'SYMPTOM', NULL, NULL, NULL, NULL, '16', NULL),
    (70, 'MATSAHS'  ,'CLARITY', NULL, NULL, NULL, NULL, '17', NULL),
    (71, 'HASHES'   ,'CHARITY', NULL, NULL, NULL, NULL, '17', NULL),
    (72, 'MOSTEST'  ,'FORMULA', NULL, NULL, NULL, NULL, '17', NULL),
    (74, 'MAMEES'   ,'HOSTILE', NULL, NULL, NULL, NULL, '18', NULL),
    (75, 'SHEATHE'  ,'MESSAGE', NULL, NULL, NULL, NULL, '18', NULL),
    (77, 'THEMES'   ,'CONTENT', NULL, NULL, NULL, NULL, '18', NULL),
    (78, 'OSTEOMA'  ,'EXPLOIT', NULL, NULL, NULL, NULL, '19', NULL),
    (79, 'SHAMMASH' ,'SAUSAGE', NULL, NULL, NULL, NULL, '19', NULL),
    (81, 'SOMATA'   ,'PEASANT', NULL, NULL, NULL, NULL, '19', NULL);


INSERT INTO `castles` (`id`, `title`, `latitude`, `longitude`, `team_id`, `color`) VALUES
    ('1', 'D akmeņi', NULL, NULL, NULL, NULL),
    ('2', 'R kalns', NULL, NULL, NULL, NULL),
    ('3', 'A izcirtuma stūris', NULL, NULL, NULL, NULL),
    ('4', 'Z krustojums', NULL, NULL, NULL, NULL),
    ('5', 'ZA izcirtuma stūris', NULL, NULL, NULL, NULL);

INSERT INTO `teams` (`id`, `castle_id`, `title`, `color`, `flag_status_id`, `available_eq`, `flag_registration_id`) VALUES
    (1, '1', 'Raccoons', 'red', 3, NULL, NULL),
    (2, '2', 'Lynx', 'orange', 3, NULL, NULL),
    (3, '3', 'Bears', 'purple', 3, NULL, NULL),
    (4, '4', 'Wolves', 'blue', 3, NULL, NULL),
    (5, '5', 'Aligators', 'gold', 3, NULL, NULL);

INSERT INTO `workshops` (`id`, `team_id`, `kit_type_id`, `type_id`, `exist`, `is_active`) VALUES
    ('1', '1', '1', '1', '1', '0'),
    ('2', '1', '2', '4', '1', '0'),
    ('3', '1', '3', '7', '1', '0'),
    ('4', '2', '1', '1', '1', '0'),
    ('5', '2', '2', '4', '1', '0'),
    ('6', '2', '3', '7', '1', '0'),
    ('7', '3', '1', '1', '1', '0'),
    ('8', '3', '2', '4', '1', '0'),
    ('9', '3', '3', '7', '1', '0'),
    ('10', '4', '1', '1', '1', '0'),
    ('11', '4', '2', '4', '1', '0'),
    ('12', '4', '3', '7', '1', '0'),
    ('13', '5', '1', '1', '1', '0'),
    ('14', '5', '2', '4', '1', '0'),
    ('15', '5', '3', '7', '1', '0');

INSERT INTO `workshop_kit` (`id`, `type_id`, `team_id`, `workshop_id`) VALUES
    (1, 1, 1, 1),
    (2, 2, 1, 2),
    (3, 3, 1, 3),
    (4, 1, 2, 4),
    (5, 2, 2, 5),
    (6, 3, 2, 6),
    (7, 1, 3, 7),
    (8, 2, 3, 8),
    (9, 3, 3, 9),
    (10, 1, 4, 10),
    (11, 2, 4, 11),
    (12, 3, 4, 12),
    (13, 1, 5, 13),
    (14, 2, 5, 14),
    (15, 3, 5, 15),
    (19, 1, NULL, NULL),
    (20, 2, NULL, NULL),
    (21, 3, NULL, NULL);

INSERT INTO `resource_total` (`id`, `team_id`, `resource_type_id`, `total_amount`, `available_amount`, `locked_amount`, `spent_amount`) VALUES
    (NULL, 1, 1, 6, 6, 0, 0),
    (NULL, 1, 2, 6, 6, 0, 0),
    (NULL, 1, 3, 6, 6, 0, 0),
    (NULL, 1, 4, 6, 6, 0, 0),
    (NULL, 1, 5, 0, 0, 0, 0),
    (NULL, 1, 6, 6, 6, 0, 0),
    (NULL, 1, 7, 0, 0, 0, 0),
    (NULL, 1, 8, 0, 0, 0, 0),
    (NULL, 1, 9, 0, 0, 0, 0),
    (NULL, 1, 10, 0, 0, 0, 0),
    (NULL, 2, 1, 6, 6, 0, 0),
    (NULL, 2, 2, 6, 6, 0, 0),
    (NULL, 2, 3, 6, 6, 0, 0),
    (NULL, 2, 4, 6, 6, 0, 0),
    (NULL, 2, 5, 0, 0, 0, 0),
    (NULL, 2, 6, 6, 6, 0, 0),
    (NULL, 2, 7, 0, 0, 0, 0),
    (NULL, 2, 8, 0, 0, 0, 0),
    (NULL, 2, 9, 0, 0, 0, 0),
    (NULL, 2, 10, 0, 0, 0, 0),
    (NULL, 3, 1, 6, 6, 0, 0),
    (NULL, 3, 2, 6, 6, 0, 0),
    (NULL, 3, 3, 6, 6, 0, 0),
    (NULL, 3, 4, 6, 6, 0, 0),
    (NULL, 3, 5, 0, 0, 0, 0),
    (NULL, 3, 6, 6, 6, 0, 0),
    (NULL, 3, 7, 0, 0, 0, 0),
    (NULL, 3, 8, 0, 0, 0, 0),
    (NULL, 3, 9, 0, 0, 0, 0),
    (NULL, 3, 10, 0, 0, 0, 0),
    (NULL, 4, 1, 6, 6, 0, 0),
    (NULL, 4, 2, 6, 6, 0, 0),
    (NULL, 4, 3, 6, 6, 0, 0),
    (NULL, 4, 4, 6, 6, 0, 0),
    (NULL, 4, 5, 0, 0, 0, 0),
    (NULL, 4, 6, 6, 6, 0, 0),
    (NULL, 4, 7, 0, 0, 0, 0),
    (NULL, 4, 8, 0, 0, 0, 0),
    (NULL, 4, 9, 0, 0, 0, 0),
    (NULL, 4, 10, 0, 0, 0, 0),
    (NULL, 5, 1, 6, 6, 0, 0),
    (NULL, 5, 2, 6, 6, 0, 0),
    (NULL, 5, 3, 6, 6, 0, 0),
    (NULL, 5, 4, 6, 6, 0, 0),
    (NULL, 5, 5, 0, 0, 0, 0),
    (NULL, 5, 6, 6, 6, 0, 0),
    (NULL, 5, 7, 0, 0, 0, 0),
    (NULL, 5, 8, 0, 0, 0, 0),
    (NULL, 5, 9, 0, 0, 0, 0),
    (NULL, 5, 10, 0, 0, 0, 0);

INSERT INTO `recipe` (`id`, `title`, `created_first_id`, `score_reason_id`) VALUES
    (21, NULL, NULL, 32),
    (22, NULL, NULL, 32),
    (23, NULL, NULL, 32),
    (24, NULL, NULL, 32),
    (25, NULL, NULL, 32),
    (26, NULL, NULL, 32),
    (27, NULL, NULL, 32),
    (28, NULL, NULL, 32),
    (29, NULL, NULL, 32),
    (30, NULL, NULL, 32),
    (31, NULL, NULL, 32),
    (32, NULL, NULL, 32),
    (33, NULL, NULL, 32),
    (34, NULL, NULL, 32),
    (35, NULL, NULL, 32),
    (36, NULL, NULL, 33),
    (37, NULL, NULL, 33),
    (38, NULL, NULL, 33),
    (39, NULL, NULL, 33),
    (40, NULL, NULL, 33),
    (41, NULL, NULL, 33),
    (42, NULL, NULL, 34);

-- 15 + 6 + 1
INSERT INTO `recipe2resource` (`recipe_id`, `resource_id`) VALUES
    (21, 5), (21, 6), (21, 7), (21, 8),
    (22, 5), (22, 6), (22, 7),          (22, 9),
    (23, 5), (23, 6), (23, 7),                   (23, 10),
    (24, 5), (24, 6),          (24, 8), (24, 9),
    (25, 5), (25, 6),          (25, 8),          (25, 10),
    (26, 5), (26, 6),                   (26, 9), (26, 10),
    (27, 5),          (27, 7), (27, 8), (27, 9),
    (28, 5),          (28, 7), (28, 8),          (28, 10),
    (29, 5),          (29, 7),          (29, 9), (29, 10),
    (30, 5),                   (30, 8), (30, 9), (30, 10),
            (31, 6), (31, 7), (31, 8), (31, 9),
            (32, 6), (32, 7), (32, 8),          (32, 10),
            (33, 6), (33, 7),          (33, 9), (33, 10),
            (34, 6),          (34, 8), (34, 9), (34, 10),
                    (35, 7), (35, 8), (35, 9), (35, 10),
    (36, 5), (36, 6), (36, 7), (36, 8), (36, 9),
    (37, 5), (37, 6), (37, 7), (37, 8),          (37, 10),
    (38, 5), (38, 6), (38, 7),          (38, 9), (38, 10),
    (39, 5), (39, 6),          (39, 8), (39, 9), (39, 10),
    (40, 5),          (40, 7), (40, 8), (40, 9), (40, 10),
            (41, 6), (41, 7), (41, 8), (41, 9), (41, 10),
    (42, 5), (42, 6), (42, 7), (42, 8), (42, 9), (42, 10);


INSERT INTO `feast_token` (`id`, `created`, `team_id`, `is_used`, `used_at`, `size`, `period_id`) VALUES
    (NULL, 1, '1', '0', NULL, '3', 1),
    (NULL, 1, '2', '0', NULL, '3', 1),
    (NULL, 1, '3', '0', NULL, '3', 1),
    (NULL, 1, '4', '0', NULL, '3', 1),
    (NULL, 1, '5', '0', NULL, '3', 1);

-- Admins --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Rota Kalniņa'             , 'Rota'       , NULL, NULL, CURRENT_TIMESTAMP, 'rota@bruna.lv'                      , '1', NULL, NULL, '0', '', 'female'),
    (NULL, 'Kalvis Kalniņš'           , 'Kalvis K'   , NULL, NULL, CURRENT_TIMESTAMP, 'kalvis@kalvis.lv'                   , '1', NULL, NULL, '0', '', 'male'),
    (NULL, 'Aivis Lisovskis'          , 'Aivis'      , NULL, NULL, CURRENT_TIMESTAMP, 'aivislisovskis@gmail.com'           , '1', '$2y$10$4n8DDmMtmBxnSR3sTqkIm.omY6iehSl0qxDtysLOkWg91Ca9Sm5Ti', NULL, '0', '', 'male'),
    (NULL, 'Andrievs Auziņš'          , 'Andrievs'   , NULL, NULL, CURRENT_TIMESTAMP, 'andrievs.auseklis.auzins@gmail.com' , '1', NULL, NULL, '0', '', 'male'),
    (NULL, 'Raivis Nigolass'          , 'Raivis'     , NULL, NULL, CURRENT_TIMESTAMP, 'nigolassr@gmail.com'                , '1', NULL, NULL, '0', '', 'male'),
    (NULL, 'Testa Lietotājs'          , 'Test'       , NULL, NULL, CURRENT_TIMESTAMP, 'test'                               , '1', '$2y$10$4n8DDmMtmBxnSR3sTqkIm.omY6iehSl0qxDtysLOkWg91Ca9Sm5Ti', NULL, '0', '', 'male');
-- Raccoons --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Evija Fokrote'           , 'Evija F'     , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Emīls Kalderauskis'      , 'Emīls'       , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Evija Zandberga'         , 'Sindra'      , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Arina Solovjova'         , 'Arina'       , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Roberts Ivanovs'         , 'Roberts'     , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Ronalds Palacis'         , 'Ronalds'     , '1' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male');
-- Lynx --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Jēkabs Fridmanis'          , 'Jēkabs'    , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Alise Frīdmane'            , 'Alise'     , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Artis Galvanovskis'        , 'Artis'     , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Oskars Sjomkāns'           , 'Oskars'    , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Jānis Plūme'               , 'Jānis'     , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Andris Locāns'             , 'Andris'    , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Ernests Tomass Auziņš'     , 'Ernests'   , '2' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Toms Balabāns'             , 'Toms'      , '2' , NULL, CURRENT_TIMESTAMP, NULL, '1', NULL, NULL, '0', '', 'male');
-- Bears --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Ņikita Vinokurovs'         , 'Ņikita'       , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Emīlija Vija Ploriņa'      , 'Emīlija'      , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Valts Liepiņš'             , 'Valts'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Magda Buša'                , 'Magda'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Klāvs Dzelzs-Vaivars'      , 'Klāvs'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Liene Millere'             , 'Liene'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Luīze Stabiņģe'            , 'Luīze'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Jānis Muižnieks'           , 'Muižnieks'    , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Rihards Vārna'             , 'Rihards'      , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Kārlis Vēbers'             , 'Kārlis'       , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Santa Klodāne'             , 'Santa'        , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Ance Zveja'                , 'Ance'         , '3' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female');
-- Wolves --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Evija Matisone'          , 'Evija'       , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Kristaps Baumanis'       , 'Kristaps'    , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Reinis Jānis Kozuliņš'   , 'Reinis'      , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Sabīne Erte'             , 'Sabīne'      , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Santa Šakeļa'            , 'Santa'       , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Marita Vītola'           , 'Marita'      , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Kristaps Krūtainis'      , 'Aksels'      , '4' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male');

-- Aligators --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Artūrs Grebstelis'           , 'Artūrs'         , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Nikolajs Karnejevs'          , 'Amiga'          , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Edgars Barkāns'              , 'Edgars'         , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Виктор Харламов'             , 'Виктор'         , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Anatolijs Kiselevs'          , 'Anatolijs'      , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Константин Семиедов'         , 'Константин'     , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Анастасия Донец'             , 'Анастасия'      , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female'),
    (NULL, 'Михаил Поеглис'              , 'Михаил'         , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Раинер Анджанс'              , 'Раинер'         , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Алексий Никишин'             , 'Алексий'        , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Konstantinas Gorskovas'      , 'Konstantinas'   , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male'),
    (NULL, 'Vitālijs Belihs'             , 'Vitālijs'       , '5' , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'male');
-- Factionless --
INSERT INTO `users` (`id`, `name`, `display_name`, `team_id`, `qr_id`, `created`, `email`, `is_admin`, `admin_password`, `qr_code`, `equipment_points`, `auth_token`, `sex`) VALUES
    (NULL, 'Ананастя Ананасова'             , 'Ананастя'       , NULL , NULL, CURRENT_TIMESTAMP, NULL, '0', NULL, NULL, '0', '', 'female');


SET FOREIGN_KEY_CHECKS=1;
COMMIT;
