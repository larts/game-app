START TRANSACTION;

--
-- Constraints for table `camps`
--
ALTER TABLE `camps`
    ADD CONSTRAINT `camps_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resource_types`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `castles`
--
ALTER TABLE `castles`
    ADD CONSTRAINT `castles_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `characters`
--
ALTER TABLE `characters`
    ADD CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`owner_team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `characters_ibfk_2` FOREIGN KEY (`owner_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `characters_ibfk_3` FOREIGN KEY (`tribute_team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `characters_ibfk_4` FOREIGN KEY (`tribute_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `characters_ibfk_5` FOREIGN KEY (`tribute_period_id`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `character_kills`
--
ALTER TABLE `character_kills`
    ADD CONSTRAINT `character_kills_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `character_kills_ibfk_2` FOREIGN KEY (`scored_team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
    ADD CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `equipment_allocated`
--
ALTER TABLE `equipment_allocated`
    ADD CONSTRAINT `equipment_allocated_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `equipment_allocated_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `equipment_bought`
--
ALTER TABLE `equipment_bought`
    ADD CONSTRAINT `equipment_bought_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
    ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `events_votes`
--
ALTER TABLE `events_votes`
    ADD CONSTRAINT `events_votes_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `events_votes_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `events_votes_ibfk_3` FOREIGN KEY (`weight_id`) REFERENCES `events_weights` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `events_votes_ibfk_4` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `flag_status`
--
ALTER TABLE `flag_status`
    ADD CONSTRAINT `flag_status_ibfk_1` FOREIGN KEY (`score_reason_id`) REFERENCES `score_reasons` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `flag_status_ibfk_2` FOREIGN KEY (`self_score_reason_id`) REFERENCES `score_reasons` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `flag_status_log`
--
ALTER TABLE `flag_status_log`
    ADD CONSTRAINT `flag_status_log_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `flag_status_log_ibfk_2` FOREIGN KEY (`changer_team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `flag_status_log_ibfk_3` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `flag_status_log_ibfk_4` FOREIGN KEY (`current_status_id`) REFERENCES `flag_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `qr_codes`
--
ALTER TABLE `qr_codes`
    ADD CONSTRAINT `qr_codes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `qr_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `qr_codes_ibfk_2` FOREIGN KEY (`last_scan_id`) REFERENCES `qr_scans` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `qr_codes_ibfk_3` FOREIGN KEY (`owner_team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `qr_scans`
--
ALTER TABLE `qr_scans`
    ADD CONSTRAINT `qr_scans_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `qr_scans_ibfk_2` FOREIGN KEY (`scanner_team_id`) REFERENCES `teams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `qr_scans_ibfk_3` FOREIGN KEY (`qr_id`) REFERENCES `qr_codes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `qr_scans_ibfk_4` FOREIGN KEY (`scanner_user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `qr_scans_ibfk_5` FOREIGN KEY (`type_id`) REFERENCES `qr_scan_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `qr_scan_types`
--
ALTER TABLE `qr_scan_types`
    ADD CONSTRAINT `qr_scan_types_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `qr_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipe`
--
ALTER TABLE `recipe`
    ADD CONSTRAINT `recipe_ibfk_1` FOREIGN KEY (`score_reason_id`) REFERENCES `score_reasons` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `recipe_ibfk_2` FOREIGN KEY (`created_first_id`) REFERENCES `recipe_log` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
--
-- Constraints for table `recipe2resource`
--
ALTER TABLE `recipe2resource`
    ADD CONSTRAINT `recipe2resource_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resource_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipe2resource_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipe_log`
--
ALTER TABLE `recipe_log`
    ADD CONSTRAINT `recipe_log_ibfk_1` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `recipe_log_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expansions`
--
ALTER TABLE `expansions`
    ADD CONSTRAINT `expansions_ibfk_1` FOREIGN KEY (`rewarded_team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `expansions_ibfk_2` FOREIGN KEY (`owner_team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `resources`
--
ALTER TABLE `resources`
    ADD CONSTRAINT `resources_ibfk_1` FOREIGN KEY (`camp_id`) REFERENCES `camps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resources_ibfk_2` FOREIGN KEY (`last_scan_id`) REFERENCES `qr_scans` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `resources_ibfk_3` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `resources_ibfk_4` FOREIGN KEY (`type_id`) REFERENCES `resource_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `resources_ibfk_5` FOREIGN KEY (`qr_id`) REFERENCES `qr_codes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `resource_log`
--
ALTER TABLE `resource_log`
    ADD CONSTRAINT `resource_log_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resource_log_ibfk_2` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resource_log_ibfk_3` FOREIGN KEY (`action_id`) REFERENCES `resource_action` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `resource_total`
--
ALTER TABLE `resource_total`
    ADD CONSTRAINT `resource_total_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resource_total_ibfk_2` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `scores`
--
ALTER TABLE `scores`
    ADD CONSTRAINT `scores_ibfk_1` FOREIGN KEY (`reason_id`) REFERENCES `score_reasons` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `scores_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scores_ibfk_3` FOREIGN KEY (`type_id`) REFERENCES `score_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `scores_ibfk_4` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `score_reasons`
--
ALTER TABLE `score_reasons`
    ADD CONSTRAINT `score_reasons_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `score_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `score_totals`
--
ALTER TABLE `score_totals`
    ADD CONSTRAINT `score_totals_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `score_totals_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `score_totals_ibfk_3` FOREIGN KEY (`type_id`) REFERENCES `score_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `score_type2period`
--
ALTER TABLE `score_type2period`
    ADD CONSTRAINT `score_type2period_ibfk_1` FOREIGN KEY (`score_type_id`) REFERENCES `score_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `score_type2period_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teams`
--
ALTER TABLE `teams`
    ADD CONSTRAINT `teams_ibfk_1` FOREIGN KEY (`flag_status_id`) REFERENCES `flag_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `teams_ibfk_2` FOREIGN KEY (`flag_registration_id`) REFERENCES `flag_status_log` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
    ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`qr_id`) REFERENCES `qr_codes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `workshop_activations`
--
ALTER TABLE `workshop_activations`
    ADD CONSTRAINT `workshop_activations_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `workshop_size` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `workshop_activations_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `workshop_size`
--
ALTER TABLE `workshop_size`
    ADD CONSTRAINT `workshop_size_ibfk_1` FOREIGN KEY (`score_reason_id`) REFERENCES `score_reasons` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

-- new / migrations 2021.08.03

ALTER TABLE `characters` ADD FOREIGN KEY (`death_season_id`) REFERENCES `periods`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `expansions` ADD FOREIGN KEY (`assignment_period_id`) REFERENCES `periods`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `feast_token` ADD FOREIGN KEY (`period_id`) REFERENCES `periods`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `feast_token` ADD FOREIGN KEY (`recipe_id`) REFERENCES `recipe`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `teams` ADD FOREIGN KEY (`castle_id`) REFERENCES `castles`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
