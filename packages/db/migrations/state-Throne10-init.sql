SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

UPDATE `settings` SET `value`='2023-08-12T07:00:00Z' WHERE `code` = 'start-time';

INSERT INTO `periods` (`id`, `title`, `next_periods_id`) VALUES
    (1, '1. Season', 2),
    (2, '2. Season', 3),
    (3, '3. Season', 4),
    (4, '4. Season', 5),
    (5, '5. Season', NULL),
    (99, '0. Season', NULL);

-- First season score target values
INSERT INTO `score_type2period` (`period_id`, `score_type_id`, `target`, `currentTotal`, `overflow`, `event`) VALUES
    ('1', '2', '40', '0', '0', '0'),
    ('1', '3', '20', '0', '0', '0'),
    ('1', '4', '30', '0', '0', '0');


-- 6 Iron, 6 Wood, 7 Food (1 Honey, 2 Tomatoes, 2 Cucumbers, 1 Raspberry, 1 Mushroom)
INSERT INTO `camps` (`id`, `longitude`, `latitude`, `title`, `display_id`, `resource_id`) VALUES
    -- food
    ( 1, 24.965000, 56.926000, 'Rough Berry'        ,  1, 10),
    ( 4, 24.965000, 56.926000, 'Squishy Red Umami'  ,  4, 7),
    ( 7, 24.965000, 56.926000, 'Savory Cherries'    ,  7, 7),
    (10, 24.965000, 56.926000, 'Yeasty Fungi'       , 10, 9),
    (13, 24.965000, 56.926000, 'The Green Water'    , 13, 8),
    (16, 24.965000, 56.926000, 'Baby Pickles'       , 16, 8),
    (19, 24.965000, 56.926000, 'Soap Flavour'       , 19, 6),
    -- iron
    ( 2, 24.965000, 56.926000, 'Ingot Foundation'   ,  2, 3),
    ( 5, 24.965000, 56.926000, 'Steel of a Legend'  ,  5, 3),
    ( 8, 24.965000, 56.926000, 'Adamantium Well'    ,  8, 3),
    (11, 24.965000, 56.926000, 'Steal the Steel'    , 11, 3),
    -- wood
    ( 3, 24.965000, 56.926000, 'Pine Line'          ,  3, 1),
    ( 6, 24.965000, 56.926000, 'Birch Song'         ,  6, 1),
    ( 9, 24.965000, 56.926000, 'Oakloan'            ,  9, 1),
    (12, 24.965000, 56.926000, 'Maple Stirrup'      , 12, 1);

INSERT INTO `expansions` (`show_code`, `reward_code`, `camp_id`) VALUES
    ('MASHES'   , 'PERCENT'  , '1'),
    ('TESTATE'  , 'FASHION'  , '1'),
    ('HOTSHOT'  , 'THEATER'  , '1'),
    ('SHOTS'    , 'TREASURE' , '1'),
    ('HOOHAS'   , 'DILEMMA'  , '2'),
    ('OSTEOMATA', 'CONTROL'  , '2'),
    ('STEMMATA' , 'EYEBROW'  , '2'),
    ('HAMTOE'   , 'CHERRY'   , '2'),
    ('STEMMA'   , 'SURFACE'  , '3'),
    ('SMOOTHE'  , 'TRAGEDY'  , '3'),
    ('HEATHS'   , 'ADVISER'  , '3'),
    ('MOSTHOE'  , 'PECKISH'  , '3'),
    ('TATTOO'   , 'REVENGE'  , '4'),
    ('MESHES'   , 'COLLEGE'  , '4'),
    ('HEMOSTATS', 'PUDDING'  , '4'),
    ('METHOSE'  , 'PLATIPUS' , '4'),
    ('TESTATES' , 'SUPPORT'  , '5'),
    ('TOMATOE'  , 'CERTAIN'  , '5'),
    ('SHAMES'   , 'SECTION'  , '5'),
    ('ATMOST'   , 'SPECIAL'  , '5'),
    ('HOTSEAT'  , 'GLACIER'  , '6'),
    ('HOTSHOE'  , 'RELEASE'  , '6'),
    ('SHEATH'   , 'FEATHER'  , '6'),
    ('OMAMA'    , 'STATUE'   , '6'),
    ('ASTHMA'   , 'WRESTLE'  , '7'),
    ('ESTATES'  , 'HOLIDAY'  , '7'),
    ('ETHOSES'  , 'SCRATCH'  , '7'),
    ('ATHOME'   , 'MIRROR'   , '7'),
    ('MOTTES'   , 'DESPAIR'  , '8'),
    ('HAMATES'  , 'COMPACT'  , '8'),
    ('SOOTHES'  , 'SPEAKER'  , '8'),
    ('EATSOME'  , 'PIZZA'    , '8'),
    ('MAESTOSO' , 'DICTATE'  , '9'),
    ('STEATOMAS', 'DIAMOND'  , '9'),
    ('SMOOTH'   , 'VARIANT'  , '9'),
    ('MOSTHEAT' , 'PORCELAIN', '9'),
    ('MOTMOTS'  , 'SOCIETY'  , '10'),
    ('STOMATA'  , 'EXPLOIT'  , '10'),
    ('HOMMOS'   , 'SPEAKER'  , '10'),
    ('SHAMOS'   , 'WINDOW'   , '10'),
    ('OSMOSE'   , 'MORNING'  , '11'),
    ('MESOSOME' , 'DEFICIT'  , '11'),
    ('TSAMMAS'  , 'MONARCH'  , '11'),
    ('HOTMEAT'  , 'SCREEN'   , '11'),
    ('SEAMATE'  , 'MINIMUM'  , '12'),
    ('HEMOSTAT' , 'ENHANCE'  , '12'),
    ('TOMATOES' , 'TRAINER'  , '12'),
    ('HOSTSHOE' , 'PURIFY'   , '12'),
    ('TATTOOS'  , 'PERSIST'  , '13'),
    ('TEASETS'  , 'OPINION'  , '13'),
    ('EMOTES'   , 'REALITY'  , '13'),
    ('MOSEES'   , 'TARNISH'  , '13'),
    ('SHEATHES' , 'LECTURE'  , '16'),
    ('MOTTOES'  , 'STADIUM'  , '16'),
    ('TEAMMATE' , 'SYMPTOM'  , '16'),
    ('SHOEHAT'  , 'PANIC'    , '16'),
    ('MAMEES'   , 'HOSTILE'  , '19'),
    ('SHEATHE'  , 'MESSAGE'  , '19'),
    ('THEMES'   , 'CONTENT'  , '19'),
    ('THEMOST'  , 'STABLE'   , '19');

INSERT INTO `castles` (`id`, `longitude`, `latitude`, `title`) VALUES
    ('1', 24.965000, 56.926000, 'North'),
    ('2', 24.965000, 56.926000, 'South-East'),
    ('3', 24.965000, 56.926000, 'South-West');

INSERT INTO `teams` (`id`, `title`, `color`, `flag_status_id`, `available_eq`) VALUES
    (1, 'Snakes'    , 'green' , 3, 2),
    (2, 'Bears'     , 'purple', 3, 2),
    (3, 'Lynx'      , 'gold'  , 3, 2);
-- assign a flag to each team
UPDATE `qr_codes` SET `owner_team_id` = '1' WHERE `qr_codes`.`qrid` = 'FL-33-ee';
UPDATE `qr_codes` SET `owner_team_id` = '2' WHERE `qr_codes`.`qrid` = 'FL-32-27';
UPDATE `qr_codes` SET `owner_team_id` = '3' WHERE `qr_codes`.`qrid` = 'FL-38-8x';

INSERT INTO `resource_total` (`team_id`, `resource_type_id`, `total_amount`, `available_amount`, `locked_amount`, `spent_amount`) VALUES
    (1, 1, 2, 2, 0, 0),
    (1, 2, 2, 2, 0, 0),
    (1, 3, 2, 2, 0, 0),
    (1, 4, 2, 2, 0, 0),
    (1, 5, 2, 2, 0, 0),
    (1, 6, 0, 0, 0, 0),
    (1, 7, 0, 0, 0, 0),
    (1, 8, 0, 0, 0, 0),
    (1, 9, 0, 0, 0, 0),
    (1, 10, 0, 0, 0, 0),
    (2, 1, 2, 2, 0, 0),
    (2, 2, 2, 2, 0, 0),
    (2, 3, 2, 2, 0, 0),
    (2, 4, 2, 2, 0, 0),
    (2, 5, 2, 2, 0, 0),
    (2, 6, 0, 0, 0, 0),
    (2, 7, 0, 0, 0, 0),
    (2, 8, 0, 0, 0, 0),
    (2, 9, 0, 0, 0, 0),
    (2, 10, 0, 0, 0, 0),
    (3, 1, 2, 2, 0, 0),
    (3, 2, 2, 2, 0, 0),
    (3, 3, 2, 2, 0, 0),
    (3, 4, 2, 2, 0, 0),
    (3, 5, 2, 2, 0, 0),
    (3, 6, 0, 0, 0, 0),
    (3, 7, 0, 0, 0, 0),
    (3, 8, 0, 0, 0, 0),
    (3, 9, 0, 0, 0, 0),
    (3, 10, 0, 0, 0, 0);

INSERT INTO `recipe` (`id`, `title`, `created_first_id`, `score_reason_id`) VALUES
    (21, NULL, NULL, 32),
    (22, NULL, NULL, 32),
    (23, NULL, NULL, 32),
    (24, NULL, NULL, 32),
    (25, NULL, NULL, 32),
    (26, NULL, NULL, 32),
    (27, NULL, NULL, 32),
    (28, NULL, NULL, 32),
    (29, NULL, NULL, 32),
    (30, NULL, NULL, 32),
    (31, NULL, NULL, 32),
    (32, NULL, NULL, 32),
    (33, NULL, NULL, 32),
    (34, NULL, NULL, 32),
    (35, NULL, NULL, 32),
    (36, NULL, NULL, 33),
    (37, NULL, NULL, 33),
    (38, NULL, NULL, 33),
    (39, NULL, NULL, 33),
    (40, NULL, NULL, 33),
    (41, NULL, NULL, 33),
    (42, NULL, NULL, 34);

-- 15 + 6 + 1
INSERT INTO `recipe2resource` (`recipe_id`, `resource_id`) VALUES
    (21, 5), (21, 6), (21, 7), (21, 8),
    (22, 5), (22, 6), (22, 7),          (22, 9),
    (23, 5), (23, 6), (23, 7),                   (23, 10),
    (24, 5), (24, 6),          (24, 8), (24, 9),
    (25, 5), (25, 6),          (25, 8),          (25, 10),
    (26, 5), (26, 6),                   (26, 9), (26, 10),
    (27, 5),          (27, 7), (27, 8), (27, 9),
    (28, 5),          (28, 7), (28, 8),          (28, 10),
    (29, 5),          (29, 7),          (29, 9), (29, 10),
    (30, 5),                   (30, 8), (30, 9), (30, 10),
             (31, 6), (31, 7), (31, 8), (31, 9),
             (32, 6), (32, 7), (32, 8),          (32, 10),
             (33, 6), (33, 7),          (33, 9), (33, 10),
             (34, 6),          (34, 8), (34, 9), (34, 10),
                      (35, 7), (35, 8), (35, 9), (35, 10),
    (36, 5), (36, 6), (36, 7), (36, 8), (36, 9),
    (37, 5), (37, 6), (37, 7), (37, 8),          (37, 10),
    (38, 5), (38, 6), (38, 7),          (38, 9), (38, 10),
    (39, 5), (39, 6),          (39, 8), (39, 9), (39, 10),
    (40, 5),          (40, 7), (40, 8), (40, 9), (40, 10),
             (41, 6), (41, 7), (41, 8), (41, 9), (41, 10),
    (42, 5), (42, 6), (42, 7), (42, 8), (42, 9), (42, 10);


INSERT INTO `feast_token` (`id`, `created`, `team_id`, `is_used`, `used_at`, `size`, `period_id`) VALUES
    (NULL, 1, '1', '0', NULL, '3', 1),
    (NULL, 1, '2', '0', NULL, '3', 1),
    (NULL, 1, '3', '0', NULL, '3', 1);

-- Admins --
INSERT INTO `users` (`name`, `display_name`, `team_id`, `created`, `is_admin`, `equipment_points`, `auth_token`, `sex`) VALUES
    ('Rota Kalniņa'             , 'Rota'       , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'female'),
    ('Kalvis Kalniņš'           , 'Kalvis K'   , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'male'),
    ('Kristaps Krūtainis'       , 'Aksels'     , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'male'),
    ('Andrievs Auziņš'          , 'Andrievs'   , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'male'),
    ('Elon Tatsu'               , 'Elon'       , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'male'),
    ('Testa Lietotājs'          , 'Test'       , NULL, CURRENT_TIMESTAMP, '1', '0', '', 'male');
-- Snakes
INSERT INTO `users` (`name`, `display_name`, `team_id`, `created`, `is_admin`, `equipment_points`, `auth_token`, `sex`) VALUES
    ('Manvydas ShadowSong Pranskus', 'Manvydas'  , '1', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Liutauras Siauciunas'        , 'Liutauras' , '1', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Deividas Bukauskas'          , 'Deividas'  , '1', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Darius Jurcevicus'           , 'Darius'    , '1', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Kulnys Aivaras'              , 'Kulnys'    , '1', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Ieva Kaziliūnaitė'           , 'Ieva'      , '1', CURRENT_TIMESTAMP, '0', '0', '', 'female');
-- Bears
INSERT INTO `users` (`name`, `display_name`, `team_id`, `created`, `is_admin`, `equipment_points`, `auth_token`, `sex`) VALUES
    ('Kristaps Baumanis'           , 'Kristaps'  , '2', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Aivars Bondars'              , 'Bondars'   , '2', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Reinis Poļaks'               , 'Reinis'    , '2', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Sindija Romane'              , 'Pīka'      , '2', CURRENT_TIMESTAMP, '0', '0', '', 'female'),
    ('Monta Vīgante'               , 'Monta'     , '2', CURRENT_TIMESTAMP, '0', '0', '', 'female'),
    ('Edgards Antipovs'            , 'Edgars'    , '2', CURRENT_TIMESTAMP, '0', '0', '', 'male'  );
-- Lynx
INSERT INTO `users` (`name`, `display_name`, `team_id`, `created`, `is_admin`, `equipment_points`, `auth_token`, `sex`) VALUES
    ('Anna Holberga'               , 'Anna'      , '3', CURRENT_TIMESTAMP, '0', '0', '', 'female'),
    ('Viktors Holbergs'            , 'Viktors'   , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Alise Frīdmane'              , 'Alise'     , '3', CURRENT_TIMESTAMP, '0', '0', '', 'female'),
    ('Artis Galvanovskis'          , 'Artis'     , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Jānis Plūme'                 , 'Jānis'     , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Pēteris Caune'               , 'Pēteris'   , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Oskars Sjomkāns'             , 'Oskars'    , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Ernests Auzins'              , 'Ernests'   , '3', CURRENT_TIMESTAMP, '0', '0', '', 'male'  );
-- Jokers
INSERT INTO `users` (`name`, `display_name`, `team_id`, `created`, `is_admin`, `equipment_points`, `auth_token`, `sex`) VALUES
    ('Valdemārs Hardijs Brūveris'  , 'Valdemārs' , NULL, CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Chingiz Akmatow'             , 'Chingiz'   , NULL, CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Jānis Znotiņš'               , 'Znotiņš'   , NULL, CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Renis Jānis Kozuliņš'        , 'Reinis'    , NULL, CURRENT_TIMESTAMP, '0', '0', '', 'male'  ),
    ('Marita Vītola'               , 'Marita'    , NULL, CURRENT_TIMESTAMP, '0', '0', '', 'female');


SET FOREIGN_KEY_CHECKS=1;
COMMIT;
