SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;


INSERT INTO `settings` (`id`, `description`, `title`, `value`, `code`, `is_system`) VALUES
    ( 1, 'boolean 1/0\r\nSwitch if game has started. Should not be adjusted manually'                                                                                                  , 'Game has started'             , '0'                  , 'switch-game_started' , 1  ),
    ( 2, 'number\r\nCurrent active season'                                                                                                                                             , 'Current season'               , '0'                  , 'var-current_period'  , 1  ),
    ( 4, 'number\r\nWhat flag status should be set when flag is stolen'                                                                                                                , 'Stolen flag scored set status', '1'                  , 'stolen_flag_reset'   , 1  ),
    ( 6, 'number\r\nCoin cost of recipe'                                                                                                                                               , 'Recipe price'                 , '2'                  , 'recipe_price'        , 0  ),
    ( 8, 'number\r\nHow many minutes people need to \"hold\" home flag before it can be scored'                                                                                        , 'Flag Home Score Interval'     , '40'                 , 'flag-home-scoring-at', 0  ),
    ( 9, 'timestamp (Y-m-d H:i:s)\r\nGame start timer in UTC. Nothing works before real time reaches this timer!!!! To start up game - set wanted REAL time point.\r\n'                , 'Starts at (UTC)'              , '2031-01-01 00:00:00', 'start-time'          , 0  ),
    (10, 'number\r\nMax allowed teamkills per season'                                                                                                                                  , 'Max kills per team per season', '7'                  , 'max-team-kills'      , 0  ),
    (11, 'number\r\nRespawn countdwon timer'                                                                                                                                           , 'Respawn period secs'          , '30'                 , 'respawn-period'      , 0  ),
    (12, 'number\r\nVP for one recipe ingredient per recipe'                                                                                                                           , 'Recipe ingredients per type'  , '1'                  , 'recipe-ing-per-type' , 0  ),
    (14, 'boolean 0/1\r\n\r\nThis will stop ALL game processes EXCEPT tick counter. for tick counter please use \"Game emergency ticker stop\" setting!!!'                             , 'Emergency shutdwown'          , '0'                  , 'emergency-shutdown'  , 0  ),
    (15, 'number\r\nScore time in seconds for single recipe'                                                                                                                           , 'Recipe points over time'      , '720'                , 'recipe-time'         , 0  ),
    (16, 'not sure this is actual for now...'                                                                                                                                          , 'Tribute scan timeout'         , '1'                  , 'tribute-expiry'      , 0  ),
    (17, 'boolean 0/1 \r\n\r\nJust to allow night scoring script to run no more than once. '                                                                                           , 'Night scoring runned'         , '0'                  , 'night-scoring'       , 1  ),
    (18, 'Was initial game resources assigned to team with script'                                                                                                                     , 'Team resources generated'     , '0'                  , 'team-resources'      , 1  ),
    (19, 'boolean 0/1\r\n\r\nIs currently feast night. Turns off some processes from doing things.'                                                                                    , 'Is Feast Night'               , '1'                  , 'is-feast-night'      , 0  ),
    (21, 'number\r\n\r\nID of feast season'                                                                                                                                            , 'Feast season'                 , '7'                  , 'feast-season-id'     , '0'),
    (22, 'number\r\n\r\ncurrent tick. DO NOT CHANGE MANUALLY - WILL BREAK THE GAME!!!'                                                                                                 , 'Current tick'                 , '1'                  , 'current_tick'        , '1'),
    (23, 'boolean 0/1\r\n\r\nenabled will stop the game ticker. Use with caution as may break things if rest of game is not stopped!'                                                  , 'Game emergency ticker stop'   , '0'                  , 'emerg_ticker_stop'   , '0'),
    (24, 'number\r\n\r\nThis value s used as blank check against real unix timestamp. If this IS different than current timestamp - game ticker will be updated by preset value \"1\"' , 'Ticker update time'           , ''                   , 'ticker_upd_time'     , '1'),
    (26, ''                                                                                                                                                                            , 'Workshop bonus timeout'       , '300'                , 'ws-bonus-to'         , '1'),
    (27, ''                                                                                                                                                                            , 'Recipe points per ingredient' , '1'                  , 'pping'               , '1'),
    (28, ''                                                                                                                                                                            , 'Forward time to'              , '0'                  , 'forward-time'        , '0'),
    (29, 'number\r\n\r\nMinutes of minimum season length (scoring cant start earlier!)'                                                                                                , 'Season min length'            , '65'                 , 'season-min'          , '0'),
    (30, 'number\r\n\r\nMinutes of maximum season length (scoring starts if ever season is longer!)'                                                                                   , 'Season max length'            , '95'                 , 'season-max'          , '0'),
    (31, 'number\r\n\r\nseason average length. Used for next pillar target calculation'                                                                                                , 'Season average length'        , '80'                 , 'season-avg'          , '0'),
    (32, 'If game is finished'                                                                                                                                                         , 'Game is Finished'             , '0'                  , 'game-is-finished'    , '1'),
    (33, 'Announcement that everyone will see all the time in the App. Make empty to disable'                                                                                          , 'Announcement'                 , ''                   , 'announcement'        , '0');


INSERT INTO `score_types` (`id`, `title`) VALUES
(1, 'Victory points'),
(2, 'Military points'),
(3, 'Agriculture points'),
(4, 'Industry points'),
(5, 'Friday Feast Points');


INSERT INTO `qr_categories` (`id`, `title`) VALUES
(1, 'resource'),
(2, 'flag'),
(3, 'player'),
(4, 'character'),
(5, 'equipment'),
(6, 'management'),
(7, '5-day-special');

INSERT INTO `score_reasons` (`id`, `title`, `amount`, `type_id`, `assign_period`) VALUES
(6, 'Score flag home', 5, 2, 1),
(7, 'Score flag \"Glory\"', 6, 2, 1),
(9, 'Score flag \"Shamed\"', 2, 2, 1),
(10, 'Score tribute', 1, 2, 1),
(11, 'Score tournament 1st', 5, 2, 1),
(12, 'Score tournament 2nd', 3, 2, 1),
(13, 'Score Tiny Workshop (4 times)', 1, 4, 1),
(14, 'Score Small Workshop (5 times)', 1, 4, 1),
(15, 'Score Medium Workshop (6 times)', 1, 4, 1),
(16, 'Score Large Workshop (7 times)', 1, 4, 1),
(17, 'Score Excessive Workshop (8 times)', 1, 4, 1),
(31, 'Score make 3 ingredient dish', 1, 3, 1),
(32, 'Score make 4 ingredient dish', 1, 3, 1),
(33, 'Score make 5 ingredient dish', 1, 3, 1),
(34, 'Score make 6 ingredient dish', 1, 3, 1),
(35, 'Score feast member as host', 1, 3, 1),
(36, 'Score feast member as guest', 2, 3, 1),
(37, '[ADMIN] Score 1 Victory point', 1, 1, 1),
(38, '[ADMIN] Score 1 Military point', 1, 2, 1),
(39, '[ADMIN] Score 1 Agriculture point', 1, 3, 1),
(40, '[ADMIN] Score 1 Industry point', 1, 4, 1),
(51, 'Score 1. place Military track', 10, 1, 1),
(52, 'Score 2. place Militarry track', 9, 1, 1),
(53, 'Score 3. place Militarry track', 8, 1, 1),
(54, 'Score 4. place Militarry track', 7, 1, 1),
(55, 'Score 5. place Militarry track', 6, 1, 1),
(56, 'Score 6. place Militarry track', 5, 1, 1),
(58, 'Score \"no\" place Military track', 0, 1, 1),
(59, 'Score 1. place Agriculture track', 10, 1, 1),
(60, 'Score 2. place Agriculture track', 9, 1, 1),
(61, 'Score 3. place Agriculture track', 8, 1, 1),
(62, 'Score 4. place Agriculture track', 7, 1, 1),
(63, 'Score 5. place Agriculture track', 6, 1, 1),
(64, 'Score 6. place Agriculture track', 5, 1, 1),
(73, 'Score 1. place Industry track', 10, 1, 1),
(74, 'Score 2. place Industry track', 9, 1, 1),
(75, 'Score 3. place Industry track', 8, 1, 1),
(76, 'Score 4. place Industry track', 7, 1, 1),
(77, 'Score 5. place Industry track', 6, 1, 1),
(78, 'Score 6. place Industry track', 5, 1, 1),
(80, 'Score \"no\" place Industry track', 0, 1, 1),
(81, 'Score \"no\" place, Agriculture track', 0, 1, 1),
(82, 'Flag stolen', 0, 2, 1),
(83, 'Expansion reward to owner', 2, 4, 1),
(84, 'Expansion reward to scorer', 2, 4, 1),
(85, 'Overflow for Culture', 1, 3, 1),
(86, 'Overflow for Industry', 1, 4, 1),
(87, 'Overflow for Military', 1, 2, 1),
(90, '5day GM special points', 1, 5, 1),
(91, '5day competition: checkers', 1, 5, 1),
(92, '5day competition: archery', 1, 5, 1),
(93, '5day competition: tournament', 1, 5, 1),
(94, '5day competition: taxing', 1, 5, 1),
(95, '5day competition: bard fight', 1, 5, 1),
(96, 'Score 1. place First Night track', 10, 1, 1),
(97, 'Score 2. place First Night track', 9, 1, 1),
(98, 'Score 3. place First Night track', 8, 1, 1),
(99, 'Score 4. place First Night track', 7, 1, 1),
(100, 'Score 5. place First Night track', 6, 1, 1),
(101, 'Score 6. place First Night track', 5, 1, 1),
(102, 'Killed 1/2 of the King', 1, 1, NULL),
(103, 'Tournamnets 3.rd place', 1, 2, NULL),
(104, 'Killed 2 of the King', 1, 1, NULL);

INSERT INTO `resource_action` (`id`, `name`, `from`, `to`) VALUES
(1, 'Scanned', 'outside', 'available'),
(2, 'Workshop reserved', 'available', 'locked'),
(3, 'Workshop spent', 'locked', 'spent'),
(4, 'Workshop cancelled', 'locked', 'available'),
(5, 'Recipe spent', 'available', 'spent'),
(6, 'Feast spent', 'available', 'spent'),
(7, 'Bandits reserved', 'available', 'locked'),
(8, 'Bandits spent', 'locked', 'spent'),
(9, 'Bandits cancelled', 'locked', 'available'),
(10, 'Equipment spent', 'available', 'spent'),
(11, 'deposited', 'outside', 'available'),
(12, 'withdrawn', 'available', 'outside'),
(13, 'Market: Resource sold', 'available', 'spent'),
(14, 'Market: Coins gained', 'outside', 'available'),
(15, 'Initial Resources', 'outside', 'available');

INSERT INTO `resource_types` (`id`, `name`, `subtype`, `qr_prefix`) VALUES
(1, 'Wood', '', 'WD'),
(2, 'Hides', '', 'HD'),
(3, 'Iron', '', 'RN'),
(4, 'Coins', '', 'CN'),
(5, 'Food', 'Honey', 'Fh'),
(6, 'Food', 'Lavender', 'Fv'),
(7, 'Food', 'Tomatoes', 'Ft'),
(8, 'Food', 'Cucumbers', 'Fc'),
(9, 'Food', 'Mushrooms', 'Fm'),
(10, 'Food', 'Raspberries', 'Fr');

INSERT INTO `qr_scan_types` (`id`, `title`, `is_enabled`, `category_id`) VALUES
(10, 'Player registration'    , 1, 3),
(11, 'Character registration' , 1, 4),
(12, 'Score tribute'          , 1, 4),
(13, 'Score local flag'       , 1, 2),
(14, 'Score enemy flag'       , 1, 2),
(15, 'Resource registration'  , 1, 1),
(16, 'Faith registration'     , 1, 6),
(17, 'Login'                  , 1, 3),
(19, 'Flag registration'      , 1, 2),
(20, '5 day special'          , 1, 7);

INSERT INTO `workshop_size` (`id`, `ip_amount`, `score_reason_id`, `wood_cost`, `resource_cost`, `coin_cost`, `cooldown`) VALUES
(4, 4, 13, 1, 1 , 1, 1500),
(5, 5, 14, 1, 3 , 2, 1500),
(6, 6, 15, 1, 7 , 3, 1500),
(7, 7, 16, 1, 13, 4, 1500),
(8, 8, 17, 1, 21, 5, 1500);

INSERT INTO `flag_status` (`id`, `name`, `score_reason_id`, `self_score_reason_id`, `next_status`) VALUES
(1, 'Stolen', 82, 82, 2),
(2, 'Shamed', 9, 6, 3),
(3, 'Glory', 7, 6, NULL);

INSERT INTO `feature_switchboard` (`id`, `title`, `status`, `details`, `category`) VALUES
(1, 'Is Game On', 1, '', 'base'),
(2, 'Is Season Change Cron On', 1, '', 'cron'),
(3, 'Is Workshop Cron on', 1, '', 'cron'),
(4, 'Friday Night Feast', 1, '', 'cron'),
(5, 'Add new users', 1, '', 'cron'),
(6, 'Ticker', '1', '', 'cron');

SET FOREIGN_KEY_CHECKS=1;
COMMIT;
