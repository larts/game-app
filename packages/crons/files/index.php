<?php
require "helpers/header.php";

require 'helpers/init.php';
require 'helpers/helpers.php';
require 'helpers/ScoreReason.php';

if ($_GET['do']) {
    switch ($_GET['do']) {
        case "work":  {
            $verify = 3;
            $file = "workshopUpdate.php";
            break;
        }
        case "season": {
            $verify = 2;
            $file = "seasonChange.php";
            break;
        }
        case "night": {
            $verify = 4;
            $file = "scoreNight.php";
            break;
        }
        case "users": {
            $verify = 5;
            $file = "userQR.php";
            break;
        }
        case "tick": {
            $verify = 6;
            $file = "ticker.php";
            break;
        }
        default: {
            $file = false;
        }
    }

    try {
        if ($file) {
            if ($locker->check($verify)) {
                require $file;

                $database->action(function ($database) {
                    import($database);

                    if (!isset($_GET['run'])) {
                        throw new Exception('Just stop! Run with required prop to commit!');
                    }

                    if (isset($_GET['log'])) {
                        var_dump($database->log());
                    }
                });
            };
        } else {
            echo 'No process selected';
        }
    } catch (Exception $e) {
        if (isset($_GET['log'])) {
            var_dump($database->log());
        }
        echo "Failed with: ", $e->getMessage(), "\n";
    }
}

require "helpers/footer.php";
