<?php

function import($database)
{
    $USER = [
       /*
        ["Alfrēds Auziņš", "PL-4n-z2"],
        ["Alise Frīdmane", "PL-4p-Ft"],
        ["Ance Zveja", "PL-3x-JA"],
        ["Anrijs Dovbņa", "PL-2Z-92"],
        ["Ansis Buiķis", "PL-28-Ft"],
        ["Arina Solovjova", "PL-3z-jK"],
        ["Artjoms Horosilovs", "PL-4N-ni"],
        ["Austėja Sironaite", "PL-4h-Db"],
        ["Austris Lauberts", "PL-4L-bJ"],
        ["Darja Vasiljeva", "PL-3t-sB"],
        ["Dārta Smaine", "PL-2N-Ej"],
        ["Ernests Tomass Auziņš", "PL-4r-4S"],
        ["Ernests Zariņš", "PL-2K-6j"],
        ["Evija Kakšte", "PL-49-Z3"],
        ["Evija Zandberga", "PL-2M-iA"],
        ["Hardijs Kalniņš", "PL-3i-6j"],
        ["Jānis Muižnieks", "PL-4G-h3"],
        ["Jānis Sprukts", "PL-2b-iB"],
        ["Jevgenijs Čačiks", "PL-3y-Ps"],
        ["Kārlis Starks", "PL-4H-Ps"],
        ["Konstantin Gorskov", "PL-2i-cT"],
        ["Kristaps Baumanis", "PL-2L-3K"],
        ["Kristaps Krūtainis", "PL-4i-Z3"],
        ["Kristaps Silbaums", "PL-4c-wj"],
        ["Kristofers Volkovs", "PL-2J-ps"],
        ["Lauma Apine", "PL-2a-TJ"],
        ["Līva Brunovska", "PL-4J-4T"],
        ["Maija Brasava", "PL-3g-3J"],
        ["Manvydas Pranskus", "PL-3h-Nj"],
        ["Marita Vītola", "PL-2S-db"],
        ["Mārtiņš Inda", "PL-4X-Fs"],
        ["Oskars Sjomkāns", "PL-4R-sA"],
        ["Reinis Jānis Kozuliņš", "PL-2R-JB"],
        ["Reinis Jurģis Sevelis", "PL-2D-Xs"],
        ["Ričards Vīdners", "PL-4W-db"],
        ["Roberts Ivanovs", "PL-4s-kS"],
        ["Ronalds Palacis", "PL-2j-Ei"],
        ["Sandra Boguša", "PL-4a-jJ"],
        ["Santa Šakela", "PL-4j-BJ"],
        ["Toms Deimonds Barvidis", "PL-4K-ei"],
        ["Vaidas Zenovicius", "PL-4b-2B"],
        ["Vineta Vērpēja", "PL-2G-SA"],
        ["Чингиз Акматов", "PL-4e-AB"],
       */
        ['Kārlis Vilhelms Vēbers', 'PL-4k-7s'],
        ['Emīls Kalderauskis', 'PL-4Z-h3'],
        ['Evija Fokrote', 'PL-4S-Z2'],
        ['Ēriks Zalmanis', 'PL-4m-TK'],
        ['Nikolajs Karņejevs', 'PL-3d-92'],
        ['Artūrs Grebstelis', 'PL-2F-LS'],
        ['Ieva Balode', 'PL-3u-nj'],
        ['Baiba Vītola', 'PL-3e-KK'],
        ['Artis Galvanovskis', 'PL-4q-Mb'],
        ['Jānis Plūme', 'PL-3v-Va'],
        ['Paula Lilienfelde', 'PL-4P-5b'],
        ['Jēkabs Frīdmanis', 'PL-4F-bK'],
    ];

    foreach ($USER as $userz) {
        $user = $database->get('users', ['id'], ['name' => $userz[0]]);

        if ($user) {
            $qr = $database->get('qr_codes', ['id'], ['qrid'=>$userz[1]]);

            if ($qr) {
                $database->update('users', ['qr_code'=>$userz[1], 'qr_id'=>$qr['id']], ['id'=>$user['id']]);
            } else {
                logStatus('No qr: ' . $userz[1], 'error');
            }
        } else {
            logStatus('No user: ' . $userz[0], 'error');
        }
    }
}
