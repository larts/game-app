<?php

class Lock {
    private $settings;

    function Lock($database) {
        $settings = $database->select('feature_switchboard', ['status', 'details', 'id'], ['category' => 'cron']);

        $re = [];

        foreach ($settings as $setting) {
            $re[$setting['id']] = $setting;
        }

        $this->settings = $re;
    }

    public function check($settingId) {
        if (isset($this->settings[$settingId])) {
            if (isset($_GET['debug'])) {
                $_GET['log'] = true;
                return true;
            }

            if ($this->settings[$settingId]['status'] === "1") {
                return true;
            }
        }

        echo 'Feature is turned off!';
        return false;
    }
}
