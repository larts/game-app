<?php

function scoreSort($a, $b)
{
    if ($a['score'] > $b['score']) {
        return 1;
    }

    if ($a['score'] < $b['score']) {
        return -1;
    }

    if ($a['lastTotal'] && !$b['lastTotal']) {
        return 1;
    }

    if (!$a['lastTotal'] && $b['lastTotal']) {
        return -1;
    }

    if ($a['lastTotal'] && $b['lastTotal']) {
        if ($a['lastTotal']['rank'] < $b['lastTotal']['rank']) {
            return 1;
        }

        if ($a['lastTotal']['rank'] > $b['lastTotal']['rank']) {
            return -1;
        }
    }

    if ($a['team'] < $b['team']) {
        return 1;
    } else {
        return -1;
    }
}

function scoreSort2($a, $b)
{
    if ($a['score'] > $b['score']) {
        return 1;
    }

    if ($a['score'] < $b['score']) {
        return -1;
    }

    if ($a['rank'] && !$b['rank']) {
        return 1;
    }

    if (!$a['rank'] && $b['rank']) {
        return -1;
    }

    if ($a['rank'] && $b['rank']) {
        if ($a['rank'] < $b['rank']) {
            return 1;
        }

        if ($a['rank'] > $b['rank']) {
            return -1;
        }
    }

    if ($a['team_id'] > $b['team_id']) {
        return -1;
    } else {
        return 1;
    }
}

function amountSort($a, $b)
{
    if ($a['amount'] > $b['amount']) {
        return 1;
    }

    if ($a['amount'] < $b['amount']) {
        return -1;
    }

    if ($a['score'] > $b['score']) {
        return 1;
    }

    if ($a['score'] < $b['score']) {
        return -1;
    }

    if ($a['lastTotal'] && !$b['lastTotal']) {
        return 1;
    }

    if (!$a['lastTotal'] && $b['lastTotal']) {
        return -1;
    }

    if ($a['lastTotal'] && $b['lastTotal']) {
        if ($a['lastTotal']['rank'] < $b['lastTotal']['rank']) {
            return 1;
        }

        if ($a['lastTotal']['rank'] > $b['lastTotal']['rank']) {
            return -1;
        }
    }

    if ($a['id'] < $b['id']) {
        return 1;
    } else {
        return -1;
    }

    return 0;
}

function show($time) {
    var_dump(date("Y-m-d H:i:s", $time), $time);
}
