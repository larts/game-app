<?php
function logStatus($row, $status) {
    $logState = 'all';

    $logWhat = [
        'update' => ['update', 'error', 'title'],
        'all' => ['update', 'error', 'process', 'debug', 'title'],
        'process' => ['update', 'error', 'process', 'title'],
    ];

    if (isset($_GET['log'])) {
        $logState = $_GET['log'];
    }

    $doLog = in_array($status, $logWhat[$logState]);

    if ($doLog) {
        echo $row . "<br /> \n\r";
    }
}

