<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-language" content="en"/>
    <meta content="initial-scale=1, maximum-scale=1 shrink-to-fit=no, user-scalable=no" name="viewport"/>
    <meta content="ie=edge" http-equiv="x-ua-compatible"/>
    <title>Tronis crons</title>
</head>
<article>
<h3>To work</h3>
    <ul>
        By default all files do dry run.
        <li>run - disable dry run</li>
        <li>log - show all database logs</li>
        <li>debug - execute && show logs even if feature disabled in admin</li>
    </ul>
</article>
