<?

class ScoreReason
{
    public $list = [];

    public function genereatelist($database)
    {
        $list = $database->select('score_reasons', ['id', 'amount', 'type_id']);

        foreach ($list as $item) {
            $this->list[$item['id']] = $item['amount'];
        }
    }

    public $reasonMap = [
// Military
        2 => [
            51, 52, 53, 54, 55, 56, 57
        ],
// Culture
        3 => [
            59, 60, 61, 62, 63, 64, 65
        ],
// Industry
        4 => [
            73, 74, 75, 76, 77, 78, 79
        ],
// Night Feast
        5 => [
            96, 97, 98, 99, 100, 101
        ]
    ];

    public $overflowReasons = [
        2 => 87,
        3 => 85,
        4 => 86,
    ];

    public $types = [
        'industry' => 4
    ];
}
