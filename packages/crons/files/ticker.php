<?php

function import($database)
{
/*
 * let's have reference point - real time. If real time has changed between now and previosu update - we can update current time.
 * hopefully this can prevent if something goes wrong with call frequency of this ticker counter (we can call 10 times in one second and it will still update only once)
 * So our ticker frequency is 1 second. If precision needs to be increased - use more precise timer function in php - microtime()
 */

    $isFinished = $database->get('settings', ['value'], ['id' => SETING_GAME_FINISHED]);

    if ($isFinished['value'] === "1") {
        throw new Exception('Game has ended!');
    }

    $game_started = $database->get('settings', ['value'], ['id' => SETTING_GAME_STARTED]);
    $game_emergency_pause = $database->get('settings', ['value'], ['id' => SETTING_GAME_EMERGENCY_TICKER_PAUSED]);

   if ($game_emergency_pause['value'] === "0" && $game_started['value'] === "1") {
       $ticker_value = $database->get('settings', ['value'], ['id' => SETTING_TICKER]);
       $ticker_update_time = $database->get('settings', ['value'], ['id' => SETTING_GAME_TICKER_UPDATE_TIME]);

       $update_real_time = time();
       if ($ticker_value['value'] == "") {
           $game_started_time = $database->get('settings', ['value'], ['id' => SETTING_GAME_STARTED_TIME]);
           $unix_date = strtotime($game_started_time['value']);
       } else {
           if ($ticker_update_time['value'] !== $update_real_time) {
               $unix_date = $ticker_value['value'] + 1;
           }
       }

       $database->update('settings', ['value' => $update_real_time], ['id' => SETTING_GAME_TICKER_UPDATE_TIME]);
       $database->update('settings', ['value' => $unix_date], ['id' => SETTING_TICKER]);
   }
}
