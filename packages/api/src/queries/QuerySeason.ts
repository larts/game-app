import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QuerySeason: ApiBaseQueryOptions<QuerySeason.Response, QuerySeason.Params> = {
    query: gql`query ${QUERY.Season}($season: Int) {
        season(season: $season) {
            id,
            title,
            started,
            ended,
        }
    }`,
}

export namespace QuerySeason {
    export type Params = {
        season?: number
    }
    export interface Response {
        season: Maybe<{
            id: NonNullable<Query['season']>['id']
            title: NonNullable<Query['season']>['title']
            started: NonNullable<Query['season']>['started']
            ended: NonNullable<Query['season']>['ended']
        }>
    }
}
