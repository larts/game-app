import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QuerySession: ApiBaseQueryOptions<QuerySession.Response, QuerySession.Params> = {
    query: gql`query ${QUERY.Session} {
        user {
            id,
        },
        baseData {
            userSessionEnd,
            isMarketDevice,
        }
    }`,
}

export namespace QuerySession {
    export type Params = never
    export interface Response {
        user: Maybe<{
            id: NonNullable<Query['user']>['id']
        }>,
        baseData: {
            userSessionEnd: Query['baseData']['userSessionEnd']
            isMarketDevice: Query['baseData']['isMarketDevice']
        },
    }
}
