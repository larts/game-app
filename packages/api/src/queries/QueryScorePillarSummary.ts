import { gql } from '@apollo/client'

import { PILLAR } from '..'
import { Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryScorePillarSummary: ApiBaseQueryOptions<QueryScorePillarSummary.Response, QueryScorePillarSummary.Params> = {
    query: gql`query ${QUERY.ScorePillarSummary}($pillarId: Int!) {
        pillar(id: $pillarId) {
            id,
            title,
            allSeasons {
                season { id },
                target,
                currentTotal,
                totalScores {
                    id,
                    score,
                    lastUpdate,
                    rank,
                    team {
                        id,
                        title,
                        color
                    }
                }
            }
        }
    }`,
}

export namespace QueryScorePillarSummary {
    export interface Params {
        pillarId: PILLAR
    }
    export interface Response {
        pillar: {
            id: NonNullable<Query['pillar']>['id']
            title: NonNullable<Query['pillar']>['title']
            allSeasons: Array<{
                season: Pick<NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['season'], 'id'>
                target: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['target']
                currentTotal: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['currentTotal']
                totalScores: Array<{
                    id: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['totalScores'][0]['id']
                    score: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['totalScores'][0]['score']
                    lastUpdate: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['totalScores'][0]['lastUpdate']
                    rank: NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['totalScores'][0]['rank']
                    team: Pick<NonNullable<NonNullable<Query['pillar']>['allSeasons'][0]>['totalScores'][0]['team'], 'id' | 'title' | 'color'>
                }>
            }>
        },
    }
}
