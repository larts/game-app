import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamEquipment: ApiBaseQueryOptions<QueryTeamEquipment.Response, QueryTeamEquipment.Params> = {
    query: gql`query ${QUERY.TeamEquipment} {
        team {
            id,
            availableEq,
            players {
                id,
                name,
                equipmentPoints,
                equipmentHasPhoenix,
            }
        }
    }`,
}

export namespace QueryTeamEquipment {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            availableEq: NonNullable<Query['team']>['availableEq']
            players: Array<Pick<NonNullable<Query['team']>['players'][0], 'id' | 'name' | 'equipmentPoints' | 'equipmentHasPhoenix'>>
        }>
    }
}
