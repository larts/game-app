import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryUserCharacter: ApiBaseQueryOptions<QueryUserCharacter.Response, QueryUserCharacter.Params> = {
    query: gql`query ${QUERY.UserCharacter} {
        user {
            id,
            respawnCount,
            character {
                charcode,
                isScored,
            },
        }
    }`,
}

export namespace QueryUserCharacter {
    export type Params = never
    export interface Response {
        user: Maybe<{
            id: NonNullable<Query['user']>['id']
            respawnCount: NonNullable<Query['user']>['respawnCount']
            character: Maybe<{
                charcode: NonNullable<NonNullable<Query['user']>['character']>['charcode']
                isScored: NonNullable<NonNullable<Query['user']>['character']>['isScored']
            }>
        }>
    }
}
