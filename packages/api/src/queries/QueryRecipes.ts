import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryRecipes: ApiBaseQueryOptions<QueryRecipes.Response, QueryRecipes.Params> = {
    query: gql`query ${QUERY.Recipes} {
        recipes {
            id,
            title,
            firstCookedBy { id },
            resources { id },
            cookLog {
                created,
                starts,
                team {
                    id,
                    title,
                },
            }
        }
    }`,
}

export namespace QueryRecipes {
    export type Params = never
    export interface Response {
        recipes: Array<{
            id: Query['recipes'][0]['id']
            title: Query['recipes'][0]['title']
            firstCookedBy: Maybe<Pick<NonNullable<Query['recipes'][0]['firstCookedBy']>, 'id'>>
            resources: Array<Pick<NonNullable<Query['recipes'][0]['resources'][0]>, 'id'>>
            cookLog: Array<{
                created: Query['recipes'][0]['cookLog'][0]['created'],
                starts: Query['recipes'][0]['cookLog'][0]['starts'],
                team: Pick<Query['recipes'][0]['cookLog'][0]['team'], 'id' | 'title'>,
            }>
        }>
    }
}
