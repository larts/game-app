import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamUpgrades: ApiBaseQueryOptions<QueryTeamUpgrades.Response, QueryTeamUpgrades.Params> = {
    query: gql`query ${QUERY.TeamUpgrades} {
        team {
            id,
            expansions {
                code,
                owners { id, title, color },
                rewarded { id, title, color },
                camp {
                    id,
                    title,
                    displayTitle,
                    resourceType { id },
                    longitude,
                    latitude,
                },
                showedAt,
                rewardedAt,
            },
        }
    }`,
}

export namespace QueryTeamUpgrades {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            expansions: Array<{
                code: NonNullable<Query['team']>['expansions'][0]['code']
                owners: Pick<NonNullable<Query['team']>['expansions'][0]['owners'], 'id' | 'title' | 'color'>
                rewarded: Maybe<Pick<NonNullable<NonNullable<Query['team']>['expansions'][0]['rewarded']>, 'id' | 'title' | 'color'>>
                camp: {
                    id: NonNullable<Query['team']>['expansions'][0]['camp']['id']
                    title: NonNullable<Query['team']>['expansions'][0]['camp']['title']
                    displayTitle: NonNullable<Query['team']>['expansions'][0]['camp']['displayTitle']
                    resourceType: Pick<NonNullable<Query['team']>['expansions'][0]['camp']['resourceType'], 'id'>
                    longitude: NonNullable<Query['team']>['expansions'][0]['camp']['longitude']
                    latitude: NonNullable<Query['team']>['expansions'][0]['camp']['latitude']
                }
                showedAt: NonNullable<Query['team']>['expansions'][0]['showedAt']
                rewardedAt: NonNullable<Query['team']>['expansions'][0]['rewardedAt']  // TODO: shouldn't this be with Maybe<>?
            }>
        }>
    }
}
