import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamBase: ApiBaseQueryOptions<QueryTeamBase.Response, QueryTeamBase.Params> = {
    query: gql`query ${QUERY.TeamBase} {
        team {
            id,
            title,
            color,
            castle {
                title,
                longitude,
                latitude
            }
        }
    }`,
}

export namespace QueryTeamBase {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id'],
            title: NonNullable<Query['team']>['title'],
            color: NonNullable<Query['team']>['color'],
            castle: Maybe<Pick<NonNullable<NonNullable<Query['team']>['castle']>, 'title' | 'longitude' | 'latitude'>>,
        }>
    }
}
