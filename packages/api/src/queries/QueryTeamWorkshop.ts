import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamWorkshop: ApiBaseQueryOptions<QueryTeamWorkshop.Response, QueryTeamWorkshop.Params> = {
    query: gql`query ${QUERY.TeamWorkshop} {
        team {
            id,
            workshopActivations {
                id,
                created,
                startTime,
                cancelled,
                finishTime,
                variant,
                size {
                    coin,
                    cooldown,
                    ip,
                    resourceCost,
                    score {
                        amount,
                        title,
                    },
                    wood,
                },
                scorePoints {
                    id,
                    created,
                    pillar { id },
                    season { id },
                    reason {
                        title,
                        amount,
                    },
                },
                priceDetails {
                    type,
                    amount,
                }
            },
        },
    }`,
}

export namespace QueryTeamWorkshop {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            workshopActivations: Array<{
                id: NonNullable<Query['team']>['workshopActivations'][0]['id']
                created: NonNullable<Query['team']>['workshopActivations'][0]['created']
                startTime: NonNullable<Query['team']>['workshopActivations'][0]['startTime']
                cancelled: NonNullable<Query['team']>['workshopActivations'][0]['cancelled']
                finishTime: NonNullable<Query['team']>['workshopActivations'][0]['finishTime']
                variant: NonNullable<Query['team']>['workshopActivations'][0]['variant']
                size: {
                    coin: NonNullable<Query['team']>['workshopActivations'][0]['size']['coin']
                    cooldown: NonNullable<Query['team']>['workshopActivations'][0]['size']['cooldown']
                    ip: NonNullable<Query['team']>['workshopActivations'][0]['size']['ip']
                    resourceCost: NonNullable<Query['team']>['workshopActivations'][0]['size']['resourceCost']
                    score: {
                        amount: NonNullable<Query['team']>['workshopActivations'][0]['size']['score']['amount']
                        title: NonNullable<Query['team']>['workshopActivations'][0]['size']['score']['title']
                    }
                    wood: NonNullable<Query['team']>['workshopActivations'][0]['size']['wood']
                }
                scorePoints: Array<{
                    id: NonNullable<Query['team']>['workshopActivations'][0]['scorePoints'][0]['id']
                    created: NonNullable<Query['team']>['workshopActivations'][0]['scorePoints'][0]['created']
                    pillar: Pick<NonNullable<Query['team']>['workshopActivations'][0]['scorePoints'][0]['pillar'], 'id'>
                    season: Pick<NonNullable<Query['team']>['workshopActivations'][0]['scorePoints'][0]['season'], 'id'>
                    reason: Pick<NonNullable<Query['team']>['workshopActivations'][0]['scorePoints'][0]['reason'], 'title' | 'amount'>
                }>
                priceDetails: Array<{
                    type: NonNullable<Query['team']>['workshopActivations'][0]['priceDetails'][0]['type']
                    amount: NonNullable<Query['team']>['workshopActivations'][0]['priceDetails'][0]['amount']
                }>
            }>
        }>
    }
}
