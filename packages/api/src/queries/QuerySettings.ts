import { gql } from '@apollo/client'

import { Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QuerySettings: ApiBaseQueryOptions<QuerySettings.Response, QuerySettings.Params> = {
    query: gql`query ${QUERY.Settings} {
        baseData {
            settings {
                code,
                title,
                value,
            },
        },
    }`,
}

export namespace QuerySettings {
    export type Params = never
    export interface Response {
        baseData: {
            settings: Array<Pick<Query['baseData']['settings'][0], 'code' | 'title' | 'value'>>
        },
    }
}
