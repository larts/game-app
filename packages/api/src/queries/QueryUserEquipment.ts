import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryUserEquipment: ApiBaseQueryOptions<QueryUserEquipment.Response, QueryUserEquipment.Params> = {
    query: gql`query ${QUERY.UserEquipment} {
        user {
            id,
            equipmentPoints,
            equipmentHasPhoenix,
        }
    }`,
}

export namespace QueryUserEquipment {
    export type Params = never
    export interface Response {
        user: Maybe<Pick<NonNullable<Query['user']>, 'id' | 'equipmentPoints' | 'equipmentHasPhoenix'>>
    }
}
