import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamNextRespawn: ApiBaseQueryOptions<QueryTeamNextRespawn.Response, QueryTeamNextRespawn.Params> = {
    query: gql`query ${QUERY.TeamNextRespawn} {
        team {
            id,
            nextRespawnTime,
        }
    }`,
}

export namespace QueryTeamNextRespawn {
    export type Params = never
    export interface Response {
        team: Maybe<Pick<NonNullable<Query['team']>, 'id' | 'nextRespawnTime'>>
    }
}
