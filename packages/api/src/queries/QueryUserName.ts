import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryUserName: ApiBaseQueryOptions<QueryUserName.Response, QueryUserName.Params> = {
    query: gql`query ${QUERY.UserName} {
        user {
            id,
            name,
            sex,
            team {
                id,
                color,
                title,
            }
        }
    }`,
}

export namespace QueryUserName {
    export type Params = never
    export interface Response {
        user: Maybe<{
            id: NonNullable<Query['user']>['id']
            name: NonNullable<Query['user']>['name']
            sex: NonNullable<Query['user']>['sex']
            team: {
                id: NonNullable<NonNullable<Query['user']>['team']>['id']
                color: NonNullable<NonNullable<Query['user']>['team']>['color']
                title: NonNullable<NonNullable<Query['user']>['team']>['title']
            }
        }>
    }
}
