import { gql } from '@apollo/client'

import { Maybe, Query } from '../graphql.schema'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamFlag: ApiBaseQueryOptions<QueryTeamFlag.Response, QueryTeamFlag.Params> = {
    query: gql`query ${QUERY.TeamFlag}($id: Int) {
        team(id: $id) {
            id,
            title,
            color,
            flagStatusId,
            flagRaisedTime,
            flagScoredTime,
        }
    }`,
}

export namespace QueryTeamFlag {
    export interface Response {
        team: Maybe<Pick<NonNullable<Query['team']>, 'id' | 'title' | 'color' | 'flagStatusId' | 'flagRaisedTime' | 'flagScoredTime'>>
    }
    export interface Params {
        id: number
    }
}
