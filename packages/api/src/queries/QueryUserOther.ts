import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryUserOther: ApiBaseQueryOptions<QueryUserOther.Response, QueryUserOther.Params> = {
    query: gql`query ${QUERY.UserOther}($qrCode: String!) {
        user(qrCode: $qrCode) {
            id,
            name,
            sex,
            team {
                id,
                color,
                title,
            }
        }
    }`,
}

export namespace QueryUserOther {
    export interface Params {
        qrCode: string
    }
    export interface Response {
        user: Maybe<{
            id: NonNullable<Query['user']>['id']
            name: NonNullable<Query['user']>['name']
            sex: NonNullable<Query['user']>['sex']
            team: {
                id: NonNullable<NonNullable<Query['user']>['team']>['id']
                color: NonNullable<NonNullable<Query['user']>['team']>['color']
                title: NonNullable<NonNullable<Query['user']>['team']>['title']
            }
        }>
    }
}
