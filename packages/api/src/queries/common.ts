import { OperationVariables, QueryOptions, TypedDocumentNode } from '@apollo/client'


export interface ApiBaseQueryOptions<TData = any, TVariables = OperationVariables> extends QueryOptions<TVariables, TData> {
    query: TypedDocumentNode<TData, TVariables>;
}

export enum QUERY {
    Kits = 'Kits',
    Recipes = 'Recipes',
    ScorePillarsBase = 'ScorePillarsBase',
    ScorePillars = 'ScorePillars',
    ScorePillarSummary = 'ScorePillarSummary',
    ScorePillar = 'ScorePillar',
    ScoreVictorySummary = 'ScoreVictorySummary',
    Season = 'Season',
    Session = 'Session',
    Settings = 'Settings',
    TeamBase = 'TeamBase',
    TeamEquipment = 'TeamEquipment',
    TeamFeasts = 'TeamFeasts',
    TeamFlag = 'TeamFlag',
    TeamKitAvailable = 'TeamKitAvailable',
    TeamNextRespawn = 'TeamNextRespawn',
    TeamResources = 'TeamResources',
    TeamsScoreTotals = 'TeamsScoreTotals',
    Teams = 'Teams',
    TeamUpgrades = 'TeamUpgrades',
    TeamWorkshop = 'TeamWorkshop',
    Time = 'Time',
    UserCharacter = 'UserCharacter',
    UserEquipment = 'UserEquipment',
    UserName = 'UserName',
    UserOther = 'UserOther',
    Workshops = 'Workshops',
}
