import { gql } from '@apollo/client'

import { Maybe, Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTeamsScoreTotals: ApiBaseQueryOptions<QueryTeamsScoreTotals.Response, QueryTeamsScoreTotals.Params> = {
    query: gql`query ${QUERY.TeamsScoreTotals} {
        team {
            id,
            scoreTotals {
                id,
                score,
                rank,
                pillar {
                    id,
                    season {
                        season {
                            id
                        }
                     },
                },
            },
        }
    }`,
}

export namespace QueryTeamsScoreTotals {
    export type Params = never
    export interface Response {
        team: Maybe<{
            id: NonNullable<Query['team']>['id']
            scoreTotals: Array<{
                id: NonNullable<Query['team']>['scoreTotals'][0]['id']
                score: NonNullable<Query['team']>['scoreTotals'][0]['score']
                rank: NonNullable<Query['team']>['scoreTotals'][0]['rank']
                pillar: {
                    id: NonNullable<Query['team']>['scoreTotals'][0]['pillar']['id'],
                    season: Maybe<{
                        season: {
                            id: NonNullable<NonNullable<Query['team']>['scoreTotals'][0]['pillar']['season']>['season']['id'],
                        }
                    }>
                }
            }>
        }>
    }
}
