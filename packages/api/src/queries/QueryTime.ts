import { gql } from '@apollo/client'

import { Query } from '..'

import { ApiBaseQueryOptions, QUERY } from './common'


export const QueryTime: ApiBaseQueryOptions<QueryTime.Response, QueryTime.Params> = {
    query: gql`query ${QUERY.Time} {
        baseData {
            settings {
                code,
                title,
                value,
            }
            currentTime,
            elapsedTime,
            elapsedSeasonTime,
        }
    }`,
}

export namespace QueryTime {
    export type Params = never
    export interface Response {
        baseData: {
            settings: Array<Pick<Query['baseData']['settings'][0], 'code' | 'title' | 'value'>>
            currentTime: Query['baseData']['currentTime']
            elapsedTime: Query['baseData']['elapsedTime']
            elapsedSeasonTime: Query['baseData']['elapsedSeasonTime']
        },
    }
}
