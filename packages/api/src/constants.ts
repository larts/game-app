// TODO: move out to it's own package

/**
 * id - integers using db row id for the team. TODO: update when teams are finalized.
 *
 */
export enum TEAM_ID {
    Snakes = 1,
    Bears = 2,
    Lynx = 3,
}

export enum TEAM_COLOR {
    Gold = 'gold',
    Red = 'red',
    Green = 'green',
    Purple = 'purple',
    Blue = 'blue',
    Orange = 'orange',
    Pink = 'pink',
}
