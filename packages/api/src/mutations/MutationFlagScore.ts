import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationFlagScore: ApiBaseMutationOptions<MutationFlagScore.Response, MutationFlagScore.Params> = {
    mutation: gql`mutation FlagRegister($qrCode: String!) {
        flag {
            score(qrCode: $qrCode),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamFlag,
    ],
}

export namespace MutationFlagScore {
    export interface Params {
        qrCode: string
    }
    export interface Response {
        flag: {
            score: MUTATION_RESULT
        }
    }
}
