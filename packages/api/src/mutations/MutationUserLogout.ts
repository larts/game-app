import { gql } from '@apollo/client'

import { QuerySession } from '../queries'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationUserLogout: ApiBaseMutationOptions<MutationUserLogout.Response, MutationUserLogout.Params> = {
    mutation: gql`mutation UserLogout {
        auth {
            userLogout
        }
    }`,
    update: (cache) => {
        cache.writeQuery({
            ...QuerySession,
            data: {
                user: null,
                baseData: {
                    userSessionEnd: null,
                    isMarketDevice: false,
                },
            },
        })
    },
}

export namespace MutationUserLogout {
    export type Params = never
    export interface Response {
        auth: {
            userLogout: MUTATION_RESULT
        }
    }
}
