import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationDebugSetTick: ApiBaseMutationOptions<MutationDebugSetTick.Response, MutationDebugSetTick.Params> = {
    mutation: gql`mutation DebugSetTick($tick: Int!) {
        debug {
            setTick(increase: $tick)
        }
    }`,
    refetchQueries: [
        QUERY.Settings,
    ],
}

export namespace MutationDebugSetTick {
    export interface Params {
        tick: number
    }
    export interface Response {
        debug: {
            setTick: MUTATION_RESULT
        }
    }
}
