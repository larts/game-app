import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationCharacterRevitalize: ApiBaseMutationOptions<MutationCharacterRevitalize.Response, MutationCharacterRevitalize.Params> = {
    mutation: gql`mutation CharacterRevitalize {
        character {
            revitalize
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.UserCharacter,
        QUERY.TeamNextRespawn,
    ],
}

export namespace MutationCharacterRevitalize {
    export type Params = never
    export interface Response {
        character: {
            revitalize: MUTATION_RESULT
        }
    }
}
