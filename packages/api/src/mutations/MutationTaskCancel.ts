import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationTaskCancel: ApiBaseMutationOptions<MutationTaskCancel.Response, MutationTaskCancel.Params> = {
    mutation: gql`mutation WorkshopTaskCancel {
        workshop {
            cancel
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
        QUERY.TeamWorkshop,
    ],
}

export namespace MutationTaskCancel {
    export type Params = never
    export interface Response {
        workshop: {
            cancel: MUTATION_RESULT
        }
    }
}
