import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationUpgradeReward: ApiBaseMutationOptions<MutationUpgradeReward.Response, MutationUpgradeReward.Params> = {
    mutation: gql`mutation ExpansionReward($code: String!) {
        expansions {
            reward(code: $code),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamUpgrades,
    ],
}

export namespace MutationUpgradeReward {
    export interface Params {
        code: string
    }
    export interface Response {
        expansions: {
            reward: MUTATION_RESULT
        }
    }
}
