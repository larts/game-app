import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationResourcesRemoveCoins: ApiBaseMutationOptions<MutationResourcesRemoveCoins.Response, MutationResourcesRemoveCoins.Params> = {
    mutation: gql`mutation ResourcesRemoveCoins($amount: Int!) {
        resources {
            removeCoins(amount: $amount),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
    ],
}

export namespace MutationResourcesRemoveCoins {
    export interface Params {
        amount: number
    }
    export interface Response {
        resources: {
            removeCoins: MUTATION_RESULT
        }
    }
}
