import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationEquipmentUnequip: ApiBaseMutationOptions<MutationEquipmentUnequip.Response, MutationEquipmentUnequip.Params> = {
    mutation: gql`mutation EquipmentUnequip {
        equipment {
            unequip,
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.UserEquipment,
    ],
}

export namespace MutationEquipmentUnequip {
    export type Params = never
    export interface Response {
        equipment: {
            unequip: MUTATION_RESULT
        }
    }
}
