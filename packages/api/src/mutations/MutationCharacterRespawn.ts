import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationCharacterRespawn: ApiBaseMutationOptions<MutationCharacterRespawn.Response, MutationCharacterRespawn.Params> = {
    mutation: gql`mutation CharacterRespawn($code: Int!) {
        character {
            respawn(code: $code)
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.UserCharacter,
        QUERY.TeamNextRespawn,
    ],
}

export namespace MutationCharacterRespawn {
    export interface Params {
        code: number
    }
    export interface Response {
        character: {
            respawn: MUTATION_RESULT
        }
    }
}
