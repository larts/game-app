import { MutationOptions, PureQueryOptions, TypedDocumentNode } from '@apollo/client'


export enum MUTATION_RESULT {
    Success = 'OK!',
    Fail = 'FAIL!',
}

export interface ApiBaseMutationOptions<TData = unknown, TVariables = never> extends MutationOptions<TData, TVariables> {
    mutation: TypedDocumentNode<TData, TVariables>;
    refetchQueries?: Array<string | PureQueryOptions>;
}

