import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationRecipeScore: ApiBaseMutationOptions<MutationRecipeScore.Response, MutationRecipeScore.Params> = {
    mutation: gql`mutation RecipeScore($id: Int!, $title: String) {
        recipe {
            score(id: $id, title: $title),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
        QUERY.Recipes,
        QUERY.TeamFeasts,
    ],
}

export namespace MutationRecipeScore {
    export interface Params {
        id: number
        title?: string
    }
    export interface Response {
        recipe: {
            score: MUTATION_RESULT
        }
    }
}
