import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationEquipmentEquip: ApiBaseMutationOptions<MutationEquipmentEquip.Response, MutationEquipmentEquip.Params> = {
    mutation: gql`mutation EquipmentEquip {
        equipment {
            equip,
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.UserEquipment,
    ],
}

export namespace MutationEquipmentEquip {
    export type Params = never
    export interface Response {
        equipment: {
            equip: MUTATION_RESULT
        }
    }
}
