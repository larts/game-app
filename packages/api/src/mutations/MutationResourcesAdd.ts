import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationResourcesAdd: ApiBaseMutationOptions<MutationResourcesAdd.Response, MutationResourcesAdd.Params> = {
    mutation: gql`mutation ResourcesAdd($qrCode: String!) {
        resources {
            add(qrCode: $qrCode),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
    ],
}

export namespace MutationResourcesAdd {
    export interface Params {
        qrCode: string
    }
    export interface Response {
        resources: {
            add: MUTATION_RESULT
        }
    }
}
