import { gql } from '@apollo/client'


import { ApiBaseMutationOptions } from './common'


export const MutationUserLogin: ApiBaseMutationOptions<MutationUserLogin.Response, MutationUserLogin.Params> = {
    mutation: gql`mutation UserLogin($qrCode: String!) {
        auth {
            userLogin(qrCode: $qrCode)
        }
    }`,
    update: (cache) => {
        cache.evict({ fieldName: 'user' })
    },
}

export namespace MutationUserLogin {
    export interface Params {
        qrCode: string
    }
    export interface Response {
        auth: {
            /**
             * In client-side testsuite overriden to contain cookie instead of MUTATION_RESULT.
             */
            userLogin: string
        }
    }
}
