import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationEquipmentBuy: ApiBaseMutationOptions<MutationEquipmentBuy.Response, MutationEquipmentBuy.Params> = {
    mutation: gql`mutation EquipmentAdd {
        equipment {
            buy,
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamEquipment,
        QUERY.TeamResources,
    ],
}

export namespace MutationEquipmentBuy {
    export type Params = never
    export interface Response {
        equipment: {
            buy: MUTATION_RESULT
        }
    }
}
