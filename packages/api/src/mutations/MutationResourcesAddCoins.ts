import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationResourcesAddCoins: ApiBaseMutationOptions<MutationResourcesAddCoins.Response, MutationResourcesAddCoins.Params> = {
    mutation: gql`mutation ResourcesAddCoins($amount: Int!) {
        resources {
            addCoins(amount: $amount),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamResources,
    ],
}

export namespace MutationResourcesAddCoins {
    export interface Params {
        amount: number
    }
    export interface Response {
        resources: {
            addCoins: MUTATION_RESULT
        }
    }
}
