import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationCharacterTribute: ApiBaseMutationOptions<MutationCharacterTribute.Response, MutationCharacterTribute.Params> = {
    mutation: gql`mutation CharacterTribute($code: Int!) {
        character {
            tribute(code: $code),
        }
    }`,
    refetchQueries: [
        QUERY.Session,
    ],
}

export namespace MutationCharacterTribute {
    export interface Params {
        code: number
    }
    export interface Response {
        character: {
            tribute: MUTATION_RESULT
        }
    }
}
