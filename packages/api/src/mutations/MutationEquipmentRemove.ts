import { gql } from '@apollo/client'

import { QUERY } from '../queries/common'

import { ApiBaseMutationOptions, MUTATION_RESULT } from './common'


export const MutationEquipmentRemove: ApiBaseMutationOptions<MutationEquipmentRemove.Response, MutationEquipmentRemove.Params> = {
    mutation: gql`mutation EquipmentAdd {
        equipment {
            remove,
        }
    }`,
    refetchQueries: [
        QUERY.Session,
        QUERY.TeamEquipment,
        QUERY.UserEquipment,
    ],
}

export namespace MutationEquipmentRemove {
    export type Params = never
    export interface Response {
        equipment: {
            remove: MUTATION_RESULT
        }
    }
}
