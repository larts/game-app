export enum PILLAR {
    Victory = 1,
    Military = 2,
    Agriculture = 3,
    Industry = 4,
    FridayFeastSpecial = 5,
}

export enum PILLAR_REAL {
    Military = 2,
    Agriculture = 3,
    Industry = 4,
}
