import { FieldPolicy, FieldReadFunction, TypePolicies, TypePolicy } from '@apollo/client/cache'


export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: { input: string; output: string; }
    String: { input: string; output: string; }
    Boolean: { input: boolean; output: boolean; }
    Int: { input: number; output: number; }
    Float: { input: number; output: number; }
};

export type Auth = {
    __typename?: 'Auth';
    deviceRegister: Scalars['String']['output'];
    deviceRemove: Scalars['String']['output'];
    userLogin: Scalars['String']['output'];
    userLogout: Scalars['String']['output'];
};


export type AuthDeviceRegisterArgs = {
    name: Scalars['String']['input'];
    team?: InputMaybe<Scalars['Int']['input']>;
};


export type AuthUserLoginArgs = {
    qrCode: Scalars['String']['input'];
};

/** Different selectable server data */
export type BaseData = {
    __typename?: 'BaseData';
    /** Get current server time */
    currentTime: Scalars['Int']['output'];
    /** Get elapsed seconds since start of current season */
    elapsedSeasonTime?: Maybe<Scalars['Int']['output']>;
    /** Get elapsed seconds since start of game */
    elapsedTime?: Maybe<Scalars['Int']['output']>;
    isMarketDevice: Scalars['Boolean']['output'];
    resourceTypes: Array<ResourceType>;
    /** Request one specific setting valuem */
    setting?: Maybe<Settings>;
    /** Get current server time */
    settings: Array<Settings>;
    userSessionEnd?: Maybe<Scalars['Int']['output']>;
};


/** Different selectable server data */
export type BaseDataSettingArgs = {
    id: Scalars['Int']['input'];
};

/** Camp */
export type Camp = {
    __typename?: 'Camp';
    displayTitle: Scalars['String']['output'];
    id: Scalars['Int']['output'];
    latitude?: Maybe<Scalars['Float']['output']>;
    longitude?: Maybe<Scalars['Float']['output']>;
    resourceType: ResourceType;
    title: Scalars['String']['output'];
};

/** Castle */
export type Castle = {
    __typename?: 'Castle';
    id: Scalars['Int']['output'];
    latitude?: Maybe<Scalars['Float']['output']>;
    longitude?: Maybe<Scalars['Float']['output']>;
    team?: Maybe<Team>;
    title: Scalars['String']['output'];
};

/** Character */
export type Character = {
    __typename?: 'Character';
    charcode: Scalars['Int']['output'];
    isScored: Scalars['Boolean']['output'];
};

export type CharacterActions = {
    __typename?: 'CharacterActions';
    respawn: Scalars['String']['output'];
    revitalize: Scalars['String']['output'];
    tribute: Scalars['String']['output'];
};


export type CharacterActionsRespawnArgs = {
    code: Scalars['Int']['input'];
};


export type CharacterActionsTributeArgs = {
    code: Scalars['Int']['input'];
};

export type DebugActions = {
    __typename?: 'DebugActions';
    forward: Scalars['String']['output'];
    setTick: Scalars['String']['output'];
};


export type DebugActionsForwardArgs = {
    increase: Scalars['Int']['input'];
};


export type DebugActionsSetTickArgs = {
    increase: Scalars['Int']['input'];
};

export type EquipmentActions = {
    __typename?: 'EquipmentActions';
    add: Scalars['String']['output'];
    buy: Scalars['String']['output'];
    equip: Scalars['String']['output'];
    remove: Scalars['String']['output'];
    unequip: Scalars['String']['output'];
};

/** Camp expansion */
export type Expansion = {
    __typename?: 'Expansion';
    camp: Camp;
    code: Scalars['String']['output'];
    /** Team who received original expansion */
    owners: Team;
    /** Team who got rewarded for expansion, null if not scanned */
    rewarded?: Maybe<Team>;
    rewardedAt: Scalars['Int']['output'];
    showedAt: Scalars['Int']['output'];
};

export type ExpansionActions = {
    __typename?: 'ExpansionActions';
    reward: Scalars['String']['output'];
};


export type ExpansionActionsRewardArgs = {
    code: Scalars['String']['input'];
};

export type FeastActions = {
    __typename?: 'FeastActions';
    score: Scalars['String']['output'];
};


export type FeastActionsScoreArgs = {
    id: Scalars['Int']['input'];
    qrList: Array<Scalars['String']['input']>;
};

/** Feast token */
export type FeastToken = {
    __typename?: 'FeastToken';
    id: Scalars['Int']['output'];
    recipe?: Maybe<Recipe>;
    season: Season;
    usedAt?: Maybe<Scalars['Int']['output']>;
};

export type FlagActions = {
    __typename?: 'FlagActions';
    score: Scalars['String']['output'];
};


export type FlagActionsScoreArgs = {
    qrCode: Scalars['String']['input'];
};

export type Mutation = {
    __typename?: 'Mutation';
    auth?: Maybe<Auth>;
    character?: Maybe<CharacterActions>;
    debug?: Maybe<DebugActions>;
    equipment?: Maybe<EquipmentActions>;
    expansions?: Maybe<ExpansionActions>;
    feast?: Maybe<FeastActions>;
    flag?: Maybe<FlagActions>;
    night?: Maybe<NightActions>;
    recipe?: Maybe<RecipeActions>;
    resources?: Maybe<ResourcesActions>;
    workshop?: Maybe<WorkshopActions>;
};

export type NightActions = {
    __typename?: 'NightActions';
    score: Scalars['String']['output'];
};


export type NightActionsScoreArgs = {
    qrCode: Scalars['String']['input'];
};

/** Pillar of scoring */
export type Pillar = {
    __typename?: 'Pillar';
    allSeasons: Array<PillarOfSeason>;
    id: Scalars['Int']['output'];
    season?: Maybe<PillarOfSeason>;
    title: Scalars['String']['output'];
};


/** Pillar of scoring */
export type PillarSeasonArgs = {
    season?: InputMaybe<Scalars['Int']['input']>;
};

/** Pillar of scoring */
export type PillarOfSeason = {
    __typename?: 'PillarOfSeason';
    /** Current total score for this season */
    currentTotal: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    pillar: Pillar;
    scores: Array<Score>;
    season: Season;
    /** Target value of this pillar for this season */
    target: Scalars['Int']['output'];
    totalScores: Array<TotalScore>;
};

export type Query = {
    __typename?: 'Query';
    baseData: BaseData;
    pillar?: Maybe<Pillar>;
    pillars: Array<Pillar>;
    recipes: Array<Recipe>;
    season?: Maybe<Season>;
    /** Request specific teams information. If no id provided will return current devices team */
    team?: Maybe<Team>;
    teams: Array<Team>;
    user?: Maybe<User>;
    victoryPoints: VictoryPoints;
    workshops: Array<WorkshopType>;
};


export type QueryPillarArgs = {
    id: Scalars['Int']['input'];
};


export type QuerySeasonArgs = {
    season?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryTeamArgs = {
    id?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryUserArgs = {
    qrCode?: InputMaybe<Scalars['String']['input']>;
};

/** Recipe */
export type Recipe = {
    __typename?: 'Recipe';
    cookLog: Array<RecipeLog>;
    firstCookedBy?: Maybe<Team>;
    id: Scalars['Int']['output'];
    resources: Array<ResourceType>;
    title?: Maybe<Scalars['String']['output']>;
};

export type RecipeActions = {
    __typename?: 'RecipeActions';
    score: Scalars['String']['output'];
};


export type RecipeActionsScoreArgs = {
    id: Scalars['Int']['input'];
    title?: InputMaybe<Scalars['String']['input']>;
};

/** Recipe */
export type RecipeLog = {
    __typename?: 'RecipeLog';
    /** When this recipe was created */
    created: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    /** When this recipe will start scoring points (differs from created if in queue) */
    starts: Scalars['Int']['output'];
    team: Team;
};

/** One resource totals for team */
export type Resource = {
    __typename?: 'Resource';
    available: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    locked: Scalars['Int']['output'];
    spent: Scalars['Int']['output'];
    team?: Maybe<Team>;
    total: Scalars['Int']['output'];
    totalAmount: Scalars['Int']['output'];
    /** Resource type */
    type: ResourceType;
};

/** Resource type information */
export type ResourceType = {
    __typename?: 'ResourceType';
    /** QR code prefix for resource */
    code: Scalars['String']['output'];
    id: Scalars['Int']['output'];
    name: Scalars['String']['output'];
    subtype: Scalars['String']['output'];
};

export type ResourcesActions = {
    __typename?: 'ResourcesActions';
    add: Scalars['String']['output'];
    addCoins: Scalars['String']['output'];
    removeCoins: Scalars['String']['output'];
    sell: Scalars['String']['output'];
};


export type ResourcesActionsAddArgs = {
    qrCode: Scalars['String']['input'];
};


export type ResourcesActionsAddCoinsArgs = {
    amount: Scalars['Int']['input'];
};


export type ResourcesActionsRemoveCoinsArgs = {
    amount: Scalars['Int']['input'];
};


export type ResourcesActionsSellArgs = {
    amount: Scalars['Int']['input'];
    id: Scalars['Int']['input'];
};

/** Score row */
export type Score = {
    __typename?: 'Score';
    created: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    pillar: Pillar;
    reason: ScoreReasons;
    scored: Scalars['Int']['output'];
    season: Season;
    team: Team;
};

/** Scorinng reasons */
export type ScoreReasons = {
    __typename?: 'ScoreReasons';
    amount: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    pillar?: Maybe<Pillar>;
    /** Season this event is attached to */
    season?: Maybe<Season>;
    title: Scalars['String']['output'];
};

/** Season data */
export type Season = {
    __typename?: 'Season';
    ended?: Maybe<Scalars['Int']['output']>;
    id: Scalars['Int']['output'];
    /** season that will follow this */
    nextSeason?: Maybe<Season>;
    /** Season's pillar data */
    pillars: Array<PillarOfSeason>;
    /** season that preceded this */
    previousSeason?: Maybe<Season>;
    started?: Maybe<Scalars['Int']['output']>;
    title: Scalars['String']['output'];
};

/** Settings file */
export type Settings = {
    __typename?: 'Settings';
    code: Scalars['String']['output'];
    title: Scalars['String']['output'];
    value: Scalars['String']['output'];
};

/** Team of players */
export type Team = {
    __typename?: 'Team';
    availableEq: Scalars['Int']['output'];
    castle?: Maybe<Castle>;
    coinbox?: Maybe<Scalars['Int']['output']>;
    color: Scalars['String']['output'];
    /** Return list of available and/or scored expansions */
    expansions: Array<Expansion>;
    feastTokens: Array<FeastToken>;
    flagRaisedTime?: Maybe<Scalars['Int']['output']>;
    flagScoredTime?: Maybe<Scalars['Int']['output']>;
    flagStatusId: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    nextRespawnTime?: Maybe<Scalars['Int']['output']>;
    players: Array<User>;
    /** All available resources for the team */
    resources: Array<Resource>;
    scoreTotals: Array<TotalScore>;
    scores: Array<Score>;
    title: Scalars['String']['output'];
    /** Return list of all workshop activations */
    workshopActivations: Array<WorkshopActivation>;
};


/** Team of players */
export type TeamScoreTotalsArgs = {
    season?: InputMaybe<Scalars['Int']['input']>;
};


/** Team of players */
export type TeamScoresArgs = {
    season?: InputMaybe<Scalars['Int']['input']>;
};

/** Total score row */
export type TotalScore = {
    __typename?: 'TotalScore';
    id: Scalars['Int']['output'];
    lastUpdate?: Maybe<Scalars['Int']['output']>;
    pillar: Pillar;
    rank: Scalars['Int']['output'];
    score: Scalars['Int']['output'];
    season: Season;
    team: Team;
};

/** Get current user */
export type User = {
    __typename?: 'User';
    character?: Maybe<Character>;
    equipmentHasPhoenix: Scalars['Boolean']['output'];
    equipmentPoints: Scalars['Int']['output'];
    fullName?: Maybe<Scalars['String']['output']>;
    /** The id of the user. */
    id: Scalars['Int']['output'];
    name?: Maybe<Scalars['String']['output']>;
    qr: Scalars['String']['output'];
    respawnCount: Scalars['Int']['output'];
    sex: Scalars['String']['output'];
    team: Team;
};

/** Victory point data */
export type VictoryPoints = {
    __typename?: 'VictoryPoints';
    points: Array<Score>;
    totals: Array<VictoryTotals>;
};

/** Victory point totals */
export type VictoryTotals = {
    __typename?: 'VictoryTotals';
    points: Scalars['Int']['output'];
    team: Team;
};

export type WorkshopActions = {
    __typename?: 'WorkshopActions';
    activate: Scalars['String']['output'];
    cancel: Scalars['String']['output'];
};


export type WorkshopActionsActivateArgs = {
    input: WorkshopActivateInput;
};

/** Workshop Activation Input */
export type WorkshopActivateFoodInput = {
    amount: Scalars['Int']['input'];
    variant: Scalars['Int']['input'];
};

/** Workshop Activation Input */
export type WorkshopActivateInput = {
    /** ID of food subtype IF selected workshop runs on food! */
    food?: InputMaybe<Array<WorkshopActivateFoodInput>>;
    sizeId: Scalars['Int']['input'];
    variant: Scalars['Int']['input'];
    wood: Scalars['Int']['input'];
};

/** Activation log for workshops */
export type WorkshopActivation = {
    __typename?: 'WorkshopActivation';
    /** NULL or timestamp if at any point was cancelled */
    cancelled?: Maybe<Scalars['Int']['output']>;
    /** At this time this was created */
    created: Scalars['Int']['output'];
    /** Timestamp of task end time */
    finishTime: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    priceDetails: Array<PriceDetailsEntry>;
    /** list of score points */
    scorePoints: Array<Score>;
    size: WorkshopType;
    /** Timestamp of task beginning time */
    startTime: Scalars['Int']['output'];
    variant: Scalars['Int']['output'];
};

/** Types of workshops - represent type of workshop - resource and size */
export type WorkshopType = {
    __typename?: 'WorkshopType';
    coin: Scalars['Int']['output'];
    cooldown: Scalars['Int']['output'];
    id: Scalars['Int']['output'];
    ip: Scalars['Int']['output'];
    /** Cost of resource that s specified in kit type */
    resourceCost: Scalars['Int']['output'];
    score: ScoreReasons;
    wood: Scalars['Int']['output'];
};

export type PriceDetailsEntry = {
    __typename?: 'priceDetailsEntry';
    amount: Scalars['Int']['output'];
    type: Scalars['Int']['output'];
};


export interface PossibleTypesResultData {
    possibleTypes: {
        [key: string]: string[]
    }
}
const result: PossibleTypesResultData = {
    'possibleTypes': {},
}
export default result
    
export type AuthKeySpecifier = ('deviceRegister' | 'deviceRemove' | 'userLogin' | 'userLogout' | AuthKeySpecifier)[];
export type AuthFieldPolicy = {
    deviceRegister?: FieldPolicy<any> | FieldReadFunction<any>,
    deviceRemove?: FieldPolicy<any> | FieldReadFunction<any>,
    userLogin?: FieldPolicy<any> | FieldReadFunction<any>,
    userLogout?: FieldPolicy<any> | FieldReadFunction<any>
};
export type BaseDataKeySpecifier = ('currentTime' | 'elapsedSeasonTime' | 'elapsedTime' | 'isMarketDevice' | 'resourceTypes' | 'setting' | 'settings' | 'userSessionEnd' | BaseDataKeySpecifier)[];
export type BaseDataFieldPolicy = {
    currentTime?: FieldPolicy<any> | FieldReadFunction<any>,
    elapsedSeasonTime?: FieldPolicy<any> | FieldReadFunction<any>,
    elapsedTime?: FieldPolicy<any> | FieldReadFunction<any>,
    isMarketDevice?: FieldPolicy<any> | FieldReadFunction<any>,
    resourceTypes?: FieldPolicy<any> | FieldReadFunction<any>,
    setting?: FieldPolicy<any> | FieldReadFunction<any>,
    settings?: FieldPolicy<any> | FieldReadFunction<any>,
    userSessionEnd?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CampKeySpecifier = ('displayTitle' | 'id' | 'latitude' | 'longitude' | 'resourceType' | 'title' | CampKeySpecifier)[];
export type CampFieldPolicy = {
    displayTitle?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    latitude?: FieldPolicy<any> | FieldReadFunction<any>,
    longitude?: FieldPolicy<any> | FieldReadFunction<any>,
    resourceType?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CastleKeySpecifier = ('id' | 'latitude' | 'longitude' | 'team' | 'title' | CastleKeySpecifier)[];
export type CastleFieldPolicy = {
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    latitude?: FieldPolicy<any> | FieldReadFunction<any>,
    longitude?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CharacterKeySpecifier = ('charcode' | 'isScored' | CharacterKeySpecifier)[];
export type CharacterFieldPolicy = {
    charcode?: FieldPolicy<any> | FieldReadFunction<any>,
    isScored?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CharacterActionsKeySpecifier = ('respawn' | 'revitalize' | 'tribute' | CharacterActionsKeySpecifier)[];
export type CharacterActionsFieldPolicy = {
    respawn?: FieldPolicy<any> | FieldReadFunction<any>,
    revitalize?: FieldPolicy<any> | FieldReadFunction<any>,
    tribute?: FieldPolicy<any> | FieldReadFunction<any>
};
export type DebugActionsKeySpecifier = ('forward' | 'setTick' | DebugActionsKeySpecifier)[];
export type DebugActionsFieldPolicy = {
    forward?: FieldPolicy<any> | FieldReadFunction<any>,
    setTick?: FieldPolicy<any> | FieldReadFunction<any>
};
export type EquipmentActionsKeySpecifier = ('add' | 'buy' | 'equip' | 'remove' | 'unequip' | EquipmentActionsKeySpecifier)[];
export type EquipmentActionsFieldPolicy = {
    add?: FieldPolicy<any> | FieldReadFunction<any>,
    buy?: FieldPolicy<any> | FieldReadFunction<any>,
    equip?: FieldPolicy<any> | FieldReadFunction<any>,
    remove?: FieldPolicy<any> | FieldReadFunction<any>,
    unequip?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ExpansionKeySpecifier = ('camp' | 'code' | 'owners' | 'rewarded' | 'rewardedAt' | 'showedAt' | ExpansionKeySpecifier)[];
export type ExpansionFieldPolicy = {
    camp?: FieldPolicy<any> | FieldReadFunction<any>,
    code?: FieldPolicy<any> | FieldReadFunction<any>,
    owners?: FieldPolicy<any> | FieldReadFunction<any>,
    rewarded?: FieldPolicy<any> | FieldReadFunction<any>,
    rewardedAt?: FieldPolicy<any> | FieldReadFunction<any>,
    showedAt?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ExpansionActionsKeySpecifier = ('reward' | ExpansionActionsKeySpecifier)[];
export type ExpansionActionsFieldPolicy = {
    reward?: FieldPolicy<any> | FieldReadFunction<any>
};
export type FeastActionsKeySpecifier = ('score' | FeastActionsKeySpecifier)[];
export type FeastActionsFieldPolicy = {
    score?: FieldPolicy<any> | FieldReadFunction<any>
};
export type FeastTokenKeySpecifier = ('id' | 'recipe' | 'season' | 'usedAt' | FeastTokenKeySpecifier)[];
export type FeastTokenFieldPolicy = {
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    recipe?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    usedAt?: FieldPolicy<any> | FieldReadFunction<any>
};
export type FlagActionsKeySpecifier = ('score' | FlagActionsKeySpecifier)[];
export type FlagActionsFieldPolicy = {
    score?: FieldPolicy<any> | FieldReadFunction<any>
};
export type MutationKeySpecifier = ('auth' | 'character' | 'debug' | 'equipment' | 'expansions' | 'feast' | 'flag' | 'night' | 'recipe' | 'resources' | 'workshop' | MutationKeySpecifier)[];
export type MutationFieldPolicy = {
    auth?: FieldPolicy<any> | FieldReadFunction<any>,
    character?: FieldPolicy<any> | FieldReadFunction<any>,
    debug?: FieldPolicy<any> | FieldReadFunction<any>,
    equipment?: FieldPolicy<any> | FieldReadFunction<any>,
    expansions?: FieldPolicy<any> | FieldReadFunction<any>,
    feast?: FieldPolicy<any> | FieldReadFunction<any>,
    flag?: FieldPolicy<any> | FieldReadFunction<any>,
    night?: FieldPolicy<any> | FieldReadFunction<any>,
    recipe?: FieldPolicy<any> | FieldReadFunction<any>,
    resources?: FieldPolicy<any> | FieldReadFunction<any>,
    workshop?: FieldPolicy<any> | FieldReadFunction<any>
};
export type NightActionsKeySpecifier = ('score' | NightActionsKeySpecifier)[];
export type NightActionsFieldPolicy = {
    score?: FieldPolicy<any> | FieldReadFunction<any>
};
export type PillarKeySpecifier = ('allSeasons' | 'id' | 'season' | 'title' | PillarKeySpecifier)[];
export type PillarFieldPolicy = {
    allSeasons?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type PillarOfSeasonKeySpecifier = ('currentTotal' | 'id' | 'pillar' | 'scores' | 'season' | 'target' | 'totalScores' | PillarOfSeasonKeySpecifier)[];
export type PillarOfSeasonFieldPolicy = {
    currentTotal?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    pillar?: FieldPolicy<any> | FieldReadFunction<any>,
    scores?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    target?: FieldPolicy<any> | FieldReadFunction<any>,
    totalScores?: FieldPolicy<any> | FieldReadFunction<any>
};
export type QueryKeySpecifier = ('baseData' | 'pillar' | 'pillars' | 'recipes' | 'season' | 'team' | 'teams' | 'user' | 'victoryPoints' | 'workshops' | QueryKeySpecifier)[];
export type QueryFieldPolicy = {
    baseData?: FieldPolicy<any> | FieldReadFunction<any>,
    pillar?: FieldPolicy<any> | FieldReadFunction<any>,
    pillars?: FieldPolicy<any> | FieldReadFunction<any>,
    recipes?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>,
    teams?: FieldPolicy<any> | FieldReadFunction<any>,
    user?: FieldPolicy<any> | FieldReadFunction<any>,
    victoryPoints?: FieldPolicy<any> | FieldReadFunction<any>,
    workshops?: FieldPolicy<any> | FieldReadFunction<any>
};
export type RecipeKeySpecifier = ('cookLog' | 'firstCookedBy' | 'id' | 'resources' | 'title' | RecipeKeySpecifier)[];
export type RecipeFieldPolicy = {
    cookLog?: FieldPolicy<any> | FieldReadFunction<any>,
    firstCookedBy?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    resources?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type RecipeActionsKeySpecifier = ('score' | RecipeActionsKeySpecifier)[];
export type RecipeActionsFieldPolicy = {
    score?: FieldPolicy<any> | FieldReadFunction<any>
};
export type RecipeLogKeySpecifier = ('created' | 'id' | 'starts' | 'team' | RecipeLogKeySpecifier)[];
export type RecipeLogFieldPolicy = {
    created?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    starts?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ResourceKeySpecifier = ('available' | 'id' | 'locked' | 'spent' | 'team' | 'total' | 'totalAmount' | 'type' | ResourceKeySpecifier)[];
export type ResourceFieldPolicy = {
    available?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    locked?: FieldPolicy<any> | FieldReadFunction<any>,
    spent?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>,
    total?: FieldPolicy<any> | FieldReadFunction<any>,
    totalAmount?: FieldPolicy<any> | FieldReadFunction<any>,
    type?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ResourceTypeKeySpecifier = ('code' | 'id' | 'name' | 'subtype' | ResourceTypeKeySpecifier)[];
export type ResourceTypeFieldPolicy = {
    code?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    name?: FieldPolicy<any> | FieldReadFunction<any>,
    subtype?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ResourcesActionsKeySpecifier = ('add' | 'addCoins' | 'removeCoins' | 'sell' | ResourcesActionsKeySpecifier)[];
export type ResourcesActionsFieldPolicy = {
    add?: FieldPolicy<any> | FieldReadFunction<any>,
    addCoins?: FieldPolicy<any> | FieldReadFunction<any>,
    removeCoins?: FieldPolicy<any> | FieldReadFunction<any>,
    sell?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ScoreKeySpecifier = ('created' | 'id' | 'pillar' | 'reason' | 'scored' | 'season' | 'team' | ScoreKeySpecifier)[];
export type ScoreFieldPolicy = {
    created?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    pillar?: FieldPolicy<any> | FieldReadFunction<any>,
    reason?: FieldPolicy<any> | FieldReadFunction<any>,
    scored?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ScoreReasonsKeySpecifier = ('amount' | 'id' | 'pillar' | 'season' | 'title' | ScoreReasonsKeySpecifier)[];
export type ScoreReasonsFieldPolicy = {
    amount?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    pillar?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type SeasonKeySpecifier = ('ended' | 'id' | 'nextSeason' | 'pillars' | 'previousSeason' | 'started' | 'title' | SeasonKeySpecifier)[];
export type SeasonFieldPolicy = {
    ended?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    nextSeason?: FieldPolicy<any> | FieldReadFunction<any>,
    pillars?: FieldPolicy<any> | FieldReadFunction<any>,
    previousSeason?: FieldPolicy<any> | FieldReadFunction<any>,
    started?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>
};
export type SettingsKeySpecifier = ('code' | 'title' | 'value' | SettingsKeySpecifier)[];
export type SettingsFieldPolicy = {
    code?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>,
    value?: FieldPolicy<any> | FieldReadFunction<any>
};
export type TeamKeySpecifier = ('availableEq' | 'castle' | 'coinbox' | 'color' | 'expansions' | 'feastTokens' | 'flagRaisedTime' | 'flagScoredTime' | 'flagStatusId' | 'id' | 'nextRespawnTime' | 'players' | 'resources' | 'scoreTotals' | 'scores' | 'title' | 'workshopActivations' | TeamKeySpecifier)[];
export type TeamFieldPolicy = {
    availableEq?: FieldPolicy<any> | FieldReadFunction<any>,
    castle?: FieldPolicy<any> | FieldReadFunction<any>,
    coinbox?: FieldPolicy<any> | FieldReadFunction<any>,
    color?: FieldPolicy<any> | FieldReadFunction<any>,
    expansions?: FieldPolicy<any> | FieldReadFunction<any>,
    feastTokens?: FieldPolicy<any> | FieldReadFunction<any>,
    flagRaisedTime?: FieldPolicy<any> | FieldReadFunction<any>,
    flagScoredTime?: FieldPolicy<any> | FieldReadFunction<any>,
    flagStatusId?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    nextRespawnTime?: FieldPolicy<any> | FieldReadFunction<any>,
    players?: FieldPolicy<any> | FieldReadFunction<any>,
    resources?: FieldPolicy<any> | FieldReadFunction<any>,
    scoreTotals?: FieldPolicy<any> | FieldReadFunction<any>,
    scores?: FieldPolicy<any> | FieldReadFunction<any>,
    title?: FieldPolicy<any> | FieldReadFunction<any>,
    workshopActivations?: FieldPolicy<any> | FieldReadFunction<any>
};
export type TotalScoreKeySpecifier = ('id' | 'lastUpdate' | 'pillar' | 'rank' | 'score' | 'season' | 'team' | TotalScoreKeySpecifier)[];
export type TotalScoreFieldPolicy = {
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    lastUpdate?: FieldPolicy<any> | FieldReadFunction<any>,
    pillar?: FieldPolicy<any> | FieldReadFunction<any>,
    rank?: FieldPolicy<any> | FieldReadFunction<any>,
    score?: FieldPolicy<any> | FieldReadFunction<any>,
    season?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>
};
export type UserKeySpecifier = ('character' | 'equipmentHasPhoenix' | 'equipmentPoints' | 'fullName' | 'id' | 'name' | 'qr' | 'respawnCount' | 'sex' | 'team' | UserKeySpecifier)[];
export type UserFieldPolicy = {
    character?: FieldPolicy<any> | FieldReadFunction<any>,
    equipmentHasPhoenix?: FieldPolicy<any> | FieldReadFunction<any>,
    equipmentPoints?: FieldPolicy<any> | FieldReadFunction<any>,
    fullName?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    name?: FieldPolicy<any> | FieldReadFunction<any>,
    qr?: FieldPolicy<any> | FieldReadFunction<any>,
    respawnCount?: FieldPolicy<any> | FieldReadFunction<any>,
    sex?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>
};
export type VictoryPointsKeySpecifier = ('points' | 'totals' | VictoryPointsKeySpecifier)[];
export type VictoryPointsFieldPolicy = {
    points?: FieldPolicy<any> | FieldReadFunction<any>,
    totals?: FieldPolicy<any> | FieldReadFunction<any>
};
export type VictoryTotalsKeySpecifier = ('points' | 'team' | VictoryTotalsKeySpecifier)[];
export type VictoryTotalsFieldPolicy = {
    points?: FieldPolicy<any> | FieldReadFunction<any>,
    team?: FieldPolicy<any> | FieldReadFunction<any>
};
export type WorkshopActionsKeySpecifier = ('activate' | 'cancel' | WorkshopActionsKeySpecifier)[];
export type WorkshopActionsFieldPolicy = {
    activate?: FieldPolicy<any> | FieldReadFunction<any>,
    cancel?: FieldPolicy<any> | FieldReadFunction<any>
};
export type WorkshopActivationKeySpecifier = ('cancelled' | 'created' | 'finishTime' | 'id' | 'priceDetails' | 'scorePoints' | 'size' | 'startTime' | 'variant' | WorkshopActivationKeySpecifier)[];
export type WorkshopActivationFieldPolicy = {
    cancelled?: FieldPolicy<any> | FieldReadFunction<any>,
    created?: FieldPolicy<any> | FieldReadFunction<any>,
    finishTime?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    priceDetails?: FieldPolicy<any> | FieldReadFunction<any>,
    scorePoints?: FieldPolicy<any> | FieldReadFunction<any>,
    size?: FieldPolicy<any> | FieldReadFunction<any>,
    startTime?: FieldPolicy<any> | FieldReadFunction<any>,
    variant?: FieldPolicy<any> | FieldReadFunction<any>
};
export type WorkshopTypeKeySpecifier = ('coin' | 'cooldown' | 'id' | 'ip' | 'resourceCost' | 'score' | 'wood' | WorkshopTypeKeySpecifier)[];
export type WorkshopTypeFieldPolicy = {
    coin?: FieldPolicy<any> | FieldReadFunction<any>,
    cooldown?: FieldPolicy<any> | FieldReadFunction<any>,
    id?: FieldPolicy<any> | FieldReadFunction<any>,
    ip?: FieldPolicy<any> | FieldReadFunction<any>,
    resourceCost?: FieldPolicy<any> | FieldReadFunction<any>,
    score?: FieldPolicy<any> | FieldReadFunction<any>,
    wood?: FieldPolicy<any> | FieldReadFunction<any>
};
export type priceDetailsEntryKeySpecifier = ('amount' | 'type' | priceDetailsEntryKeySpecifier)[];
export type priceDetailsEntryFieldPolicy = {
    amount?: FieldPolicy<any> | FieldReadFunction<any>,
    type?: FieldPolicy<any> | FieldReadFunction<any>
};
export type StrictTypedTypePolicies = {
    Auth?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | AuthKeySpecifier | (() => undefined | AuthKeySpecifier),
        fields?: AuthFieldPolicy,
    },
    BaseData?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | BaseDataKeySpecifier | (() => undefined | BaseDataKeySpecifier),
        fields?: BaseDataFieldPolicy,
    },
    Camp?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | CampKeySpecifier | (() => undefined | CampKeySpecifier),
        fields?: CampFieldPolicy,
    },
    Castle?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | CastleKeySpecifier | (() => undefined | CastleKeySpecifier),
        fields?: CastleFieldPolicy,
    },
    Character?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | CharacterKeySpecifier | (() => undefined | CharacterKeySpecifier),
        fields?: CharacterFieldPolicy,
    },
    CharacterActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | CharacterActionsKeySpecifier | (() => undefined | CharacterActionsKeySpecifier),
        fields?: CharacterActionsFieldPolicy,
    },
    DebugActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | DebugActionsKeySpecifier | (() => undefined | DebugActionsKeySpecifier),
        fields?: DebugActionsFieldPolicy,
    },
    EquipmentActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | EquipmentActionsKeySpecifier | (() => undefined | EquipmentActionsKeySpecifier),
        fields?: EquipmentActionsFieldPolicy,
    },
    Expansion?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ExpansionKeySpecifier | (() => undefined | ExpansionKeySpecifier),
        fields?: ExpansionFieldPolicy,
    },
    ExpansionActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ExpansionActionsKeySpecifier | (() => undefined | ExpansionActionsKeySpecifier),
        fields?: ExpansionActionsFieldPolicy,
    },
    FeastActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | FeastActionsKeySpecifier | (() => undefined | FeastActionsKeySpecifier),
        fields?: FeastActionsFieldPolicy,
    },
    FeastToken?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | FeastTokenKeySpecifier | (() => undefined | FeastTokenKeySpecifier),
        fields?: FeastTokenFieldPolicy,
    },
    FlagActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | FlagActionsKeySpecifier | (() => undefined | FlagActionsKeySpecifier),
        fields?: FlagActionsFieldPolicy,
    },
    Mutation?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | MutationKeySpecifier | (() => undefined | MutationKeySpecifier),
        fields?: MutationFieldPolicy,
    },
    NightActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | NightActionsKeySpecifier | (() => undefined | NightActionsKeySpecifier),
        fields?: NightActionsFieldPolicy,
    },
    Pillar?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | PillarKeySpecifier | (() => undefined | PillarKeySpecifier),
        fields?: PillarFieldPolicy,
    },
    PillarOfSeason?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | PillarOfSeasonKeySpecifier | (() => undefined | PillarOfSeasonKeySpecifier),
        fields?: PillarOfSeasonFieldPolicy,
    },
    Query?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | QueryKeySpecifier | (() => undefined | QueryKeySpecifier),
        fields?: QueryFieldPolicy,
    },
    Recipe?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | RecipeKeySpecifier | (() => undefined | RecipeKeySpecifier),
        fields?: RecipeFieldPolicy,
    },
    RecipeActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | RecipeActionsKeySpecifier | (() => undefined | RecipeActionsKeySpecifier),
        fields?: RecipeActionsFieldPolicy,
    },
    RecipeLog?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | RecipeLogKeySpecifier | (() => undefined | RecipeLogKeySpecifier),
        fields?: RecipeLogFieldPolicy,
    },
    Resource?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ResourceKeySpecifier | (() => undefined | ResourceKeySpecifier),
        fields?: ResourceFieldPolicy,
    },
    ResourceType?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ResourceTypeKeySpecifier | (() => undefined | ResourceTypeKeySpecifier),
        fields?: ResourceTypeFieldPolicy,
    },
    ResourcesActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ResourcesActionsKeySpecifier | (() => undefined | ResourcesActionsKeySpecifier),
        fields?: ResourcesActionsFieldPolicy,
    },
    Score?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ScoreKeySpecifier | (() => undefined | ScoreKeySpecifier),
        fields?: ScoreFieldPolicy,
    },
    ScoreReasons?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | ScoreReasonsKeySpecifier | (() => undefined | ScoreReasonsKeySpecifier),
        fields?: ScoreReasonsFieldPolicy,
    },
    Season?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | SeasonKeySpecifier | (() => undefined | SeasonKeySpecifier),
        fields?: SeasonFieldPolicy,
    },
    Settings?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | SettingsKeySpecifier | (() => undefined | SettingsKeySpecifier),
        fields?: SettingsFieldPolicy,
    },
    Team?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | TeamKeySpecifier | (() => undefined | TeamKeySpecifier),
        fields?: TeamFieldPolicy,
    },
    TotalScore?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | TotalScoreKeySpecifier | (() => undefined | TotalScoreKeySpecifier),
        fields?: TotalScoreFieldPolicy,
    },
    User?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | UserKeySpecifier | (() => undefined | UserKeySpecifier),
        fields?: UserFieldPolicy,
    },
    VictoryPoints?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | VictoryPointsKeySpecifier | (() => undefined | VictoryPointsKeySpecifier),
        fields?: VictoryPointsFieldPolicy,
    },
    VictoryTotals?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | VictoryTotalsKeySpecifier | (() => undefined | VictoryTotalsKeySpecifier),
        fields?: VictoryTotalsFieldPolicy,
    },
    WorkshopActions?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | WorkshopActionsKeySpecifier | (() => undefined | WorkshopActionsKeySpecifier),
        fields?: WorkshopActionsFieldPolicy,
    },
    WorkshopActivation?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | WorkshopActivationKeySpecifier | (() => undefined | WorkshopActivationKeySpecifier),
        fields?: WorkshopActivationFieldPolicy,
    },
    WorkshopType?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | WorkshopTypeKeySpecifier | (() => undefined | WorkshopTypeKeySpecifier),
        fields?: WorkshopTypeFieldPolicy,
    },
    priceDetailsEntry?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
        keyFields?: false | priceDetailsEntryKeySpecifier | (() => undefined | priceDetailsEntryKeySpecifier),
        fields?: priceDetailsEntryFieldPolicy,
    }
};
export type TypedTypePolicies = StrictTypedTypePolicies & TypePolicies;