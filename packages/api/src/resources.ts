export enum RESOURCE_TYPE {
    Wood = 1,
    Hides = 2,
    Iron = 3,
    Coins = 4,
    Food_Honey = 5,
    Food_Lavender = 6,
    Food_Tomatoes = 7,
    Food_Cucumbers = 8,
    Food_Mushrooms = 9,
    Food_Raspberries = 10,
    Food = 99,  // TODO: refactor away.
}

export enum FOOD_SUBTYPE {
    Honey = 5,
    Lavender = 6,
    Tomatoes = 7,
    Cucumbers = 8,
    Mushrooms = 9,
    Raspberries = 10,
}

export enum AGENT_TYPE {
    Team = 'team',
    Player = 'player',
}

export enum WORKSHOP {
    Tiny = 4,
    Small = 5,
    Medium = 6,
    Large = 7,
    Excessive = 8,
}
