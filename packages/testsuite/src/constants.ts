import { RESOURCE_TYPE } from '@larts/api'

import qrCodes from './qrCodes.json'


export const QR_ALL = qrCodes

export const QR_BY_TYPE = {
    [RESOURCE_TYPE.Wood            ]: qrCodes.filter((qr) => qr.startsWith('WD')),
    [RESOURCE_TYPE.Hides           ]: qrCodes.filter((qr) => qr.startsWith('HD')),
    [RESOURCE_TYPE.Iron            ]: qrCodes.filter((qr) => qr.startsWith('RN')),
    [RESOURCE_TYPE.Food_Honey      ]: qrCodes.filter((qr) => qr.startsWith('Fh')),
    [RESOURCE_TYPE.Food_Lavender   ]: qrCodes.filter((qr) => qr.startsWith('Fv')),
    [RESOURCE_TYPE.Food_Tomatoes   ]: qrCodes.filter((qr) => qr.startsWith('Ft')),
    [RESOURCE_TYPE.Food_Cucumbers  ]: qrCodes.filter((qr) => qr.startsWith('Fc')),
    [RESOURCE_TYPE.Food_Mushrooms  ]: qrCodes.filter((qr) => qr.startsWith('Fm')),
    [RESOURCE_TYPE.Food_Raspberries]: qrCodes.filter((qr) => qr.startsWith('Fr')),
}
