import fetch from 'cross-fetch'

import { execAsync } from './utils'


export default class Admin {
    private dockerNetwork: string

    private dockerMysqlContainer: string
    private dbRootUser: string
    private dbRootPassword: string
    private dbDatabase: string
    private dbRawQueryPrefix: string

    private cronUrlPrefix: string

    constructor(jestWorkerId: number) {
        this.dockerNetwork = `throne-app-testsuite-${jestWorkerId}_network_testsuite_${jestWorkerId}`

        this.dockerMysqlContainer = `t-db-mysql-testsuite-${jestWorkerId}`
        this.dbRootUser = 'root'
        this.dbRootPassword = 'qwerty'
        this.dbDatabase = 'tronis'
        this.dbRawQueryPrefix = [
            'docker run',
            '-i',
            `--network ${this.dockerNetwork}`,
            `-e MYSQL_PWD=${this.dbRootPassword}`,
            '--rm mysql',
            'mysql',
            `-h ${this.dockerMysqlContainer}`,
            `-u ${this.dbRootUser}`,
        ].join(' ')

        this.cronUrlPrefix = `http://lcron.tronis.lv:8${jestWorkerId}/`
    }

    private async dbQuery(query: string) {
        if (query.includes('"')) throw new Error('Queries with doublequote (") are not supported.')
        return execAsync(`${this.dbRawQueryPrefix} ${this.dbDatabase} -e "${query}"`)
    }

    async dbImport(filePath: string) {
        return execAsync(`${this.dbRawQueryPrefix} ${this.dbDatabase} < ${filePath}`)
    }
    async dbCreateDatabase() {
        return execAsync(`${this.dbRawQueryPrefix}                    -e 'CREATE DATABASE ${this.dbDatabase};'`)
    }

    async authDevice(token: string) {
        return this.dbQuery(`UPDATE devices SET is_enabled = '1' WHERE devices.auth_token = '${token}';`)
    }

    async setupGame() {
        await this.dbQuery(`UPDATE settings SET value = '${new Date().toISOString().slice(0, 19).replace('T', ' ')}' WHERE code = 'start-time';`)
    }

    async gameEnabled(value: boolean) {
        await this.dbQuery(`UPDATE settings SET value = '${value ? 1 : 0}' WHERE code = 'switch-game_started';`)
    }

    async tick() {
        const response = await fetch(`${this.cronUrlPrefix}?do=season&run=1`)  // TODO: &log=1 makes too much output and bugs out that php script.
        return response
    }

}
