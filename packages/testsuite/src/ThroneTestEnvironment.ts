import { ChildProcessWithoutNullStreams, spawn } from 'child_process'

import type { EnvironmentContext, JestEnvironmentConfig } from '@jest/environment'
import { TestEnvironment } from 'jest-environment-node'
import { join } from 'path'

import Admin from './Admin'
import ClientAnonymous from './ClientAnonymous'


class ThroneTestEnvironment extends TestEnvironment {
    testPath: string
    docblockPragmas: Partial<Record<'teststate', string>>
    teardowns: (void | (() => void | Promise<void>))[] = []
    declare global: (InstanceType<typeof TestEnvironment>['global']) & typeof globalThis

    constructor({ globalConfig, projectConfig }: JestEnvironmentConfig, context: EnvironmentContext) {
        super({ globalConfig, projectConfig }, context)
        this.testPath = context.testPath
        this.docblockPragmas = context.docblockPragmas
    }

    async teardown() {
        for (const teardown of this.teardowns.reverse()) await teardown?.()
        super.teardown()
    }

    async setup() {
        await super.setup()
        this.teardowns.push(await this.setupServer())
        this.teardowns.push(await this.setupDb())
        this.teardowns.push(this.setupClient())
    }

    async setupServer() {
        let child: ChildProcessWithoutNullStreams

        try {
            await new Promise<void>((resolve, reject) => {
                let readinessCountdown = 2
                const script = join(__dirname, '..', 'scripts', 'new-docker-instance.sh')
                child = spawn(script, [ `${process.env.JEST_WORKER_ID}`], {
                    cwd: join(__dirname, '..'),
                    env: { ...process.env },
                    stdio: 'pipe',
                })

                child.stdout.on('data', (dataBufferOrString) => {
                    for (const line of dataBufferOrString.toString().trim().split('\n')) {
                        console.log(process.env.JEST_WORKER_ID, 'stdout', line)
                        if (line.includes('/usr/sbin/mysqld: ready for connections')) readinessCountdown--
                        if (readinessCountdown === 0) resolve()
                    }
                })

                child.stderr.on('data', (dataBufferOrString) => {
                    for (const line of dataBufferOrString.toString().trim().split('\n')) {
                        console.log(process.env.JEST_WORKER_ID, 'stderr', line)
                    }
                })

                child.on('error', (error: Error) => {
                    console.error(process.env.JEST_WORKER_ID, 'error', error)
                    reject(error)
                })

                child.on('close', (code, signal) => {
                    console.error(process.env.JEST_WORKER_ID, 'close', code, signal)
                    reject(new Error(`Docker failed to initialize (process exited). Make sure you are able to run "yarn instance 1" from .env locally.\n${script}`))
                })
            })
            this.global.admin = new Admin(Number(process.env.JEST_WORKER_ID))
        } catch (err) {
            console.error(process.env.JEST_WORKER_ID, err)
            process.exit(1)
        }
        return () => {
            return new Promise<void>((resolve, reject) => {
                child.on('close', resolve)
                child.kill()
            })
        }
    }

    async setupDb() {
        // await sleep(20)  // TODO: detect readiness. FYI 5 secs are not enough.
        // try {
        //     execSync(`docker run -i --network ${network} -e MYSQL_PWD=${password} --rm mysql mysql -h ${mysqlContainer} -u ${user} -e 'DROP DATABASE \`${db}\`;'`)
        // } catch (err) {
        //     // Try to drop the database just in case.
        // }
        const migrations = [
            '0-limited-db-user.sql',
            '1-tables.sql',
            '2-foreign-keys.sql',
            '3a-constants.sql',
            '3b-constants-qr-codes.sql',
            '4-updates.sql',
        ]
        const timer = Date.now()
        console.log(process.env.JEST_WORKER_ID, 'Creating database..')
        await this.global.admin.dbCreateDatabase()
        for (const [i, migration] of migrations.entries()) {
            console.log(process.env.JEST_WORKER_ID, `Applying base migrations: ${i+1}/${migrations.length} "${migration}"..`)
            await this.global.admin.dbImport(`../db/migrations/${migration}`)
        }
        if (this.docblockPragmas.teststate) {
            console.log(process.env.JEST_WORKER_ID, `Applying teststate migration: ${this.docblockPragmas.teststate}"..`)
            await this.global.admin.dbImport(`../db/migrations/${this.docblockPragmas.teststate}.sql`)
        }
        console.log(process.env.JEST_WORKER_ID, `Database prepared in ${Math.round((Date.now() - timer) / 100)/10} seconds.`)

        return () => undefined
    }

    setupClient() {
        this.global.clientAnonymous = new ClientAnonymous(`http://lapi.tronis.lv:8${process.env.JEST_WORKER_ID}/graphql`)
        this.global.clientAnonymous.admin = this.global.admin

        return () => this.global.clientAnonymous.teardown()
    }
}

module.exports = ThroneTestEnvironment
