import { exec } from 'child_process'

import { ApolloError, ApolloQueryResult } from '@apollo/client'
import { PILLAR } from '@larts/api'
import { assertApolloError, assertError } from '@larts/common'

import ClientUser from './ClientUser'


export const execAsync = async (cmd: string): Promise<string> =>
    new Promise<string>((res, rej) => exec(cmd, (err, str) => err ? rej(err) : res(str)))

export const execAsyncBuffer = async (cmd: string): Promise<Buffer> =>
    new Promise<Buffer>((res, rej) => exec(cmd, { encoding: 'buffer' }, (err, buf) => err ? rej(err) : res(buf)))


export class NoError extends Error {
    constructor(result: string) {
        super('catchError function failed to error.')
        Object.defineProperty(this, 'stack', { value: JSON.stringify(result, undefined, 2) })
        this.stack = result
    }
}
export const catchApolloError = async (fn: () => unknown | Promise<unknown>): Promise<ApolloError> => {
    let result
    try {
        result = await fn()
    } catch (err) {
        assertApolloError(err)
        return err
    }
    throw new NoError(result)
}


export const beforeAllReturn = <T>(fn: () => Promise<T>, timeout?: number): {current: T | null} => {
    const box: {current: T | null} = { current: null }
    beforeAll(async () => {
        box.current = await fn()
    }, timeout)
    return box
}

export const beforeEachReturn = <T>(fn: () => Promise<T>, timeout?: number): {current: T | null} => {
    const box: {current: T | null} = { current: null }
    beforeEach(async () => {
        box.current = await fn()
    }, timeout)
    return box
}

export const testGraphqlError = (errorMsgOrMsgs: string/* | string[] */, cb: () => Promise<unknown>) => {
    const errorMsgs = Array.isArray(errorMsgOrMsgs) ? errorMsgOrMsgs : [errorMsgOrMsgs]
    test(`graphQL throws one error with message "${errorMsgs[0]}"`, async () => {
        const error = await catchApolloError(cb)
        try { expect(error.message).toEqual(errorMsgs[0])                } catch (e) { assertError(e); console.log(String(e.stack).replace(/\s/g, '+')); e.stack = e.stack?.split('\n').slice(0, 4).concat('\n', error.stack ?? '').join('\n'); throw e }
        try { expect(error.graphQLErrors).toHaveLength(errorMsgs.length) } catch (e) { assertError(e); console.log(String(e.stack).replace(/\s/g, '+')); e.stack = e.stack?.split('\n').slice(0, 4).concat('\n', error.stack ?? '').join('\n'); throw e }
    })
}

export const expectGraphqlQuery = async <T>(promise: Promise<ApolloQueryResult<T>>, result: T) => {
    return expect(promise).resolves.toMatchObject({ data: result })
}

export const testTeamScore = (message: string, user: {current: ClientUser | null}, pillarId: PILLAR, score: number, rank = 1) => {
    const shortname =
        pillarId === PILLAR.Victory ? 'VP' :
            pillarId === PILLAR.Military ? 'MP' :
                pillarId === PILLAR.Agriculture ? 'AP' :
                    pillarId === PILLAR.Industry ? 'IP' :
                        ''

    test(`${message} (total ${score} ${shortname}, ranked ${rank})`, async () => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const seasonId = (await user.current!.querySeason({})).data.season!.id
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const scoreTotals = (await user.current!.queryTeamsScoreTotals()).data.team?.scoreTotals
        const scoreTotal = scoreTotals?.find((v) => v.pillar.id === pillarId && v.pillar.season?.season.id === seasonId)

        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        expect(scoreTotal).toMatchObject({
            pillar: expect.objectContaining({
                id: pillarId,
                season: expect.objectContaining({
                    season: expect.objectContaining({
                        id: seasonId,
                    }),
                }),
            }),
            score,
            rank,
        })
    })
}

export const testTeamScores = (message: string, user: {current: ClientUser | null}, scores: Map<PILLAR, [number, number]>) => {
    const summary = [...scores.entries()].map(([pillarId, score]) => score + ' ' + (
        pillarId === PILLAR.Victory ? 'VP' :
            pillarId === PILLAR.Military ? 'MP' :
                pillarId === PILLAR.Agriculture ? 'AP' :
                    pillarId === PILLAR.Industry ? 'IP' :
                        ''
    )).join(', ')

    test(`${message} (total ${summary})`, async () => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const seasonId = (await user.current!.querySeason({})).data.season!.id

        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        await expect(user.current!.queryTeamsScoreTotals()).resolves.toMatchObject({
            data: {
                team: {
                    scoreTotals: expect.arrayContaining([...scores.entries()].map(([pillarId, [score, rank]]) => (
                        expect.objectContaining({
                            pillar: expect.objectContaining({
                                id: pillarId,
                                season: pillarId === PILLAR.Victory ? null : expect.objectContaining({
                                    season: expect.objectContaining({
                                        id: seasonId,
                                    }),
                                }),
                            }),
                            score,
                            rank,
                        })
                    ))),
                },
            },
        })
    })
}
