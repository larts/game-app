import { ApolloClient, ApolloLink, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { MutationTeamLogout, MutationUserLogin, QueryRecipes, QueryScorePillar, QueryScorePillarSummary, QueryScorePillars, QueryScorePillarsBase, QueryScoreVictorySummary, QuerySeason, QuerySession, QuerySettings, QueryTeamBase, QueryTeamEquipment, QueryTeamFeasts, QueryTeamFlag, QueryTeamNextRespawn, QueryTeamResources, QueryTeamUpgrades, QueryTeamWorkshop, QueryTeams, QueryTeamsScoreTotals, QueryTime } from '@larts/api'
import fetch from 'cross-fetch'

import Admin from './Admin'
import ClientUser from './ClientUser'


export default class ClientTeam {
    readonly uri: string
    readonly deviceToken: string
    readonly client: ApolloClient<NormalizedCacheObject>
    readonly users: ClientUser[] = []
    admin: Admin | null = null

    constructor(uri: string, deviceToken: string) {
        this.uri = uri
        this.deviceToken = deviceToken

        const authLink = new ApolloLink((operation, forward) => {
            operation.setContext(({ headers = {} }) => ({
                headers: {
                    ...headers,
                    cookie: `deviceToken=${deviceToken}`,
                },
            }))
            return forward(operation).map((data) => {
                // Override return for user login, because BE sets cookies instead of returns. TODO: simplify.
                const userCookie = (operation.getContext().response as Response).headers.get('set-cookie')
                if (userCookie) return { data: { auth: { __typename: 'Auth', userLogin: `deviceToken=${deviceToken}; ${userCookie}` } } }

                return data
            })
        })
        this.client = new ApolloClient({
            link: ApolloLink.from([
                authLink,
                new HttpLink({ uri, fetch }),
            ]),
            cache: new InMemoryCache(),
            credentials: 'include',
        })
        this.client.watchQuery(QueryRecipes)
        this.client.watchQuery(QueryScorePillarsBase)
        this.client.watchQuery(QueryScorePillars)
        this.client.watchQuery(QueryScorePillarSummary)
        this.client.watchQuery(QueryScorePillar)
        this.client.watchQuery(QueryScoreVictorySummary)
        this.client.watchQuery(QuerySeason)
        this.client.watchQuery(QuerySession)
        this.client.watchQuery(QuerySettings)
        this.client.watchQuery(QueryTeamBase)
        this.client.watchQuery(QueryTeamEquipment)
        this.client.watchQuery(QueryTeamFeasts)
        this.client.watchQuery(QueryTeamFlag)
        this.client.watchQuery(QueryTeamNextRespawn)
        this.client.watchQuery(QueryTeamResources)
        this.client.watchQuery(QueryTeamsScoreTotals)
        this.client.watchQuery(QueryTeams)
        this.client.watchQuery(QueryTeamUpgrades)
        this.client.watchQuery(QueryTeamWorkshop)
        this.client.watchQuery(QueryTime)
    }

    teardown() {
        for (const user of this.users) user.teardown()
        this.client.stop()
    }

    async loginUser(qrCode: string) {
        const { data, errors } = await this.client.mutate({ ...MutationUserLogin, variables: { qrCode } })
        if (!data) throw new Error(errors?.[0].message)
        const cookieWithBothTokens = data.auth.userLogin
        const user = new ClientUser(this.uri, cookieWithBothTokens)

        if (this.admin) {
            user.admin = this.admin
        }

        this.users.push(user)
        return user
    }

    queryRecipes             () { return this.client.query(QueryRecipes)             }
    queryScorePillarsBase    () { return this.client.query(QueryScorePillarsBase)    }
    queryScorePillars        () { return this.client.query(QueryScorePillars)        }
    queryScorePillarSummary  () { return this.client.query(QueryScorePillarSummary)  }
    queryScorePillar         () { return this.client.query(QueryScorePillar)         }
    queryScoreVictorySummary () { return this.client.query(QueryScoreVictorySummary) }
    querySeason              () { return this.client.query(QuerySeason)              }
    querySession             () { return this.client.query(QuerySession)             }
    querySettings            () { return this.client.query(QuerySettings)            }
    queryTeamBase            () { return this.client.query(QueryTeamBase)            }
    queryTeamEquipment       () { return this.client.query(QueryTeamEquipment)       }
    queryTeamFeasts          () { return this.client.query(QueryTeamFeasts)          }
    queryTeamNextRespawn     () { return this.client.query(QueryTeamNextRespawn)     }
    queryTeamResources       () { return this.client.query(QueryTeamResources)       }
    queryTeamsScoreTotals    () { return this.client.query(QueryTeamsScoreTotals)    }
    queryTeams               () { return this.client.query(QueryTeams)               }
    queryTeamUpgrades        () { return this.client.query(QueryTeamUpgrades)        }
    queryTeamWorkshops       () { return this.client.query(QueryTeamWorkshop)        }
    queryTime                () { return this.client.query(QueryTime)                }

    queryTeamFlag            (id: number) { return this.client.query({ ...QueryTeamFlag, variables: { id } })            }
    mutationTeamLogout           (variables: MutationTeamLogout.Params           ) { return this.client.mutate({ ...MutationTeamLogout           , variables }) }

}
