/* eslint-disable @typescript-eslint/no-explicit-any */
import { ApolloClient, ApolloLink, DefaultContext, HttpLink, InMemoryCache, MutationOptions, NormalizedCacheObject, OperationVariables } from '@apollo/client'
import { MutationCharacterRespawn, MutationCharacterRevitalize, MutationCharacterTribute, MutationEquipmentAdd, MutationEquipmentBuy, MutationEquipmentEquip, MutationEquipmentRemove, MutationEquipmentUnequip, MutationFeastScore, MutationFlagScore, MutationRecipeScore, MutationResourcesAdd, MutationResourcesAddCoins, MutationResourcesRemoveCoins, MutationResourcesSell, MutationTaskCancel, MutationTaskQueue, MutationTeamLogout, MutationUpgradeReward, MutationUserLogout, QueryRecipes, QueryScorePillar, QueryScorePillarSummary, QueryScorePillars, QueryScorePillarsBase, QueryScoreVictorySummary, QuerySeason, QuerySession, QuerySettings, QueryTeamBase, QueryTeamEquipment, QueryTeamFeasts, QueryTeamFlag, QueryTeamNextRespawn, QueryTeamResources, QueryTeamUpgrades, QueryTeamWorkshop, QueryTeams, QueryTeamsScoreTotals, QueryTime, QueryUserCharacter, QueryUserEquipment, QueryUserName, QueryUserOther } from '@larts/api'
import { sleep } from '@larts/common'
import fetch from 'cross-fetch'

import Admin from './Admin'


export default class ClientUser {
    readonly uri: string
    /**
     * Cookie that contains both device and user tokens: `deviceToken=${deviceToken}; ${userCookie}`
     */
    readonly cookie: string
    readonly client: ApolloClient<NormalizedCacheObject>
    admin: Admin | null = null
    workaroundSleepAfterMutation = 0

    constructor(uri: string, cookie: string) {
        this.uri = uri
        this.cookie = cookie

        const authLink = new ApolloLink((operation, forward) => {
            operation.setContext(({ headers = {} }) => ({
                headers: {
                    ...headers,
                    cookie,
                },
            }))
            return forward(operation)
        })
        this.client = new ApolloClient({
            link: ApolloLink.from([
                authLink,
                new HttpLink({ uri, fetch }),
            ]),
            cache: new InMemoryCache(),
            credentials: 'include',
        })
        this.client.watchQuery(QueryRecipes)
        this.client.watchQuery(QueryScorePillarsBase)
        this.client.watchQuery(QueryScorePillars)
        this.client.watchQuery(QueryScorePillarSummary)
        this.client.watchQuery(QueryScorePillar)
        this.client.watchQuery(QueryScoreVictorySummary)
        this.client.watchQuery(QuerySeason)
        this.client.watchQuery(QuerySession)
        this.client.watchQuery(QuerySettings)
        this.client.watchQuery(QueryTeamBase)
        this.client.watchQuery(QueryTeamEquipment)
        this.client.watchQuery(QueryTeamFeasts)
        this.client.watchQuery(QueryTeamFlag)
        this.client.watchQuery(QueryTeamNextRespawn)
        this.client.watchQuery(QueryTeamResources)
        this.client.watchQuery(QueryTeamsScoreTotals)
        this.client.watchQuery(QueryTeams)
        this.client.watchQuery(QueryTeamUpgrades)
        this.client.watchQuery(QueryTeamWorkshop)
        this.client.watchQuery(QueryTime)
        this.client.watchQuery(QueryUserCharacter)
        this.client.watchQuery(QueryUserEquipment)
        this.client.watchQuery(QueryUserName)
        this.client.watchQuery(QueryUserOther)
    }

    teardown() {
        this.client.stop()
    }

    queryRecipes             (...args: QueryRecipes.Params             extends never ? [] : [variables: QueryRecipes.Params             ]) { return this.client.query({ ...QueryRecipes            , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillarsBase    (...args: QueryScorePillarsBase.Params    extends never ? [] : [variables: QueryScorePillarsBase.Params    ]) { return this.client.query({ ...QueryScorePillarsBase   , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillars        (...args: QueryScorePillars.Params        extends never ? [] : [variables: QueryScorePillars.Params        ]) { return this.client.query({ ...QueryScorePillars       , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillarSummary  (...args: QueryScorePillarSummary.Params  extends never ? [] : [variables: QueryScorePillarSummary.Params  ]) { return this.client.query({ ...QueryScorePillarSummary , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScorePillar         (...args: QueryScorePillar.Params         extends never ? [] : [variables: QueryScorePillar.Params         ]) { return this.client.query({ ...QueryScorePillar        , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryScoreVictorySummary (...args: QueryScoreVictorySummary.Params extends never ? [] : [variables: QueryScoreVictorySummary.Params ]) { return this.client.query({ ...QueryScoreVictorySummary, variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySeason              (...args: QuerySeason.Params              extends never ? [] : [variables: QuerySeason.Params              ]) { return this.client.query({ ...QuerySeason             , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySession             (...args: QuerySession.Params             extends never ? [] : [variables: QuerySession.Params             ]) { return this.client.query({ ...QuerySession            , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    querySettings            (...args: QuerySettings.Params            extends never ? [] : [variables: QuerySettings.Params            ]) { return this.client.query({ ...QuerySettings           , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamBase            (...args: QueryTeamBase.Params            extends never ? [] : [variables: QueryTeamBase.Params            ]) { return this.client.query({ ...QueryTeamBase           , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamEquipment       (...args: QueryTeamEquipment.Params       extends never ? [] : [variables: QueryTeamEquipment.Params       ]) { return this.client.query({ ...QueryTeamEquipment      , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamFeasts          (...args: QueryTeamFeasts.Params          extends never ? [] : [variables: QueryTeamFeasts.Params          ]) { return this.client.query({ ...QueryTeamFeasts         , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamFlag            (...args: QueryTeamFlag.Params            extends never ? [] : [variables: QueryTeamFlag.Params            ]) { return this.client.query({ ...QueryTeamFlag           , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamNextRespawn     (...args: QueryTeamNextRespawn.Params     extends never ? [] : [variables: QueryTeamNextRespawn.Params     ]) { return this.client.query({ ...QueryTeamNextRespawn    , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamResources       (...args: QueryTeamResources.Params       extends never ? [] : [variables: QueryTeamResources.Params       ]) { return this.client.query({ ...QueryTeamResources      , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamsScoreTotals    (...args: QueryTeamsScoreTotals.Params    extends never ? [] : [variables: QueryTeamsScoreTotals.Params    ]) { return this.client.query({ ...QueryTeamsScoreTotals   , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeams               (...args: QueryTeams.Params               extends never ? [] : [variables: QueryTeams.Params               ]) { return this.client.query({ ...QueryTeams              , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamUpgrades        (...args: QueryTeamUpgrades.Params        extends never ? [] : [variables: QueryTeamUpgrades.Params        ]) { return this.client.query({ ...QueryTeamUpgrades       , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTeamWorkshops       (...args: QueryTeamWorkshop.Params        extends never ? [] : [variables: QueryTeamWorkshop.Params        ]) { return this.client.query({ ...QueryTeamWorkshop      , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryTime                (...args: QueryTime.Params                extends never ? [] : [variables: QueryTime.Params                ]) { return this.client.query({ ...QueryTime               , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryUserCharacter       (...args: QueryUserCharacter.Params       extends never ? [] : [variables: QueryUserCharacter.Params       ]) { return this.client.query({ ...QueryUserCharacter      , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryUserEquipment       (...args: QueryUserEquipment.Params       extends never ? [] : [variables: QueryUserEquipment.Params       ]) { return this.client.query({ ...QueryUserEquipment      , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryUserName            (...args: QueryUserName.Params            extends never ? [] : [variables: QueryUserName.Params            ]) { return this.client.query({ ...QueryUserName           , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }
    queryUserOther           (...args: QueryUserOther.Params           extends never ? [] : [variables: QueryUserOther.Params           ]) { return this.client.query({ ...QueryUserOther          , variables: (args as any)[0], fetchPolicy: 'no-cache' }) }

    async mutate<
        TData,
        TVariables extends OperationVariables = OperationVariables,
        TContext extends DefaultContext = DefaultContext,
    >(options: MutationOptions<TData, TVariables, TContext>) {
        const response = await this.client.mutate<TData, TVariables, TContext>(options)
        if (this.workaroundSleepAfterMutation) await sleep(this.workaroundSleepAfterMutation)
        return response
    }

    mutationCharacterRespawn     (...args: MutationCharacterRespawn.Params     extends never ? [] : [variables: MutationCharacterRespawn.Params     ]) { return this.mutate({ ...MutationCharacterRespawn     , variables: (args as any)[0] }) }
    mutationCharacterRevitalize  (...args: MutationCharacterRevitalize.Params  extends never ? [] : [variables: MutationCharacterRevitalize.Params  ]) { return this.mutate({ ...MutationCharacterRevitalize  , variables: (args as any)[0] }) }
    mutationCharacterTribute     (...args: MutationCharacterTribute.Params     extends never ? [] : [variables: MutationCharacterTribute.Params     ]) { return this.mutate({ ...MutationCharacterTribute     , variables: (args as any)[0] }) }
    mutationEquipmentAdd         (...args: MutationEquipmentAdd.Params         extends never ? [] : [variables: MutationEquipmentAdd.Params         ]) { return this.mutate({ ...MutationEquipmentAdd         , variables: (args as any)[0] }) }
    mutationEquipmentUnequip     (...args: MutationEquipmentUnequip.Params     extends never ? [] : [variables: MutationEquipmentUnequip.Params     ]) { return this.mutate({ ...MutationEquipmentUnequip     , variables: (args as any)[0] }) }
    mutationEquipmentEquip       (...args: MutationEquipmentEquip.Params       extends never ? [] : [variables: MutationEquipmentEquip.Params       ]) { return this.mutate({ ...MutationEquipmentEquip       , variables: (args as any)[0] }) }
    mutationEquipmentBuy         (...args: MutationEquipmentBuy.Params         extends never ? [] : [variables: MutationEquipmentBuy.Params         ]) { return this.mutate({ ...MutationEquipmentBuy         , variables: (args as any)[0] }) }
    mutationEquipmentRemove      (...args: MutationEquipmentRemove.Params      extends never ? [] : [variables: MutationEquipmentRemove.Params      ]) { return this.mutate({ ...MutationEquipmentRemove      , variables: (args as any)[0] }) }
    mutationFeastScore           (...args: MutationFeastScore.Params           extends never ? [] : [variables: MutationFeastScore.Params           ]) { return this.mutate({ ...MutationFeastScore           , variables: (args as any)[0] }) }
    mutationFlagScore            (...args: MutationFlagScore.Params            extends never ? [] : [variables: MutationFlagScore.Params            ]) { return this.mutate({ ...MutationFlagScore            , variables: (args as any)[0] }) }
    mutationRecipeScore          (...args: MutationRecipeScore.Params          extends never ? [] : [variables: MutationRecipeScore.Params          ]) { return this.mutate({ ...MutationRecipeScore          , variables: (args as any)[0] }) }
    mutationResourcesAddCoins    (...args: MutationResourcesAddCoins.Params    extends never ? [] : [variables: MutationResourcesAddCoins.Params    ]) { return this.mutate({ ...MutationResourcesAddCoins    , variables: (args as any)[0] }) }
    mutationResourcesAdd         (...args: MutationResourcesAdd.Params         extends never ? [] : [variables: MutationResourcesAdd.Params         ]) { return this.mutate({ ...MutationResourcesAdd         , variables: (args as any)[0] }) }
    mutationResourcesRemoveCoins (...args: MutationResourcesRemoveCoins.Params extends never ? [] : [variables: MutationResourcesRemoveCoins.Params ]) { return this.mutate({ ...MutationResourcesRemoveCoins , variables: (args as any)[0] }) }
    mutationResourcesSell        (...args: MutationResourcesSell.Params        extends never ? [] : [variables: MutationResourcesSell.Params        ]) { return this.mutate({ ...MutationResourcesSell        , variables: (args as any)[0] }) }
    mutationTaskCancel           (...args: MutationTaskCancel.Params           extends never ? [] : [variables: MutationTaskCancel.Params           ]) { return this.mutate({ ...MutationTaskCancel           , variables: (args as any)[0] }) }
    mutationTaskQueue            (...args: MutationTaskQueue.Params            extends never ? [] : [variables: MutationTaskQueue.Params            ]) { return this.mutate({ ...MutationTaskQueue            , variables: (args as any)[0] }) }
    mutationTeamLogout           (...args: MutationTeamLogout.Params           extends never ? [] : [variables: MutationTeamLogout.Params           ]) { return this.mutate({ ...MutationTeamLogout           , variables: (args as any)[0] }) }
    mutationUpgradeReward        (...args: MutationUpgradeReward.Params        extends never ? [] : [variables: MutationUpgradeReward.Params        ]) { return this.mutate({ ...MutationUpgradeReward        , variables: (args as any)[0] }) }
    mutationUserLogout           (...args: MutationUserLogout.Params           extends never ? [] : [variables: MutationUserLogout.Params           ]) { return this.mutate({ ...MutationUserLogout           , variables: (args as any)[0] }) }
}

