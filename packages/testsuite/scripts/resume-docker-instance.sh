#!/bin/bash

# Project name can be make persistent in the compose file, soon. (https://github.com/docker/compose/issues/745)

set -e

INSTANCE=$1
if [ -z "$INSTANCE" ] ; then
    echo "Usage:"
    echo "    yarn resume <instance-number>"
    exit 1
fi

DIR="tmp/instance$INSTANCE"

cd $DIR
    trap "kill \$(jobs -p); docker-compose -p throne-app-testsuite-$INSTANCE down" EXIT  # kill current subprocess when this script is killed (e.g. via Jest).
    docker-compose -p throne-app-testsuite-$INSTANCE up
