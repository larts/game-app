#!/bin/bash

set -e

INSTANCE=$1
if [ -z "$INSTANCE" ] ; then
    echo "Usage:"
    echo "    yarn instance <instance-number>"
    exit 1
fi

DIR="tmp/instance$INSTANCE"

rm -rf $DIR
mkdir -p $DIR

cp scripts/docker-compose-template.yaml $DIR/docker-compose.yaml
sed -i "s/\$INSTANCE/$INSTANCE/g" $DIR/docker-compose.yaml
sed -i "s/\$USER/$(id -u)/g" $DIR/docker-compose.yaml
sed -i "s/\$GROUP/$(id -g)/g" $DIR/docker-compose.yaml


cd $DIR
    mkdir volume-db

    cp -r ../../../api/config volume-api
    sed -i "s/t-api:8888/t-api-testsuite-$INSTANCE:8888/g" volume-api/default.conf
    sed -i "s/t-crons:80/t-crons-testsuite-$INSTANCE:80/g" volume-api/cron.conf
    sed -i "s/t-admin-mysql:80/t-admin-mysql-testsuite-$INSTANCE:80/g" volume-api/pma.conf

    cp -r ../../../crons/files volume-crons

    cp -r ../../../webserver volume-webserver

    # copy-paste other packages, so that all packages are hardcoded to the instance.
    # Note: dependencies are still linked, be careful (that's much faster).
    cd volume-webserver
        rm -rf node_modules/@larts
        mkdir node_modules/@larts
        cp -r ../../../../common node_modules/@larts
        cp -r ../../../../api node_modules/@larts
    cd ..

    docker-compose -p throne-app-testsuite-$INSTANCE down --remove-orphans
    docker-compose -p throne-app-testsuite-$INSTANCE build

    trap "kill \$(jobs -p); docker-compose -p throne-app-testsuite-$INSTANCE down" EXIT  # kill current subprocess when this script is killed (e.g. via Jest).
    docker-compose -p throne-app-testsuite-$INSTANCE up
