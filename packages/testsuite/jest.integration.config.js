/* eslint-disable @typescript-eslint/no-var-requires */
const dotenv = require('dotenv-safe')


module.exports = {
    // Transpile ESM to CommonJS.
    preset: 'ts-jest/presets/js-with-babel',
    transformIgnorePatterns: [],  // Let Babel to transpile *all* files within node_modules (as they have ESM too).

    notify: true,
    rootDir: '.',
    testEnvironment: './src/ThroneTestEnvironment.ts',
    globals: dotenv.config(),
    maxConcurrency: 4,
    testTimeout: 10 * 1000, // Double default timeout, because forwarding 300 ticks takes already around 5 seconds.
}
