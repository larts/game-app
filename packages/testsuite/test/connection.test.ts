/* eslint-disable @typescript-eslint/no-non-null-assertion */
import fetch from 'cross-fetch'

import { beforeAllReturn } from '../src/utils'


describe ('SERVER CONNECTION', () => {
    const response = beforeAllReturn(() => fetch(`http://lapi.tronis.lv:8${process.env.JEST_WORKER_ID}`))
    test('server responds with "GET query missing."', () => expect(response.current!.text()).resolves.toStrictEqual('GET query missing.'))
})

describe('APOLLO CONNECTION', () => {
    test('apollo responds to GraphQL query', () => expect(global.clientAnonymous.currentTick()).resolves.toEqual(1))
})
