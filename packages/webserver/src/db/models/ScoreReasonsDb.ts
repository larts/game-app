import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'reason',
    freezeTableName: true,
    tableName: 'score_reasons',
    timestamps: false,
})

export class ScoreReasonsDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    title: string;

    @Column
    amount: number;

    @Column
    type_id: number;

    @Column
    assign_period: number;
}
