import { Column, Model, Table } from 'sequelize-typescript'

import { SeasonDb } from './SeasonDb'
import { TotalScoreDb } from './TotalScoreDb'


const ARBITRARY_TARGET = 50

@Table({
    modelName: 'pillar2period',
    freezeTableName: true,
    tableName: 'score_type2period',
    timestamps: false,
})

export class Pillar2SeasonDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    score_type_id: number;

    @Column
    period_id: number;

    @Column
    currentTotal: number;

    @Column
    target: number;

    public static async recalculate(season: SeasonDb, pillarId: number) {
        const pillarScore = await TotalScoreDb.sum('score', { where: {
            type_id: pillarId,
            period_id: season.id,
        } })

        const total = await Pillar2SeasonDb.findOne({
            where: {
                score_type_id: pillarId,
                period_id: season.id,
            },
        })

        if (total) {
            total.currentTotal = pillarScore
            await total.save()
        } else {
            const nTotal = new Pillar2SeasonDb()
            nTotal.currentTotal = pillarScore
            nTotal.score_type_id = pillarId
            nTotal.period_id = season.id
            nTotal.target = ARBITRARY_TARGET
            await nTotal.save()
        }
    }
}
