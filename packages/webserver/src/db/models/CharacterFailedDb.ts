import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'characterfailed',
    freezeTableName: true,
    tableName: 'character_faileds',
    timestamps: false,
})

export class CharacterFailedDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    user_id: number;

    @Column
    charcode: number;

    @Column
    created: string;

}
