import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'resourceaction',
    freezeTableName: true,
    tableName: 'resource_action',
    timestamps: false,
})

export class ResourceActionDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    name: string;

    @Column
    from: string;

    @Column
    to: string;
}
