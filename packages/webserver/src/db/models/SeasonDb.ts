import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'season',
    freezeTableName: true,
    tableName: 'periods',
    timestamps: false,
})

export class SeasonDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    title: string;

    @Column
    ended: string;

    @Column
    started: string;

    @Column
    next_periods_id: number;
}
