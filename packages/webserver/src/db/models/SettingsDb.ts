import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'settings',
    freezeTableName: true,
    tableName: 'settings',
    timestamps: false,
})

export class SettingsDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    title: string;

    @Column
    value: string;

    @Column
    code: string;

    @Column
    is_system: boolean;
}
