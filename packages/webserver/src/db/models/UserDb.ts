import { Column, Model, Table } from 'sequelize-typescript'

import { QrCodeDb } from './QrCodeDb'
import { TeamDb } from './TeamDb'


@Table({
    modelName: 'user',
    freezeTableName: true,
    tableName: 'users',
    timestamps: false,
})

export class UserDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    name: string;

    @Column
    display_name: string;

    @Column
    email: string;

    @Column
    equipment_points: number;

    @Column
    equipment_has_phoenix: number;

    @Column
    created: string;

    @Column
    qr_code: number;

    @Column
    qr_id: number;

    @Column
    team_id: number;

    @Column
    auth_token: string;

    @Column
    sex: string;

    private team: TeamDb
    private qr: QrCodeDb
        
    public async getTeam(reset = false): Promise<TeamDb> {
        if (!this.team || reset) {
            this.team = await TeamDb.findByPk(this.team_id)
        }

        return this.team
    }

    public async getQr(reset = false): Promise<QrCodeDb> {
        if (!this.qr || reset) {
            this.qr = await QrCodeDb.findByPk(this.qr_id)
        }

        return this.qr
    }
}
