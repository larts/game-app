import { Column, Model, Table } from 'sequelize-typescript';


@Table({
    modelName: 'qrscancategories',
    freezeTableName: true,
    tableName: 'qr_scan_categories',
    timestamps: false,
})

export class QrScanCategoriesDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    title: string;
}
