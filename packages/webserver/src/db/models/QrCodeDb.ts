import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'qrcodes',
    freezeTableName: true,
    tableName: 'qr_codes',
    timestamps: false,
})

export class QrCodeDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    qrid: string;

    @Column
    is_used: boolean;

    @Column
    category_id: number;

    @Column
    owner_team_id: number;

    @Column
    last_scan_id: number;

    @Column
    uuid: number;

    @Column
    details: string;
}
