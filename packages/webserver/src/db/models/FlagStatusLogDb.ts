import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'flagstatuslog',
    freezeTableName: true,
    tableName: 'flag_status_log',
    timestamps: false,
})

export class FlagStatusLogDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    team_id: number;

    @Column
    changer_team_id: number;

    @Column
    created: string;

    @Column
    current_status_id: number;

    @Column
    action: string;

    @Column
    period_id: number;
}
