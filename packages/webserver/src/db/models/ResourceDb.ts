import { ApolloError } from 'apollo-server-errors'
import { Column, Model, Table } from 'sequelize-typescript'

import { ResourceActionColumn } from '../../helpers/enums'
import { genericError } from '../../helpers/error'

import { DeviceDb } from './DeviceDb'
import { ResourceActionDb } from './ResourceActionDb'
import { ResourceLogDb } from './ResourceLogDb'
import { ResourceTypeDb } from './ResourceTypeDb'
import { UserDb } from './UserDb'


interface ResourceInfo {
    status: ResourceDb
    price: Price
}

export interface Price {
    type: ResourceTypeDb;
    amount: number;
}

@Table({
    modelName: 'resourcetotal',
    freezeTableName: true,
    tableName: 'resource_total',
    timestamps: false,
})

export class ResourceDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @Column
    team_id: number;

    @Column
    resource_type_id: number;

    @Column
    total_amount: number;

    @Column
    available_amount: number;

    @Column
    locked_amount: number;

    @Column
    spent_amount: number;

    public static async spendResources (prices: Price[], type: ResourceActionDb, user: UserDb | DeviceDb, currentTimestamp: string, details = ''): Promise<boolean> {
        try {
            const resourceStatuses: ResourceInfo[] | void  = await Promise.all(prices.map(async (price: Price): Promise<ResourceInfo> => {
                const resourceStatus = (await ResourceDb.findOne({
                    where: {
                        resource_type_id: price.type.id,
                        team_id: user.team_id,
                    },
                })) || new ResourceDb()

                if (resourceStatus.isNewRecord) {
                    // during this step allow only new status row creation!!!
                    resourceStatus.team_id = user.team_id
                    resourceStatus.spent_amount = 0
                    resourceStatus.available_amount = 0
                    resourceStatus.locked_amount = 0
                    resourceStatus.resource_type_id = price.type.id
                    resourceStatus.total_amount = 0
                    resourceStatus.save()
                }

                switch (type.from) {
                    case ResourceActionColumn.Available: {
                        if (resourceStatus.available_amount < price.amount) {
                            throw new ApolloError(`Not enough "available" ${price.type.name} ${price.type.subtype}`, `SPEND_RESOURCES_NO_AVAILABLE_${price.type.qr_prefix}`)
                        }

                        resourceStatus.available_amount = resourceStatus.available_amount - price.amount
                        break
                    }
                    case ResourceActionColumn.Spent: {
                        if (resourceStatus.spent_amount < price.amount) {
                            throw new ApolloError(`Not enough "spent" ${price.type.name} ${price.type.subtype}`, `SPEND_RESOURCES_NO_SPENT_${price.type.qr_prefix}`)
                        }

                        resourceStatus.spent_amount = resourceStatus.spent_amount - price.amount
                        break
                    }
                    case ResourceActionColumn.Locked: {
                        if (resourceStatus.locked_amount < price.amount) {
                            throw new ApolloError(`Not enough "locked" ${price.type.name} ${price.type.subtype}`, `SPEND_RESOURCES_NO_LOCKED_${price.type.qr_prefix}`)
                        }

                        resourceStatus.locked_amount = resourceStatus.locked_amount - price.amount
                        break
                    }
                }

                switch (type.to) {
                    case ResourceActionColumn.Available: {
                        resourceStatus.available_amount = resourceStatus.available_amount + price.amount
                        break
                    }
                    case ResourceActionColumn.Spent: {
                        resourceStatus.spent_amount = resourceStatus.spent_amount + price.amount
                        break
                    }
                    case ResourceActionColumn.Locked: {
                        resourceStatus.locked_amount = resourceStatus.locked_amount + price.amount
                        break
                    }
                }

                resourceStatus.total_amount = resourceStatus.available_amount + resourceStatus.locked_amount + resourceStatus.spent_amount
                return { status: resourceStatus, price }
            })).catch(e => {
                genericError(e, 'Failed to pay resource', 'SPEND_RESOURCES_FAIL_SINGLE')
            })

            resourceStatuses && await Promise.all(resourceStatuses.map(async (resourceInfo: ResourceInfo): Promise<boolean> => {
                resourceInfo.status.save()
                
                const log = new ResourceLogDb()
                log.action_id = type.id
                log.team_id = resourceInfo.status.team_id
                log.amount = resourceInfo.price.amount
                log.details = details
                log.resource_type_id = resourceInfo.price.type.id
                log.created = currentTimestamp

                await log.save()

                return true
            })).catch(e => {
                genericError(e, 'Failed to save resource payment', 'SPEND_RESOURCES_FAIL_SAVE')
            })

        } catch (e) {
            genericError(e, 'Failed to pay resources', 'SPEND_RESOURCES_FAIL')
            return false
        }
    }
}

