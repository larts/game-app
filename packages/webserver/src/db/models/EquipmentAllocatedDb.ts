import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'equiomentallocated',
    freezeTableName: true,
    tableName: 'equipment_allocated',
    timestamps: false,
})

export class EquipmentAllocatedDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    team_id: number;

    @Column
    created: string;

    @Column
    user_id: number;

    @Column
    add: boolean;
}
