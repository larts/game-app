import { Column, Model, Table } from 'sequelize-typescript';


@Table({
    modelName: 'pillar',
    freezeTableName: true,
    tableName: 'score_types',
    timestamps: false,
})

export class PillarDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    title: string;
}
