import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'device',
    freezeTableName: true,
    tableName: 'devices',
    timestamps: false,
})

export class DeviceDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true, 
    })
    id: number;

    @Column
    name: string;

    @Column
    team_id: number;

    @Column
    auth_token: string;

    @Column
    is_enabled: boolean;

    @Column
    is_marketplace: boolean;
}
