import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'flagstatus',
    freezeTableName: true,
    tableName: 'flag_status',
    timestamps: false,
})

export class FlagStatusDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    name: string;

    @Column
    score_reason_id: number;

    @Column
    self_score_reason_id: number;

    @Column
    next_status: number;
}
