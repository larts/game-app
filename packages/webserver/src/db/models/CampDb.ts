import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'camps',
    freezeTableName: true,
    tableName: 'camps',
    timestamps: false,
})

export class CampDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    latitude: number;

    @Column
    longitude: string;

    @Column
    title: string;

    @Column
    display_id: string;

    @Column
    resource_id: number;
}
