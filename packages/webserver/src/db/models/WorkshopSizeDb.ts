import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'workshopSize',
    freezeTableName: true,
    tableName: 'workshop_size',
    timestamps: false,
})

export class WorkshopSizeDb extends Model {
    @Column({ primaryKey: true, autoIncrement: true, })
    id: number;

    @Column
    score_reason_id: number;

    @Column
    wood_cost: number;

    @Column
    resource_cost: number;

    @Column
    coin_cost: number;

    @Column
    ip_amount: number;

    @Column
    cooldown: number;
}
