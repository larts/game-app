import { Column, Model, Table } from 'sequelize-typescript'

import { Recipe2ResourceDb } from './Recipe2ResourceDb'


@Table({
    modelName: 'recipe',
    freezeTableName: true,
    tableName: 'recipe',
    timestamps: false,
})

export class RecipeDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    title: string;

    @Column
    created_first_id: number;

    @Column
    score_reason_id: number;

    private resources: Recipe2ResourceDb[]

    public async getResources(reset = false): Promise<Recipe2ResourceDb[]> {
        if (!this.resources || reset) {
            this.resources = await Recipe2ResourceDb.findAll({
                where: {
                    recipe_id: this.id,
                },
            })
        }

        return this.resources
    }

}
