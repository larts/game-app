import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'equioment',
    freezeTableName: true,
    tableName: 'equipment_bought',
    timestamps: false,
})

export class EquipmentDb extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    team_id: number;

    @Column
    created: string;

    @Column
    user_id: number;
}
