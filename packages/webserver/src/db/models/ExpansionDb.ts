import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'expansions',
    freezeTableName: true,
    tableName: 'expansions',
    timestamps: false,
})

export class ExpansionDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    show_code: string;

    @Column
    reward_code: string;

    @Column
    owner_team_id: number;

    @Column
    rewarded_team_id: number;

    @Column
    camp_id: number;

    @Column
    showed: string;

    @Column
    rewarded: string;

    @Column
    assignment_period_id: number;
}
