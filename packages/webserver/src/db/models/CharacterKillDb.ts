import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'characterkills',
    freezeTableName: true,
    tableName: 'character_kills',
    timestamps: false,
})

export class CharacterKillDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    team_id: number;

    @Column
    scored_team_id: number;

    @Column
    period_id: number;

    @Column
    amount: number;

}
