import { ApolloError } from 'apollo-server-errors'
import { Op } from 'sequelize'
import { Column, Model, Table } from 'sequelize-typescript'

import { Context } from '../../types/Context'

import { ScoreDb } from './ScoreDb'


@Table({
    modelName: 'workshopActivations',
    freezeTableName: true,
    tableName: 'workshop_activations',
    timestamps: false,
})

export class WorkshopActivationDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    size_id: number;

    @Column
    variant: number;

    @Column
    created: string;

    @Column
    cancelled: string;

    @Column
    scored_points: number;

    @Column
    last_updated: string;

    @Column
    team_id: number;

    @Column
    finished: string;

    @Column
    started: string;

    @Column
    price_details: string;

    public async getScores (context: Context): Promise<ScoreDb[]> {
        try {
            return await ScoreDb.findAll({
                where: {
                    reason_id: { [Op.any]: [
                        // TODO
                    ] },
                    score_time: {
                        [Op.lte]: context.settings.currentTimeString,
                    },
                },
            })
        } catch (e) {
            throw new ApolloError('Failed to get score', 'GET_SCORE_FAIL')
        }
    }
}
