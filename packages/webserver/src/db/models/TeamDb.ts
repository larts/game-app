import { Column, Model, Table } from 'sequelize-typescript'


@Table({
    modelName: 'teams',
    freezeTableName: true,
    tableName: 'teams',
    timestamps: false,
})

export class TeamDb extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id: number;

    @Column
    title: string;

    @Column
    color: string;

    @Column
    flag_status_id: number;

    @Column
    available_eq: number;

    @Column
    flag_registration_id: number;

    @Column
    coinbox: number;
}

