import { Sequelize } from 'sequelize-typescript'


let db: Sequelize | null = null

if (!db) {
    db = new Sequelize(
        `mysql://${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}:${process.env.MYSQL_PORT}/${process.env.MYSQL_DB}`,
        {
            models: [__dirname + '/models'],
        },
    )
}

export const getConnection = () => {
    return () => db
}

export const closeConnection = () => {
    if (db) {
        db.close()
    }
}
