/* eslint-disable import/no-unused-modules */
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core'
import { ApolloError } from 'apollo-server-errors'
import { ApolloServer } from 'apollo-server-express'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import dotenv from 'dotenv'
import express from 'express'

import { getConnection } from './db/index'
import { DeviceDb } from './db/models/DeviceDb'
import { SettingsDb } from './db/models/SettingsDb'
import { UserDb } from './db/models/UserDb'
import { schema } from './graphql/schema'
import { Settings } from './helpers/enums'
import { Context } from './types/Context'


getConnection()

dotenv.config()

const PORT = 8888

console.info('Server starting!')

const DEVICE_COOKIE = 'deviceToken'
const USER_COOKIE = 'userToken'
const USER_SESSION_TIME = 'userTime'

const DEVICE_TIMEOUT = 345600
const USER_TIMEOUT = 60 * 10

;(async () => {
    const server = new ApolloServer({
        schema,
        plugins: [
            ApolloServerPluginLandingPageGraphQLPlayground(),
        ],
        context: (async ({ req, res }): Promise<Context> => {
            const isShutDown = await SettingsDb.findByPk(Settings.EmergencyShutdown)
            if (isShutDown.value === '1') {
                throw new ApolloError('Emergency shutdown', 'EMERGENCY_SHUTDOWN')
            }

            let deviceData: null | DeviceDb = null

            if (req?.cookies?.[DEVICE_COOKIE]) {
                deviceData = await DeviceDb.findOne({ where: { auth_token: req.cookies[DEVICE_COOKIE], is_enabled: 1 } })

                if (deviceData && !deviceData.team_id) {
                    if (!deviceData.is_marketplace) {
                        deviceData = null
                    }
                }
            }

            const gameStarted = await SettingsDb.findByPk(Settings.GameHasStarted)
            const currentTime = new Date(parseInt((await SettingsDb.findByPk(Settings.CurrentTick))?.value) * 1000)
            const currentSeason = Number((await SettingsDb.findByPk(Settings.CurrentSeason))?.value)

            const bonusTimeout = Number((await SettingsDb.findByPk(Settings.WorkshopBonusTimeout))?.value)

            return ({
                settings: {
                    currentTime,
                    currentTimeString: currentTime.toISOString().slice(0, 19).replace('T', ' '),
                    currentSeason,
                    bonusTimeout,
                    gameStarted: gameStarted.value === '1',
                },
                authDevice: {
                    token: req?.cookies?.[DEVICE_COOKIE],
                    data: deviceData,
                    register: (token) => {
                        const date = new Date() // Should NOT use currentTime (related to cookies and client time!)
                        res.cookie(DEVICE_COOKIE, token, { maxAge: DEVICE_TIMEOUT * 1000, expires: new Date(date.setSeconds(date.getSeconds() + DEVICE_TIMEOUT)) })
                    },
                    remove: () => {
                        res.clearCookie(DEVICE_COOKIE)
                    },
                },
                authUser: {
                    token: req.cookies[USER_COOKIE],
                    data: req?.cookies?.[USER_COOKIE] ? await UserDb.findOne({ where: { auth_token: req.cookies[USER_COOKIE] } }) : null,
                    login: (token) => {
                        const date = new Date() // Should NOT use currentTime (related to cookies and client time!)
                        const time = new Date(date.setSeconds(date.getSeconds() + USER_TIMEOUT))
                        res.cookie(USER_COOKIE, token, { maxAge: USER_TIMEOUT * 1000, expires: time })
                        res.cookie(USER_SESSION_TIME, Math.round(new Date(time).getTime() / 1000), { maxAge: USER_TIMEOUT * 1000, expires: time })
                    },
                    reset: () => {
                        const date = new Date() // Should NOT use currentTime (related to cookies and client time!)
                        const time = new Date(date.setSeconds(date.getSeconds() + USER_TIMEOUT))
                        res.cookie(USER_COOKIE, req.cookies[USER_COOKIE], { maxAge: USER_TIMEOUT * 1000, expires: time })
                        res.cookie(USER_SESSION_TIME, Math.round(new Date(time).getTime() / 1000), { maxAge: USER_TIMEOUT * 1000, expires: time })
                    },
                    logout: () => { 
                        res.clearCookie(USER_COOKIE)
                    },
                    getSession: () => req.cookies[USER_SESSION_TIME],
                },
            })
        }),
    })

    const app = express()
    app.use(cookieParser())

    app.use(cors({
        origin: ['https://app.tronis.lv', 'https://lapp.tronis.lv:8080'],
        credentials: true,
    }))

    app.options('*', cors({
        origin: ['https://app.tronis.lv', 'https://lapp.tronis.lv:8080'],
        credentials: true,
    }))

    await server.start()
    server.applyMiddleware({
        path: '/',
        app,
        cors: false,
    })

    app.listen({ port: PORT }, () =>
        console.log(`Server ready at http://lapi.tronis.lv:${PORT}${server.graphqlPath}`),
    )
})().catch((err) => {
    console.error('CRITICAL', err)
    process.exit(1)
})
