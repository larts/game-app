import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'
import { Op } from 'sequelize'

import { ScoreDb } from '../db/models/ScoreDb'
import { TeamDb } from '../db/models/TeamDb'
import { Pillar } from '../helpers/enums'
import { Context } from '../types/Context'

import { score } from './Score'
import { victoryTotals } from './VictoryTotals'


export const victoryPoints = new GraphQLObjectType({
    name: 'VictoryPoints',
    description: 'Victory point data',
    fields: () => ({
        points: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(score))),
            resolve: async (_, __, context: Context): Promise<ScoreDb[]> => await ScoreDb.findAll({ where: {
                type_id: Pillar.Victory,
                score_time: {
                    [Op.lte]: context.settings.currentTimeString,
                } } }),
        },
        totals: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(victoryTotals))),
            resolve: async (): Promise<TeamDb[]> => await TeamDb.findAll(),
        },
    }),
})
