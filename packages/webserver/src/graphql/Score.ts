import {
    GraphQLInt, GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'
import { Op } from 'sequelize'

import { PillarDb } from '../db/models/PillarDb'
import { ScoreDb } from '../db/models/ScoreDb'
import { ScoreReasonsDb } from '../db/models/ScoreReasonsDb'
import { SeasonDb } from '../db/models/SeasonDb'
import { TeamDb } from '../db/models/TeamDb'
import { Context } from '../types/Context'

import { pillar } from './Pillar'
import { scoreReasons } from './ScoreReasons'
import { season } from './Season'
import { team } from './Team'


export const score = new GraphQLObjectType({
    name: 'Score',
    description: 'Score row',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        created: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: ScoreDb) => new Date(source.created).getTime() / 1000,
        },
        scored: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: ScoreDb) => new Date(source.score_time).getTime() / 1000,
        },
        pillar: {
            type: new GraphQLNonNull(pillar),
            resolve: (data: ScoreDb) => PillarDb.findByPk(data.type_id),
        },
        team: {
            type: new GraphQLNonNull(team),
            resolve: (data: ScoreDb) => TeamDb.findByPk(data.team_id),
        },
        season: {
            type: new GraphQLNonNull(season),
            resolve: (data: ScoreDb) => SeasonDb.findByPk(data.period_id),
        },
        reason: {
            type: new GraphQLNonNull(scoreReasons),
            resolve: (data: ScoreDb) => ScoreReasonsDb.findByPk(data.reason_id),
        },
    }),
})

export const periodScoreQuery = {
    type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(score))),
    args: {
        season: {
            description: 'Season id',
            type: GraphQLInt,
        },
    },
    resolve: async (data: TeamDb, { team, season }: { team: number, season?: number }, context: Context): Promise<ScoreDb[]> => !season ?
        await ScoreDb.findAll({
            where: {
                team_id: data.id,
                score_time: {
                    [Op.lte]: context.settings.currentTimeString,
                },
            },
        }) : await ScoreDb.findAll({
            where: {
                team_id: data.id,
                period_id: season,
                score_time: {
                    [Op.lte]: context.settings.currentTimeString,
                },
            },
        }),
}
