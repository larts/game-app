import { GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql'

import { Pillar2SeasonDb } from '../db/models/Pillar2SeasonDb'
import { SeasonDb } from '../db/models/SeasonDb'
import { SettingsDb } from '../db/models/SettingsDb'
import { Pillar, Settings } from '../helpers/enums'

import { pillar2season } from './Pillar2Season'


export const season = new GraphQLObjectType({
    name: 'Season',
    description: 'Season data',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        started: {
            type: GraphQLInt,
            resolve: (source: SeasonDb) => source.started ? new Date(source.started).getTime() / 1000 : null,
        },
        ended: {
            type: GraphQLInt,
            resolve: (source: SeasonDb) => source.ended ? new Date(source.ended).getTime() / 1000 : null,
        },
        nextSeason: {
            description: 'season that will follow this',
            type: season,
            resolve: async (data: SeasonDb) => await SeasonDb.findByPk(data.next_periods_id),
        },
        previousSeason: {
            description: 'season that preceded this',
            type: season,
            resolve: async (data: SeasonDb) => await SeasonDb.findOne({ where: { next_periods_id:  data.next_periods_id } }),
        },
        pillars: {
            description: 'Season\'s pillar data',
            type:  new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(pillar2season))),
            resolve: async (data: SeasonDb): Promise<Pillar2SeasonDb[]> => {
                const isFriday = await SettingsDb.findByPk(Settings.IsFridayNight)

                const search = isFriday.value == '1' ? Pillar.Friday : [Pillar.Industry, Pillar.Military, Pillar.Culture, Pillar.Industry]

                return await Pillar2SeasonDb.findAll({
                    where: {
                        period_id: data.id,
                        score_type_id: search,
                    },
                })
            },
        },
    }),
})
