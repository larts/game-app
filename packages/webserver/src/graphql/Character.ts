import {
    GraphQLBoolean,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'


import { CharacterDb } from '../db/models/CharacterDb'


export const character = new GraphQLObjectType({
    name: 'Character',
    description: 'Character',
    fields: () => ({
        charcode: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        isScored: {
            type: new GraphQLNonNull(GraphQLBoolean),
            resolve: (source: CharacterDb) => !!source.tribute_time,
        },
    }),
})
