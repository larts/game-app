import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { PillarDb } from '../db/models/PillarDb'

import { pillarOfSeasonAllQuery, pillarOfSeasonQuery } from './Pillar2Season'


export const pillar = new GraphQLObjectType({
    name: 'Pillar',
    description: 'Pillar of scoring',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        season: pillarOfSeasonQuery,
        allSeasons: pillarOfSeasonAllQuery,
    }),
})

export const pillarQuery = {
    type: pillar,
    args: {
        id: {
            description: 'Pillar id',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_source: unknown, { id }: { id?: string }): Promise<PillarDb | null> => await PillarDb.findByPk(id),
}
