import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'

import { ScoreReasonsDb } from '../db/models/ScoreReasonsDb'
import { WorkshopSizeDb } from '../db/models/WorkshopSizeDb'

import { scoreReasons } from './ScoreReasons'


export const workshopSize = new GraphQLObjectType({
    name: 'WorkshopType',
    description: 'Types of workshops - represent type of workshop - resource and size',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        cooldown: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        wood: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopSizeDb) => source.wood_cost,
        },
        resourceCost: {
            description: 'Cost of resource that s specified in kit type',
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopSizeDb) => source.resource_cost,
        },
        coin: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopSizeDb) => source.coin_cost,
        },
        score: {
            type: new GraphQLNonNull(scoreReasons),
            resolve: (source: WorkshopSizeDb) => ScoreReasonsDb.findByPk(source.score_reason_id),
        },
        ip: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopSizeDb) => source.ip_amount,
        },
    }),
})
