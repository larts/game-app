import {
    GraphQLInt, GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { Recipe2ResourceDb } from '../db/models/Recipe2ResourceDb'
import { RecipeDb } from '../db/models/RecipeDb'
import { RecipeLogDb } from '../db/models/RecipeLogDb'
import { ResourceTypeDb } from '../db/models/ResourceTypeDb'
import { TeamDb } from '../db/models/TeamDb'

import { recipeLog } from './RecipeLog'
import { resourceType } from './ResourceTypes'
import { team } from './Team'


export const recipe = new GraphQLObjectType({
    name: 'Recipe',
    description: 'Recipe',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: GraphQLString,
        },
        firstCookedBy: {
            type: team,
            resolve: async (source: RecipeDb): (Promise<TeamDb> | null) => {
                if (!source.created_first_id) { return null }

                const log = await RecipeLogDb.findByPk(source.created_first_id)

                if (log) {
                    return await TeamDb.findByPk(log.team_id)
                }

                return null
            },
        },
        resources: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(resourceType))),
            resolve: async (source: RecipeDb): Promise<ResourceTypeDb[]> => {
                const resourcePointers = await Recipe2ResourceDb.findAll({
                    where: {
                        recipe_id: source.id,
                    },
                })

                const resources: ResourceTypeDb[] = await Promise.all(resourcePointers.map(async (pointer): Promise<ResourceTypeDb> => {
                    return await ResourceTypeDb.getType(pointer.resource_id)
                }))

                return resources
            },
        },
        cookLog: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(recipeLog))),
            resolve: async (source: RecipeDb): Promise<RecipeLogDb[]> => await RecipeLogDb.findAll({ where: {
                recipe_id: source.id,
            }, order: [['created', 'DESC']] }),
        },
    }),
})
