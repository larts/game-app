import {
    GraphQLBoolean,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'
import { GraphQLList } from 'graphql'

import { SeasonDb } from '../db/models/SeasonDb'
import { SettingsDb } from '../db/models/SettingsDb'
import { Context } from '../types/Context'

import { resourceQuery } from './ResourceTypes'
import { settings as setting } from './Settings'


const START_TIME_ID = 9
const CURRENT_SEASON_ID = 2

export const baseData = new GraphQLObjectType({
    name: 'BaseData',
    description: 'Different selectable server data',
    fields: {
        settings: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(setting))),
            description: 'Get current server time',
            resolve: async (): Promise<SettingsDb[]> => await SettingsDb.findAll(),
        },
        setting: {
            type: setting,
            description: 'Request one specific setting valuem',
            args: {
                id: {
                    description: 'Setting id',
                    type: new GraphQLNonNull(GraphQLInt),
                },
            },
            resolve: async (_, { id }: { id: number }) => await SettingsDb.findByPk(id),
        },
        resourceTypes: resourceQuery,
        currentTime: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'Get current server time',
            resolve: (_, __, context: Context) => Math.round(context.settings.currentTime.getTime() / 1000),
        },
        isMarketDevice: {
            type: new GraphQLNonNull(GraphQLBoolean),
            resolve: (_, __, context: Context) => context.authDevice.data?.is_marketplace ?? false,
        },
        elapsedTime: {
            type: GraphQLInt,
            description: 'Get elapsed seconds since start of game',
            resolve: async (_, __, context: Context): Promise<number> => {
                const start = await SettingsDb.findByPk(START_TIME_ID)

                if (start.value === '') {
                    return null
                }

                const startTime = new Date(start.value)
                const currentTime = context.settings.currentTime

                return Math.round((currentTime.getTime() - startTime.getTime())/ 1000)
            },
        },
        elapsedSeasonTime: {
            type: GraphQLInt,
            description: 'Get elapsed seconds since start of current season',
            resolve: async (_, __, context: Context): Promise<number> => {
                const currentSeasonId = await SettingsDb.findByPk(CURRENT_SEASON_ID)

                const currentSeason = await SeasonDb.findByPk(currentSeasonId.value)

                if (!currentSeason?.started) {
                    return null
                }

                const startTime = new Date(currentSeason?.started)
                const currentTime = context.settings.currentTime

                return Math.round((currentTime.getTime() - startTime.getTime())/ 1000)
            },
        },
        userSessionEnd: {
            type: GraphQLInt,
            resolve: (_, __, context) => context.authUser.getSession(),
        },
    },
})
