import {
    GraphQLInt, GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { ResourceTypeDb } from '../db/models/ResourceTypeDb'


export const resourceType = new GraphQLObjectType({
    name: 'ResourceType',
    description: 'Resource type information',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
        },
        subtype: {
            type: new GraphQLNonNull(GraphQLString),
        },
        code: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'QR code prefix for resource',
            resolve: (source: ResourceTypeDb): string => source.qr_prefix,
        },
    }),
})

export const resourceQuery = {
    type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(resourceType))),
    resolve: async (): Promise<ResourceTypeDb[]> => await ResourceTypeDb.findAll(),
}
