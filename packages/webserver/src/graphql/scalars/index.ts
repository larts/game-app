import { customTypes, customTypesDef } from './graphqlCustomTypes';

const queries = {
  ...customTypes,
};

const typeDef = [
  customTypesDef
];

export default { queries, typeDef };

