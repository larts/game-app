import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'

import { RecipeLogDb } from '../db/models/RecipeLogDb'
import { TeamDb } from '../db/models/TeamDb'

import { team } from './Team'


export const recipeLog = new GraphQLObjectType({
    name: 'RecipeLog',
    description: 'Recipe',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        created: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: RecipeLogDb) => new Date(source.created).getTime() / 1000,
            description: 'When this recipe was created',
        },
        starts: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: RecipeLogDb) => new Date(source.starts).getTime() / 1000,
            description: 'When this recipe will start scoring points (differs from created if in queue)',
        },
        team: {
            type:  new GraphQLNonNull(team),
            resolve: async (source: RecipeLogDb): Promise<TeamDb> => await TeamDb.findByPk(source.team_id),
        },
    }),
})
