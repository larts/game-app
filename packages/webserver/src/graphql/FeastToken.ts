import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'

import { FeastTokenDb } from '../db/models/FeastTokenDb'
import { RecipeDb } from '../db/models/RecipeDb'
import { SeasonDb } from '../db/models/SeasonDb'

import { recipe } from './Recipe'
import { season } from './Season'


export const feastToken = new GraphQLObjectType({
    name: 'FeastToken',
    description: 'Feast token',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        usedAt: {
            type: GraphQLInt,
            resolve: (source: FeastTokenDb) =>  source.is_used ? new Date(source.used_at).getTime() / 1000 : null,
        },
        season: {
            type: new GraphQLNonNull(season),
            resolve: async (source: FeastTokenDb) => await SeasonDb.findByPk(source.period_id),
        },
        recipe: {
            type: recipe,
            resolve: async (source: FeastTokenDb) => source.recipe_id ? await RecipeDb.findByPk(source.recipe_id) : null,
        },
    }),
})
