import {
    GraphQLFloat,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { CampDb } from '../db/models/CampDb'
import { ResourceTypeDb } from '../db/models/ResourceTypeDb'

import { resourceType } from './ResourceTypes'


export const camp = new GraphQLObjectType({
    name: 'Camp',
    description: 'Camp',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        displayTitle: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: (source: CampDb) => source.display_id,
        },
        resourceType: {
            type: new GraphQLNonNull(resourceType),
            resolve: async (source: CampDb) => await ResourceTypeDb.getType(source.resource_id),
        },
        longitude: {
            type: GraphQLFloat,
        },
        latitude: {
            type: GraphQLFloat,
        },
    }),
})
