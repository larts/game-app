import { GraphQLSchema } from 'graphql'

import { mutation } from '../mutations/mutations'

import { query } from './query'


export const schema: GraphQLSchema = new GraphQLSchema({
    query,
    mutation,
})
