import {
    GraphQLInt, GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'
import { Op } from 'sequelize'

import { CastleDb } from '../db/models/CastleDb'
import { CharacterDb } from '../db/models/CharacterDb'
import { ExpansionDb } from '../db/models/ExpansionDb'
import { FeastTokenDb } from '../db/models/FeastTokenDb'
import { FlagStatusLogDb } from '../db/models/FlagStatusLogDb'
import { ResourceDb } from '../db/models/ResourceDb'
import { SettingsDb } from '../db/models/SettingsDb'
import { TeamDb } from '../db/models/TeamDb'
import { UserDb } from '../db/models/UserDb'
import { WorkshopActivationDb } from '../db/models/WorkshopActivationDb'
import { FlagAction, Settings } from '../helpers/enums'
import { Context } from '../types/Context'

import { castle } from './Castle'
import { expansion } from './Expansion'
import { feastToken } from './FeastToken'
import { resource } from './Resource'
import { periodScoreQuery } from './Score'
import { periodTotalScoreQuery } from './TotalScore'
import { user } from './User'
import { workshopActivation } from './WorkshopActivation'


export const team = new GraphQLObjectType({
    name: 'Team',
    description: 'Team of players',
    fields: () => {
        return ({
            id: {
                type: new GraphQLNonNull(GraphQLInt),
            },
            title: {
                type: new GraphQLNonNull(GraphQLString),
            },
            color: {
                type: new GraphQLNonNull(GraphQLString),
            },
            flagStatusId: {
                type: new GraphQLNonNull(GraphQLInt),
                resolve: (team: TeamDb) => team.flag_status_id,
            },
            flagRaisedTime: {
                type: GraphQLInt,
                resolve: async (team: TeamDb, _, context) => {
                    const season = context.settings.currentSeason
                    if (season !== 0) {
                        const flagStatus = await FlagStatusLogDb.findOne({
                            where: {
                                team_id: team.id,
                                action: FlagAction.Register,
                                period_id: season,
                            },
                        })

                        if (!flagStatus) { return null}

                        return new Date(flagStatus.created).getTime() / 1000
                    }

                    return  null
                },
            },
            flagScoredTime: {
                type: GraphQLInt,
                resolve: async (team: TeamDb, _, context) => {
                    const season = context.settings.currentSeason
                    if (season !== 0) {
                        const flagStatus = await FlagStatusLogDb.findOne({
                            where: {
                                team_id: team.id,
                                action: FlagAction.Score,
                                period_id: season,
                            },
                        })

                        if (!flagStatus) { return null}

                        return new Date(flagStatus.created).getTime() / 1000
                    }

                    return  null
                },
            },
            availableEq: {
                type: new GraphQLNonNull(GraphQLInt),
                resolve: (team: TeamDb) => team.available_eq,
            },
            scores: periodScoreQuery,
            scoreTotals: periodTotalScoreQuery,
            coinbox: {
                type: GraphQLInt,
            },
            players: {
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(user))),
                resolve: async (source: TeamDb): Promise<UserDb[]> => await UserDb.findAll({ where: { team_id: source.id } }),
            },
            resources: {
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(resource))),
                description: 'All available resources for the team',
                resolve: async (source: TeamDb): Promise<ResourceDb[]> => await ResourceDb.findAll({ where: { team_id: source.id } }),
            },
            workshopActivations: {
                description: 'Return list of all workshop activations',
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(workshopActivation))),
                resolve: async (source: TeamDb): Promise<WorkshopActivationDb[]> =>
                    await WorkshopActivationDb.findAll({
                        where: {
                            team_id: source.id,
                        },
                    }),
            },
            nextRespawnTime: {
                type: GraphQLInt,
                resolve: async (team: TeamDb) => {
                    const previousChar = await CharacterDb.findOne({
                        where: {
                            owner_team_id: team.id,
                        }, order: [['created', 'DESC']],
                    })

                    if (previousChar) {
                        const respawnPeriod = await SettingsDb.findByPk(Settings.RespawnPeriod)
                        const date = new Date(previousChar.created)

                        return new Date(date.setSeconds(date.getSeconds() + Number(respawnPeriod.value))).getTime() / 1000
                    }

                    return null
                },
            },
            feastTokens: {
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(feastToken))),
                resolve: async (source: TeamDb) => await FeastTokenDb.findAll({
                    where: {
                        team_id: source.id,
                    },
                }),
            },
            expansions: {
                description: 'Return list of available and/or scored expansions',
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(expansion))),
                resolve: async (source: TeamDb): Promise<ExpansionDb[]> =>
                    await ExpansionDb.findAll({
                        where: {
                            [Op.or]: [
                                { owner_team_id: source.id },
                                { rewarded_team_id: source.id },
                            ],
                        },
                        order: [
                            ['showed', 'DESC'],
                        ],
                    }),
            },
            castle: {
                type: castle,
                resolve: async (source: TeamDb): Promise<CastleDb> => await CastleDb.findOne({ where: { team_id: source.id } }),
            },
        })
    },
})

export const teamQuery = {
    description: 'Request specific teams information. If no id provided will return current devices team',
    type: team,
    args: {
        id: {
            description: 'Teams id or blank for current devices team',
            type: GraphQLInt,
        },
    },
    resolve: async (_source: unknown, { id }: { id?: string }, context: Context): Promise<TeamDb | null> => (id || context.authDevice.data) ? await TeamDb.findByPk(id ?? context.authDevice.data.team_id) : null,
}
