import {
    GraphQLBoolean,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'
import { Op } from 'sequelize'

import { CharacterDb } from '../db/models/CharacterDb'
import { TeamDb } from '../db/models/TeamDb'
import { UserDb } from '../db/models/UserDb'

import { character } from './Character'
import { team } from './Team'


export const user = new GraphQLObjectType({
    name: 'User',
    description: 'Get current user',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
            description: 'The id of the user.',
        },
        fullName: {
            type: GraphQLString,
            resolve: (data: UserDb) => data.name,
        },
        name: {
            type: GraphQLString,
            resolve: (data: UserDb) => data.display_name,
        },
        sex: {
            type: new GraphQLNonNull(GraphQLString),
        },
        equipmentPoints: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (data: UserDb) => data.equipment_points,
        },
        equipmentHasPhoenix: {
            type: new GraphQLNonNull(GraphQLBoolean),
            resolve: (data: UserDb) => Boolean(data.equipment_has_phoenix),
        },
        team: {
            type: new GraphQLNonNull(team),
            resolve: (data: UserDb): Promise<TeamDb> => TeamDb.findByPk(data.team_id),
        },
        qr: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: (data: UserDb) => data.qr_code,
        },
        respawnCount: {
            type: new GraphQLNonNull(GraphQLInt),
            resolve: async (data: UserDb) => await CharacterDb.count({ where: { owner_user_id: data.id } }),
        },
        character: {
            type: character,
            resolve: async (data: UserDb) => await CharacterDb.findOne({ where: { owner_user_id: data.id, death_registered_at: { [Op.eq]: null } } }),
        },
    }),
})
