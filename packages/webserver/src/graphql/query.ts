import { GraphQLInt, GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql'
import { GraphQLList } from 'graphql'

import { PillarDb } from '../db/models/PillarDb'
import { RecipeDb } from '../db/models/RecipeDb'
import { SeasonDb } from '../db/models/SeasonDb'
import { TeamDb } from '../db/models/TeamDb'
import { UserDb } from '../db/models/UserDb'
import { WorkshopSizeDb } from '../db/models/WorkshopSizeDb'
import { Context } from '../types/Context'

import { baseData } from './BaseData'
import { pillar, pillarQuery } from './Pillar'
import { recipe } from './Recipe'
import { season } from './Season'
import { team, teamQuery } from './Team'
import { user } from './User'
import { victoryPoints } from './VictoryPoints'
import { workshopSize } from './WorkshopSize'


export const query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        team: teamQuery,
        teams: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(team))),
            resolve: () => TeamDb.findAll(),
        },
        pillar: pillarQuery,
        pillars: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(pillar))),
            resolve: () => PillarDb.findAll(),
        },
        season: {
            type: season,
            args: {
                season: {
                    description: 'Season id',
                    type: GraphQLInt,
                },
            },
            resolve: async (_, { season }: { season?: number }, context) => {
                const seasonData = context.settings.currentSeason
                const currentSeason = season || seasonData || 1

                return await SeasonDb.findByPk(currentSeason)
            },
        },
        victoryPoints: {
            type: new GraphQLNonNull(victoryPoints),
            resolve: () => ({}),
        },
        baseData: {
            type: new GraphQLNonNull(baseData),
            resolve: () => ({}),
        },
        user: {
            type: user,
            args: {
                qrCode: {
                    description: 'Users qr code',
                    type: GraphQLString,
                },
            },
            description: '',
            resolve: async (_, { qrCode }: { qrCode?: string }, context: Context): Promise<UserDb> => qrCode ? await UserDb.findOne({ where: { qr_code: qrCode } } ) : context.authUser.data,
        },
        recipes: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(recipe))),
            resolve: async (): Promise<RecipeDb[]> => await RecipeDb.findAll(),
        },
        workshops: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(workshopSize))),
            resolve: async (): Promise<WorkshopSizeDb[]> => await WorkshopSizeDb.findAll(),
        },
    },
})
