import {
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'
import { Op } from 'sequelize'

import { Pillar2SeasonDb } from '../db/models/Pillar2SeasonDb'
import { PillarDb } from '../db/models/PillarDb'
import { ScoreDb } from '../db/models/ScoreDb'
import { SeasonDb } from '../db/models/SeasonDb'
import { TotalScoreDb } from '../db/models/TotalScoreDb'
import { Context } from '../types/Context'

import { pillar } from './Pillar'
import { score } from './Score'
import { season } from './Season'
import { totalScore } from './TotalScore'


export const pillar2season = new GraphQLObjectType({
    name: 'PillarOfSeason',
    description: 'Pillar of scoring',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        target: {
            description: 'Target value of this pillar for this season',
            type: new GraphQLNonNull(GraphQLInt),
        },
        currentTotal: {
            description: 'Current total score for this season',
            type: new GraphQLNonNull(GraphQLInt),
        },
        season: {
            type: new GraphQLNonNull(season),
            resolve: (data: Pillar2SeasonDb) => SeasonDb.findByPk(data.period_id),
        },
        pillar: {
            type: new GraphQLNonNull(pillar),
            resolve: (data: Pillar2SeasonDb): Promise<PillarDb | null> => PillarDb.findByPk(data.score_type_id),
        },
        totalScores: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(totalScore))),
            resolve: (data: Pillar2SeasonDb) => TotalScoreDb.findAll({ where: { period_id: data.period_id, type_id: data.score_type_id } }),
        },
        scores: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(score))),
            resolve: async (data: Pillar2SeasonDb, _, context: Context): Promise<ScoreDb[]> => {
                const season = await SeasonDb.findByPk(data.period_id)
                return ScoreDb.findAll({ where: {
                    type_id: data.score_type_id,
                    score_time: {
                        [Op.gte]: season.started,
                        ...season.ended ? ({
                            [Op.lt]: season.ended,
                        }) : ({
                            [Op.lte]: context.settings.currentTimeString,
                        }),
                    },
                } })
            },
        },
    }),
})

export const pillarOfSeasonQuery = {
    type: pillar2season,
    args: {
        season: {
            description: 'Season id, defaults to current or 1. if game not started (for friday night)',
            type: GraphQLInt,
        },
    },
    resolve: async (data: PillarDb, {
        season,
    }: { season?: number }, context: Context): Promise<Pillar2SeasonDb | null> => {
        const seasonData = context.settings.currentSeason

        const currentSeason = season || seasonData || 1

        return await Pillar2SeasonDb.findOne({
            where: {
                score_type_id: data.id,
                period_id: currentSeason,
            },
        })
    },
}

export const pillarOfSeasonAllQuery = {
    type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(pillar2season))),
    resolve: async (data: PillarDb): Promise<Pillar2SeasonDb[] | null> =>
        await Pillar2SeasonDb.findAll({
            where: {
                score_type_id: data.id,
            },
        }),
}

