import { GraphQLList } from 'graphql'
import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
} from 'graphql'

import { ScoreDb } from '../db/models/ScoreDb'
import { WorkshopActivationDb } from '../db/models/WorkshopActivationDb'
import { WorkshopSizeDb } from '../db/models/WorkshopSizeDb'
import { Context } from '../types/Context'

import { score } from './Score'
import { workshopSize } from './WorkshopSize'


export const workshopActivation = new GraphQLObjectType({
    name: 'WorkshopActivation',
    description: 'Activation log for workshops',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        created: {
            description: 'At this time this was created',
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopActivationDb) => new Date(source.created).getTime() / 1000,
        },
        startTime: {
            description: 'Timestamp of task beginning time',
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopActivationDb) => new Date(source.started).getTime() / 1000,
        },
        cancelled: {
            description: 'NULL or timestamp if at any point was cancelled',
            type: GraphQLInt,
            resolve: (source: WorkshopActivationDb) => source.cancelled ? new Date(source.cancelled).getTime() / 1000 : null,
        },
        finishTime: {
            description: 'Timestamp of task end time',
            type: new GraphQLNonNull(GraphQLInt),
            resolve: (source: WorkshopActivationDb) => new Date(source.finished).getTime() / 1000,
        },
        size: {
            type: new GraphQLNonNull(workshopSize),
            resolve: (source: WorkshopActivationDb) => WorkshopSizeDb.findByPk(source.size_id),
        },
        variant: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        // TODO: create usedResource entry
        scorePoints: {
            description: 'list of score points',
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(score))),
            resolve: async (source: WorkshopActivationDb, _, context: Context): Promise<ScoreDb[]> => {
                const workshop = await WorkshopSizeDb.findByPk(source.size_id)

                return ScoreDb.findAll({
                    where: {
                        reason_id: workshop.score_reason_id,
                        details: source.id,
                    },
                })
            },
        },
        priceDetails: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(new GraphQLObjectType({
                name: 'priceDetailsEntry',
                fields: {
                    type: { type: new GraphQLNonNull(GraphQLInt) },
                    amount: { type: new GraphQLNonNull(GraphQLInt) },
                },
            })))),
            resolve: (source: WorkshopActivationDb) => JSON.parse(source.price_details!),
        },
    }),
})
