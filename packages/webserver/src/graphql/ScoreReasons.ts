import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql'

import { PillarDb } from '../db/models/PillarDb'
import { ScoreReasonsDb } from '../db/models/ScoreReasonsDb'
import { SeasonDb } from '../db/models/SeasonDb'

import { pillar } from './Pillar'
import { season } from './Season'


export const scoreReasons = new GraphQLObjectType({
    name: 'ScoreReasons',
    description: 'Scorinng reasons',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            type: new GraphQLNonNull(GraphQLString),
        },
        amount: {
            type: new GraphQLNonNull(GraphQLInt),
        },
        season: {
            description: 'Season this event is attached to',
            type: season,
            resolve: (data: ScoreReasonsDb) => SeasonDb.findByPk(data.assign_period),
        },
        pillar: {
            type: pillar,
            resolve: (data: ScoreReasonsDb) => PillarDb.findByPk(data.type_id),
        },
    }),
})
