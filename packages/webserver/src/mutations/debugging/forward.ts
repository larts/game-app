import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { SettingsDb } from '../../db/models/SettingsDb'
import { Settings } from '../../helpers/enums'


export const forward = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        increase: {
            description: 'Seconds to fast forward',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { increase }: { increase: number }, context): Promise<string> => {
        try {
            const currentTime = await SettingsDb.findByPk(Settings.CurrentTick)

            const forwardTime = await SettingsDb.findByPk(Settings.ForwardTimeTo)

            forwardTime.value = String(Number(currentTime.value) + increase)

            await forwardTime.save()

            return 'Ok!'
        } catch (e) {
            throw new ApolloError('Time forward failed', 'TIME_FORWARD_FAILED')
        }

    },
}
