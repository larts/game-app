import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { SettingsDb } from '../../db/models/SettingsDb'
import { Settings } from '../../helpers/enums'


export const setTick = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        increase: {
            description: 'In seconds needed time position',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { increase }: { increase: number }): Promise<string> => {
        try {
            const forwardTime = await SettingsDb.findByPk(Settings.ForwardTimeTo)

            forwardTime.value = String(increase)

            await forwardTime.save()

            return 'Ok!'
        } catch (e) {
            throw new ApolloError('Time jump failed', 'TIME_JUMP_FAILED')
        }

    },
}
