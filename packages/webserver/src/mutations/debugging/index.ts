import { GraphQLObjectType } from 'graphql'

import { forward } from './forward'
import { setTick } from './setTick'


export const debugMutations = new GraphQLObjectType({
    name: 'DebugActions',
    fields: {
        forward,
        setTick,
    },
})
