import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { CharacterDb } from '../../db/models/CharacterDb'
import { CharacterKillDb } from '../../db/models/CharacterKillDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SeasonDb } from '../../db/models/SeasonDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { UserDb } from '../../db/models/UserDb'
import { checkAuth } from '../../helpers/auth'
import { ScoreReason, Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const tribute = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        code: {
            description: 'Character Code',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { code }: { code: number }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const character = await CharacterDb.findOne({
                where: {
                    charcode: code,
                },
                order: [['created', 'DESC']],
            })

            if (!character) {
                throw new ApolloError(`No character found with wristband "${code}"`,'TRIBUTE_NOT_REGISTERED')
            }

            if (character.owner_team_id === context.authUser.data.team_id) {
                throw new ApolloError('This character is in Your team!', 'TRIBUTE_SAME_TEAM')
            }

            if (character.tribute_team_id) {
                throw new ApolloError('This tribute is already registered!', 'TRIBUTE_WAS_SCANNED')
            }

            const seasonId = context.settings.currentSeason

            if (seasonId === 0) {
                throw new ApolloError('Season not set!', 'TRIBUTE_NO_SEASON_SET')
            }

            const season = await SeasonDb.findByPk(seasonId)

            // Kill registration

            const characterKills: CharacterKillDb = (await CharacterKillDb.findOne({
                where: {
                    team_id: context.authUser.data.team_id,
                    scored_team_id: character.owner_team_id,
                    period_id: seasonId,
                },
            })) || new CharacterKillDb()

            if (characterKills.isNewRecord) {
                characterKills.amount = 0
                characterKills.team_id = context.authUser.data.team_id
                characterKills.scored_team_id = character.owner_team_id
                characterKills.period_id = seasonId
                await characterKills.save()
            }

            const maxKills = await SettingsDb.findByPk(Settings.MaxTeamKills)

            if (characterKills.amount >= Number(maxKills.value)) {
                throw new ApolloError('Max tributes for this team reached', 'TRIBUTE_MAX_EXCEED')
            } 

            character.tribute_team_id = context.authUser.data.team_id
            character.tribute_user_id = context.authUser.data.id
            character.tribute_period_id = seasonId
            character.tribute_time = context.settings.currentTimeString
            character.death_registered_at = context.settings.currentTimeString
            await character.save()

            characterKills.amount = characterKills.amount + 1
            await characterKills.save()

            // Scoring

            await ScoreDb.do(ScoreReason.Tribute, String(character.id), context.authUser.data.team_id, context)

            const corpse = await UserDb.findByPk(character.owner_user_id)
            return corpse.display_name
        } catch (e) {
            genericError(e,'Was error while registerring tribute', 'TRIBUTE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
