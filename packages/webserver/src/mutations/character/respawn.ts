import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'
import { Op } from 'sequelize'

import { CharacterDb } from '../../db/models/CharacterDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { checkAuth } from '../../helpers/auth'
import { Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const respawn = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        code: {
            description: 'Character Code',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { code }: { code: number }, context: Context): Promise<string> => {
        checkAuth(context)

        const seasonId = context.settings.currentSeason

        if (seasonId === 0) {
            throw new ApolloError('Season not set!', 'RESPAWN_NO_SEASON_SET')
        }

        try {
            await CharacterDb.validateCode(code, context.authUser.data, context)

            const previousChar = await CharacterDb.findOne({ where: {
                owner_team_id: context.authUser.data.team_id,
            }, order: [['created', 'DESC']] })

            if (previousChar) {
                const respawnPeriod = await SettingsDb.findByPk(Settings.RespawnPeriod)
                const date = new Date(previousChar.created)

                const distance = new Date(date.setSeconds(date.getSeconds() + Number(respawnPeriod.value)))

                if (distance > context.settings.currentTime) {
                    throw new ApolloError('Please wait cooldown for respawn!', 'RESPAWN_TOO_SOON')
                }
            }

            const existingCharacter = await CharacterDb.findOne({
                where: {
                    owner_user_id: context.authUser.data.id,
                    death_registered_at: { [Op.eq]: null },
                },
            })

            if (existingCharacter) {
                existingCharacter.death_registered_at = context.settings.currentTimeString
                existingCharacter.death_season_id = seasonId
                await existingCharacter.save()
            }

            const character = new CharacterDb()

            character.owner_team_id = context.authUser.data.team_id
            character.owner_user_id = context.authUser.data.id
            character.charcode = code
            character.created = context.settings.currentTimeString

            const key = Math.floor(code/10)

            character.charkey = key

            await character.save()
        } catch (e) {
            genericError(e,'Respawn failed', 'RESPAWN_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
