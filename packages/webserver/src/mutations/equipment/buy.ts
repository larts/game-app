import { GraphQLNonNull, GraphQLString } from 'graphql'

import { EquipmentDb } from '../../db/models/EquipmentDb'
import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { ResourceDb } from '../../db/models/ResourceDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { equipmentPrice } from '../../helpers/fixedData'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const buy = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const price = await equipmentPrice(context.settings.currentSeason)

            await ResourceDb.spendResources(price, await ResourceActionDb.findByPk(ResourceActionId.EquipmentSpent), context.authUser.data, context.settings.currentTimeString)

            const eq = new EquipmentDb()

            eq.team_id = context.authUser.data.team_id
            eq.user_id = context.authUser.data.id
            eq.created = context.settings.currentTimeString

            await eq.save()

            const team = await context.authUser.data.getTeam()
            team.available_eq = team.available_eq + 1
            await team.save()

        } catch (e) {
            genericError(e,'Failed to buy equipment point', 'EQ_BUY_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
