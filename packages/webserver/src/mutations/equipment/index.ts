import { GraphQLObjectType } from 'graphql'

import { add } from './add'
import { buy } from './buy'
import { equip } from './equip'
import { remove } from './remove'
import { unequip } from './unequip'


export const equipmentMutations = new GraphQLObjectType({
    name: 'EquipmentActions',
    fields: {
        buy,
        add,
        remove,
        equip,
        unequip,
    },
})
