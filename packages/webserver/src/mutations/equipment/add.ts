import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'

import { EquipmentAllocatedDb } from '../../db/models/EquipmentAllocatedDb'
import { checkAuth } from '../../helpers/auth'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const add = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const team = await context.authUser.data.getTeam()

            if (team.available_eq <= 0) {
                throw new ApolloError('Team has no available EQ', 'EQ_ALLOCATE_NO_AVAILABLE')
            }

            const allocate = new EquipmentAllocatedDb()

            allocate.team_id = team.id
            allocate.user_id = context.authUser.data.id
            allocate.add = true
            allocate.created = context.settings.currentTimeString
            await allocate.save()

            context.authUser.data.equipment_points = context.authUser.data.equipment_points + 1
            await context.authUser.data.save()

            team.available_eq = team.available_eq - 1
            await team.save()

        } catch (e) {
            genericError(e,'Failed to allocate equipment point', 'EQ_ALLOCATE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
