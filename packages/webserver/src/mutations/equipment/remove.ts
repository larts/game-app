import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'

import { EquipmentAllocatedDb } from '../../db/models/EquipmentAllocatedDb'
import { checkAuth } from '../../helpers/auth'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const remove = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            if (context.authUser.data.equipment_points <= 0) {
                throw new ApolloError('User has no available EQ', 'EQ_REMOVE_NO_AVAILABLE')
            }
            if (context.authUser.data.equipment_points === 1 && context.authUser.data.equipment_has_phoenix) {
                throw new ApolloError('User has to unequip Amulet of Phoenix first', 'EQ_REMOVE_HAS_PHOENIX')
            }

            const team = await context.authUser.data.getTeam()

            const allocate = new EquipmentAllocatedDb()

            allocate.team_id = team.id
            allocate.user_id = context.authUser.data.id
            allocate.add = false
            allocate.created = context.settings.currentTimeString
            await allocate.save()

            context.authUser.data.equipment_points = context.authUser.data.equipment_points - 1
            await context.authUser.data.save()

            team.available_eq = team.available_eq + 1
            await team.save()

        } catch (e) {
            genericError(e,'Failed to remove equipment point', 'EQ_REMOVE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
