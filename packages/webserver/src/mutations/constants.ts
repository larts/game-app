/**
 * @deprecated use MUTATION_RESULT enum from api instead.
 */
export const MUTATION_SUCCESS = 'OK!' as const
/**
 * @deprecated use MUTATION_RESULT enum from api instead.
 */
export const MUTATION_FAIL = 'FAIL!' as const

export const WORKSHOP_KIT_TYPE_FOOD = 1
