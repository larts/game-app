import { GraphQLObjectType } from 'graphql'

import { reward } from './reward'


export const expansionMutations = new GraphQLObjectType({
    name: 'ExpansionActions',
    fields: {
        reward,
    },
})
