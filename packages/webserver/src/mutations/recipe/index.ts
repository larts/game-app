import { GraphQLObjectType } from 'graphql'

import { score } from './score'


export const recipeMutations = new GraphQLObjectType({
    name: 'RecipeActions',
    fields: {
        score,
    },
})
