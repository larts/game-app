import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'
import { Op } from 'sequelize'

import { FeastTokenDb } from '../../db/models/FeastTokenDb'
import { RecipeDb } from '../../db/models/RecipeDb'
import { RecipeLogDb } from '../../db/models/RecipeLogDb'
import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId, Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const score = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        id: {
            description: 'Recipe id',
            type: new GraphQLNonNull(GraphQLInt),
        },
        title: {
            description: 'Title of recipe if this has no name',
            type: GraphQLString,
        },
    },
    resolve: async (_, { id, title }: { id: number, title?: string }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const seasonId = context.settings.currentSeason

            if (seasonId === 0) {
                throw new ApolloError('Season not set!', 'RECIPE_SCORE_NO_SEASON_SET')
            }

            const recipe = await RecipeDb.findByPk(id)

            if (!recipe) {
                throw new ApolloError('Recipe does not exist', 'RECIPE_SCORE_INVALID_RECIPE')
            }

            const resources = await recipe.getResources()

            const IngredientAmount = await SettingsDb.findByPk(Settings.RecipeIngredientsPerType)

            const price: Price[] = await Promise.all(resources.map(async (resource): Promise<Price> => {
                return {
                    type: await resource.getResource(),
                    amount: Number(IngredientAmount.value),
                }
            }))

            await ResourceDb.spendResources(price, await ResourceActionDb.findByPk(ResourceActionId.RecipeSpent), context.authDevice.data, context.settings.currentTimeString, String(recipe.id))

            const previousRecipeCopy = await RecipeLogDb.findOne({
                where: {
                    recipe_id: recipe.id,
                },
            })

            const RecipeScoreTime = await SettingsDb.findByPk(Settings.RecipeScoreOverTime)

            const threshold = new Date(context.settings.currentTime.getTime() - (Number(RecipeScoreTime.value) * 1000))

            // Lets create score values for future
            const previousRecipeTeam = await RecipeLogDb.findOne({
                where: {
                    team_id: context.authDevice.data.team_id,
                    starts: {
                        [Op.gte]: threshold.toISOString().slice(0, 19).replace('T', ' '),
                    },
                },
                order: [
                    ['starts', 'DESC'],
                ],
            })

            const recipeLog = new RecipeLogDb()
            recipeLog.recipe_id = recipe.id
            recipeLog.team_id = context.authDevice.data.team_id
            recipeLog.created = context.settings.currentTimeString
            await recipeLog.save()

            if (!recipe.created_first_id) {
                recipe.created_first_id = recipeLog.id
                recipe.title = title
                await recipe.save()
            }

            if (previousRecipeCopy === null) {
                const feastToken = new FeastTokenDb()

                feastToken.team_id = context.authDevice.data.team_id
                feastToken.period_id = seasonId
                feastToken.recipe_id = recipe.id
                feastToken.created = context.settings.currentTimeString
                await feastToken.save()
            }

            const RecipePointsPerIngredient = await SettingsDb.findByPk(Settings.RecipePointsPerIngredient)

            const pointAmount = resources.length * Number(RecipePointsPerIngredient.value)

            const CPperiod = Number(RecipeScoreTime.value) / pointAmount

            const currentCreateTimeOffset: Date = previousRecipeTeam !== null ? new Date((new Date(previousRecipeTeam.starts)).getTime() + (Number(RecipeScoreTime.value) * 1000)) : context.settings.currentTime

            recipeLog.starts = currentCreateTimeOffset.toISOString().slice(0, 19).replace('T', ' ')
            await recipeLog.save()

            // assign predicted score point values

            for (let i = 0; i < pointAmount; i++) {
                await ScoreDb.do(recipe.score_reason_id, String(recipeLog.id), context.authDevice.data.team_id, context, null, new Date(currentCreateTimeOffset.getTime() + (CPperiod * (i + 1) * 1000)))
            }
        } catch (e) {
            genericError(e,'Recipe scoring failed', 'RECIPE_SCORE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
