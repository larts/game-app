import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'

import { FlagStatusDb } from '../../db/models/FlagStatusDb'
import { FlagStatusLogDb } from '../../db/models/FlagStatusLogDb'
import { QrCodeDb } from '../../db/models/QrCodeDb'
import { QrScanDb } from '../../db/models/QrScanDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { SettingsDb } from '../../db/models/SettingsDb'
import { TeamDb } from '../../db/models/TeamDb'
import { checkAuth } from '../../helpers/auth'
import { FlagAction, FlagStatus, ScanTypes, Settings } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const score = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        qrCode: {
            description: 'Flags QR Code',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { qrCode }: { qrCode: string }, context: Context): Promise<string> => {
        // user checks happen depending on weather this should be scored as teams flag or enemies flag
        checkAuth(context, false)

        if (!context.authUser.token) {
            throw new ApolloError('User has not registered', 'FLAG_SCORE_NO_USER')
        }

        try {
            const qr = await QrCodeDb.findOne({
                where: {
                    qrid: qrCode,
                },
            })

            if (!qr) {
                throw new ApolloError('QR code not found', 'FLAG_SCORE_NO_QR')
            }

            if (qr.owner_team_id === context.authDevice.data.team_id) {
                context.authUser.reset()
                if (context.authDevice.data.team_id !== context.authUser.data.team_id) {
                    throw new ApolloError('User and team mismatch!', 'FLAG_SCORE_L_INCORRECT_TEAM')
                }

                const scan = await QrScanDb.registerScan(qrCode, ScanTypes.ScoreLocalFlag, context.authDevice.data, context.authUser.data, context)

                const seasonId = context.settings.currentSeason

                if (seasonId === 0) {
                    throw new ApolloError('Season not set!', await scan.saveWithError('FLAG_SCORE_L_NO_SEASON_SET'))
                }

                const team: TeamDb = await TeamDb.findByPk(qr.owner_team_id)

                // Should occur when stolen flag was scored this season
                if (team.flag_status_id === FlagStatus.Stolen) {
                    throw new ApolloError('Flag was already scored!', await scan.saveWithError('FLAG_SCORE_L_ALREADY_SCORED'))
                }

                const lastRegistration = await FlagStatusLogDb.findByPk(team.flag_registration_id)

                if (!lastRegistration ||  lastRegistration.period_id !== seasonId) {
                    throw new ApolloError('Flag has not been registered', await scan.saveWithError('FLAG_SCORE_L_NOT_REGISTERED'))
                }

                const flagScoreTime = await SettingsDb.findByPk(Settings.FlagHomeScoreInterval)
                const registered = new Date(lastRegistration.created)
                const now = context.settings.currentTime

                if (new Date(registered.setMinutes(registered.getMinutes() + Number(flagScoreTime.value))) > now) {
                    throw new ApolloError('Flag cant be scored yet!', await scan.saveWithError('FLAG_SCORE_L_M_SAID_NOT_YET'))
                }

                const lastScoring = await FlagStatusLogDb.findOne({
                    where: {
                        team_id: team.id,
                        period_id: seasonId,
                        action: FlagAction.Score,
                    },
                })

                // Should occur when team scores it's own flag
                if (lastScoring) {
                    throw new ApolloError('Flag was already scored!', await scan.saveWithError('FLAG_SCORE_L_ALREADY_SCORED_2'))
                }

                const flagStatusEntry = await FlagStatusDb.findByPk(team.flag_status_id)

                if (flagStatusEntry.next_status) {
                    team.flag_status_id = flagStatusEntry.next_status
                    team.save()
                }

                const flagStatus = new FlagStatusLogDb()
                flagStatus.team_id = team.id
                flagStatus.changer_team_id = context.authUser.data.team_id
                flagStatus.current_status_id = team.flag_status_id
                flagStatus.action = FlagAction.Score
                flagStatus.period_id = seasonId
                flagStatus.created = context.settings.currentTimeString

                await flagStatus.save()

                await ScoreDb.do(flagStatusEntry.self_score_reason_id, String(team.id), context.authUser.data.team_id, context)

            } else {
                const scan = await QrScanDb.registerScan(qrCode, ScanTypes.ScoreEnemyFlag, context.authDevice.data, context.authUser.data, context)

                if (!context.authDevice.data.is_marketplace) {
                    throw new ApolloError('Must be scanned only in marketplace!', await scan.saveWithError('FLAG_SCORE_NOT_MARKETPLACE'))
                }

                const seasonId = context.settings.currentSeason

                if (seasonId === 0) {
                    throw new ApolloError('Season not set!', await scan.saveWithError('FLAG_SCORE_E_NO_SEASON_SET'))
                }

                const team: TeamDb = await TeamDb.findByPk(qr.owner_team_id)

                // Should occur when stolen flag was scored this season
                if (team.flag_status_id === FlagStatus.Stolen) {
                    throw new ApolloError('Flag was already scored!', await scan.saveWithError('FLAG_SCORE_E_ALREADY_SCORED'))
                }

                const lastRegistration = await FlagStatusLogDb.findByPk(team.flag_registration_id)

                if (!lastRegistration ||  lastRegistration.period_id !== seasonId) {
                    throw new ApolloError('Flag is not valid for scoring!', await scan.saveWithError('FLAG_SCORE_E_NOT_VALID'))
                }

                const lastScoring = await FlagStatusLogDb.findOne({
                    where: {
                        team_id: team.id,
                        period_id: seasonId,
                        action: FlagAction.Score,
                    },
                })

                // Should occur when team scores it's own flag
                if (lastScoring) {
                    throw new ApolloError('Flag was already scored!', await scan.saveWithError('FLAG_SCORE_E_ALREADY_SCORED_2'))
                }

                // flag has been registered in this season
                if (!lastRegistration ||  lastRegistration.period_id !== seasonId) {
                    throw new ApolloError('Flag is not valid for scoring!', await scan.saveWithError('FLAG_SCORE_E_NOT_VALID'))
                }

                const flagStatusEntry = await FlagStatusDb.findByPk(team.flag_status_id)

                team.flag_status_id = FlagStatus.Stolen
                team.save()

                const flagStatus = new FlagStatusLogDb()
                flagStatus.team_id = team.id
                flagStatus.changer_team_id = context.authUser.data.team_id
                flagStatus.current_status_id = team.flag_status_id
                flagStatus.action = FlagAction.Score
                flagStatus.period_id = seasonId
                flagStatus.created = context.settings.currentTimeString
                await flagStatus.save()

                await ScoreDb.do(flagStatusEntry.score_reason_id, String(team.id), context.authUser.data.team_id, context)
            }

        } catch (e) {
            genericError(e,'Flag scoring failed', 'FLAG_SCORE_FAIL')
        }

        return MUTATION_SUCCESS
    },
}
