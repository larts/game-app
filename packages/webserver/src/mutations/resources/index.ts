import { GraphQLObjectType } from 'graphql'

import { add } from './add'
import { addCoins } from './addCoins'
import { removeCoins } from './removeCoins'
import { sell } from './sell'


export const resourceMutations = new GraphQLObjectType({
    name: 'ResourcesActions',
    fields: {
        add,
        addCoins,
        removeCoins,
        sell,
    },
})
