import { GraphQLNonNull, GraphQLString } from 'graphql'

import { QrScanDb } from '../../db/models/QrScanDb'
import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId, ResourceKeys, ScanTypes } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const add = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        qrCode: {
            description: 'QR Code',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { qrCode }: { qrCode: string }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const scan = await QrScanDb.registerScan(qrCode, ScanTypes.ResourceRegistration, context.authDevice.data, context.authUser.data, context, true)

            const qr = await scan.getQrCode()
            const typeCode = qr.qrid.substr(0, 2)

            const resource: Price = {
                type: await ResourceTypeDb.getType(ResourceKeys[typeCode]),
                amount: 1,
            }

            await ResourceDb.spendResources([resource], await ResourceActionDb.findByPk(ResourceActionId.Scanned), context.authUser.data, context.settings.currentTimeString)
        } catch (e) {
            genericError(e,'Resource failed to register resource', 'RESOURCE_REGISTER_ERROR')
        }

        return MUTATION_SUCCESS
    },
}
