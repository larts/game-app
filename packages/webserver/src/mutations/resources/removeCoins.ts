import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'

import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId, ResourceType } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const removeCoins = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        amount: {
            description: 'amount of coins being added',
            type: new GraphQLNonNull(GraphQLInt),
        },
    },
    resolve: async (_, { amount }: { amount: number }, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            if (amount <= 0) {
                throw new ApolloError('Please specify positive amount', 'REMOVE_COINS_ZERO_OR_LESS')
            }

            const team = await context.authUser.data.getTeam()

            if (team.coinbox - amount < 0) {
                throw new ApolloError('Not enough coins in box', 'REMOVE_COINS_EMPTY_BOX')
            }

            team.coinbox = team.coinbox - amount
            await team.save()

            const resource: Price = {
                type: await ResourceTypeDb.getType(ResourceType.Coins),
                amount: amount,
            }

            await ResourceDb.spendResources([resource], await ResourceActionDb.findByPk(ResourceActionId.Withdrawn), context.authUser.data, context.settings.currentTimeString)
        } catch (e) {
            genericError(e,'Failed to withdraw coins', 'REMOVE_COINS_ERROR')
        }

        return MUTATION_SUCCESS
    },
}
