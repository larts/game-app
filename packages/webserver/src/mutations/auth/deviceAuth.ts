import { TEAM_ID } from '@larts/api'
import { ApolloError } from 'apollo-server-errors'
import { GraphQLInt, GraphQLNonNull, GraphQLString } from 'graphql'
import { v4 as uuidv4 } from 'uuid'

import { DeviceDb } from '../../db/models/DeviceDb'
import { MUTATION_SUCCESS } from '../constants'


export const deviceRegister = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        team: {
            description: 'Device\'s team',
            type: GraphQLInt,
        },
        name: {
            description: 'Device name for identification',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { team, name }: {team: TEAM_ID | null, name: string}, context) => {
        if (!context.authDevice.token) {
            const device = new DeviceDb()

            device.auth_token = uuidv4()
            device.is_enabled = false
            device.team_id = team
            device.is_marketplace = team === null
            device.name = name

            try {
                await device.save()
                context.authDevice.register(device.auth_token)
            } catch (e) {
                throw new ApolloError('Device failed to register, try again please', 'DEVICE_REGISTRATION_ERROR')
            }

            return device.auth_token
        }

        return context.authDevice.token
    },
}

export const deviceRemove = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context) => {
        if (context.authDevice.data) {
            context.authDevice.data.is_enabled = 0

            try {
                await context.authDevice.data.save()
                context.authDevice.remove()
            } catch (e) {
                throw new ApolloError('Was issues with removing device.', 'DEVICE_REMOVAL_ERROR')
            }
        } else {
            context.authDevice.remove()
        }

        return MUTATION_SUCCESS
    },
}
