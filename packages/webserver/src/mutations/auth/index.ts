import { GraphQLObjectType } from 'graphql'

import { deviceRegister, deviceRemove } from './deviceAuth'
import { userLogin, userLogout } from './userAuth'


export const authMutations = new GraphQLObjectType({
    name: 'Auth',
    fields: {
        deviceRegister,
        deviceRemove,
        userLogin,
        userLogout,
    },
})
