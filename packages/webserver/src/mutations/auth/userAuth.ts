import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'
import { v4 as uuidv4 } from 'uuid'

import { QrScanDb } from '../../db/models/QrScanDb'
import { UserDb } from '../../db/models/UserDb'
import { ScanTypes } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const userLogin = {
    type: new GraphQLNonNull(GraphQLString),
    args: {
        qrCode: {
            description: 'User QR code',
            type: new GraphQLNonNull(GraphQLString),
        },
    },
    resolve: async (_, { qrCode }: { qrCode: string }, context: Context) => {
        await QrScanDb.registerScan(qrCode, ScanTypes.Login, context.authDevice.data, context.authUser.data, context, null)

        try {
            const user = await UserDb.findOne({ where: { qr_code: qrCode } })
            if (user) {
                if (user.team_id === context.authDevice.data.team_id || context.authDevice.data.is_marketplace) {
                    user.auth_token = uuidv4()

                    await user.save()
                    context.authUser.login(user.auth_token)
                } else {
                    throw new ApolloError('Cant login in opposing teams device!', 'USER_LOGIN_ERROR_WRONG_TEAM')
                }
            } else {
                throw new ApolloError('Please enter correct QR code!', 'USER_LOGIN_ERROR_INCORRECT_QR')
            }
        } catch (e) {
            genericError(e,'User failed to log in, try again please', 'USER_LOGIN_ERROR')
        }

        return MUTATION_SUCCESS
    },
}

export const userLogout = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context) => {
        if (context.authUser.data) {
            context.authUser.data.auth_token = uuidv4()

            try {
                await context.authUser.data.save()
                context.authUser.logout()
            } catch (e) {
                throw new ApolloError('Was issues with removing user.', 'USER_REMOVAL_ERROR')
            }
        } else {
            context.authUser.logout()
        }

        return MUTATION_SUCCESS
    },
}
