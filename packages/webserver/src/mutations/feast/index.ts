import { GraphQLObjectType } from 'graphql'

import { score } from './score'


export const feastMutations = new GraphQLObjectType({
    name: 'FeastActions',
    fields: {
        score,
    },
})
