import { GraphQLObjectType } from 'graphql'

import { activate } from './activate'
import { cancel } from './cancel'


export const workshopMutations = new GraphQLObjectType({
    name: 'WorkshopActions',
    fields: {
        cancel,
        activate,
    },
})
