import { ApolloError } from 'apollo-server-errors'
import { GraphQLNonNull, GraphQLString } from 'graphql'
import { Op } from 'sequelize'

import { ResourceActionDb } from '../../db/models/ResourceActionDb'
import { Price, ResourceDb } from '../../db/models/ResourceDb'
import { ResourceTypeDb } from '../../db/models/ResourceTypeDb'
import { ScoreDb } from '../../db/models/ScoreDb'
import { WorkshopActivationDb } from '../../db/models/WorkshopActivationDb'
import { WorkshopSizeDb } from '../../db/models/WorkshopSizeDb'
import { checkAuth } from '../../helpers/auth'
import { ResourceActionId } from '../../helpers/enums'
import { genericError } from '../../helpers/error'
import { Context } from '../../types/Context'
import { MUTATION_SUCCESS } from '../constants'


export const cancel = {
    type: new GraphQLNonNull(GraphQLString),
    resolve: async (_, __, context: Context): Promise<string> => {
        checkAuth(context)

        try {
            const activation = await WorkshopActivationDb.findOne({
                where: {
                    started: {
                        [Op.gte]: context.settings.currentTimeString,
                    },
                    cancelled: {
                        [Op.eq]: null,
                    },
                },
                order: [
                    ['started', 'DESC'],
                ] })

            if (!activation) {
                throw new ApolloError('Workshop activation not found', 'WORKSHOP_CANCEL_NO_ACTIVATION')
            }

            if (activation.cancelled) {
                throw new ApolloError('This activation already is cancelled', 'WORKSHOP_CANCEL_ALREADY_CANCELLED')
            }

            if (new Date(activation.finished) <= context.settings.currentTime) {
                throw new ApolloError('This activation already is completed', 'WORKSHOP_CANCEL_ALREADY_FINISHED')
            }

            const workshopSize = await WorkshopSizeDb.findByPk(activation.size_id)

            await ScoreDb.cancel(workshopSize.score_reason_id, String(activation.id), context)

            const priceData = JSON.parse(activation.price_details)

            const price: Price[] = await Promise.all(priceData.map(async (priceItem): Promise<Price> => {
                return {
                    type: await ResourceTypeDb.getType(priceItem.type),
                    amount: priceItem.amount,
                }
            }))

            await ResourceDb.spendResources(price, await ResourceActionDb.findByPk(ResourceActionId.WoerkshopCancelled), context.authDevice.data, context.settings.currentTimeString)

            activation.cancelled = context.settings.currentTimeString
            await activation.save()
        } catch (e) {
            genericError(e, 'Failed to cancel activation!', 'WORKSHOP_CANCEL_FAILED')
        }

        return MUTATION_SUCCESS
    },
}
