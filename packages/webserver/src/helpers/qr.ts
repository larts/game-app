import { qrToUid } from '@larts/common'
import { ApolloError } from 'apollo-server-errors'

import { QrScanDb } from '../db/models/QrScanDb'


export async function qrValidate (codeEncodedDashed: string, scan: QrScanDb, offset = 0) {
    try {
        qrToUid(codeEncodedDashed, offset)
        return true
    } catch (err) {
        const errMsg = (err as Error).message ?? ''
        if (errMsg.includes('Code must be 6 characters long')) throw new ApolloError(errMsg, await scan.saveWithError('QR_VALIDATION_INCORRECT_LENGTH'))
        if (errMsg.includes('Checksum does not match.')) throw new ApolloError(errMsg, await scan.saveWithError('QR_VALIDATION_CHECKSUMS_MISMATCH'))
        throw err
    }
}
