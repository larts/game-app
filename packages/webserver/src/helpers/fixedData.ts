import { equipmentPrice as equipmentPriceApi } from '@larts/api'

import { Price } from '../db/models/ResourceDb'
import { ResourceTypeDb } from '../db/models/ResourceTypeDb'


export const equipmentPrice = async (seasonId: number): Promise<Price[]> => {

    return Promise.all(equipmentPriceApi(seasonId).map(async ({ type, amount }) => ({
        type: await ResourceTypeDb.getType(type),
        amount,
    })))
}
