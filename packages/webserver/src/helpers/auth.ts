import { ApolloError } from 'apollo-server-errors'


export const checkAuth = (context, requireUser = true, revitalize = true) => {
    if (!context.settings.gameStarted) {
        throw new ApolloError('Game not started!', 'AUTH_CHECK_GAME_NOT_STARTED')
    }

    if (!context.authDevice.data) {
        throw new ApolloError('No device found!', 'AUTH_CHECK_NO_DEVICE')
    }

    if (requireUser) {
        if (!context.authUser.data) {
            throw new ApolloError('No user found!', 'AUTH_CHECK_NO_USER')
        }

        if (!context.authDevice.data.is_marketplace && context.authUser.data.team_id !== context.authDevice.data.team_id) {
            throw new ApolloError('Users and devices team does not match!', 'AUTH_CHECK_WRONG_TEAM')
        }

        if (revitalize) {
            context.authUser.reset()
        }
    }
}
